/* ///////////////////////////////////////////////////////////////////// */
#include "pluto.h"

#if COOLING == MINEq
  #include "cooling_defs.h"
  COOL_COEFF CoolCoeffs;
  PHOTO_COEFF PhotoCoeffs;
  SELF_SPEC SelfSpec;
#endif


/* *********************************************************** */
void CoolingSource (const Data *d, double dt, Time_Step *Dts, Grid *GXYZ)
/*!
 * Integrate cooling and reaction source terms.
 *
 * \param [in,out]  d   pointer to Data structure
 * \param [in]     dt   the time step to be taken
 * \param [out]    Dts  pointer to the Time_Step structure
 * \param [in]    GXYZ  pointer to an array of Grid structures
 *
 *************************************************************** */
{
 int  nv, k, j, i, stiff;
 double err, scrh, min_tol = 2.e-6;
 double mu0, T0, T1, mu1;
 double v0[NVAR], v1[NVAR], k1[NVAR];
 double maxrate;
 char str[200];

 int nsub, kk;
 double dtsub, dtnew, t;

 #if COOLING == MINEq && RadTran == YES
 DoRadiativeTransfer(d, GXYZ);

 if (g_include_dust)  EvolveDust(d, dt);

 /* saving some variables */
 int n, sh, el_id, ion_id, ne, nuid;
 double dnu, r,dr, dV;
 static double *dnu_by_hnu;
 if (dnu_by_hnu==NULL){
   dnu_by_hnu = calloc(1000, sizeof(double));
   for (n=0; n<PhotoCoeffs.Nnu-1; n++){
     dnu_by_hnu[n]	= PhotoCoeffs.dnu[n]/(CONST_h*PhotoCoeffs.nuc[n]);
   }
 }
 #endif

  for (nv = 0; nv < NVAR; nv++) k1[nv] = 0.0;  
/*  ----------------------------------------------------------- 
                   Begin Integration 
    -----------------------------------------------------------  */
 
  DOM_LOOP(k,j,i){  /* -- span the computational domain -- */

  /* --------------------------------------------------
      Skip integration if cell has been tagged with 
      FLAG_INTERNAL_BOUNDARY or FLAG_SPLIT_CELL 
      (only for AMR)
     -------------------------------------------------- */ 

    #if INTERNAL_BOUNDARY == YES
     if (d->flag[k][j][i] & FLAG_INTERNAL_BOUNDARY) continue;
    #endif
    
    if (d->flag[k][j][i] & FLAG_SPLIT_CELL) continue;
    
    for (nv = 0; nv < NVAR; nv++){
      v0[nv] = v1[nv] = d->Vc[nv][k][j][i];
    }

    mu0 = MeanMolecularWeight(v0);
    T0  = v0[PRS]/v0[RHO]*KELVIN*mu0;

    if (T0 <= 0.0){
      print ("! CoolingSource: negative initial temperature\n");
      print (" %12.6e  %12.6e\n",v0[PRS], v0[RHO]);
      print (" at: %f %f\n",GXYZ[IDIR].x[i], GXYZ[JDIR].x[j]);
      QUIT_PLUTO(1);
    }

 #if COOLING == MINEq && RadTran == YES
    PhotoCoeffs.locJnu = PhotoCoeffs.totJnu[i];
 #endif

/* -------------------------------------------
    Get estimated time step based on 
    the max rate of the reaction network.
   ------------------------------------------- */

    Radiat(v0, k1);
    maxrate = GetMaxRate (v0, k1, T0);
    stiff = (dt*maxrate) > 0.5 ? 1 : 0 ;
 
/* ----------------------------------------------------------------
     if the system is not stiff, then try to advance
     with an explicit 2-nd order midpoint rule
   ---------------------------------------------------------------- */

    if (!stiff){ /* -- no stiffness: try explicit -- */
      scrh = SolveODE_RKF12 (v0, k1, v1, dt, min_tol); 
/*    scrh = SolveODE_RKF23 (v0, k1, v1, dt, min_tol);  */

/* -- error is too big ? --> use some other integrator -- */
      if (scrh < 0.0) SolveODE_CK45 (v0, k1, v1, dt, min_tol);
    }else{ /* -- if stiff = 1 use more sophisticated integrators -- */

/*  SolveODE_ROS34 (v0, k1, v1, dtsub, min_tol);  */   // for using implicit integration
      nsub  = ceil(dt*maxrate);
      dtsub = dt/(double)nsub;

    /* ----------------------------------
            use sub-time stepping
       ---------------------------------- */
      t = 0.0;
      for (kk = 1; 1; kk++){
        dtnew = SolveODE_CK45 (v0, k1, v1, dtsub, min_tol);   
     /* dtnew = SolveODE_ROS34(v0, k1, v1, dtsub, min_tol);  */
        t    += dtsub;

        if (fabs(t/dt - 1.0) < 1.e-9) break;

        v0[PRS] = v1[PRS];
        for (nv = NFLX; nv < (NFLX + NIONS); nv++) v0[nv] = v1[nv];
        Radiat(v0, k1);
        dtsub = MIN (dtnew, dt - t);
      }

      if (fabs(t/dt - 1.0) > 1.e-12) {
        print ("! CoolingSource: dt mismatch\n");
        QUIT_PLUTO(1);
      } 
  
    }  /* -- end if (stiff) -- */

  /* -- Constrain ions to lie between [0,1] -- */
    for (nv = NFLX; nv < NFLX + NIONS; nv++){  //kartick
      v1[nv] = MAX(v1[nv], 0.0);
      v1[nv] = MIN(v1[nv], 1.0);
    }

  /* -- pressure must be positive -- */
    if (v1[PRS] < 0.0) v1[PRS] = g_smallPressure;

  /* -- Check final temperature -- */
    mu1 = MeanMolecularWeight(v1);
    T1  = v1[PRS]/v1[RHO]*KELVIN*mu1;

    if (T1 < g_minCoolingTemp && T0 > g_minCoolingTemp)
      v1[PRS] = g_minCoolingTemp*v1[RHO]/(KELVIN*mu1);

    err = fabs(v1[PRS]/d->Vc[PRS][k][j][i] - 1.0);

    #if COOLING == MINEq
     for (nv = NFLX; nv < NFLX + NIONS - Fe_IONS; nv++) 
    #else
     for (nv = NFLX; nv < NFLX + NIONS; nv++) 
    #endif
      err = MAX(err, fabs(d->Vc[nv][k][j][i] - v1[nv]));

    scrh = dt*g_maxCoolingRate/err; // max variation allowed in quantities is 10%;
   				    // since g_max..=0.1

    Dts->dt_cool = MIN(Dts->dt_cool, scrh);

   /* calculate the energy loss from this step */
   r  = GXYZ[IDIR].x[i];
   dr = GXYZ[IDIR].dx[i];
   dV = 4.*CONST_PI*r*r*dr*pow(g_unitLength, 3.0);
   g_Eloss += 1.5*(d->Vc[PRS][k][j][i] - v1[PRS])
	         *g_unitDensity*g_unitVelocity*g_unitVelocity*dV;

   /* ---- Update solution array ---- */
   d->Vc[PRS][k][j][i] = v1[PRS];
   for (nv = NFLX; nv < NFLX + NIONS; nv++) d->Vc[nv][k][j][i] = v1[nv];

 }; /* -- end loop on points -- */

// fclose(fspec);
}
 
/* ********************************************************************* */
void Numerical_Jacobian (double *v, double **J)
/*!
 *  Compute Jacobian matrix numerically and 
 *  get eigenvector and eigenvalue decomposition
 *
 *  The purpose of this function is to detect
 *  stiffness. 
 *
 *  Note: This function is EXTREMELY time consuming.
 *
 *********************************************************************** */
{
  int  n, nv, k, l;
  double eps;
  double vpp[NVAR], vp[NVAR], vm[NVAR], vmm[NVAR];
  double Rpp[NVAR], Rp[NVAR], Rm[NVAR], Rmm[NVAR];

  eps = 1.e-5;
  n = NIONS + 1;

/* -- partial derivs with respect to pressure -- */

  for (nv = 0; nv < NVAR; nv++){
    vp[nv] = vm[nv] = v[nv];
  }
  vp[PRS] *= 1.0 + eps;
  vm[PRS] *= 1.0 - eps;
  
  Radiat (vp, Rp);
  Radiat (vm, Rm);

  for (k = 0; k < n - 1; k++){
    J[k][n - 1] = (Rp[k + NFLX] - Rm[k + NFLX])/(2.0*v[PRS]*eps);
  }
  J[n - 1][n - 1] = (Rp[PRS] - Rm[PRS])/(2.0*v[PRS]*eps);

/* -- partial deriv with respect to ions -- */

  for (l = 0; l < n - 1; l++){

    for (nv = 0; nv < NVAR; nv++){
      vp[nv] = vm[nv] = v[nv];
    }
    vp [l + NFLX] = v[l + NFLX] + eps;
    vm [l + NFLX] = v[l + NFLX] - eps;
                      
    vp [l + NFLX] = MIN(vp [l + NFLX], 1.0);
    vm [l + NFLX] = MAX(vm [l + NFLX], 0.0);
                         
    Radiat (vp , Rp);
    Radiat (vm , Rm);

    for (k = 0; k < n - 1; k++){
      J[k][l] = (Rp[k + NFLX] - Rm[k + NFLX])/(vp[l + NFLX] - vm[l + NFLX]); 
    }
    J[n - 1][l] = (Rp[PRS] - Rm[PRS])/(vp[l + NFLX] - vm[l + NFLX]);
  }
}

