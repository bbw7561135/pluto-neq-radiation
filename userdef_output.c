#include "pluto.h"

#if COOLING == MINEq
 #include "cooling_defs.h"
 PHOTO_COEFF PhotoCoeffs;
 SELF_SPEC   SelfSpec;
#endif




/* *************************************************************** */
void ComputeUserVar (const Data *d, Grid *grid)
/*
 *
 *  PURPOSE
 *
 *    Define user-defined output variables
 *
 *
 *
 ***************************************************************** */
{
  int i, j, k;  
  double ***Jnu, ***Fnu, ***RF, ***Ntot, ***NuvH, ***NuvHe;
  double *xl, *x;
  xl = grid[IDIR].xl;
  x  = grid[IDIR].x;
  Jnu = GetUserVar("Jnu");
  Fnu = GetUserVar("Fnu");
  RF  = GetUserVar("RF");
  Ntot  = GetUserVar("Ntot");
  NuvH  = GetUserVar("NuvH");
  NuvHe = GetUserVar("NuvHe");

  DOM_LOOP(k,j,i){
	  Jnu[k][j][i] = PhotoCoeffs.totJnu[i][9]; 
	  Fnu[k][j][i] = PhotoCoeffs.Fnu[i][9]; 
	  RF[k][j][i]  = PhotoCoeffs.radforce[i];
	  Ntot[k][j][i]   = SelfSpec.N_totphot[i]; 
	  NuvH[k][j][i]   = SelfSpec.N_uvH[i];
	  NuvHe[k][j][i]  = SelfSpec.N_uvHe[i];
  }
}
/* ************************************************************* */
void ChangeDumpVar ()
/* 
 *
 * 
 *************************************************************** */
{ 
  Image *image;

}





