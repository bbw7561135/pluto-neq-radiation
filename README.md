# Non Equilibrium ionisation and radiation transport in PLUTO 4.0
 
  This folder contains code to run a self-consistent ionisation network and 
  frequency dependent radiation transport (spherically symmetric). Compatible
  with PLUTO-4.0 MINEq option. 
  Written/upgraded by Kartick Sarkar. Date: 30.09.2020
  More details in Sarkar, Sternberg & Gnat, 2020 
  -----------------------------------------------------

# How to  plug in? 
1. Use the MINEq folder and files added here to replace the 
   PLUTO-4.0 MINEq folder. For safety, keep a copy of the
   original always. This folder includes data files, reference
   tables etc in addition to source files.
2. Keep the files of the main folder in your working directory.
   This files are not a part of the MINEq/ folder but the PLUTO/Src
   folder. They contain new additions and changes to the existing
   source files. 
3. Compile the code as usual, but make sure to include the switches 
   as shown in inti.example.c for an example. 
4. Add a command to copy the object files from the atomic data/ folder
   into the working directory. This additional command can be found 
   towards the end of the makefile.example under the cop: command.

# What changed? 
1. In source folder: 
  -    adv_flux.c : added support for HeIII, Mg, Si and a full network of Fe.
  -   cooling_source.c : Added support for radiation transfer, Dust evolution
  -  other files : minor changes in the string lengths so that they are able 
        to save a bigger array of strings/numbers (as big as 128 variables).

2. In MINEq/ folder
  -   tomic_data/ (new): contains routines to provide rates, cross sections etc
  -  auger.c     (new): calculates auger rates.
  - comp_equil.c (old) : added support for upto 30 ions of single species. 
			    Added support for calculating photo-equilibrium. 
  -  cooling_defs.h (old) : Added definitions of new functions, variables. 
  -   cooling.h (old) : Added new macros. Define whether to consider Rad-Trans.
  -  dust.c    (new) : evolves dust according to sputtering laws. 
  - emissivities.c (new) : returns frequency dependent emissivity at a 
			      given temperature. The currently included tables
			      consider only CIE emissivity although the ionisation
			      network considers full non-equilibrium.
 - Gnat_cooling/  (new) : ion-by-ion cooling tabled from Gnat & Ferland, 2012
 -   input_tables/  (neq) : various input tables, including HM12 background, auger
			      probabilities, dust extinction etc. 
 -    make_tables.c  (old) : nothing major.
 -   maxrate.c      (old) : Added rates for photo-ionisation and auger rates.
 -  mineq_prototpyes.c (new) : various functions.
 - photo_cross_section.c (new) : provides atomic data related to photo-ionisation.
 -  photo_heat.c   (new) : calculated photo-electron heating and charge
			      transfer heating. 
 -   radiat.c       (old) : major upgrade to add support for extended network and 
			      new rates and photo-ionisation. 
 -  radtran.h	      (new) : defines functions, arrays and structures needed for 
			      photo-ionisation. 
 - read_cooltable.c (new) : reads ion-by-ion cooling tables and modify it according 
				to a trimmed ion network. 
 - read_opacity.c (new) : just defining a bunch of arrays. 
 - selfrad.c       (new) : routine for performing spherically symmetric 
			      radiation-transport

 --------------------------------------------------------------------------------