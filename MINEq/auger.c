#include "pluto.h"
#include "cooling_defs.h"

PHOTO_COEFF PhotoCoeffs;
const int ions_id[] 	 = {1, 2, 6, 7, 8, 10, 12, 14, 16, 26};
const int ions_beg_idx[] = {HI, HeI, CI, NI, OI, NeI, MgI, SiI,
       				SI, FeI};

/* ********************************************************* */
void GetAugerRates(double *x)
{
 int nv, nz, state, cur_state, shell, N, spc_beg_idx, printh;
 double rate, gam, Arate[NIONS];
 int n_ion_array = NIONS;

 if (PhotoCoeffs.Auger_rate == NULL){
     PhotoCoeffs.Auger_rate = ARRAY_1D(NIONS, double);
     GetAugerProb();
 }
/*
 for (nv=0; nv<NIONS; nv++){
     nz         = ions_id[elem_part[nv]];
     cur_state  = rad_rec_z[nv]-1;
     spc_beg_idx= ions_beg_idx[elem_part[nv]];
     get_aug_rate_(&n_ion_array, x+NFLX+1, &nv, &nz, &cur_state, &gam);
     PhotoCoeffs.Auger_rate[nv] = gam;
 }
*/

/*  **** The Following snippet also has the same output as the
 *   using Orly's subroutine to find Auger probability. */
 for (nv=0; nv<NIONS; nv++){
     nz		= ions_id[elem_part[nv]];
     cur_state	= rad_rec_z[nv]-1;
     spc_beg_idx= ions_beg_idx[elem_part[nv]];

     PhotoCoeffs.Auger_rate[nv] = 0.0;
 
    for (state=0; state<cur_state; state++){
         rate = 0.0;
	 if (cur_state-state >= 10) continue;    // probablity of ejecting 10 electrons is zero
	 for (shell=1; shell<=7; shell++){
	     rate += PhotoCoeffs.Gamma[nz][state][shell]
		    *PhotoCoeffs.Auger_prob[nz][state][shell][cur_state-state];
	 }
	 PhotoCoeffs.Auger_rate[nv] += rate*x[spc_beg_idx + state];	 
     }
 }

}

double PartialAugerRate(int nz, int state, int Ne)
/* *************************************************************
 * Provides  Gamma(nz,state)*Auger_P(nz, state, Ne) for all the 
 * shells combined. This is to be used in comp_equil.c.
 * Input: 
 * nz		= atomic number
 * state	= state (for CIV, nz = 6, state = 3)
 * Ne		= number of electrons to remove
 * *************************************************************** */
{
 double rate;
 int shell;

 rate = 0.0;
 for (shell=1; shell<=7; shell++)
     rate += PhotoCoeffs.Gamma[nz][state][shell]*PhotoCoeffs.Auger_prob[nz][state][shell][Ne];

 return rate;

}
