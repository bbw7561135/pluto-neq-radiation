#include "pluto.h"
#include "cooling_defs.h"

double **MyArray2D(int, int);
void Free2D(int, void **);
void Free3D(int, int, void ***);
void Get_Tindex(double **, double, int *);
void read_element(int, double **);
int readCIEfrac(double **x, char *CIEfile);
void ModifyIonicCooling(double ***dat);


double Get_loss_data(int el_id, int ion_id, double temp)
{
 if (ion_id > el_id){ print("! Error: ion_id should be < el_id.\n"); exit(1);};

 int i, j, k, ncol;
 static double ***dat;
 int tindex;
 double logT, thi, tlo, dt, llo, lhi, plambda, lambda;
 
 if (dat == NULL){
     dat = calloc(26, sizeof(double **));		// reads till Fe
     double **del;
//     del = MyArray2D(200, 30);
     for (i=0; i<26; i++){
         del = MyArray2D(200, 30);
         read_element(i+1, del);
         dat[i] = del;	
     }
     print1("> All atomic data (till Fe) is read.\n");

     if (NIONS < 111){  /* necessary for trimmed network */
       ModifyIonicCooling(dat);
       print1("> Ion cooling modified due to a trimmed ion network\n");
     }
 }

 if (temp < g_minCoolingTemp) temp = g_minCoolingTemp;	
 if (temp > g_maxCoolingTemp) temp = g_maxCoolingTemp;

 logT = log10(temp);
 Get_Tindex(dat[el_id-1], logT, &tindex);

 tlo = dat[el_id-1][tindex][0];
 thi = dat[el_id-1][tindex+1][0];
 dt  = thi-tlo;
 llo = dat[el_id-1][tindex][ion_id+1];
 lhi = dat[el_id-1][tindex+1][ion_id+1];

 plambda = (thi-logT)/dt*llo + (logT-tlo)/dt*lhi;
 lambda = pow(10., plambda);

 return lambda;
}

void read_element(int el_id, double **d)
{
 int i, j, ncol, count=0, offset;
 char datafile[200];

// sprintf(datafile, "/home/sarkar/softwares/PLUTO/Src/Cooling/MINEq/Gnat_cooling/data%d.txt",  el_id);
 sprintf(datafile, "%s/../Gnat_cooling/data%d.txt",  g_tablesdir, el_id);
 FILE *fdata = fopen(datafile,"r");
 double xd[7000];
 if (fdata==NULL){ print("! Could not open %s\n", datafile); exit(1);}

 ncol = el_id+3; 
 i = 0;
 while(fscanf(fdata, "%lf ", &xd[i]) != EOF){
     if (i%ncol == ncol-1){
         count = i/ncol;
	 offset = i-ncol+1;
         d[count][0] = log10(xd[offset]);
         for (j=1; j<ncol; j++) d[count][j] = log10(xd[offset+j]);
     }
     i++;
 }
 
 fclose(fdata);
}

/* ********************************************* */
double **MyArray2D(int ny, int nx)
{
 int i;
 double **v;
 v = calloc(ny, sizeof(double *));

 for (i=0; i<ny; i++) v[i] = calloc(nx, sizeof(double));

 return v;
}

void Free2D(int ny, void **v)
{
 int i;
 for (i=0; i<ny; i++) free(v[i]);
 free(v);
}

void Free3D(int nz, int ny, void ***v)
{
 int i;
 for (i=0; i<nz; i++) Free2D(ny, (void **)v[i]);
 free(v);
}
/* ********************************** */
void Get_Tindex(double **d, double lT, int *idx)
{
 double pdT = 0.050;
 int ibeg, imid, iend;
 double Tbeg, Tmid, Tend;

 ibeg = floor((lT-3.5)/pdT);
/*
 iend = ibeg + 1;

 while (ibeg != iend-1){
     imid = (ibeg+iend)/2;
     Tmid = d[imid][0];
     if (lT <= Tmid) iend = imid;
     else ibeg = imid;
 }
*/
  *idx = ibeg;
}

/* ***************************************************** */
void ModifyIonicCooling(double ***dat)
/*
 * Modifyies the cooling function for the last ion for a species.
 * This comes in handy in case of a trimmed IN. The cooling function
 * for the last ion is taken as
 *
 * \Lambda_li (T) = \Sum_{i>=li} {x_i(T) * \Lambda_i (T)}
 *                    /\Sum_{i>=li} {x_i(T)}
 *
 * where, x_i(T) are the CIE ion fractions of that species and
 * \Lambda_i(T) is the ion-by-ion cooling from Gnat & Ferland, 2012.
 * The prescription is same as Gray et.al., 2015.
 * *************************************************************** */
{
 double x_sum, lambda;
 int i, t, nt, el, el_id, last_ion, idx;
 char CIEfile[128];
 /* describing a full IN */
 int c_ions=7, n_ions=8, o_ions=9, ne_ions=11, mg_ions=13;
 int si_ions=15, s_ions=17, fe_ions=27;
 int cbeg, nbeg, obeg, nebeg, mgbeg, sibeg, sbeg, febeg;
 int tot_ions[] = {1, 3, 7, 8, 9, 11, 13, 15, 17, 27};

 cbeg = 5; nbeg=cbeg+c_ions; obeg=nbeg+n_ions; nebeg=obeg+o_ions;
 mgbeg=nebeg+ne_ions; sibeg=mgbeg+mg_ions; sbeg=sibeg+si_ions;
 febeg=sbeg+s_ions;

 int el_beg[] = {1, 2, cbeg, nbeg, obeg, nebeg, mgbeg,
                        sibeg, sbeg, febeg};

 static double **fcie;
 if (fcie==NULL) fcie = ARRAY_2D(110, 150, double);

/* CIE ion-frac file must be calculated at the same temperatures
  * as the ion-by-ion cooing. */
 sprintf(CIEfile,"%s/cie-ion-frac.txt", g_tablesdir);

 /* reading CIE cooling for a full IN */
 nt = readCIEfrac(fcie, CIEfile);

 /* accounting for cooling function for higher ions that are not 
  * considered in the IN */
 for (t=0; t<nt; t++){
   for (el=2; el<Nspecies; el++){ /* start from Carbon */
     el_id = ions_nz[el];
     last_ion = ions_num[el]-1; /* last ion of the trimmed IN */
     x_sum = 0.0;
     lambda = 0.0;
     for (i=last_ion; i<=el_id; i++){
        idx = el_beg[el];
        lambda += fcie[t][idx+i]*pow(10,dat[el_id-1][t][i+1]);
        x_sum  += fcie[t][idx+i];
        dat[el_id-1][t][i+1] = -60;
     }
     if (x_sum > 1e-20)
       dat[el_id-1][t][last_ion+1] = log10(lambda/x_sum);
   } /* loop over elements done */

  /* test print modified cooling functions */
/*
   print1("%2.4e \n", dat[0][t][0]);
   for (el=0; el<Nspecies; el++){
     el_id = ions_nz[el];
     for (i=0; i<=el_id; i++)
         print1("%2.4e ", pow(10.,dat[el_id-1][t][i+1]));
     print1("\n");
   }
*/
 } /* loop over the temperature is done */

}

/* ******************************************************* */
int readCIEfrac(double **x, char *CIEfile)
/*
 * Reads the cooling functions for a full IN 
 * ----------------------------------------------------- */
{
 int c_ions=7, n_ions=8, o_ions=9, ne_ions=11, mg_ions=13;
 int si_ions=15, s_ions=17, fe_ions=27;
 int cbeg, nbeg, obeg, nebeg, mgbeg, sibeg, sbeg, febeg;
 int ncol, i, j, count, ofst;

 cbeg = 5; nbeg=cbeg+c_ions; obeg=nbeg+n_ions; nebeg=obeg+o_ions;
 mgbeg=nebeg+ne_ions; sibeg=mgbeg+mg_ions; sbeg=sibeg+si_ions;
 febeg=sbeg+s_ions;

 FILE *f = fopen(CIEfile, "r");
 double xd[20000];
 if (f == NULL){
   print1("! cie ion frac could not find.\n");
   exit(10);
 }

 ncol = febeg+fe_ions;
 i = 0;
 count = 0;
 while (fscanf(f, "%lf ", &xd[i]) !=EOF){
   if (i%ncol==ncol-1){
     count = i/ncol;
     ofst  = i-ncol+1;
     x[count][0] = xd[ofst];
     for (j=1; j<=111; j++)
       x[count][j] = xd[ofst+j];
   }
   i++;
 }

 fclose(f);

 return count+1;
}

