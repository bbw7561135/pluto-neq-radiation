#include "pluto.h"
#include "cooling_defs.h"

double GetTotCooling(double ne, double N, double T, double *v);
double SupplementaryCooling(double);

/* --------------------------------------------------------- 
 Global variables definition. They are declared inside cooling_defs.h
 ------------------------------------------------------------- */

#define XXX_H	0.922169848439544   	  // Z = 1.
#define ZZZ	1.0

/* H      He      C      N      O       Ne      Mg	     S      Fe  */
//const double elem_ab[] = {  0.93, 0.074, 3.0e-4,  5.e-5, 4.0e-4, 7.0e-5, 3.64e-5, 1.5e-5, 2.69e-5 }; /* Number densities */
/* H   He  C   N     O    Ne    Mg	Si    S    Fe  */
const double elem_ab[] = { XXX_H, (XXX_H/12.), (ZZZ*XXX_H*2.45e-4), (ZZZ*XXX_H*6.03e-5), (ZZZ*XXX_H*4.57e-4), (ZZZ*XXX_H*1.95e-4), (ZZZ*XXX_H*3.39e-5), (ZZZ*XXX_H*3.24e-5), (ZZZ*XXX_H*1.38e-5), (ZZZ*XXX_H*2.82e-5)}; // test-case

/* Following are some other examples. Currently unused.
 ============================================================== */
double   elem_ab_est[] = {  0.93, 0.074, 3.0e-4,  5.e-5, 4.0e-4, 7.0e-5, 1.5e-5 };  /* Number densities in Orion nebula, Esteban & al 2004 */
double   elem_ab_lod[] = {  0.92,  0.08, 2.3e-4, 6.2e-5, 4.4e-4, 7.0e-5, 1.25e-5 }; /* Number densities, Solar, Lodders 2003 ApJ */
double   elem_ab_sol[] = {  0.93, 0.074, 3.0e-4,  9.e-5,  7.e-4,  7.e-5, 1.e-5 };   /* Number fractions, Solar    */
double   elem_ab_uni[] = {  0.93, 0.072, 5.0e-4,  9.e-5,  8.e-4,  8.e-5, 2.e-5 };   /* Number fractions, Universe */
/* =============================================================== */

const double   elem_mass[]   = { 1.007, 4.002,  12.01,  14.01,  15.99,  20.18, 24.305, 28.0855, 32.07, 55.845 };  /*   Atomic mass, in a.m.u.   */

const int ions_nz[] = {1, 2, 6, 7, 8, 10, 12, 14, 16, 26};
const int ions_num[] = {2, 3, C_IONS, N_IONS, O_IONS, Ne_IONS, Mg_IONS, Si_IONS, S_IONS, Fe_IONS};

const int elem_part[] = {0, 1, 1, 1
                   C_EXPAND(el_C, el_C, el_C, el_C, el_C, el_C, el_C) 
                   N_EXPAND(el_N, el_N, el_N, el_N, el_N, el_N, el_N, el_N) 
                   O_EXPAND(el_O, el_O, el_O, el_O, el_O, el_O, el_O, el_O, el_O) 
                  Ne_EXPAND(el_Ne, el_Ne, el_Ne, el_Ne, el_Ne, el_Ne, el_Ne, el_Ne, el_Ne, el_Ne, el_Ne) 
                  Mg_EXPAND(el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg, el_Mg) 
                  Si_EXPAND(el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si, el_Si)
                   S_EXPAND(el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S, el_S)
                  Fe_EXPAND(el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe,el_Fe) };

const double rad_rec_z[] = { 1., 1., 2., 3.
                       C_EXPAND(1., 2., 3., 4., 5., 6., 7.)
                       N_EXPAND(1., 2., 3., 4., 5., 6., 7., 8.) 
                       O_EXPAND(1., 2., 3., 4., 5., 6., 7., 8., 9.)
                      Ne_EXPAND(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11.) 
                      Mg_EXPAND(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13.) 
                      Si_EXPAND(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15.)
                       S_EXPAND(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16., 17.)
                      Fe_EXPAND(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16., 17., 18., 19., 20., 21., 22., 23., 24., 25., 26., 27.) };

 COOL_COEFF CoolCoeffs;
 PHOTO_COEFF PhotoCoeffs;

/* ************************************************************* */
void Radiat (real *v, real *rhs)
/*
 *************************************************************** */
{
  int  nv, j, k, cooling_nT, cooling_nNe, jj;
  int  ti1, ti2, ne1, ne2, nrt;
  real   mu, sT, scrh, tmpT, tmpNe, tt1, tt2, nn1, nn2, tf1, tf2, nf1, nf2;
  real   N, n_el, rlosst, em, cf1, cf2;
  real   T, *X, *RS;
  static real ***tab;
  static real N_rho;
  static real E_cost, Unit_Time; /* for dimensionalization purposes */
  real em2, em3;

/* --------------------------------------------------------------
            Load tables from disk at first call.
            Tables are 2-D with T and Ne being the coordinates.
   ------------------------------------------------------------ */
  static int first_time_here = 1;
  double ttt, rrr[7], scrh2, scrh_tmp;
  int el_id, ion_id, get_pir = 1;

  if (first_time_here) {  

    Unit_Time = g_unitLength/g_unitVelocity;
    E_cost    = Unit_Time/(g_unitDensity*g_unitVelocity*g_unitVelocity);

    N_rho = find_N_rho();

    double Hmf, muH;
    Hmf = H_MassFrac();
    muH = 1./Hmf;
    CoolCoeffs.nH = v[RHO]/(muH*CONST_mH);
    first_time_here = 0;
  }  

/* ---------------------------------------
    Force species to lie between 0 and 1 
   --------------------------------------- */

  for (nv = NFLX; nv < (NFLX + NIONS); nv++){
    v[nv] = MIN(v[nv], 1.0);
    v[nv] = MAX(v[nv], 0.0);
  }

  if (v[PRS] < 0.0) v[PRS] = g_smallPressure;

  mu = MeanMolecularWeight(v); 
  T  = v[PRS]/v[RHO]*KELVIN*mu;
  if (T > g_maxCoolingTemp) T = g_maxCoolingTemp; // kartick

  if (mu < 0.0){
    print ("! Radiat: negative mu\n");
    QUIT_PLUTO(1);
  }

 /* ---------------------------------
     offset pointers to work more
     efficiently.
    --------------------------------- */
  X  = v + NFLX; 
  RS = rhs + NFLX;

  sT = sqrt(T);
  N  = v[RHO]*N_rho;       /* -- Total number density -- */
  CoolCoeffs.N = N;

/* -----------------------------------
     compute electron number density 
   ----------------------------------- */
  CoolCoeffs.dnel_dX[0] = -N*elem_ab[el_H];   
  n_el = N*(1.0 - v[HI])*elem_ab[el_H]; /* -- contribution from ionized H --  */

  for (nv = 1; nv < NIONS; nv++) {
    CoolCoeffs.dnel_dX[nv] = N*(rad_rec_z[nv] - 1.0)*elem_ab[elem_part[nv]];
    n_el                  += X[nv]*CoolCoeffs.dnel_dX[nv];    
  }

  if (n_el/N < 1.e-10) n_el = N*1.e-10;  /*  OK ????? */
   
  CoolCoeffs.Ne = n_el;  
 
  Find_Rates(T, n_el, N, v);  /*  find transition rates  */

/* ---------------------------------------------------------
          Compute RHS of the system of ODE.
          Evaluate the transition rates  
         (multiply the rates given by find_rates()
          by numerical densities)
   ----------------------------------------------------------- */

   /* ----------------------------------------------
       Compute right hand side for all ions 
      ---------------------------------------------- */

  RS[0] = (1.0 - X[0])*CoolCoeffs.Rrate[0] - X[H_I]*(CoolCoeffs.Crate[0]+PhotoCoeffs.Pi_Rate[0]);   /* separate for H */

  for (nv = 1; nv <= NIONS - 2; nv++) {  
      RS[nv] =   X[nv - 1]* CoolCoeffs.Lrate[nv]			/* Ionised from the lower state */ 
             - X[nv]*(CoolCoeffs.Crate[nv] + PhotoCoeffs.Pi_Rate[nv]) /* ionised to higher state or recombined to lower state */
             + X[nv + 1]* CoolCoeffs.Rrate[nv]			/* recombined from the higher state */
	     + PhotoCoeffs.Auger_rate[nv];			/* excited from lower states via Auger proc */
  }
  
  nv = NIONS - 1;
  RS[nv] = X[nv - 1]*CoolCoeffs.Lrate[nv] - X[nv]*(CoolCoeffs.Crate[nv]+PhotoCoeffs.Pi_Rate[nv])
             + PhotoCoeffs.Auger_rate[nv];

  for (nv = 0; nv < NIONS; nv++) RS[nv] *= Unit_Time;

 /* **************************************************
  * Calculating the radiative losses 
  * Tables are generated from Gnat & Ferland, 2012
  * ************************************************ */
  rlosst = elem_ab[el_H]*(1.-X[0])*Get_loss_data(1, 1, T); // for HII

  for (nv=0; nv<NIONS; nv++){
      get_ion(nv, &el_id, &ion_id);
      rlosst	+= elem_ab[elem_part[nv]] * X[nv] * Get_loss_data(el_id, ion_id, T);
  }
  rlosst   *= n_el*N;		// in CGS erg/s/cm^3

  /* *****************************************
   * supplemantary cooling function   
   * ***************************************** */
//  rlosst = SupplementaryCooling(T);
//  rlosst *= n_el*N*elem_ab[el_H];	// ne * nH * Lambda(T) 


  /* *********************************************************
   * The net cooling rate = ne*\Sum_X( nX*Lambda(T, X, Z)) 
   * 				- \Sum_X nX*H(X, Jnu) 
   * ************************************************************* */
  double PH, ct_heating;
  int sh;

  PhotoCoeffs.PHeating = 0.0;
  for (nv=0; nv<NIONS; nv++){
    PH = PhotoHeating(v, nv);	/* Photo-heating */
    PhotoCoeffs.PHeating += PH;
  }

  ct_heating = GetCTHeating(T, N, v);	/* charge transfer heating */
  
//  PhotoCoeffs.PHeating = ReadPhotoheatingTable(T);		/* supply alternative cooling table if req. */

  rlosst   = rlosst - (PhotoCoeffs.PHeating + ct_heating);
  rhs[PRS] = -E_cost*rlosst*(g_gamma - 1.0); // bringing back to 
  					     // code units

  rhs[PRS]   *= 1.0/(1.0 + exp( -(T - g_minCoolingTemp)/100.));  /* -- cut cooling function smoothly--*/

/*// since now heating is added, there can be energy gain than loss
  if (rhs[PRS] > 0.0) {
     print ("! Error: positive radiative losses!  %12.6e  %12.6e  %12.6e \n", em, n_el, N);
     QUIT_PLUTO(1);
  }  
*/

  return;			
 
}

/* ************************************************************** */
void Find_Rates(real T, real Ne, real N, real *v)
/*
 *
 * PURPOSE
 *
 *  Transition rates computation routine   
 *
 *   Output: CoolCoeffs.Crate, CoolCoeffs.Rrate, CoolCoeffs.Lrate
 *
 *   dN = (   CoolCoeffs.Lrate * n_(ion-1) 
 *          + CoolCoeffs.Rrate  * n_(ion+1) - CoolCoeffs.Crate * n_ion ) * dt
 *
 *
 ********************************************************** */
{
  real dn, lam, t4, tmprec, ft1, ft2, tmpT, scrh;
  int ti1, ti2, cindex, i, ions, nv, tindex, j, k, state;
  double xi[NIONS], al[NIONS], al_ct_HI[NIONS], xi_ct_HII[NIONS];
  double al_ct_HeI[NIONS], xi_ct_HeII[NIONS];	
  int H_II = H_I + 1;

  if (CoolCoeffs.ion_data == NULL) {  /* -- get rates tables -- */
    CoolCoeffs.ion_data = ARRAY_3D(I_g_stepNumber, 7, NIONS, double);
    Create_Ion_Coeff_Tables(CoolCoeffs.ion_data); 
  
    GetUVB_spec();
    print1("> UVB Spectra read. Total number of freq bins = %d\n", 
		    	PhotoCoeffs.Nnu-1);
    GetPhotoCrossSection();
    print1("> PhotoCoeffs.* allocated\n");

    #if RadTran == YES
    if (g_include_dust) GetDustProperties();
    print1("> Dust properties read \n");
    ReadOpacityTables();    // just set memory for the pointers
    print1("> Opacity pointers allocated.\n");
    #endif
    /* The following is to make sure that the code starts with 
     * some radiation. But the locJnu can be changed during the
     *  runtime in cooling_source.c */
    for (i=0; i<PhotoCoeffs.Nnu; i++) 
      PhotoCoeffs.locJnu[i] = get_photoequilibrium
	      			*PhotoCoeffs.UVB_Jnu[i];
  }  

  GetPhotoIonRate(PhotoCoeffs.nu, PhotoCoeffs.locJnu);  
  GetAugerRates(v);

  for (nv = 0; nv < NIONS; nv++ ) {
    CoolCoeffs.Rrate[nv] = 0.0;
    CoolCoeffs.Lrate[nv] = 0.0;
    CoolCoeffs.Crate[nv] = 0.0;
  }
 

  double logT, logL, iondata1, iondata2;
  logT		= log10(T);
  if      (logT > I_TEND) logT = I_TEND - I_TSTEP*0.5;	
  else if (logT < I_TBEG) logT = I_TBEG + I_TSTEP*0.5;

  tindex	= floor( (logT-I_TBEG)/I_TSTEP );
  ft1	= ((I_TBEG + I_TSTEP*(tindex+1))*1.0 - logT)/I_TSTEP;
  ft2	= (logT - (I_TBEG + I_TSTEP*tindex)*1.0)/I_TSTEP;

  for (k = 0; k < 7; k++) {
  for (ions = 0; ions < NIONS; ions++) {
    iondata1 = MAX(CoolCoeffs.ion_data[tindex][k][ions],1e-60);
    iondata2 = MAX(CoolCoeffs.ion_data[tindex+1][k][ions],1e-60);
    logL = log10(iondata1)*ft1 + log10(iondata2)*ft2;
//  logL = log10(CoolCoeffs.ion_data[tindex][k][ions])*ft1 
//  + log10(CoolCoeffs.ion_data[tindex + 1][k][ions])*ft2;
    /* New variables. suitable for eye (kartick) */
    if (k==0) xi[ions] 		= pow(10., logL);
    if (k==1 & ions < NIONS-1)
 	      al[ions+1]	= pow(10., logL);
    if (k==3) xi_ct_HII[ions]	= pow(10., logL);
    if (k==4 & ions < NIONS-1) 
	      al_ct_HI[ions+1]	= pow(10., logL);
    if (k==5 & ions < NIONS-1) 
	      al_ct_HeI[ions+1]	= pow(10., logL);
    if (k==6) xi_ct_HeII[ions]  = pow(10., logL);

  }}

  /* == imposing no statistical charge transfer for ions >= 4+  == */
/*
 for (ions=0; ions<NIONS-1; ions++){
     state  = (int)rad_rec_z[ions] - 1;
     if (state >= 4){
         al_ct_HI[ions]	 = 0.0;
         al_ct_HeI[ions] = 0.0;
     }
 }
*/
 /* ====== statistical charge transfer modified: kartick == */

 int el_id, ion_id, ion;
 double NH, NHe, NHI, NHII, NHeI, NHeII, NHeIII;
 NH	= N*elem_ab[el_H];
 NHe	= N*elem_ab[el_He];
 NHI	= v[HI]*NH;
 NHII	= (1.-v[HI])*NH;
 NHeI	= v[HeI]*NHe;
 NHeII	= v[HeII]*NHe;
 NHeIII	= (1.-v[HeI]-v[HeII])*NHe;

 /* -------------------------------------------
  Calculating reaction rates for H 
  ---------------------------------------- */
  CoolCoeffs.Crate[0] = Ne*xi[H_I] + NHeII*al_ct_HI[He_II] 
	  			+ NHeIII*al_ct_HI[He_III];
  CoolCoeffs.Rrate[0] = Ne*al[H_II] + NHeI*xi_ct_HII[He_I] 
	  			+ NHeII*xi_ct_HII[He_II]; 

 /* ---------------------------------
    Computing rates for He 
   ------------------------------------- */
 CoolCoeffs.Rrate[He_I] = Ne*al[He_II] + NHI*al_ct_HI[He_II];
 CoolCoeffs.Crate[He_I] = Ne*xi[He_I] + NHII*xi_ct_HII[He_I]; 
 				// + NHeIII*al_ct_HeI[He_III];	
			//last term is new but this rate is zero
 CoolCoeffs.Lrate[He_I] = 0.0;

/* This quantities are needed only when calculating the Jacobian 
 * in the implicit method of integration */
/*
 CoolCoeffs.Ra[He_I]	+= al[He_II];	
 CoolCoeffs.Ca[He_I]	+= xi[He_I];
 CoolCoeffs.Rb[He_I]	+= al_ct_HI[He_II]*NH;
 CoolCoeffs.Cb[He_I]	+= xi_ct_HII[He_I]*NH;
*/

 CoolCoeffs.Rrate[He_II] = Ne*al[He_III] + NHI*al_ct_HI[He_III];
 CoolCoeffs.Crate[He_II] = Ne*xi[He_II] + NHII*xi_ct_HII[He_II] 
	 		   + Ne*al[He_II] + NHI*al_ct_HI[He_II];
 CoolCoeffs.Lrate[He_II] = Ne*xi[He_I] + NHII*xi_ct_HII[He_I];	
  
 CoolCoeffs.Rrate[He_III] = 0.0;
 CoolCoeffs.Crate[He_III] = NHI*al_ct_HI[He_III] + Ne*al[He_III];
 		// + NHeI*al_ct_HeI[He_III];	//last term is new
 CoolCoeffs.Lrate[He_III] = Ne*xi[He_II] + NHII*xi_ct_HII[He_II]; 

 /* -------------------------------------------------------
  * More detailed reaction equations. only for C is given.
  * Higher ions follow a similar trend. A more compact form 
  * has been used after this example of C. Just to show how 
  * the detailed reactions work. 
  * ------------------------------------------------------- */
/*
 CoolCoeffs.Rrate[C_I]	= Ne*al[C_II] + NHI*al_ct_HI[C_II] 
	 		  + NHeI*al_ct_HeI[C_II];
 CoolCoeffs.Crate[C_I]	= Ne*xi[C_I] + NHII*xi_ct_HII[C_I] 
	 		  + NHeII*xi_ct_HeII[C_I];
 CoolCoeffs.Lrate[C_I] 	= 0.0 ;

 
 CoolCoeffs.Rrate[C_II]	= Ne*al[C_III] + NHI*al_ct_HI[C_III] 
	 		  + NHeI*al_ct_HeI[C_III];
 CoolCoeffs.Crate[C_II]	= Ne*xi[C_II] + NHII*xi_ct_HII[C_II] 
	 		  + NHeII*xi_ct_HeII[C_II] 
			  + CoolCoeffs.Rrate[C_I];
//			  + Ne*al[C_II] + NHI*al_ct_HI[C_II] 
//			  + NHeI*al_ct_HeI[C_II];// has been 
//				   replaced by CoolCoeffs.Rrate[C_I]
// CoolCoeffs.Lrate[C_II] = Ne*xi[C_I] + NHII*xi_ct_HII[C_I] 
// 			   + NHeII*xi_ct_HeII[C_I] ;//replaced
// 			    by CoolCoeffs.Crate[C_I] to avoid 
// 			    computating multiple times
 CoolCoeffs.Lrate[C_II]  = CoolCoeffs.Crate[C_I];
 
 CoolCoeffs.Rrate[C_III] = Ne*al[C_IV] + NHI*al_ct_HI[C_IV] 
	 			  + NHeI*al_ct_HeI[C_IV];
 CoolCoeffs.Crate[C_III] = Ne*xi[C_III] +NHII*xi_ct_HII[C_III]
			  + NHeII*xi_ct_HeII[C_III]
			  + CoolCoeffs.Rrate[C_II];
//			  + Ne*al[C_III] +NHI*al_ct_HI[C_III]
//			  + NHeI*al_ct_HeI[C_III];
// CoolCoeffs.Lrate[C_III] = Ne*xi[C_II] + NHII*xi_ct_HII[C_II] 
// 			    + NHeII*xi_ct_HeII[C_II];
 CoolCoeffs.Lrate[C_III]   = CoolCoeffs.Crate[C_II] 
	 		     - CoolCoeffs.Rrate[C_I];
 
 CoolCoeffs.Rrate[C_IV]	= Ne*al[C_V] + NHI*al_ct_HI[C_V] 
	 		  + NHeI*al_ct_HeI[C_V];
 CoolCoeffs.Crate[C_IV]	= Ne*xi[C_IV] + NHII*xi_ct_HII[C_IV] 
	 		  + NHeII*xi_ct_HeII[C_IV]
			  + CoolCoeffs.Rrate[C_III];
//			  + Ne*al[C_IV] + NHI*al_ct_HI[C_IV] 
//			  + NHeI*al_ct_HeI[C_IV];
// CoolCoeffs.Lrate[C_IV] = Ne*xi[C_III] +NHII*xi_ct_HII[C_III] 
// 			   + NHeII*xi_ct_HeII[C_III];
 CoolCoeffs.Lrate[C_IV]	  = CoolCoeffs.Crate[C_III] 
	 		   - CoolCoeffs.Rrate[C_II];

 CoolCoeffs.Rrate[C_V]	= Ne*al[C_VI] + NHI*al_ct_HI[C_VI] 
	 		  + NHeI*al_ct_HeI[C_VI];
 CoolCoeffs.Crate[C_V]	= Ne*xi[C_V] + NHII*xi_ct_HII[C_V] 
	 		  + NHeII*xi_ct_HeII[C_V]
			  + CoolCoeffs.Rrate[C_IV];
//			  + Ne*al[C_V] + NHI*al_ct_HI[C_V] 
//			  + NHeI*al_ct_HeI[C_V];
// CoolCoeffs.Lrate[C_V] = Ne*xi[C_IV] + NHII*xi_ct_HII[C_IV] 
// 			  + NHeII*xi_ct_HeII[C_IV];
 CoolCoeffs.Lrate[C_V]	 = CoolCoeffs.Crate[C_IV] 
	 		  - CoolCoeffs.Rrate[C_III];

 CoolCoeffs.Rrate[C_VI]	 = Ne*al[C_VII] + NHI*al_ct_HI[C_VII] 
	 		  + NHeI*al_ct_HeI[C_VII];
 CoolCoeffs.Crate[C_VI]	 = Ne*xi[C_VI] + NHII*xi_ct_HII[C_VI] 
	 		  + NHeII*xi_ct_HeII[C_VI]
			  + CoolCoeffs.Rrate[C_V];
//			  + Ne*al[C_VI] + NHI*al_ct_HI[C_VI] 
//			  + NHeI*al_ct_HeI[C_VI];
// CoolCoeffs.Lrate[C_VI] = Ne*xi[C_V] + NHII*xi_ct_HII[C_V] 
// 			   + NHeII*xi_ct_HeII[C_V];
 CoolCoeffs.Lrate[C_VI]	  = CoolCoeffs.Crate[C_V] 
	 		   - CoolCoeffs.Rrate[C_IV];

 CoolCoeffs.Rrate[C_VII]   = 0.0;
// CoolCoeffs.Crate[C_VII] = Ne*al[C_VII] + NHI*al_ct_HI[C_VII] 
// 			    + NHeI*al_ct_HeI[C_VII];
 CoolCoeffs.Crate[C_VII]   = 0.0 + CoolCoeffs.Rrate[C_VI];
// CoolCoeffs.Lrate[C_VII] = Ne*xi[C_VI] + NHII*xi_ct_HII[C_VI] 
// 			    + NHeII*xi_ct_HeII[C_VI];
 CoolCoeffs.Lrate[C_VII]   = CoolCoeffs.Crate[C_VI] 
	 		    - CoolCoeffs.Rrate[C_V];
*/

/* --------------------------------------------------------------
                Do the other ions (> He) now   
   ------------------------------------------------------------ */
 for (ions = C_I; ions < NIONS - 1; ions++) {
     CoolCoeffs.Rrate[ions] = Ne*al[ions+1] + NHI*al_ct_HI[ions+1]
	    		      + NHeI*al_ct_HeI[ions+1];
     CoolCoeffs.Crate[ions] = Ne*xi[ions] + NHII*xi_ct_HII[ions]
	    		      + NHeII*xi_ct_HeII[ions]
			      + CoolCoeffs.Rrate[ions-1];
     CoolCoeffs.Lrate[ions] = CoolCoeffs.Crate[ions-1]
	    		      - CoolCoeffs.Rrate[ions-2];
 }

 /* For the last ion of all */
 CoolCoeffs.Rrate[ions] = 0.0;
 CoolCoeffs.Crate[ions] = 0.0 + CoolCoeffs.Rrate[ions-1];
 CoolCoeffs.Lrate[ions] = CoolCoeffs.Crate[ions-1]
			 - CoolCoeffs.Rrate[ions-2];

/* ***************************************************************
 * Change in H and He fractions due to charge excange with metals.
 * Given that the H and He ion fractions are much more that the
 * metals, this change can be neglected. 
 * ************************************************************** */
/*
 for (ions=4; ions<NIONS-1; ions++) {
     // for H
     CoolCoeffs.Crate[H_I] += v[ions+1+NFLX] * N * elem_ab[elem_part[ions+1]] * al_ct_HI[ions+1];
     CoolCoeffs.Rrate[H_I] += v[ions+NFLX]   * N * elem_ab[elem_part[ions]]   * xi_ct_HII[ions];

     // for He
     tmprec = v[ions+1+NFLX] * N * elem_ab[elem_part[ions+1]] * al_ct_HeI[ions+1];
     CoolCoeffs.Crate[He_I]  += tmprec;
     CoolCoeffs.Lrate[He_II] += tmprec;

     tmprec = v[ions+NFLX] * N * elem_ab[elem_part[ions]] * xi_ct_HeII[ions];
     CoolCoeffs.Rrate[He_I]  += tmprec;
     CoolCoeffs.Crate[He_II] += tmprec;
 }
*/

/* ------------------------------------------------------------- 
       C H E C K 
   ------------------------------------------------------------- */
/*
for (ions = 0; ions < NIONS; ions++) {
  CoolCoeffs.Lrate[ions] *= g_unitLength/g_unitVelocity;
  CoolCoeffs.Crate[ions] *= g_unitLength/g_unitVelocity;
  CoolCoeffs.Rrate[ions] *= g_unitLength/g_unitVelocity;
}

print ("H:     %12.6e  %12.6e  %12.6e\n",
         CoolCoeffs.Lrate[HI-NFLX], CoolCoeffs.Crate[HI-NFLX],CoolCoeffs.Rrate[HI-NFLX]);

print ("--------------------------------------------------------------- \n");
for (nv = HeI; nv <= HeII; nv++){
print ("He(%d): %12.6e  %12.6e  %12.6e\n",nv-HeI+1,
         CoolCoeffs.Lrate[nv-NFLX], CoolCoeffs.Crate[nv-NFLX],CoolCoeffs.Rrate[nv-NFLX]);
}

print ("--------------------------------------------------------------- \n");
for (nv = CI; nv <= CV; nv++){
print ("C(%d):  %12.6e  %12.6e  %12.6e\n",nv-CI+1,
         CoolCoeffs.Lrate[nv-NFLX], CoolCoeffs.Crate[nv-NFLX],CoolCoeffs.Rrate[nv-NFLX]);
}
print ("--------------------------------------------------------------- \n");

for (nv = NI; nv <= NV; nv++){
print ("N(%d):  %12.6e  %12.6e  %12.6e\n",nv-NI+1,
         CoolCoeffs.Lrate[nv-NFLX], CoolCoeffs.Crate[nv-NFLX],CoolCoeffs.Rrate[nv-NFLX]);
}
print ("--------------------------------------------------------------- \n");

for (nv = OI; nv <= OV; nv++){
print ("O(%d):  %12.6e  %12.6e  %12.6e\n",nv-OI+1,
         CoolCoeffs.Lrate[nv-NFLX], CoolCoeffs.Crate[nv-NFLX],CoolCoeffs.Rrate[nv-NFLX]);
}
print ("--------------------------------------------------------------- \n");

for (nv = NeI; nv <= NeV; nv++){
print ("Ne(%d): %12.6e  %12.6e  %12.6e\n",nv-NeI+1,
         CoolCoeffs.Lrate[nv-NFLX], CoolCoeffs.Crate[nv-NFLX],CoolCoeffs.Rrate[nv-NFLX]);
}
print ("--------------------------------------------------------------- \n");
for (nv = SI; nv <= SV; nv++){
print ("Se(%d):  %12.6e  %12.6e  %12.6e\n",nv-SI+1,
         CoolCoeffs.Lrate[nv-NFLX], CoolCoeffs.Crate[nv-NFLX],CoolCoeffs.Rrate[nv-NFLX]);
}


exit(1);
*/
}
 
/* ********************************************************** */
double find_N_rho ()
/*
 *
 *  PURPOSE
 *
 *  Find the number density of atoms / ions (divided by the density
 *  so that is a constant depending on composition only), knowing 
 *  the ions ratios and density.
 *  We compute a /mu taking into account only the heavy particles 
 *  (nuclei).
 *
 * LAST MODIFIED: July 18, 2006 by Ovidiu Tesileanu
 *
 ************************************************************* */
{
  int i, j;
  double mu, mu1, mu2;
   
  mu1 = mu2 = 0.0; 
  for (i = 0; i < Nspecies; i++) {
    mu1 += elem_ab[i]*elem_mass[i]; /*    Numerator part of mu    */
    mu2 += elem_ab[i];              /*    Denominator part of mu  */
  }
  mu = mu1 / mu2;
  return (g_unitDensity/mu*CONST_NA);  /* This is N/rho, with N the
					 total number density of 
					 atoms and ions */
}

/* ************************************************************ */
double MeanMolecularWeight (real *V)
/*
 *
 *
 * PURPOSE
 * 
 *   Compute the mean molecular weight as function of the 
 *   composition of the gas.
 *   The definition of the mean molecular weight \mu is 
 *   the standard one:
 *
 *     1     \sum_k f_k n_k
 *    --- = ----------------     (Clayton, pag 82-83)
 *    \mu    \sum_k f_k A_k
 * 
 *   where 
 *
 *    f_k   : is the fractional abundance (by number), 
                f_k = N_k/N_tot
 *
 *    A_K   : is the atomic weight
 *
 *    n_k   : is the number of free particles 
 *            contributed to the gas by element k
 *
 *   The mean molecular weight satifies 
 *
 *               \rho = \mu m_{amu} N_{tot}
 *   
 *   where N_{tot} is the total number density of particles
 *
 * ARGUMENTS
 *
 *   V:   a set of primitive variables
 *
 *********************************************************************** */
{
  double mmw1, mmw2;
  int    i, j;
  
  mmw1 = mmw2 = 0.0;
  for (i = 0; i < NIONS; i++) {
    if (V[NFLX+i] < 0.0) V[NFLX+i] = 0.0;
    if (V[NFLX+i] > 1.0) V[NFLX+i] = 1.0;
    CoolCoeffs.dmuN_dX[i] = elem_mass[elem_part[i]]*elem_ab[elem_part[i]];
    CoolCoeffs.dmuD_dX[i] = elem_ab[elem_part[i]]  *rad_rec_z[i];
    mmw1 += CoolCoeffs.dmuN_dX[i]*V[NFLX+i];  /*    Numerator part of mu    */
    mmw2 += CoolCoeffs.dmuD_dX[i]*V[NFLX+i];  /*    Denominator part of mu  */    
  }

  /* --------------------------------------------------
          add now contribution from ionized H  
     --------------------------------------------------  */

  CoolCoeffs.dmuN_dX[0] += -elem_mass[0]*elem_ab[el_H];
  CoolCoeffs.dmuD_dX[0] += -2.0*elem_ab[el_H];

  mmw1 += elem_mass[0]*elem_ab[el_H]*(1.0 - V[HI]); 
  mmw2 += elem_ab[el_H]*(1.0 - V[HI])*2.;

  CoolCoeffs.muN = mmw1;
  CoolCoeffs.muD = mmw2;

  if (mmw1 != mmw1) {
    print(">>> Error!  MMW1  NaN! %ld\n",g_stepNumber);
    QUIT_PLUTO(1);
  }
  if (mmw2 != mmw2) {
    print(">>> Error!  MMW2  NaN!\n");
    QUIT_PLUTO(1);
  }
  
  return (mmw1/mmw2);
}

/* ********************************************************************* */
double H_MassFrac (void)
/*
 *
 *
 * PURPOSE
 * 
 *   Compute the mass fraction X of Hydrogen as function of the 
 *   composition of the gas.
 *
 *             f_H A_H
 *    X = ----------------   
 *         \sum_k f_k A_k
 * 
 *   where 
 *
 *    f_k   : is the fractional abundance (by number), 
 *               f_k = N_k/N_tot
 *            of atomic species (no matter ionization degree).
 *
 *    A_K   : is the atomic weight
 *
 *
 *   where N_{tot} is the total number density of particles
 *
 * ARGUMENTS
 *
 *   none
 *
 *********************************************************************** */
{
  double mmw2;
  int    i, j;
  
  mmw2 = 0.0;
//  for (i = 0; i < (Fe_IONS==0?7:8); i++) {   //kartick
  for (i = 0; i < Nspecies; i++) {
    mmw2 += elem_mass[i]*elem_ab[i];  /*    Denominator part of X  */    
  }
  return ( (elem_mass[0]*elem_ab[0]) / mmw2);
}

/* ******************************************************** */
void CHECK_NORMALIZATION (double *v, char *str)
/*
 *
 * PURPOSE:
 * 
 *   Check that the sum of all ions of a given chemical element is 
 *   correctly normalize to 1.
 *
 ************************************************************** */
{
  int nv, ifail = 0;
  double phi, tol=1.e-3;

  phi = 0.0;
  for (nv = HeI; nv <= HeIII; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [He] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = CI; nv < CI+C_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [C] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = NI; nv < NI+N_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [N] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = OI; nv < OI+O_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [O] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = NeI; nv < NeI+Ne_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [Ne] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = MgI; nv < MgI+Mg_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [Mg] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = SiI; nv < SiI+Si_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [Si] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }

  phi = 0.0;
  for (nv = SI; nv < SI+S_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [S] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }
/*
  phi = 0.0;
  for (nv = FeI; nv < FeI+Fe_IONS; nv++) phi += v[nv];
  if (fabs(phi-1.0) > tol) {
    print ("%s:  Norm of [Fe] not correct: %12.6e\n",str, phi);
    ifail = 1;
  }
*/

 // if (ifail) QUIT_PLUTO(1);

}

#ifdef CH_SPACEDIM
/* ******************************************************************************* */
void NORMALIZE_IONS (double *u)
/*
 oolCoeffs.Crate[Fe_XX] - CoolCoeffs.Rrate[Fe_XIX];
 *
 *  PURPOSE
 *
 *    This function re-normalize the set of conservative variables u 
 *    in such a way that the sum of all ion fractions is equal to 1. 
 *    Since u is a vector of conserved quantities, the normalization 
 *    condition is:
 *
 *      \sum_j (\rho*X_j) = \rho
 * 
 *    where, for instance, X_j = {OI, OII, OIII, OIV, OV}.
 *    In order to fulfill the previous equation, each conservative
 *    variable is redefined according to 
 *
 *                                 (\rho*X_j)
 *       (\rho*X_j) -->  \rho * -------------------
 *                               \sum_j (\rho*X_j) 
 *
 *    This function is necessary only when used with Chombo AMR since
 *    the coarse-to-fine interpolation fails to conserve the correct
 *    normalization. Re-fluxing and fine-to-coarse do not need this
 *    treatment since the normalization is preserved.
 *
 *
 *
 *  LAST MODIFIED
 *
 *    Aug 25, 2010 by A. Mignone (mignone@ph.unito.it)
 *
 *
 ********************************************************************************** */
{
  int nv;
  double phi;

  phi = 0.0;
  for (nv = HeI; nv <= HeIII; nv++) phi   += u[nv];
  for (nv = HeI; nv <= HeIII; nv++) u[nv] *= u[RHO]/phi;

  phi = 0.0;
  for (nv = CI; nv < CI+C_IONS; nv++) phi   += u[nv];
  for (nv = CI; nv < CI+C_IONS; nv++) u[nv] *= u[RHO]/phi;

  phi = 0.0;
  for (nv = NI; nv < NI+N_IONS; nv++) phi   += u[nv];
  for (nv = NI; nv < NI+N_IONS; nv++) u[nv] *= u[RHO]/phi;


  phi = 0.0;
  for (nv = OI; nv < OI+O_IONS; nv++) phi   += u[nv];
  for (nv = OI; nv < OI+O_IONS; nv++) u[nv] *= u[RHO]/phi;
    
  phi = 0.0;
  for (nv = NeI; nv < NeI+Ne_IONS; nv++) phi   += u[nv];
  for (nv = NeI; nv < NeI+Ne_IONS; nv++) u[nv] *= u[RHO]/phi;

  phi = 0.0;
  for (nv = MgI; nv < MgI+Mg_IONS; nv++) phi   += u[nv];
  for (nv = MgI; nv < MgI+Mg_IONS; nv++) u[nv] *= u[RHO]/phi;

  phi = 0.0;
  for (nv = SiI; nv < SiI+Si_IONS; nv++) phi   += u[nv];
  for (nv = SiI; nv < SiI+Si_IONS; nv++) u[nv] *= u[RHO]/phi;

  phi = 0.0;
  for (nv = SI; nv < SI+S_IONS; nv++) phi   += u[nv];
  for (nv = SI; nv < SI+S_IONS; nv++) u[nv] *= u[RHO]/phi;

  phi = 0.0;
  for (nv = FeI; nv < FeI+Fe_IONS; nv++) phi   += u[nv];
  for (nv = FeI; nv < FeI+Fe_IONS; nv++) u[nv] *= u[RHO]/phi;
}
#endif

/* ************************************************************
 Supplying a supplementary non-equilibrium cooling function
  from Gnat & Sternberg 2007. This avoids direct calculation the
  cooling function from the ion fractions.
 * Just for test puposes
* ************************************************************* */
double SupplementaryCooling(double T)
{
 int klo, khi, kmid, i;
 double tlo, thi, Tmid, dT, llo, lhi, lam;
 double x1, x2;
 static int ntab;
 static double *L_tab, *T_tab;

 FILE *fcool;

 /* Read the cooling table and store it */
 if (T_tab == NULL){
     print1 ("> Reading supplementary NE cooling function.\n");
     fcool = fopen("GS_cool.txt","r");
     if (fcool == NULL) print("! Error: supplementary cooling function GS_cool.txt does not exist.\n");
     T_tab = calloc(200, sizeof(double));
     L_tab = calloc(200, sizeof(double));

     ntab = 0;
     while (fscanf(fcool, "%lf %lf \n", &x1, &x2) != EOF){
	 T_tab[ntab] = x1;
	 L_tab[ntab] = x2;
	 ntab ++;
     }
     print1("# > ntab = %d\n", ntab);
     for (i=0; i<ntab; i++) print1("T = %2.4e; lam=%2.4e\n", T_tab[i], L_tab[i]);
 }

 /* Looking at the table for the temperature provided */
  klo = 0;
  khi = ntab - 1;
  while (klo != (khi - 1)){
    kmid = (klo + khi)/2;
    Tmid = T_tab[kmid];
    if (T >= Tmid){
      klo = kmid;
    }else if (T < Tmid){
      khi = kmid;
    }
  }

 /* Interpolating */
 thi = T_tab[klo];	tlo = T_tab[khi];
 lhi = L_tab[klo];	llo = L_tab[khi];
 dT = thi - tlo;

 lam = llo*(thi-T)/dT + lhi*(T-tlo)/dT;
 return lam;

}

/* ************************************************ */
double GetTotCooling(double ne, double N, double T, double *v)
 /*
  * Calculating the radiative losses based on ion-by-ion cooling
  * Tables generated from cloudy17 (Gnat & Ferland, 2012)
  * Can be used from anywhere the code to calculate total cooling.
  * ****************************************************** */
{
 int nv, el_id, ion_id;
 double rlosst, *X;
 X = v + NFLX;

 rlosst = elem_ab[el_H]*(1.-X[0])*Get_loss_data(1, 1, T); // for HII

 for (nv=0; nv<NIONS; nv++){
   get_ion(nv, &el_id, &ion_id);
   rlosst    += elem_ab[elem_part[nv]] * X[nv]
               * Get_loss_data(el_id, ion_id, T);
 }
 rlosst   *= ne*N;           // in CGS erg/s/cm^3

 return rlosst;
}

