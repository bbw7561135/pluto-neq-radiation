/* ############################################################
      
     FILE:     cooling_defs.h

     PURPOSE:  contains shared definitions with scope
               limited to the cooling module ONLY

   ############################################################ */

/* ##############################################################
     
                   P R O T O T Y P I N G 

   ############################################################## */

void   Check_Species (double *);
void   Find_Rates(double, double, double, double *);
double find_N_rho ();
int    Create_Ion_Coeff_Tables(double ***);
int    Create_Losses_Tables(double ***, int *, int *);


/* ############################################################################

                            New structures

   ############################################################################ */

typedef struct COOLING_COEFFICIENTS
{
  double Lrate[NIONS];  /*  Abundance increase rates. This is to be multiplied with N(x-1) - it's the ionization from x-1 to x */
  double Crate[NIONS];  /*  Abundance decrease rates. This is to be multiplied with N(x) - it's the ionization/recombination from x  */
  double Rrate[NIONS];  /*  Abundance increase rates. This is to be multiplied with N(x+1) - it's the recombination from x+1 to x  */
  double N;                    /*  Total number density                 */
  double Ne;                   /*  Electron number density ( cm^{-3} )  */
  double T;                    /*  Temperature (K)                      */
  double nH;			/* hydrogen number density = rho/(mu_H*mH)*/
  double La[NIONS];     /*  The coefficient of N_el in Lrate     */
  double Lb[NIONS];     /*  The coefficient of X_H in Lrate      */
  double Lc[NIONS];     /*  The coefficient of X_He in Lrate     */
  double Ca[NIONS];     /*  The coefficient of N_el in Crate     */
  double Cb[NIONS];     /*  The coefficient of X_H in Crate      */
  double Cc[NIONS];     /*  The coefficient of X_He in Crate     */
  double Ra[NIONS];     /*  The coefficient of N_el in Rrate     */
  double Rb[NIONS];     /*  The coefficient of X_H in Rrate      */
  double Rc[NIONS];     /*  The coefficient of X_He in Rrate     */
  double muN, muD;        /*  The numerator and denominator of the mean molecular weight - used for d\mu computations  */
  double de[NIONS];     /*  The radiative losses read from cooling tables, interpolated for T and N_el               */
  double de_dne[NIONS]; /*  The slope coefficient in the radiative losses interpolation function of N_el             */

/* -------- NEW ENTRIES -------- */
  
  double dnel_dX[NIONS];
  double fCH, fRH;
  double dmuN_dX[NIONS];
  double dmuD_dX[NIONS];
  double dLIR_dX[NIONS];

  double ***ion_data;		/* to save the reaction coeffs for ions */ //kartick
} COOL_COEFF;


/* **********************************************************************
          Macros
   ********************************************************************** */

#define dEtoA(x) (12375./(x))
#define to_ev(T) (1.38/1.602*1.e-4*(T))

/* **************************************************************
                label elements and ions
   ************************************************************** */

#define el_H   0
#define el_He  1
#define el_C   2
#define el_N   3
#define el_O   4
#define el_Ne  5
#define el_Mg  6
#define el_Si  7
#define el_S   8
#define el_Fe  9

#define Nspecies	(el_Fe+1)

/* ################################################################

        Global variable declaration.  They are defined in radiat.c

   ############################################################### */

extern const double elem_ab[Nspecies];   /* Number fractions   */
extern double elem_ab_sol[7];   /* Number fractions, Solar    */
extern double elem_ab_uni[7];   /* Number fractions, Universe */
extern const double elem_mass[Nspecies];  /*   Atomic mass, in a.m.u.   */
extern const int elem_part[NIONS];
extern const double rad_rec_z[NIONS];
extern const double coll_ion_dE[3];		// kartick; only for H, HeI, HeII
extern COOL_COEFF CoolCoeffs;

/* ****************************************************
   First, the definitions for the 
   Radiative Losses Tables computation
   **************************************************** */


/* ****************************************************
   nlev  = number of levels involved 
   A     = transition probabilities in sec^(-1)
   wgth  = statistical weights
   dE    = energy of the transition in eV 
   N     = Abundance of the ion;
   Ni    = populations of the level;
           notice that we must have \sum Ni = N
   **************************************************** */

typedef struct ION {
   int      nlev, nTom, isMAP, isCV, isH, isCHEB;                 
   double   N;  
   double   wght[30];    /* max number of levels in a ion is 16 */    
   double   Ni[30];	/* Now max number of ion is 30 : kartick */
   double   **A, **dE;  
   double   ***omega, Tom[Nspecies];
} Ion;

/*  Lagrange interpolation function */
double lagrange (double *x, double *y, double xp, int n, int ii, int jj); 

/* Linear system solver  */
void Solve_System (Ion *X, double Ne, double T);
void Symmetrize_Coeff (Ion *X);

/* Emission lines definitions  */
void INIT_ATOM(Ion *, int);
void HI_INIT (Ion *HIv);
void HeI_INIT (Ion *HeIv);
void HeII_INIT (Ion *HeIIv);
void CI_INIT (Ion *CIv);
void CII_INIT (Ion *CIIv);
void CIII_INIT (Ion *CIIv);
void CIV_INIT (Ion *CIIv);
void CV_INIT (Ion *CIIv);
void NI_INIT (Ion *NIv);
void NII_INIT (Ion *NIIv);
void NIII_INIT (Ion *NIIIv);
void NIV_INIT (Ion *NIIIv);
void NV_INIT (Ion *NIIIv);
void OI_INIT (Ion *OIv);
void OII_INIT (Ion *OIIv);
void OIII_INIT (Ion *OIIIv);
void OIV_INIT (Ion *OIVv);
void OV_INIT (Ion *OVv);
void NeI_INIT (Ion *NeIv);
void NeII_INIT (Ion *NeIIv);
void NeIII_INIT (Ion *NeIIIv);
void NeIV_INIT (Ion *NeIIIv);
//odNVII In *NeIIIv);
void SI_INIT (Ion *SIIv);
void SII_INIT (Ion *SIIv);
void SIII_INIT (Ion *SIIv);
void SIV_INIT (Ion *SIIv);
void SV_INIT (Ion *SIIv);
void FeI_INIT (Ion *FeIv);
void FeII_INIT (Ion *FeIIv);
void FeIII_INIT (Ion *FeIIIv);

/* -----------------------------------------------
    Configure ionization tables parameters here:
    start temperature, temperature step,
    number of steps, number of ion species
   ----------------------------------------------- */

#define  kB        8.617343e-5  	// in eV/K 

#define I_g_stepNumber    550
#define I_TSTEP    0.01 
#define I_TBEG     3.00
#define I_TEND     8.50 

/* --------------------------------------------------------
    Configure cooling tables parameters here:
    electron number density range (C_NeMIN to C_NeMAX),
   -------------------------------------------------------- */

#define C_NeMIN   1.0e-2
#define C_NeMAX   1.0e+2
#define C_NeSTEP   0.12

#define C_TMIN    1e3
#define C_TMAX    1.0e+8
#define C_TSTEP    0.025


/* ------------------------------------------------------------
   New variables/structures defined by kartick 
   ------------------------------------------------------------ */

typedef struct PHOTO_COEFFICIENTS
{
 double *Lnu0;		// spectrum of the source
			// shold be calculated after each time step
 double *Spec;		// Spectrum at r (UVB + Brems)
 double *UVB_spec;	// Background UV spectra from HM12
 double *UVB_Jnu, *locJnu, **totJnu, **Fnu;
// double ***Inu_p, ***Inu_m;
 double *radforce;	// radiation force 
 double ***psi;
 double ***Gamma;	// Photo-ionisation rates for nz,state,shell for const UVB (ind of location or time)
 double *Pi_Rate;	// Total Photo-ionisation rate (dep. on location)
			// Should be calculated after each time-step at any r
 double *UVB_PiRate;	// Photo-ionisation rate for UV background (indep. of location or time)
 double *nu, *dnu, *nuc, **IP, *nu0;
 double ***Sigma, **shellintg_Sigma;	// Photo-ionisation cross section for at every 
 					// wavelength given by nu 
 double PHeating;
 double ****Auger_prob; // Auger probability. Const matrix
 double *Auger_rate;	// Auger ionisation rates. depends on time
 int Nnu;		// size of the spectrum considered

} PHOTO_COEFF;


double Get_loss_data(int, int, double);					
//extern void getrate(double *, int *, int *, double *, double *, int *);
extern void getrate(double *, int *, int *, double *);
extern void get_pi_rate(int *, int *, double *, double *);
extern void phfit2(int *, int *, int *, double *, double *);
//extern double allphot_vec(int *, int *, int *);
extern void get_gamma(int *, int *, int *, double *);

//void GetUVB_spec(double *, int);
void GetUVB_spec();		
void GetUVB_gamma();
void CalculateCentralLuminosity(const Data *, Grid *);
double PhotoHeating(double *, int);
double ReadPhotoheatingTable(double T);
void GetIonisationPotentials();
void GetIP();
void GetPhotoCrossSection();
double ElectronDensity(double *v);
void GetAugerProb();
void GetAugerRates(double *);
void get_aug_rate(int *, double *, int *, int *, int *, double *);
double PartialAugerRate(int nz, int state, int Ne);
void GetPhotoIonRate(double *nu, double *Jnu);

void Write_cloudy_input(double nH, double *v, char *infile, double T);
void GetCT_DE(double *, double *);
double GetCTHeating(double, double, double *);
void FORCE_NORMALISATION(double *v);

void spline(double x[], double y[], int n, double yp1, double ypn, double y2[]);
void splint(double xa[], double ya[], double y2a[], int n, double x, double *y);

extern const int ions_nz[NIONS];
extern const int ions_num[Nspecies];
extern PHOTO_COEFF PhotoCoeffs;

/*
 int H_I, H_II, He_I, He_II, He_III, C_I, C_II, C_III, C_IV, C_V, C_VI, C_VII; 
 int N_I, N_II, N_III, N_IV, N_V, N_VI, N_VII, N_VIII, O_I, O_II, O_III, O_IV, O_V, O_VI, O_VII, O_VIII, O_IX;
 int Ne_I, Ne_II, Ne_III, Ne_IV, Ne_V, Ne_VI, Ne_VII, Ne_VIII, Ne_IX, Ne_X, Ne_XI;
 int Mg_I, Mg_II, Mg_III, Mg_IV, Mg_V, Mg_VI, Mg_VII, Mg_VIII, Mg_IX, Mg_X, Mg_XI, Mg_XII, Mg_XIII;
 int Si_I, Si_II, Si_III, Si_IV, Si_V, Si_VI, Si_VII, Si_VIII, Si_IX, Si_X, Si_XI, Si_XII, Si_XIII, Si_XIV, Si_XV;
 int S_I, S_II, S_III, S_IV, S_V, S_VI, S_VII, S_VIII, S_IX, S_X, S_XI, S_XII, S_XIII, S_XIV, S_XV, S_XVI, S_XVII;
 int Fe_I, Fe_II, Fe_III, Fe_IV, Fe_V, Fe_VI, Fe_VII, Fe_VIII, Fe_IX, Fe_X, Fe_XI, Fe_XII, Fe_XIII, Fe_XIV, Fe_XV;
 int Fe_XVI, Fe_XVII, Fe_XVIII, Fe_XIX, Fe_XX, Fe_XXI, Fe_XXII, Fe_XXIII, Fe_XXIV, Fe_XXV, Fe_XXVI, Fe_XXVII;
*/
enum {
  H_I = 0, He_I, He_II, He_III
  C_EXPAND(C_I, C_II, C_III, C_IV, C_V, C_VI, C_VII)
  N_EXPAND(N_I, N_II, N_III, N_IV, N_V, N_VI, N_VII, N_VIII)
  O_EXPAND(O_I, O_II, O_III, O_IV, O_V, O_VI, O_VII, O_VIII, O_IX)
  Ne_EXPAND(Ne_I, Ne_II, Ne_III, Ne_IV, Ne_V, Ne_VI, Ne_VII, Ne_VIII, Ne_IX, Ne_X, Ne_XI)
  Mg_EXPAND(Mg_I, Mg_II, Mg_III, Mg_IV, Mg_V, Mg_VI, Mg_VII, Mg_VIII, Mg_IX, Mg_X, Mg_XI, Mg_XII, Mg_XIII)
  Si_EXPAND(Si_I, Si_II, Si_III, Si_IV, Si_V, Si_VI, Si_VII, Si_VIII, Si_IX, Si_X, Si_XI, Si_XII, Si_XIII, Si_XIV, Si_XV)
  S_EXPAND(S_I, S_II, S_III, S_IV, S_V, S_VI, S_VII, S_VIII, S_IX, S_X, S_XI, S_XII, S_XIII, S_XIV, S_XV, S_XVI, S_XVII)
  Fe_EXPAND(Fe_I, Fe_II, Fe_III, Fe_IV, Fe_V, Fe_VI, Fe_VII, Fe_VIII, Fe_IX, Fe_X, Fe_XI, Fe_XII, Fe_XIII, Fe_XIV, Fe_XV, Fe_XVI, Fe_XVII, Fe_XVIII, Fe_XIX, Fe_XX, Fe_XXI, Fe_XXII, Fe_XXIII, Fe_XXIV, Fe_XXV, Fe_XXVI, Fe_XXVII)
};

#if RadTran
  #include "radtran.h"
#endif
