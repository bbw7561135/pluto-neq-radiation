#include "pluto.h"
#include "cooling_defs.h"

PHOTO_COEFF PhotoCoeffs;
SELF_SPEC SelfSpec;

/* ********************************************* */
double eps_cloudy(int nu_idx, double nHp, double Tp)
/* 
 * Calculates the cloudy emissivities at density nHp
 * and temp Tp (assuming equilibrium from cloudy) at any
 * given frequency index nu_idx. final emissivity is 
 * eps_cloudy(). 
 * *********************************************************** */
{

 static double *NU, ***EPS;
 static double nHbeg, nHend, Tbeg, Tend, dnH, dT;
 double nH, T;
 int i, j;
 static int NnH, NT, cout;


 if (NU == NULL){
     cout = PhotoCoeffs.Nnu;
     nHbeg      = -4.0; nHend   = 4.0;
     Tbeg       = 3.5;  Tend    = 8.5;
     dnH        = 0.1;
     dT         = 0.1;

     NnH        = (int)((nHend-nHbeg)/dnH);
     NT = (int)((Tend-Tbeg)/dT);

     NU         = calloc(1000, sizeof(double));
     EPS        = ARRAY_3D(NnH, NT, cout, double);
     LoadCloudyEmissivity(NU, EPS);
     
     print1("> Cloudy emissivity files loaded.\n");
 }

 nH = log10(nHp);
 T  = MIN(log10(Tp), 8.50);  //ceiling and floor for temp since reading from tables
 T  = MAX(T, 3.50);  

 i = (int)round((nH-nHbeg)/dnH);
 j = (int)round((T-Tbeg)/dT);

 // ceiling and floor for nH, since it is being read from tables
 i = (i<0)? 0 : (i>80)? 80 : i;	

 if (i<0 || j<0 || i> 80 || j >50)
 print1 ("inHidx=%d, Tidx=%d at nH=%2.4e, T=%2.4e\n", i, j, nHp, Tp);

 return EPS[i][j][nu_idx];

}

/* ********************************************* */

void LoadCloudyEmissivity(double *NU, double ***EPS)
{
 double nHbeg, Tbeg, nHend, Tend, dnH, dT, nH, T;
 double x1, x2;
 int NnH, NT, i, j;
 char infilename[128];

 nHbeg  = -4.0; nHend   = 4.0;
 Tbeg   = 3.5;  Tend    = 8.5;
 dnH    = 0.1;
 dT     = 0.1;

 NnH    = (int)((nHend-nHbeg)/dnH);
 NT     = (int)((Tend-Tbeg)/dT);

 for (i=0; i<NnH; i++){
     nH  = nHbeg + i*dnH;
     for (j=0; j<NT; j++){
         T      = Tbeg +j*dT;
         sprintf(infilename, "%s/nH%1.2f_T%1.2f.spout", g_specdir, nH, T);
         ReadCloudySpectraFile(NU, EPS[i][j], infilename);
     }
 }
}

/* *************************************************************** */
void ReadCloudySpectraFile(double *NU, double *EPS, char *infilename)
{
 double x1, x2, x3;
 int cout=0;
 FILE *fin;

 fin    = fopen(infilename, "r");

 while(fscanf(fin, "%lf  %lf  %lf", &x1, &x2, &x3) != EOF){
     NU[cout]   = x1;
     EPS[cout]  = x2;
     cout ++;
 }
 fclose(fin);

}

