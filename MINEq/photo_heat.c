#include "pluto.h"
#include "cooling_defs.h"

PHOTO_COEFF PhotoCoeffs;
COOL_COEFF CoolCoeffs;

double PhotoHeating(double *v, int nv)
/* 
 * Calculates photoheating rates for a given ion of index nv
 * Input
   *v      = Array containing primitive variables, rho, vx1, prs, 
   		fHI, fHeI ...
    nv      = ion index 0=H_I, 1=He_I
 * return
 *       Gamma  = n(X)* \int_0^inf 4*pi*Jnu/(h*nu)* h*(nu-nu0)
 *       		* sigma_nu(X) * dnu 
                                          for different subshells
 *
 * *********************************************************** */
{
 static double n_rho, **ph;
 static int Nnu;
 int i, shell, ion;
 double scrh, scrh2, nX, integrand;

 if (ph == NULL){
   n_rho   = find_N_rho();
   ph      = ARRAY_2D(NIONS, PhotoCoeffs.Nnu, double);	// ion,nu
   Nnu     = PhotoCoeffs.Nnu;

   /* The following loop precalculates (\nu-\nu_0)/\nu*\Sigma_{\nu}*d\nu to save time */
   for (ion=0; ion<NIONS; ion++){
     for   (i=0; i<Nnu-1; i++){
       scrh = 0.0;
       for (shell=1; shell<=7; shell++){
         if (PhotoCoeffs.nu[i] < PhotoCoeffs.IP[ion][shell]) continue;
         integrand  = (PhotoCoeffs.nuc[i]-PhotoCoeffs.IP[ion][shell])
		 	*PhotoCoeffs.Sigma[ion][shell][i];
         integrand *= PhotoCoeffs.dnu[i] / PhotoCoeffs.nuc[i];
	 scrh += integrand;
       } /* end of shell loop */
       ph[ion][i] = 4.*CONST_PI*scrh;
     }/* end of i (nu) loop */
   }   /* end of ion loop */

 }     /* end of first time loop (ph==NULL) */


 /* The following is the total integration for the photo-heating rate */
 scrh = 0.0;
 for (i=0; i<Nnu-1; i++){ 
   scrh += ph[nv][i]*PhotoCoeffs.locJnu[i];
 }
 nX    = v[RHO]*n_rho*elem_ab[elem_part[nv]]*v[nv+NFLX];

/*
 * This was to check the consistency with HM12 heating rates. They are consistent.

 if (nv==0) print1 ("Heating rate for HI %2.4e, scrh=%2.4e\n", nX*scrh, scrh);
 if (nv==1) print ("Heating rate for HeI %2.4e, scrh=%2.4e\n", nX*scrh, scrh);
 if (nv==2) print ("Heating rate for HeII %2.4e, scrh=%2.4e\n", nX*scrh, scrh);
*/

 return nX*scrh;
   

}


double ReadPhotoheatingTable(double tmp)
/* 
 * This returns non-eq isochoric photoheating for a given temperature.
 * for nH = 1.0, HM12, z=0
 * Table is from Gnat, 2017
 * ****************************************************** */

{
 int k, klo, kmid, khi;
 static int count;
 static double *T, *PH;
 double tt, phzem3, phzem2, phzem1, phz1, phz2;

 if (T == NULL){
     T  = calloc(1000 ,sizeof(double));
     PH = calloc(1000 ,sizeof(double));
     FILE *f = fopen("input_tables/datafile8AN.txt","r");
     print("Warning! the photoheating table input_tables/datafile8AN.txt is meant for nH = 1.0. You may want to change the file\n");
     if (f == NULL) print("!Error: input_tables/datafile8AN.txt not found.\n");

     count = 0;
     while( fscanf(f, "%lf  %lf  %lf  %lf  %lf", &tt, &phzem2, &phzem1, &phz1, &phz2) != EOF){
         T[count]       = tt;
         PH[count]      = phz1;
         count ++;
     }
     fclose(f);
 }

 if (tmp < T[count-1] || tmp > T[0]) return 0.0;

 /* Interpolating the heating rate to the required temperature */
 klo  = 0;
 khi  = count-1;

 while (khi != klo+1){
     kmid = (khi+klo)/2;
     if (tmp < T[kmid])
         klo = kmid;
     else
         khi = kmid;
 }

 double tlo, thi, dt, phlo, phhi;
 tlo = T[khi];          thi = T[klo];   dt = thi-tlo;
 phlo = PH[khi];        phhi = PH[klo];

 return phlo*(thi - tmp)/dt + phhi*(tmp-tlo)/dt;

} 


double GetCTHeating(double T, double N, double *v)
/* ********************************************************* 
 * This calculates the heating due to charge transfer reactions with H0 or H+.
 * In many cases, the CT heating can become much larger than the photo-heating.
 * Heating rate for each ion is 
 * 
 * Gamma = n_H * n_ion * CTrate * DE 
 * 
 * n_H (n_H0 / n_H+)    = H0 or H+ density 
 * n_ion		= ion density
 * CTrate		= charge transfer reaction coeff
 * DE			= the energy defect
 *			Following Kingdon and Ferland, ApJ, 516, L107
 *
 * T	= temperature
 * Ne	= electron density
 * N	= total ion number density
 * CTH  = CT heating
 * *************************************************************** */
{
 double ft1, ft2,xi_ct_HII[NIONS], al_ct_HI[NIONS], CTH;
 int tindex, ion;
 static double *CT_DE_rec, *CT_DE_ion;

 if (CT_DE_rec == NULL){
     CT_DE_rec = calloc(NIONS, sizeof(double));
     CT_DE_ion = calloc(NIONS, sizeof(double));
     GetCT_DE(CT_DE_rec, CT_DE_ion);
 }

 /* Interpolate to the given temperature */
 double logT, logL;                            // kartick
 logT          = log10(T);
 if      (logT > I_TEND) logT = I_TEND - I_TSTEP*0.5;
 else if (logT < I_TBEG) logT = I_TBEG + I_TSTEP*0.5;

 tindex        = floor( (logT-I_TBEG)/I_TSTEP );
 ft1   = ((I_TBEG + I_TSTEP*(tindex+1))*1.0 - logT)/I_TSTEP;
 ft2   = (logT - (I_TBEG + I_TSTEP*tindex)*1.0)/I_TSTEP;

 /* Save the coeffs in a recognisable variable */ 
 for (ion=0; ion<NIONS-1; ion++){
     logL	= log10(CoolCoeffs.ion_data[tindex][3][ion])*ft1 
		  + log10(CoolCoeffs.ion_data[tindex+1][3][ion])*ft2;
     xi_ct_HII[ion]	= pow(10., logL);
     logL	= log10(CoolCoeffs.ion_data[tindex][4][ion])*ft1
		  + log10(CoolCoeffs.ion_data[tindex+1][4][ion])*ft2;
     al_ct_HI[ion+1]	= pow(10., logL);
 }

 double nHI, nHII, n_ion;
 nHI	= N*elem_ab[el_H] * v[HI];
 nHII	= N*elem_ab[el_H] * (1.0-v[HI]);

 CTH = 0.0;
/* print("Calculating CT heating\n");  */
 for (ion=He_I; ion<NIONS; ion++){
    n_ion	= N*elem_ab[elem_part[ion]] * v[ion+NFLX];
    if (CT_DE_rec[ion] > 1.e-20) 
        CTH	+= nHI * n_ion * al_ct_HI[ion] * CT_DE_rec[ion];
    if (CT_DE_ion[ion] > 1.e-20) 
	CTH	+= nHII* n_ion * xi_ct_HII[ion]* CT_DE_ion[ion];
 }

 return CTH;
}


void GetCT_DE(double *DE_rec, double *DE_ion)
/* 
 * This is the energy defect for a given charge transfer reaction
 * DE_rec is the energy for CT recombination with H0
 * DE_ion is the energy for CT ionisation with H+
 * 
 * Data taken from Kingdon and Ferland, ApJ, 516, L107
 * ***************************************************************************************** */
{
 int i, state;
 for (i=0; i<NIONS; i++){
     DE_rec[i] = 0.0;
     DE_ion[i] = 0.0;
    
     state = (int)rad_rec_z[i] - 1;
     if (state >= 5) DE_rec[i] = 2.86 * (state-1) * CONST_eV;  /*** NOTES **/
     /* add 2.86*(state-1) eV for state >= 5 i.e. VI and onwards 
      * since they are unknown. Following cloudy 17.01 */ 
 }

 /* for He */
 DE_rec[He_II]		= 10.99	* CONST_eV;
 DE_rec[He_III]		= -40.81* CONST_eV;/*cloudy 17.01 additions*/

 /* for C */
 C_SELECT( ,
 DE_rec[C_II]		= -2.34	* CONST_eV;,
 DE_rec[C_III]		= 4.01	* CONST_eV;,
 DE_rec[C_IV]		= 5.73	* CONST_eV;,
 DE_rec[C_V]		= 11.30	* CONST_eV;, , );

 C_SELECT(
 DE_ion[C_I]		= 2.34	* CONST_eV;, , , , , , );
 
 /* for N */
 N_SELECT( ,
 DE_rec[N_II]		= 0.94	* CONST_eV;,
 DE_rec[N_III]		= 4.56	* CONST_eV;,
 DE_rec[N_IV]		= 6.40	* CONST_eV;,
 DE_rec[N_V]		= 11.00	* CONST_eV;, , , );
 N_SELECT(
 DE_ion[N_I]		= -0.94	* CONST_eV;, , , , , , , );

 /* for O */
 O_SELECT( ,
 DE_rec[O_II]		= 0.02	* CONST_eV;,
 DE_rec[O_III]		= 6.65	* CONST_eV;,
 DE_rec[O_IV]		= 5.00	* CONST_eV;,
 DE_rec[O_V]		= 8.47	* CONST_eV;, , , , );
 O_SELECT(
 DE_ion[O_I]		= -0.02	* CONST_eV;, , , , , , , , );

 /* for Ne */
  Ne_SELECT( , ,
 DE_rec[Ne_III]		= -27.36* CONST_eV;,
 DE_rec[Ne_IV]		= 5.82	* CONST_eV;,
 DE_rec[Ne_V]		= 8.60	* CONST_eV;, , , , , , );

 /* for Mg */
 Mg_SELECT( , ,
 DE_rec[Mg_III]		= 1.44	* CONST_eV;, 
 DE_rec[Mg_IV]		= 5.73	* CONST_eV;,
 DE_rec[Mg_V]		= 8.60	* CONST_eV;, , , , , , , , );
 Mg_SELECT(
 DE_ion[Mg_I]		= 1.52	* CONST_eV;,
 DE_ion[Mg_II]		= -1.44	* CONST_eV;, , , , , , , , , , , );

 /* for Si */
 Si_SELECT( , ,
 DE_rec[Si_III]		= 2.72	* CONST_eV;,
 DE_rec[Si_IV]		= 4.23	* CONST_eV;,
 DE_rec[Si_V]		= 7.49	* CONST_eV;, , , , , , , , , , );
 Si_SELECT(
 DE_ion[Si_I]		= 0.12	* CONST_eV;,
 DE_ion[Si_II]		= -2.72	* CONST_eV;, , , , , , , , , , , , , );

 /* for S */
 S_SELECT( ,
 DE_rec[S_II]		= -3.24	* CONST_eV;,
 DE_rec[S_III]		= -9.73	* CONST_eV;,
 DE_rec[S_IV]		= 5.73	* CONST_eV;,
 DE_rec[S_V]		= 8.60	* CONST_eV;, , , , , , , , , , , , );
 #if (S_IONS > 0)
 DE_ion[S_I]		= -3.24	* CONST_eV;
 #endif

 /* for Fe */
 #if (Fe_IONS > 2)
 DE_rec[Fe_III]		= 2.56	* CONST_eV;
 #endif
 #if (Fe_IONS > 3)
 DE_rec[Fe_IV]		= 6.30	* CONST_eV;
 #endif
 #if (Fe_IONS > 4)
 DE_rec[Fe_V]		= 10.0	* CONST_eV;
 #endif

 #if (Fe_IONS > 1)
 DE_ion[Fe_II]		= -2.56	* CONST_eV;
 #endif

}
