
double Lloss, g_Eloss;
double g_maxCoolingTemp;
int g_Rshock;
int g_includeRadForce;
int g_include_dust;
int get_photoequilibrium;
char g_specdir[128], g_dustfile[128], output_dir[128], g_tablesdir[128];
double standard_dust_size;

void ReadOpacityTables();
void GetShockPosition(const Data *d, Grid *, int *);
double eps_cloudy(int nu_idx, double nHp, double Tp);
void LoadCloudyEmissivity(double *NU, double ***EPS);
void ReadCloudySpectraFile(double *NU, double *EPS, char *infilename);
void DoRadiativeTransfer(const Data *d, Grid *grid);
void GetDustProperties();
void EvolveDust(const Data *d, double dt);

/* Structure to contain self-radiation related variables */
typedef struct SELF_SPECTRA
{
 double **g_opac, *logTop, *nu, *dnu;   /* Cloudy opacity arrays */
 double ****eps_nu;             /* cloudy emissivity array */
 double ****opac, ****opac_ions, ****opac_dust_ext;             /* Local opacity */
 double ***Temp, ***mu, ***nH1;         /* local Temp and mu */
 double ****am, ****bm;         /* Major and minor axes of LSF ellipse towards centre */
 double ****ap, ****bp;         /* Major and minor azes of LSF ellipse opposite to centre */
 double ****Inu;
 int ****optically_thin;
 double *N_totphot, *N_uvH, *N_uvHe;  /* Total no of photons, photons>13.6eV,
                                        photons>24.6eV
                                        luminosity at a radius [photons/sec]*/
} SELF_SPEC;

/* Structure to contain dust related variables */
typedef struct DUST
{
 double *albedo, *costheta, *sigma_ext, *sigma_abs;
 double *sigma_scat, *sigma_pr;
} DUST;

extern SELF_SPEC SelfSpec;
extern DUST Dust;

