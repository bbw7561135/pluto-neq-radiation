#include "pluto.h"
#include "cooling_defs.h"

/* ********************************************************** */
real GetMaxRate (real *v0, real *k1, real T0)
/*
 *
 *  PURPOSE:
 *
 *    return an estimate of the maximum rate (dimension 1/time) 
 *    in the chemical network. This will serve as a
 *    "stiffness" detector in the main ode integrator.
 *   
 *    For integration to be carried explicitly all the time,
 *    return a small value (1.e-12).
 *
 ************************************************************ */
{
  int  nv, ion;
  real   ev1[NIONS+1];
  real   scrh, maxrate;
  static real **J1;

  if (J1 == NULL)  {
    J1 = ARRAY_2D(NIONS+1, NIONS+1, double);
  }

//  maxrate = 1.e-12;
  maxrate = fabs(k1[PRS])/v0[PRS];
  
//  for (nv = 0; nv < NIONS - Fe_IONS; nv++) {  /* exclude Iron */
  for (nv = 0; nv < NIONS; nv++) {  	/*  do not exclude 
					 any ions :kartick */
  /* ---------------------------------------------- 
       if the initial concentration is close to 0,
       do not take Crate as a stiffness indicator
     ---------------------------------------------- */
    ion = (int) ( rad_rec_z[nv] - 1.0);
    if (v0[NFLX + nv] < 1.e-6 || ion >= 9) continue;   // do not consider ions > 8+ to decide time-step

    maxrate = MAX(maxrate, fabs(k1[nv+NFLX]/v0[nv+NFLX]) );

    maxrate = MAX(maxrate, (fabs(CoolCoeffs.Crate[nv]) + fabs(PhotoCoeffs.Pi_Rate[nv])) );
    maxrate = MAX(maxrate, fabs(CoolCoeffs.Rrate[nv]));	// kartick
    maxrate = MAX(maxrate, fabs(CoolCoeffs.Lrate[nv]));	// kartick
    maxrate = MAX(maxrate, fabs(PhotoCoeffs.Auger_rate[nv]/v0[nv+NFLX]) );	// kartick
  
  } 
  maxrate *= g_unitLength/g_unitVelocity;

/*
  Numerical_Jacobian    (v0, J1);
  lmax = Decompose(J1[0], NIONS + 1, ev1);
  Radiat(v0, k1);
  scrh = maxrate/fabs(lmax) - 1.0;
  if (fabs(scrh) > 5.e-2){
     printf ("Crate are not the max eig, %12.6e  %12.6e\n", 
              maxrate, fabs(lmax));
     QUIT_PLUTO(1);
  }
  return(fabs(lmax));
*/

  return (maxrate);
}
