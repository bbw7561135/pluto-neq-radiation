#include "pluto.h"
#include "cooling_defs.h"

PHOTO_COEFF PhotoCoeffs;
double InterpolatedJnu(double nu);

void GetPhotoCrossSection(){
/* 
 * Saves the photo-ionisation cross section of each ions for 
 * shells 1-7 for a given radiation fields. The current code 
 * calculates these rates at the coarse grain frequencies and have 
 * been averaged over the frequencies using HM12 (z=0) spectra so that
 * \Sum J_{\nu, HM12} \Sigma_{el,ion,shell} remains same in a 
 * particular frequency band.
 * *************************************************************** */
 int i, nv, el_id, ion_id, Nnu, ne, shell;
 int id, Nfine=20, last_ion;
 double nu, E, sig, nutoeV;
 double Dnu, dnu, Jnu, Sum;

 Nnu 	= PhotoCoeffs.Nnu;
 nutoeV = CONST_h/(CONST_eV);

 /* start with a zero PhotoCoeffs.Sigma */
 for (nv=0; nv<NIONS; nv++){
     for (shell=1;shell<=7; shell++)
         for (i=0; i<Nnu-1; i++)
             PhotoCoeffs.Sigma[nv][shell][i] = 1e-60;
 }

 for (nv=0; nv<NIONS; nv++){
     get_ion(nv, &el_id, &ion_id);
    /* following is an important checkpoint in case a full IN 
     * is not used. This switches off photo-ionisation from last ion
     * to higher ions. */
     last_ion = ions_num[elem_part[nv]]-1;
     if (el_id >=2 & ion_id >= last_ion)
             continue;  //only for elements > Carbon
    /* --------------------------------------------------------- */
     ne = el_id-ion_id;
     for (shell=1;shell<=7; shell++){
         for (i=0; i<Nnu-1; i++){
	     Sum = 0.0;
	     Dnu = PhotoCoeffs.dnu[i];
	     dnu = Dnu/(Nfine*1.0);
	     for (id=0; id<Nfine; id++){
  	         E = nutoeV*(PhotoCoeffs.nu[i]+id*dnu + dnu/2.0);
	         phfit2_(&el_id, &ne, &shell, &E, &sig);
		 Jnu = InterpolatedJnu(E);
		 Sum += Jnu*sig*dnu;
	     }
	     PhotoCoeffs.Sigma[nv][shell][i] = 1.e-18*Sum/(PhotoCoeffs.UVB_Jnu[i]*Dnu);
	 }
     }
 }

 /* Getting the shell integrated sigma. to be used in calculating
  * opacity (absorption coeffs) */
 for (nv=0; nv<NIONS; nv++){
   for (i=0; i<PhotoCoeffs.Nnu-1; i++){
     Sum = 0.0;
     for (shell=1; shell<=7; shell++) 
	 Sum += PhotoCoeffs.Sigma[nv][shell][i];
     PhotoCoeffs.shellintg_Sigma[nv][i] = Sum;
   }
 }

/*
// Testing 
 print ("Sigma for nv=0, shell=1, \n");
 for (i=0; i<Nnu; i++)
    print("%2.4e  %2.4e\n", PhotoCoeffs.nu[i], PhotoCoeffs.Sigma[0][1][i]);
*/

}

/* ***************************************************
 * Calculate interpolated Jnu from the HM12 spectra 
 * *************************************************** */
double InterpolatedJnu(double E)
/* E is energy in eV */
{
 static int first_time_here = 1, count;
 static double xd[575], yd[575], y2[575];
 double Jnu; 

 if (first_time_here){
     double x1, x2;
     char HM12file[128];

     /* interpolating from a HM12 spectra after removing
      * multivalued points since they caus troubles for 
      * interpolation */
     sprintf(HM12file, "%s/UVB_HM12_cp.out", g_tablesdir);
     FILE *fdata = fopen(HM12file, "r");
     count=0;
     while (fscanf(fdata, "%lf %lf", &x1, &x2) != EOF){
       xd[count] = log10(x1*1e-8);
       yd[count] = log10(x2);
       count++;
     }
     spline(xd-1, yd-1, count, 1.0, -0.1, y2-1);
     fclose(fdata);
     first_time_here = 0;
 }

 splint(xd-1, yd-1, y2-1, count, log10(CONST_h*CONST_c/(E*CONST_eV)), &Jnu);

 return pow(10., Jnu);
}



//void GetUVB_gamma()
/* ************************************************************
 * Gets the photo-ionisation rates for a given nz,state,shell
 * and a constant UV background spectrum 
 * To save time.
 * **********************************************************  */
/*
{
 int nz, state, shell;
 double gam;

 if (PhotoCoeffs.UVB_gamma == NULL){
     PhotoCoeffs.UVB_gamma = ARRAY_3D(27, 27, 8, double);
 } 
 for (nz=1; nz<27; nz++)
   for (state=0; state<nz; state++)
     for (shell=1; shell<=7; shell++){
       get_gamma_(&nz, &state, &shell, &gam);
//       PhotoCoeffs.UVB_gamma[nz][state][shell] = gam;
     }


 return;
}
*/


void GetIP()
/* *************************************************************
 loads the ionisation potentials (in Hz) into the RAM
 * ************************************************************* */
{
  
 int nv, shell;

 if (PhotoCoeffs.IP == NULL){
     PhotoCoeffs.IP	= ARRAY_2D(NIONS, 8, double);
     int nv, shell;
     for (nv=0; nv<NIONS; nv++)
     for (shell=1; shell<=7; shell++)
	 PhotoCoeffs.IP[nv][shell] = 1e6;
 }

 PhotoCoeffs.IP[H_I][1]		= 1.360E+01;// PH1(I, 1, 1, 1)
 PhotoCoeffs.IP[He_II][1]	= 5.442E+01;// PH1(I, 2, 1, 1)
 PhotoCoeffs.IP[He_I][1]	= 2.459E+01;// PH1(I, 2, 2, 1)

 C_SELECT(
 PhotoCoeffs.IP[C_I][1]		= 2.910E+02;
 PhotoCoeffs.IP[C_I][2]		= 1.939E+01;
 PhotoCoeffs.IP[C_I][3]		= 1.126E+01;,
 PhotoCoeffs.IP[C_II][1]	= 3.076E+02;
 PhotoCoeffs.IP[C_II][2]	= 3.047E+01;
 PhotoCoeffs.IP[C_II][3]	= 2.438E+01;,
 PhotoCoeffs.IP[C_III][1]	= 3.289E+02;
 PhotoCoeffs.IP[C_III][2]	= 4.789E+01;,
 PhotoCoeffs.IP[C_IV][1]	= 3.522E+02;
 PhotoCoeffs.IP[C_IV][2]	= 6.449E+01;,
 PhotoCoeffs.IP[C_V][1]		= 3.921E+02;,
 PhotoCoeffs.IP[C_VI][1]	= 4.900E+02;, );

 N_SELECT( 
 PhotoCoeffs.IP[N_I][1]		= 4.048E+02;
 PhotoCoeffs.IP[N_I][2]		= 2.541E+01;
 PhotoCoeffs.IP[N_I][3]		= 1.453E+01;,
 PhotoCoeffs.IP[N_II][1]	= 4.236E+02;
 PhotoCoeffs.IP[N_II][2]	= 3.796E+01;
 PhotoCoeffs.IP[N_II][3]	= 2.960E+01;,
 PhotoCoeffs.IP[N_III][1]	= 4.473E+02;
 PhotoCoeffs.IP[N_III][2]	= 5.545E+01;
 PhotoCoeffs.IP[N_III][3]	= 4.745E+01;,
 PhotoCoeffs.IP[N_IV][1]	= 4.753E+02;
 PhotoCoeffs.IP[N_IV][2]	= 7.747E+01;,
 PhotoCoeffs.IP[N_V][1]		= 5.043E+02;
 PhotoCoeffs.IP[N_V][2]		= 9.789E+01;,
 PhotoCoeffs.IP[N_VI][1]	= 5.521E+02;,
 PhotoCoeffs.IP[N_VII][1]	= 6.671E+02;, );

 O_SELECT(
 PhotoCoeffs.IP[O_I][1]		= 5.380E+02;
 PhotoCoeffs.IP[O_I][2]		= 2.848E+01;
 PhotoCoeffs.IP[O_I][3]		= 1.362E+01;,
 PhotoCoeffs.IP[O_II][1]	= 5.581E+02;
 PhotoCoeffs.IP[O_II][2]	= 4.599E+01;
 PhotoCoeffs.IP[O_II][3]	= 3.512E+01;,
 PhotoCoeffs.IP[O_III][1]	= 5.840E+02;
 PhotoCoeffs.IP[O_III][2]	= 6.551E+01;
 PhotoCoeffs.IP[O_III][3]	= 5.494E+01;,
 PhotoCoeffs.IP[O_IV][1]	= 6.144E+02;
 PhotoCoeffs.IP[O_IV][2]	= 8.737E+01;
 PhotoCoeffs.IP[O_IV][3]	= 7.741E+01;,
 PhotoCoeffs.IP[O_V][1]		= 6.491E+02;
 PhotoCoeffs.IP[O_V][2]		= 1.139E+02;,
 PhotoCoeffs.IP[O_VI][1]	= 6.837E+02;
 PhotoCoeffs.IP[O_VI][2]	= 1.381E+02;,
 PhotoCoeffs.IP[O_VII][1]	= 7.393E+02;,
 PhotoCoeffs.IP[O_VIII][1]	= 8.714E+02;, );

 Ne_SELECT(
 PhotoCoeffs.IP[Ne_I][1]	= 8.701E+02;
 PhotoCoeffs.IP[Ne_I][2]	= 4.847E+01;
 PhotoCoeffs.IP[Ne_I][3]	= 2.156E+01;,
 PhotoCoeffs.IP[Ne_II][1]	= 8.831E+02;
 PhotoCoeffs.IP[Ne_II][2]	= 6.374E+01;
 PhotoCoeffs.IP[Ne_II][3]	= 4.096E+01;,
 PhotoCoeffs.IP[Ne_III][1]	= 9.131E+02;
 PhotoCoeffs.IP[Ne_III][2]	= 8.721E+01;
 PhotoCoeffs.IP[Ne_III][3]	= 6.346E+01;,
 PhotoCoeffs.IP[Ne_IV][1]	= 9.480E+02;
 PhotoCoeffs.IP[Ne_IV][2]	= 1.132E+02;
 PhotoCoeffs.IP[Ne_IV][3]	= 9.712E+01;,
 PhotoCoeffs.IP[Ne_V][1]	= 9.873E+02;
 PhotoCoeffs.IP[Ne_V][2]	= 1.415E+02;
 PhotoCoeffs.IP[Ne_V][3]	= 1.262E+02;,
 PhotoCoeffs.IP[Ne_VI][1]	= 1.031E+03;
 PhotoCoeffs.IP[Ne_VI][2]	= 1.719E+02;
 PhotoCoeffs.IP[Ne_VI][3]	= 1.579E+02;,
 PhotoCoeffs.IP[Ne_VII][1]	= 1.078E+03;
 PhotoCoeffs.IP[Ne_VII][2]	= 2.073E+02;,
 PhotoCoeffs.IP[Ne_VIII][1]	= 1.125E+03;
 PhotoCoeffs.IP[Ne_VIII][2]	= 2.391E+02;,
 PhotoCoeffs.IP[Ne_IX][1]	= 1.196E+03;,
 PhotoCoeffs.IP[Ne_X][1]	= 1.362E+03;, );

 Mg_SELECT(
 PhotoCoeffs.IP[Mg_I][1]	= 1.311E+03;
 PhotoCoeffs.IP[Mg_I][2]	= 9.400E+01;
 PhotoCoeffs.IP[Mg_I][3]	= 5.490E+01;
 PhotoCoeffs.IP[Mg_I][4]	= 7.646E+00;,
 PhotoCoeffs.IP[Mg_II][1]	= 1.320E+03;
 PhotoCoeffs.IP[Mg_II][2]	= 9.881E+01;
 PhotoCoeffs.IP[Mg_II][3]	= 6.569E+01;
 PhotoCoeffs.IP[Mg_II][4]	= 1.504E+01;,
 PhotoCoeffs.IP[Mg_III][1]	= 1.336E+03;
 PhotoCoeffs.IP[Mg_III][2]	= 1.111E+02;
 PhotoCoeffs.IP[Mg_III][3]	= 8.014E+01;,
 PhotoCoeffs.IP[Mg_IV][1]	= 1.356E+03;
 PhotoCoeffs.IP[Mg_IV][2]	= 1.411E+02;
 PhotoCoeffs.IP[Mg_IV][3]	= 1.093E+02;,
 PhotoCoeffs.IP[Mg_V][1]	= 1.400E+03;
 PhotoCoeffs.IP[Mg_V][2]	= 1.735E+02;
 PhotoCoeffs.IP[Mg_V][3]	= 1.413E+02;,
 PhotoCoeffs.IP[Mg_VI][1]	= 1.449E+03;
 PhotoCoeffs.IP[Mg_VI][2]	= 2.076E+02;
 PhotoCoeffs.IP[Mg_VI][3]	= 1.865E+02;,
 PhotoCoeffs.IP[Mg_VII][1]	= 1.501E+03;
 PhotoCoeffs.IP[Mg_VII][2]	= 2.444E+02;
 PhotoCoeffs.IP[Mg_VII][3]	= 2.249E+02;,
 PhotoCoeffs.IP[Mg_VIII][1]	= 1.558E+03;
 PhotoCoeffs.IP[Mg_VIII][2]	= 2.839E+02;
 PhotoCoeffs.IP[Mg_VIII][3]	= 2.660E+02;,
 PhotoCoeffs.IP[Mg_IX][1]	= 1.618E+03;
 PhotoCoeffs.IP[Mg_IX][2]	= 3.282E+02;,
 PhotoCoeffs.IP[Mg_X][1]	= 1.675E+03;
 PhotoCoeffs.IP[Mg_X][2]	= 3.675E+02;,
 PhotoCoeffs.IP[Mg_XI][1]	= 1.762E+03;,
 PhotoCoeffs.IP[Mg_XII][1]	= 1.963E+03;, );

Si_SELECT(
 PhotoCoeffs.IP[Si_I][1]	= 1.846E+03;
 PhotoCoeffs.IP[Si_I][2]	= 1.560E+02;
 PhotoCoeffs.IP[Si_I][3]	= 1.060E+02;
 PhotoCoeffs.IP[Si_I][4]	= 1.517E+01;
 PhotoCoeffs.IP[Si_I][5]	= 8.152E+00;,
 PhotoCoeffs.IP[Si_II][1]	= 1.848E+03;
 PhotoCoeffs.IP[Si_II][2]	= 1.619E+02;
 PhotoCoeffs.IP[Si_II][3]	= 1.186E+02;
 PhotoCoeffs.IP[Si_II][4]	= 2.240E+01;
 PhotoCoeffs.IP[Si_II][5]	= 1.635E+01;,
 PhotoCoeffs.IP[Si_III][1]	= 1.852E+03;
 PhotoCoeffs.IP[Si_III][2]	= 1.744E+02;
 PhotoCoeffs.IP[Si_III][3]	= 1.311E+02;
 PhotoCoeffs.IP[Si_III][4]	= 3.349E+01;,
 PhotoCoeffs.IP[Si_IV][1]	= 1.868E+03;
 PhotoCoeffs.IP[Si_IV][2]	= 1.899E+02;
 PhotoCoeffs.IP[Si_IV][3]	= 1.466E+02;
 PhotoCoeffs.IP[Si_IV][4]	= 4.514E+01;,
 PhotoCoeffs.IP[Si_V][1]	= 1.887E+03;
 PhotoCoeffs.IP[Si_V][2]	= 2.076E+02;
 PhotoCoeffs.IP[Si_V][3]	= 1.668E+02;,
 PhotoCoeffs.IP[Si_VI][1]	= 1.946E+03;
 PhotoCoeffs.IP[Si_VI][2]	= 2.468E+02;
 PhotoCoeffs.IP[Si_VI][3]	= 2.051E+02;,
 PhotoCoeffs.IP[Si_VII][1]	= 2.001E+03;
 PhotoCoeffs.IP[Si_VII][2]	= 2.872E+02;
 PhotoCoeffs.IP[Si_VII][3]	= 2.465E+02;,
 PhotoCoeffs.IP[Si_VIII][1]	= 2.058E+03;
 PhotoCoeffs.IP[Si_VIII][2]	= 3.310E+02;
 PhotoCoeffs.IP[Si_VIII][3]	= 3.032E+02;,
 PhotoCoeffs.IP[Si_IX][1]	= 2.125E+03;
 PhotoCoeffs.IP[Si_IX][2]	= 3.756E+02;
 PhotoCoeffs.IP[Si_IX][3]	= 3.511E+02;,
 PhotoCoeffs.IP[Si_X][1]	= 2.194E+03;
 PhotoCoeffs.IP[Si_X][2]	= 4.234E+02;
 PhotoCoeffs.IP[Si_X][3]	= 4.014E+02;,
 PhotoCoeffs.IP[Si_XI][1]	= 2.268E+03;
 PhotoCoeffs.IP[Si_XI][2]	= 4.761E+02;,
 PhotoCoeffs.IP[Si_XII][1]	= 2.336E+03;
 PhotoCoeffs.IP[Si_XII][2]	= 5.235E+02;,
 PhotoCoeffs.IP[Si_XIII][1]	= 2.438E+03;,
 PhotoCoeffs.IP[Si_XIV][1]	= 2.673E+03;, );

S_SELECT(
 PhotoCoeffs.IP[S_I][1]		= 2.477E+03;
 PhotoCoeffs.IP[S_I][2]		= 2.350E+02;
 PhotoCoeffs.IP[S_I][3]		= 1.700E+02;
 PhotoCoeffs.IP[S_I][3]		= 2.130E+01;
 PhotoCoeffs.IP[S_I][4]		= 1.036E+01;,
 PhotoCoeffs.IP[S_II][1]	= 2.478E+03;
 PhotoCoeffs.IP[S_II][2]	= 2.387E+02;
 PhotoCoeffs.IP[S_II][3]	= 1.846E+02;
 PhotoCoeffs.IP[S_II][4]	= 3.190E+01;
 PhotoCoeffs.IP[S_II][5]	= 2.333E+01;,
 PhotoCoeffs.IP[S_III][1]	= 2.486E+03;
 PhotoCoeffs.IP[S_III][2]	= 2.536E+02;
 PhotoCoeffs.IP[S_III][3]	= 1.995E+02;
 PhotoCoeffs.IP[S_III][4]	= 4.415E+01;
 PhotoCoeffs.IP[S_III][5]	= 3.483E+01;,
 PhotoCoeffs.IP[S_IV][1]	= 2.502E+03;
 PhotoCoeffs.IP[S_IV][2]	= 2.703E+02;
 PhotoCoeffs.IP[S_IV][3]	= 2.164E+02;
 PhotoCoeffs.IP[S_IV][4]	= 5.750E+01;
 PhotoCoeffs.IP[S_IV][5]	= 4.731E+01;,
 PhotoCoeffs.IP[S_V][1]		= 2.522E+03;
 PhotoCoeffs.IP[S_V][2]		= 2.888E+02;
 PhotoCoeffs.IP[S_V][3]		= 2.350E+02;
 PhotoCoeffs.IP[S_V][4]		= 7.268E+01;,
 PhotoCoeffs.IP[S_VI][1]	= 2.544E+03;
 PhotoCoeffs.IP[S_VI][2]	= 3.094E+02;
 PhotoCoeffs.IP[S_VI][3]	= 2.557E+02;
 PhotoCoeffs.IP[S_VI][4]	= 8.805E+01;,
 PhotoCoeffs.IP[S_VII][1]	= 2.569E+03;
 PhotoCoeffs.IP[S_VII][2]	= 3.321E+02;
 PhotoCoeffs.IP[S_VII][3]	= 2.809E+02;,
 PhotoCoeffs.IP[S_VIII][1]	= 2.641E+03;
 PhotoCoeffs.IP[S_VIII][2]	= 3.797E+02;
 PhotoCoeffs.IP[S_VIII][3]	= 3.282E+02;,
 PhotoCoeffs.IP[S_IX][1]	= 2.705E+03;
 PhotoCoeffs.IP[S_IX][2]	= 4.296E+02;
 PhotoCoeffs.IP[S_IX][3]	= 3.791E+02;,
 PhotoCoeffs.IP[S_X][1]		= 2.782E+03;
 PhotoCoeffs.IP[S_X][2]		= 4.804E+02;
 PhotoCoeffs.IP[S_X][3]		= 4.471E+02;,
 PhotoCoeffs.IP[S_XI][1]	= 2.859E+03;
 PhotoCoeffs.IP[S_XI][2]	= 5.346E+02;
 PhotoCoeffs.IP[S_XI][3]	= 5.048E+02;,
 PhotoCoeffs.IP[S_XII][1]	= 2.941E+03;
 PhotoCoeffs.IP[S_XII][2]	= 5.906E+02;
 PhotoCoeffs.IP[S_XII][3]	= 5.647E+02;,
 PhotoCoeffs.IP[S_XIII][1]	= 3.029E+03;
 PhotoCoeffs.IP[S_XIII][2]	= 6.517E+02;,
 PhotoCoeffs.IP[S_XIV][1]	= 3.107E+03;
 PhotoCoeffs.IP[S_XIV][2]	= 7.072E+02;,
 PhotoCoeffs.IP[S_XV][1]	= 3.224E+03;,
 PhotoCoeffs.IP[S_XVI][1]	= 3.494E+03;, );

 Fe_SELECT(
 PhotoCoeffs.IP[Fe_I][1]	= 7.124E+03;
 PhotoCoeffs.IP[Fe_I][2]	= 8.570E+02;
 PhotoCoeffs.IP[Fe_I][3]	= 7.240E+02;
 PhotoCoeffs.IP[Fe_I][4]	= 1.040E+02;
 PhotoCoeffs.IP[Fe_I][5]	= 6.600E+01;
 PhotoCoeffs.IP[Fe_I][6]	= 1.470E+01;
 PhotoCoeffs.IP[Fe_I][7]	= 7.902E+00;,
 PhotoCoeffs.IP[Fe_II][1]	= 7.140E+03;
 PhotoCoeffs.IP[Fe_II][2]	= 8.608E+02;
 PhotoCoeffs.IP[Fe_II][3]	= 7.341E+02;
 PhotoCoeffs.IP[Fe_II][4]	= 1.102E+02;
 PhotoCoeffs.IP[Fe_II][5]	= 7.617E+01;
 PhotoCoeffs.IP[Fe_II][6]	= 2.193E+01;
 PhotoCoeffs.IP[Fe_II][7]	= 1.619E+01;,
 PhotoCoeffs.IP[Fe_III][1]	= 7.155E+03;
 PhotoCoeffs.IP[Fe_III][2]	= 8.710E+02;
 PhotoCoeffs.IP[Fe_III][3]	= 7.451E+02;
 PhotoCoeffs.IP[Fe_III][4]	= 1.211E+02;
 PhotoCoeffs.IP[Fe_III][5]	= 8.705E+01;
 PhotoCoeffs.IP[Fe_III][6]	= 3.065E+01;,
 PhotoCoeffs.IP[Fe_IV][1]	= 7.169E+03;
 PhotoCoeffs.IP[Fe_IV][2]	= 8.871E+02;
 PhotoCoeffs.IP[Fe_IV][3]	= 7.669E+02;
 PhotoCoeffs.IP[Fe_IV][4]	= 1.411E+02;
 PhotoCoeffs.IP[Fe_IV][5]	= 1.067E+02;
 PhotoCoeffs.IP[Fe_IV][6]	= 5.480E+01;,
 PhotoCoeffs.IP[Fe_V][1]	= 7.184E+03;
 PhotoCoeffs.IP[Fe_V][2]	= 9.101E+02;
 PhotoCoeffs.IP[Fe_V][3]	= 7.920E+02;
 PhotoCoeffs.IP[Fe_V][4]	= 1.633E+02;
 PhotoCoeffs.IP[Fe_V][5]	= 1.288E+02;
 PhotoCoeffs.IP[Fe_V][6]	= 7.501E+01;,
 PhotoCoeffs.IP[Fe_VI][1]	= 7.199E+03;
 PhotoCoeffs.IP[Fe_VI][2]	= 9.383E+02;
 PhotoCoeffs.IP[Fe_VI][3]	= 8.202E+02;
 PhotoCoeffs.IP[Fe_VI][4]	= 1.876E+02;
 PhotoCoeffs.IP[Fe_VI][5]	= 1.527E+02;
 PhotoCoeffs.IP[Fe_VI][6]	= 9.906E+01;,
 PhotoCoeffs.IP[Fe_VII][1]	= 7.217E+03;
 PhotoCoeffs.IP[Fe_VII][2]	= 9.693E+02;
 PhotoCoeffs.IP[Fe_VII][3]	= 8.512E+02;
 PhotoCoeffs.IP[Fe_VII][4]	= 2.135E+02;
 PhotoCoeffs.IP[Fe_VII][5]	= 1.783E+02;
 PhotoCoeffs.IP[Fe_VII][6]	= 1.250E+02;,
 PhotoCoeffs.IP[Fe_VIII][1]	= 7.237E+03;
 PhotoCoeffs.IP[Fe_VIII][2]	= 1.003E+03;
 PhotoCoeffs.IP[Fe_VIII][3]	= 8.849E+02;
 PhotoCoeffs.IP[Fe_VIII][4]	= 2.409E+02;
 PhotoCoeffs.IP[Fe_VIII][5]	= 2.055E+02;
 PhotoCoeffs.IP[Fe_VIII][6]	= 1.511E+02;,
 PhotoCoeffs.IP[Fe_IX][1]	= 7.275E+03;
 PhotoCoeffs.IP[Fe_IX][2]	= 1.039E+03;
 PhotoCoeffs.IP[Fe_IX][3]	= 9.211E+02;
 PhotoCoeffs.IP[Fe_IX][4]	= 2.696E+02;
 PhotoCoeffs.IP[Fe_IX][5]	= 2.336E+02;,
 PhotoCoeffs.IP[Fe_X][1]	= 7.316E+03;
 PhotoCoeffs.IP[Fe_X][2]	= 1.076E+03;
 PhotoCoeffs.IP[Fe_X][3]	= 9.590E+02;
 PhotoCoeffs.IP[Fe_X][4]	= 2.990E+02;
 PhotoCoeffs.IP[Fe_X][5]	= 2.621E+02;,
 PhotoCoeffs.IP[Fe_XI][1]	= 7.359E+03;
 PhotoCoeffs.IP[Fe_XI][2]	= 1.115E+03;
 PhotoCoeffs.IP[Fe_XI][3]	= 9.983E+02;
 PhotoCoeffs.IP[Fe_XI][4]	= 3.292E+02;
 PhotoCoeffs.IP[Fe_XI][5]	= 2.902E+02;,
 PhotoCoeffs.IP[Fe_XII][1]	= 7.403E+03;
 PhotoCoeffs.IP[Fe_XII][2]	= 1.155E+03;
 PhotoCoeffs.IP[Fe_XII][3]	= 1.039E+03;
 PhotoCoeffs.IP[Fe_XII][4]	= 3.600E+02;
 PhotoCoeffs.IP[Fe_XII][5]	= 3.308E+02;,
 PhotoCoeffs.IP[Fe_XIII][1]	= 7.450E+03;
 PhotoCoeffs.IP[Fe_XIII][2]	= 1.197E+03;
 PhotoCoeffs.IP[Fe_XIII][3]	= 1.081E+03;
 PhotoCoeffs.IP[Fe_XIII][4]	= 3.916E+02;
 PhotoCoeffs.IP[Fe_XIII][5]	= 3.610E+02;,
 PhotoCoeffs.IP[Fe_XIV][1]	= 7.499E+03;
 PhotoCoeffs.IP[Fe_XIV][2]	= 1.240E+03;
 PhotoCoeffs.IP[Fe_XIV][3]	= 1.125E+03;
 PhotoCoeffs.IP[Fe_XIV][4]	= 4.238E+02;
 PhotoCoeffs.IP[Fe_XIV][5]	= 3.922E+02;,
 PhotoCoeffs.IP[Fe_XV][1]	= 7.553E+03;
 PhotoCoeffs.IP[Fe_XV][2]	= 1.287E+03;
 PhotoCoeffs.IP[Fe_XV][3]	= 1.181E+03;
 PhotoCoeffs.IP[Fe_XV][4]	= 4.570E+02;,
 PhotoCoeffs.IP[Fe_XVI][1]	= 7.599E+03;
 PhotoCoeffs.IP[Fe_XVI][2]	= 1.329E+03;
 PhotoCoeffs.IP[Fe_XVI][3]	= 1.216E+03;
 PhotoCoeffs.IP[Fe_XVI][4]	= 4.893E+02;,
 PhotoCoeffs.IP[Fe_XVII][1]	= 7.651E+03;
 PhotoCoeffs.IP[Fe_XVII][2]	= 1.375E+03;
 PhotoCoeffs.IP[Fe_XVII][3]	= 1.262E+03;,
 PhotoCoeffs.IP[Fe_XVIII][1]	= 7.769E+03;
 PhotoCoeffs.IP[Fe_XVIII][2]	= 1.460E+03;
 PhotoCoeffs.IP[Fe_XVIII][3]	= 1.358E+03;,
 PhotoCoeffs.IP[Fe_XIX][1]	= 7.918E+03;
 PhotoCoeffs.IP[Fe_XIX][2]	= 1.559E+03;
 PhotoCoeffs.IP[Fe_XIX][3]	= 1.456E+03;,
 PhotoCoeffs.IP[Fe_XX][1]	= 8.041E+03;
 PhotoCoeffs.IP[Fe_XX][2]	= 1.648E+03;
 PhotoCoeffs.IP[Fe_XX][3]	= 1.582E+03;,
 PhotoCoeffs.IP[Fe_XXI][1]	= 8.184E+03;
 PhotoCoeffs.IP[Fe_XXI][2]	= 1.745E+03;
 PhotoCoeffs.IP[Fe_XXI][3]	= 1.689E+03;,
 PhotoCoeffs.IP[Fe_XXII][1]	= 8.350E+03;
 PhotoCoeffs.IP[Fe_XXII][2]	= 1.847E+03;
 PhotoCoeffs.IP[Fe_XXII][3]	= 1.799E+03;,
 PhotoCoeffs.IP[Fe_XXIII][1]	= 8.484E+03;
 PhotoCoeffs.IP[Fe_XXIII][2]	= 1.950E+03;,
 PhotoCoeffs.IP[Fe_XXIV][1]	= 8.638E+03;
 PhotoCoeffs.IP[Fe_XXIV][2]	= 2.046E+03;,
 PhotoCoeffs.IP[Fe_XXV][1]	= 8.829E+03;,
 PhotoCoeffs.IP[Fe_XXVI][1]	= 9.278E+03;, );

 double eVtoHz = CONST_eV/CONST_h;
 for (nv=0; nv<NIONS; nv++)
     for (shell=1; shell<=7; shell++)
         PhotoCoeffs.IP[nv][shell] *= eVtoHz; 

}

/* ******************************************************
 * The following codes are NR recipes. They are intended to be used
 * for interpolating things, like, the HM12 spectra etc.
 * Input x and y are two pointers starting from the i=1 to n-1. It is 
 * possible to pass pointers *x1 = calloc (0,n-1, ...) as x1-1 to match
 * the index.
 * *********************************************************************** */

void spline(double x[], double y[], int n, double yp1, double ypn, double y2[])
{
	int i,k;
	double p,qn,sig,un,*u, *up;

	up	= calloc(n-1, sizeof(double));
//	u=vector(1,n-1);
	u 	= up-1;
	if (yp1 > 0.99e30)
		y2[1]=u[1]=0.0;
	else {
		y2[1] = -0.5;
		u[1]=(3.0/(x[2]-x[1]))*((y[2]-y[1])/(x[2]-x[1])-yp1);
	}
	for (i=2;i<=n-1;i++) {
		sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
		p=sig*y2[i-1]+2.0;
		y2[i]=(sig-1.0)/p;
		u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
		u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
	}
	if (ypn > 0.99e30)
		qn=un=0.0;
	else {
		qn=0.5;
		un=(3.0/(x[n]-x[n-1]))*(ypn-(y[n]-y[n-1])/(x[n]-x[n-1]));
	}
	y2[n]=(un-qn*u[n-1])/(qn*y2[n-1]+1.0);
	for (k=n-1;k>=1;k--)
		y2[k]=y2[k]*y2[k+1]+u[k];
//	free_vector(u,1,n-1);
	free(up);
}


void splint(double xa[], double ya[], double y2a[], int n, double x, double *y)
{
//	void nrerror(char error_text[]);
	int klo,khi,k;
	double h,b,a;

	klo=1;
	khi=n;
	while (khi-klo > 1) {
		k=(khi+klo) >> 1;
		if (xa[k] > x) khi=k;
		else klo=k;
	}
	h=xa[khi]-xa[klo];
	if (h == 0.0) printf("Bad xa input to routine splint\n");
	a=(xa[khi]-x)/h;
	b=(x-xa[klo])/h;
	*y=a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
}

