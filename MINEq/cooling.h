/* ############################################################
      
     FILE:     cooling.h

     PURPOSE:  contains common definitions for the 
               whole CODE

     Notice: the order is absolutely important and MUST NOT
             be changed !!!
   ############################################################ */


#define RadTran YES

#define NRAY    16
#define CONST_yr        3.1536e7

#define C_IONS  7   	/* in [1-7] */
#define N_IONS  8   	/* in [1-8] */
#define O_IONS  9   	/* in [1-9] */
#define Ne_IONS 10   	/* in [1-11] */
#define Mg_IONS 10   	/* in [1-13] */
#define Si_IONS 10  	/* in [1-10,13,15] */
#define S_IONS  10   	/* in [1-10,12,14,16,17] */
#define Fe_IONS 10   	/* in [1-10,15,20,23,27] */

#if C_IONS == 0
 #define C_EXPAND(a,b,c,d,e,f,g) 
 #define C_SELECT(a,b,c,d,e,f,g)
#elif C_IONS == 1       
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a
 #define C_SELECT(a,b,c,d,e,f,g)   a
#elif C_IONS == 2     
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b
 #define C_SELECT(a,b,c,d,e,f,g)   a b
#elif C_IONS == 3       
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c
 #define C_SELECT(a,b,c,d,e,f,g)   a b c
#elif C_IONS == 4
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d
 #define C_SELECT(a,b,c,d,e,f,g)   a b c d
#elif C_IONS == 5      
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d,e
 #define C_SELECT(a,b,c,d,e,f,g)   a b c d e
#elif C_IONS == 6      
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d,e,f
 #define C_SELECT(a,b,c,d,e,f,g)   a b c d e f
#elif C_IONS == 7
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d,e,f,g
 #define C_SELECT(a,b,c,d,e,f,g)   a b c d e f g
#endif

#if N_IONS == 0
 #define N_EXPAND(a,b,c,d,e,f,g,h) 
 #define N_SELECT(a,b,c,d,e,f,g,h)
#elif N_IONS == 1      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a
 #define N_SELECT(a,b,c,d,e,f,g,h)   a
#elif N_IONS == 2     
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b
#elif N_IONS == 3       
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c 
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b c
#elif N_IONS == 4
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b c d
#elif N_IONS == 5      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b c d e
#elif N_IONS == 6      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e,f
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b c d e f
#elif N_IONS == 7      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e,f,g
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b c d e f g
#elif N_IONS == 8
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e,f,g,h
 #define N_SELECT(a,b,c,d,e,f,g,h)   a b c d e f g h
#endif

#if O_IONS == 0
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9)
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)
#elif O_IONS == 1      
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1
#elif O_IONS == 2     
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2
#elif O_IONS == 3       
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3
#elif O_IONS == 4
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3 x4
#elif O_IONS == 5      
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3 x4 x5
#elif O_IONS == 6      
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5,x6
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3 x4 x5 x6
#elif O_IONS == 7
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5,x6,x7
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3 x4 x5 x6 x7
#elif O_IONS == 8
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5,x6,x7,x8
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3 x4 x5 x6 x7 x8
#elif O_IONS == 9
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
 #define O_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9)  x1 x2 x3 x4 x5 x6 x7 x8 x9
#endif

#if Ne_IONS == 0
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) 
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) 
#elif Ne_IONS == 1      
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1
#elif Ne_IONS == 2     
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2
#elif Ne_IONS == 3       
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3
#elif Ne_IONS == 4
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4
#elif Ne_IONS == 5      
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5
#elif Ne_IONS == 6
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5 x6
#elif Ne_IONS == 7
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5 x6 x7
#elif Ne_IONS == 8
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7,x8
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5 x6 x7 x8
#elif Ne_IONS == 9
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5 x6 x7 x8 x9
#elif Ne_IONS == 10
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10
#elif Ne_IONS == 11
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11
 #define Ne_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11
#endif

#if Mg_IONS == 0
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)
#elif Mg_IONS == 1      
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1
#elif Mg_IONS == 2     
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2
#elif Mg_IONS == 3       
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3
#elif Mg_IONS == 4
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4
#elif Mg_IONS == 5      
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5
#elif Mg_IONS == 6
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6
#elif Mg_IONS == 7
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7
#elif Mg_IONS == 8
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7 x8
#elif Mg_IONS == 9
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7 x8 x9
#elif Mg_IONS == 10
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10
#elif Mg_IONS == 11
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11
#elif Mg_IONS == 12
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12
#elif Mg_IONS == 13
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13
 #define Mg_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13
#endif


#if Si_IONS == 1      
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1
#elif Si_IONS == 2     
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2
#elif Si_IONS == 3       
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3
#elif Si_IONS == 4
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4
#elif Si_IONS == 5      
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5
#elif Si_IONS == 6
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6
#elif Si_IONS == 7
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6 x7
#elif Si_IONS == 8
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6 x7 x8
#elif Si_IONS == 9
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6 x7 x8 x9
#elif Si_IONS == 10
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10
#elif Si_IONS == 13
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13
#elif Si_IONS == 15
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15
 #define Si_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15
#endif

#if S_IONS == 1      
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1
#elif S_IONS == 2     
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2
#elif S_IONS == 3       
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3
#elif S_IONS == 4
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4
#elif S_IONS == 5      
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5
#elif S_IONS == 6
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6
#elif S_IONS == 7
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7
#elif S_IONS == 8
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8
#elif S_IONS == 9
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8 x9
#elif S_IONS == 10
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10
#elif S_IONS == 12
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12
#elif S_IONS == 14
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
#elif S_IONS == 16
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16
#elif S_IONS == 17
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17
 #define S_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17
#endif


#if Fe_IONS == 0
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)
#elif Fe_IONS == 1      
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1
#elif Fe_IONS == 2     
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2
#elif Fe_IONS == 3       
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3
#elif Fe_IONS == 4       
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4
#elif Fe_IONS == 5
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5
#elif Fe_IONS == 6
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6
#elif Fe_IONS == 7
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7
#elif Fe_IONS == 8
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8
#elif Fe_IONS == 9
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8 x9
#elif Fe_IONS == 10
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10
#elif Fe_IONS == 15
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15
#elif Fe_IONS == 20
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17 x18 x19 x20
#elif Fe_IONS == 23
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17 x18 x19 x20 x21 x22 x23
#elif Fe_IONS == 27
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27
 #define Fe_SELECT(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27)  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17 x18 x19 x20 x21 x22 x23 x24 x25 x26 x27
#endif

/* ****************************************************************
  Ions are labeled progressively, depending on how many ionization 
  stages are effectively included in the network through the previous 
  X_EXPAND macros. 
  Elements are ordered as {H, He, C, N, O, Ne, S, Fe} and must be 
  carefully respected everywhere in the code. 
  Hydrogen and Helium are always included.
  **************************************************************** */

enum {
  HI = NFLX, HeI, HeII, HeIII
  C_EXPAND(CI, CII, CIII, CIV, CV, CVI, CVII)
  N_EXPAND(NI, NII, NIII, NIV, NV, NVI, NVII, NVIII)
  O_EXPAND(OI, OII, OIII, OIV, OV, OVI, OVII, OVIII, OIX)
  Ne_EXPAND(NeI, NeII, NeIII, NeIV, NeV, NeVI, NeVII, NeVIII, NeIX, NeX, NeXI)
  Mg_EXPAND(MgI, MgII, MgIII, MgIV, MgV, MgVI, MgVII, MgVIII, MgIX, MgX, MgXI, MgXII, MgXIII)
  Si_EXPAND(SiI, SiII, SiIII, SiIV, SiV, SiVI, SiVII, SiVIII, SiIX, SiX, SiXI, SiXII, SiXIII, SiXIV, SiXV)
  S_EXPAND(SI, SII, SIII, SIV, SV, SVI, SVII, SVIII, SIX, SX, SXI, SXII, SXIII, SXIV, SXV, SXVI, SXVII)
  Fe_EXPAND(FeI, FeII, FeIII, FeIV, FeV, FeVI, FeVII, FeVIII, FeIX, FeX, FeXI, FeXII, FeXIII, FeXIV, FeXV, FeXVI, FeXVII, FeXVIII, FeXIX, FeXX, FeXXI, FeXXII, FeXXIII, FeXXIV, FeXXV, FeXXVI, FeXXVII)
};

#define NIONS  (4+C_IONS+N_IONS+O_IONS+Ne_IONS+Mg_IONS+Si_IONS+S_IONS+Fe_IONS)

real GetMaxRate          (double *, double *, double);
real MeanMolecularWeight (double *);
double H_MassFrac (void);
real CompEquil            (double, double, double *);
real find_N_rho ();
void Radiat (double *, double *);

void CHECK_NORMALIZATION (double *, char *);
void NORMALIZE_IONS (double *);
/*
int Ros4_expl, Ros4_impl, Ros4_sup_dt;
*/

void get_ion(int, int *, int *);
int g_include_pi;		//kartick





