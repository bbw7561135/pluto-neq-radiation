#include "pluto.h"
#include "cooling_defs.h"

PHOTO_COEFF PhotoCoeffs;

/* ************************************************ */
void Solve_System (Ion *X, double Ne, double T)
/*
 *
 *  Solve sytem  A*x = rhs
 *  adapted from the GSL library
 *  This is used to compute the level populations
 *  for the ions.
 *
 ************************************************** */
{
  int    i, j, k, nlev;
  double scrh,tmpx, d;
  double **q;
  double **M, *rhs;
  int *p;  
  
  nlev = X->nlev;
  M    = ARRAY_2D(nlev, nlev, double);
  rhs  = ARRAY_1D(nlev, double);
  p    = ARRAY_1D(nlev, int);
  q    = ARRAY_2D (nlev, nlev, double);

  for (i = 0; i < nlev; i++)  {
    rhs[i] = 0.0;
    p[i] = 0.0;
    for (j = 0; j < nlev; j++)  {
      M[i][j] = 0.0;
      q[i][j] = 0.0;
    }
  }

/* -------------------------------------------
                Define qij
   -------------------------------------------  */

  for (i = 0; i < nlev; i++) { 
  for (j = i + 1; j < nlev; j++)  {
    if ( (!X->isMAP) && (!X->isCV) && (!X->isH) && (!X->isCHEB) )
      scrh = lagrange(X->Tom, X->omega[i][j], T, X->nTom, i, j); /* remove i and j after testing */
    if (X->isCV)  scrh = X->omega[i][j][0] * pow( (T / pow(10.,X->omega[i][j][2])), X->omega[i][j][1]);
    if (X->isMAP) scrh = X->omega[i][j][0] * pow( T/10000., X->omega[i][j][1]);
    if (X->isH)  {
      if (T<55000) scrh = X->omega[i][j][0] + T*X->omega[i][j][1] + T*T*X->omega[i][j][2] + T*T*T*X->omega[i][j][3];
      else scrh = X->omega[i][j][4] + T*X->omega[i][j][5] + T*T*X->omega[i][j][6] + T*T*T*X->omega[i][j][7];
    }
    if (X->isCHEB) {
      tmpx = 0.6199646 * log(T) - 6.2803580;
      scrh = 0.5 * X->omega[i][j][0] + X->omega[i][j][1] * tmpx + X->omega[i][j][2] * (2.*tmpx*tmpx - 1) + X->omega[i][j][3] * (4.*tmpx*tmpx*tmpx - 3*tmpx);
      scrh = exp(scrh);
    }

    q[j][i]  = scrh/X->wght[j];
    q[j][i] *= 8.629e-6/sqrt(T);
    q[i][j]  = (X->wght[j]/X->wght[i])*q[j][i]*exp( - X->dE[i][j]/(kB*T)); 
  }}               
  for (i = 0; i < nlev; i++) q[i][i] = 0.0;

/* -----------------------------------------
       compute coefficient matrix 
   -----------------------------------------    */

  for (i = 0 ; i < nlev ; i++) {
  for (j = 0 ; j < nlev ; j++) {
    scrh = 0.0; 
    if (j == i) {
      for (k = 0 ; k < nlev ; k++) {
          if (k != i) scrh += Ne*q[i][k];
          if (k < i)  scrh += X->A[i][k];
      }
      M[i][j] = -scrh;
    }
    if (j < i) M[i][j] = Ne*q[j][i];
    if (j > i) M[i][j] = Ne*q[j][i] + X->A[j][i];
  }}

/*  -------------------------------------
     Replace 1st eq. with normalization
     condition and define rhs 
    -------------------------------------  */

  for (j = 0; j < nlev; j++) {
    M[nlev-1][j] = 1.0;
    rhs[j] = 0.0;
  } 
  rhs[nlev-1] = 1.0;

/* ----------------------------------
    Solve equations by LU decomp
   ---------------------------------- */
  LUDecomp (M, nlev, p, &d);
  LUBackSubst (M, nlev, p, rhs);
  for (i = 0 ; i < nlev ; i++) X->Ni[i] = rhs[i];
}

/* ******************************************* */
void Symmetrize_Coeff (Ion *X)
/*
 *
 *
 * 
 ********************************************* */
{
  int i,j,n;
  
/*  -------------------------------------------------- 
         symmetrize dE, A, and Omega   
         in order to prevent swapped index in the
         ion routines  
    -------------------------------------------------- */

  for (i = 0; i < X->nlev; i++) {
  for (j = 0; j < X->nlev; j++) {
    if (X->dE[i][j] > 0.0) {
      X->dE[j][i] = X->dE[i][j];  
      X->A[j][i]  = X->A[i][j];     
      for (n = 0; n < X->nTom; n++){          
        if ( (X->omega[i][j][n] > 0.0) || (X->isH && X->omega[i][j][n] != 0.0) || (X->isCHEB && X->omega[i][j][n] != 0.0) )
          X->omega[j][i][n]  = X->omega[i][j][n];
      }
    }
  }}

/* ----------------------------------------------------
    2. convert energy to correct units
   ---------------------------------------------------- */

  for (i = 0; i < X->nlev; i++){ 
  for (j = 0; j < X->nlev; j++) {
    if (X->dE[i][j] < 0.0) { X->dE[i][j] = 0.0; X->A[i][j] = 0.0; for (n = 0; n < X->nTom; n++) X->omega[i][j][n] = 0.000; }
    if (X->A[i][j] < 0.0) { X->A[i][j] = 0.0; for (n = 0; n < X->nTom; n++) X->omega[i][j][n] = 0.000; }
    if (X->dE[i][j] > 0.0) X->dE[i][j] = 1.2399e-4/(1.e-8*X->dE[i][j])  ;  /* multply by hc to get energy  */
    if ( (X->omega[i][j][0] < 0.0)&&(!X->isCV)&&(!X->isH)&&(!X->isCHEB)&&(!X->isMAP) ) for (n = 0; n < X->nTom; n++) X->omega[i][j][n] = 0.000; 
  }}


/*  -------------------------------------------------- 
      3. set A(i,j) = 0  when j >= i
    -------------------------------------------------- */

  for (i = 0; i < X->nlev; i++) {
  for (j = i; j < X->nlev; j++) {
    X->A[i][j] = 0.0;                     
  }}

/*  -------------------------------------------------- 
      4. set diagonal elements of dE and omega to 
         zero
    -------------------------------------------------- */

  for (i = 0; i < X->nlev; i++) {
    for (n = 0; n < X->nTom; n++) X->omega[i][i][n] = 0.000;
    X->dE[i][i]    = 0.0;
    X->A[i][i]     = 0.0;
  }

}

/* ******************************************************** */
double lagrange (double *x, double *y, double xp, int n, int ii, int jj)
/*
 * PURPOSE:
 *
 *   Return lagrange interpolation for a tabulated function.
 *
 *  x : a vector of n-components with the tabulated values
 *      of the independent variables x;
 *
 *  y : a vecotr of n-components with the tabulated values
 *      y[n] = y(x[n]);
 *
 * xp: the point at which interpolation is desired
 *
 * n : the number of points in the table, also
 *     the degree of the intrpolant
 *
 *
 ********************************************************** */
{
  int    i, k, j;
  double scrh, yp;

  yp = 0.0;
  if (xp < x[0])    return (y[0]);
  if (xp > x[n-1])  return (y[n-1]);

  for (i = 0; i < n; i++){
    scrh = 1.0;
    for (k = 0; k < n; k++){
      if (k == i) continue;
      scrh *= (xp - x[k])/(x[i] - x[k]);
    }
    yp += scrh*y[i];
  }
  return(yp);
}

/* *************************************************************** */
int Create_Ion_Coeff_Tables(double ***tbl)
/*
 *  PURPOSE: Compute and save to memory the coefficients depending
 *           only on temperature used at runtime to compute the
 *           ionization balance.
 *
 ***************************************************************** */
#define ZERO_5X  0.0, 0.0, 0.0, 0.0, 0.0
{
  double T, t4, tmprec, lam, f1, f2, x1, x2, P, Q, F[2], Tn[9];

  long int nv, i, j, ti1, ti2, cindex;
  
  print1("> MINeq: creating ionization coefficients tables (No-PIR)...\n");

  /* -- generate table -- */

  double ttt, rrr[7], rt1, rt2;
  int el_id, ion_id, pi_r;
  char *msg;

  for (i = 0; i < I_g_stepNumber; i++){
    ttt = pow(10., I_TBEG+I_TSTEP*i*1.0);
    for (nv = 0; nv < NIONS; nv++) {
      get_ion(nv, &el_id, &ion_id);             // get the ion id from nv
      getrate_(&ttt, &el_id, &ion_id, rrr);     // get the reaction rates from fcn2.f

      tbl[i][0][nv] = MAX(rrr[0], 1e-40); // \xi(coll)_{k,ion}
      tbl[i][1][nv] = MAX(rrr[1], 1e-40); // \alpha(rr)_{k, ion+1}
      tbl[i][2][nv] = 1.e-40; 		  // \alpha(dr)_{k, ion+1}
      tbl[i][3][nv] = MAX(rrr[2], 1e-40); // \xi(ct-HII)_{k,ion}
      tbl[i][4][nv] = MAX(rrr[3], 1e-40); // \alpha(ct-HI)_{k, ion+1}
      tbl[i][5][nv] = MAX(rrr[4], 1e-40); // \alpha(ct-HeI)_{k, ion+1}
      tbl[i][6][nv] = MAX(rrr[5], 1e-40); // \xi(ct-HeII)_{k,ion}
    }

    for (j = 0; j < 7; j++) {
      tbl[i][j][HeI + 3-1-NFLX]	      = 0.0;	// kartick	
      tbl[i][j][CI  +  C_IONS-1-NFLX] = 0.0;
      tbl[i][j][NI  +  N_IONS-1-NFLX] = 0.0;
      tbl[i][j][OI  +  O_IONS-1-NFLX] = 0.0;
      tbl[i][j][NeI + Ne_IONS-1-NFLX] = 0.0;
      tbl[i][j][MgI + Mg_IONS-1-NFLX] = 0.0;
      tbl[i][j][SiI + Si_IONS-1-NFLX] = 0.0;
      tbl[i][j][SI  +  S_IONS-1-NFLX] = 0.0;
      tbl[i][j][FeI + Fe_IONS-1-NFLX] = 0.0;
    }
  }
  print1("  * Done.\n");
  
  return 0;
}
#undef ZERO_5X

