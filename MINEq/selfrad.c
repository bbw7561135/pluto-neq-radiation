
#include "pluto.h"
//#include "locals.h"

#if COOLING == MINEq
 #include "cooling_defs.h"
#endif

COOL_COEFF CoolCoeffs;
PHOTO_COEFF PhotoCoeffs;
SELF_SPEC SelfSpec;
DUST Dust;

void DoRadiativeTransfer(const Data *d, Grid *grid)
{
  /* definitions for self spectra */
  int tindx, nu;
  double nH1, logT;
  clock_t  s_time, e_time, c_time;
  char warning_str[200];

//  ReadOpacityTables();    // has been moved to Find_Rates()

  int nv, n, Nnu, i, j, k, nghost;
  double *x, *dx, *xl, *xr, **alpha;
  static double *r, *dr, *dV, *mu, *dmu;
  static double **aM, **bM, **dM, **fM, **RM;
  static double *A, *Abar, *B, *Bbar, *C, *Cbar, aMp, bMp, dMp, fMp, RMp;
  double Jnu, Jnu1, Fnu, Fnu1, dmup, radforce, alpha_scat, alpha_abs, alpha_pr;
  double psi0, theta_p, len, mu_crit, mu_0;
  int nlo, nhi;
  static double *additional_source, *Jnu_at_outer_boundary, mu_H;

  x  = grid[IDIR].x;
  r  = grid[IDIR].xl;
  dx = grid[IDIR].dx;
  nghost = grid[IDIR].nghost;
  j = JBEG; k= KBEG;
  Nnu = PhotoCoeffs.Nnu; 

  if (dV == NULL){
      dV  = calloc (NX1_TOT, sizeof(double));
      dr  = calloc (NX1_TOT, sizeof(double));
      mu  = calloc(NRAY, sizeof(double));
      dmu = calloc(NRAY, sizeof(double));
      aM  = ARRAY_2D(NX1_TOT, NRAY, double);
      bM  = ARRAY_2D(NX1_TOT, NRAY, double);
      dM  = ARRAY_2D(NX1_TOT, NRAY, double);
      fM  = ARRAY_2D(NX1_TOT, NRAY, double);
      A	    = ARRAY_1D(NRAY, double);
      Abar  = ARRAY_1D(NRAY, double);
      B     = ARRAY_1D(NX1_TOT, double);
      Bbar  = ARRAY_1D(NX1_TOT, double);
      C     = ARRAY_1D(NX1_TOT, double);
      Cbar  = ARRAY_1D(NX1_TOT, double);
      additional_source  = ARRAY_1D(1000, double);
      Jnu_at_outer_boundary  = ARRAY_1D(1000, double);

      mu_H = 1./H_MassFrac();

      for (i=0; i<NX1_TOT-1; i++) dr[i] = r[i+1]-r[i];

      mu[0] = -1.0;
      dmup  = 2./((NRAY-1)*1.0);
      for (n=1;n<NRAY;n++) mu[n] = -1.+ n*dmup;

      for (n=0; n<NRAY-1; n++){
          dmu[n]     = mu[n+1]-mu[n];
          A[n]       = dmu[n]/6.*(mu[n+1]+2*mu[n]);
          Abar[n]    = -dmu[n]/6.*(mu[n]+2*mu[n+1]);
      }
      for (i=0; i<=NX1_TOT-2;i++){
          B[i]     = 1./(12.*dr[i])*(pow(r[i+1],4.0) - 4.*pow(r[i],3.0)*r[i+1]+3*pow(r[i],4.0));
          Bbar[i]  = 1./(12.*dr[i])*(3.*pow(r[i+1],4.0) - 4*r[i]*pow(r[i+1],3.0) + pow(r[i],4.0));
          C[i]     = dr[i]/6.*( r[i+1] + 2.*r[i] );
          Cbar[i]  = dr[i]/6.*( 2*r[i+1] + r[i] );
          dV[i]    = 1./3.*( pow(r[i+1],3.0)-pow(r[i],3.0) ); // This is actuall dV/(4\pi)
      }

      for (i=0; i<NX1_TOT-1; i++){
      for (n=0; n<NRAY-1; n++){
//      for (nu=0; nu<Nnu-1; nu++){
           aM[i][n] = -A[n]*r[i]*r[i] - (1.-mu[n]*mu[n])*C[i]; 
           bM[i][n] = A[n]*r[i+1]*r[i+1] - (1-mu[n]*mu[n])*Cbar[i];
           dM[i][n] = Abar[n]*r[i]*r[i] + (1-mu[n+1]*mu[n+1])*C[i];
           fM[i][n] = - Abar[n]*r[i+1]*r[i+1] + (1-mu[n+1]*mu[n+1])*Cbar[i];
//      }
      }}

      /* -------------- Additional source -----------------------------------------
       * Set whatever additional source is wanted at the centre of the spehere. 
       * This is constant. If a time varying source is wanted, take this line
       * out of the dV==NULL loop so that it can run at every-time. 
       * --------------------------------------------------------------------------- */
      for (nu=0; nu<Nnu-1; nu++){
	 additional_source[nu] = 0.0; //1e5*PhotoCoeffs.UVB_Jnu[nu];
	 Jnu_at_outer_boundary[nu] = PhotoCoeffs.UVB_Jnu[nu];
					  //since the boundary is very close to
					  //the final shock location, we set this
					  //to 0. In a larger box, the most of the 
					  //radiation does not penetrate more than
					  //few pc at nH=1. 
      }
  }

 /* Calculate opacity in non-eq fashion */
 static int first_time = 1;
 double nX, N;
 static double N_rho;
 double mu0, T0, T1, mu1;
 double v0[NVAR], dust_frac;

 if (first_time){ N_rho = find_N_rho(); first_time=0;}

 FILE *ftrans;
 char specfile[50];
 if (g_stepNumber==0){ /* printing out an example of transport */
   int errdir = mkdir("rad-trans/",0770);
   sprintf(specfile,"rad-trans/proc-%d.out",prank);
   ftrans = fopen (specfile, "w");
   fprintf(ftrans, "# r(pc)  nH1    mu    Temp(K)   opac_ions[9](/cm)  opac_ext[9]   opac[20]  eps_nu[1]  eps_nu[9]  eps_nu[20]\n");
 }

// if (prank==0) s_time = clock();

 for (i=0; i<NX1_TOT; i++){
   for (nv = 0; nv < NVAR; nv++)  v0[nv] = d->Vc[nv][k][j][i];

   N = v0[RHO] * N_rho;
   SelfSpec.nH1[k][j][i] = v0[RHO]/(mu_H*CONST_mp);
   SelfSpec.mu[k][j][i]   = MeanMolecularWeight(v0);
   SelfSpec.Temp[k][j][i] = v0[PRS]/v0[RHO]*KELVIN*SelfSpec.mu[k][j][i]; 
   dust_frac		= v0[TRC]*v0[TRC]/(standard_dust_size*standard_dust_size);

   for (nu=0; nu<Nnu-1; nu++){
     SelfSpec.opac_ions[k][j][i][nu] = 0.0;	
     for (nv=0; nv<NIONS; nv++){
	nX	= MAX(N * elem_ab[elem_part[nv]] * v0[nv+NFLX], 0.0);
	SelfSpec.opac_ions[k][j][i][nu]	+= nX * PhotoCoeffs.shellintg_Sigma[nv][nu];
     }
     SelfSpec.opac_dust_ext[k][j][i][nu] = SelfSpec.nH1[k][j][i]*Dust.sigma_ext[nu]*dust_frac;

     SelfSpec.opac[k][j][i][nu] = SelfSpec.opac_ions[k][j][i][nu] 
				+ SelfSpec.opac_dust_ext[k][j][i][nu];
     SelfSpec.opac[k][j][i][nu] = MAX(1.e-40, SelfSpec.opac[k][j][i][nu]);

/*     SelfSpec.eps_nu[k][j][i][nu] = SelfSpec.nH1[k][j][i]*SelfSpec.nH1[k][j][i]
			*eps_bynHnH(nu, SelfSpec.nH1[k][j][i], SelfSpec.Temp[k][j][i]);
*/
     SelfSpec.eps_nu[k][j][i][nu] = eps_cloudy(nu, SelfSpec.nH1[k][j][i], SelfSpec.Temp[k][j][i]);
     SelfSpec.eps_nu[k][j][i][nu] /= (4.*CONST_PI);
   }

   if (g_stepNumber==0) 
     fprintf(ftrans, "%2.4e  %2.4e  %2.4e  %2.4e  %2.4e  %2.4e  %2.4e  %2.4e  %2.4e  %2.4e\n",
         x[i]/CONST_pc, SelfSpec.nH1[k][j][i], SelfSpec.mu[k][j][i],
	 SelfSpec.Temp[k][j][i], SelfSpec.opac_ions[k][j][i][9], SelfSpec.opac_dust_ext[k][j][i][9], 
	 SelfSpec.opac[k][j][i][9], SelfSpec.eps_nu[k][j][i][1], SelfSpec.eps_nu[k][j][i][9],
	 SelfSpec.eps_nu[k][j][i][20]);

 } /* ends loop on k,j,i */
 /* Non-equilibrium opacity calculation completed  */


 /* ------------------ Starting integration of the RTE ---------------- ------- */
 alpha = SelfSpec.opac[k][j];

 /* Propagating rays radially inwards. Here I_i means the at the boundary between
  * r_i and r_{i+1} cells. When calculating a cell averaged J_i, we should take an
  * average of I_i and I_{i-1} */

#ifdef PARALLEL
 MPI_Barrier(MPI_COMM_WORLD);
 
 if (grid[IDIR].rbound){
   for (nu=0; nu<Nnu-1; nu++)
     for (n=0; n<NRAY/2; n++)
       PhotoCoeffs.psi[NX1_TOT-1][nu][n] = Jnu_at_outer_boundary[nu]; //PhotoCoeffs.UVB_Jnu[nu];
 }else{
   for (nu=0; nu<Nnu-1; nu++)
     MPI_Recv(PhotoCoeffs.psi[NX1_TOT-1][nu], NRAY/2, MPI_DOUBLE, prank+1, nu, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//     print("Inu_m data received from proc %d at proc %d.\n", prank+1, prank);
 }

 #else
 for (nu=0; nu<Nnu-1; nu++)
   for (n=0; n<NRAY/2; n++)
     PhotoCoeffs.psi[NX1_TOT-1][nu][n] = Jnu_at_outer_boundary[nu]; //PhotoCoeffs.UVB_Jnu[nu];
 #endif

 /* Solving for \mu=-1 ray */
  for (i=NX1_TOT-2; i>=0; i--)
    for (nu=0; nu<Nnu-1; nu++)
     PhotoCoeffs.psi[i][nu][0] = PhotoCoeffs.psi[i+1][nu][0]*exp(-alpha[i][nu]*dr[i]) 
	         + SelfSpec.eps_nu[k][j][i][nu]/alpha[i][nu]*(1.0-exp(-alpha[i][nu]*dr[i]));

 /* Now colving for other \mu<0 rays */
 for (nu=0; nu<Nnu-1; nu++){
   for (n=1; n<NRAY/2; n++){
     for (i=NX1_TOT-2; i>=0; i--){
	aMp = aM[i][n-1] + alpha[i][nu]*dmu[n-1]/2.*B[i];
	bMp = bM[i][n-1] + alpha[i][nu]*dmu[n-1]/2.*Bbar[i];
	dMp = dM[i][n-1] + alpha[i][nu]*dmu[n-1]/2.*B[i];
	fMp = fM[i][n-1] + alpha[i][nu]*dmu[n-1]/2.*Bbar[i];
	RMp = SelfSpec.eps_nu[k][j][i][nu]*dmu[n-1]*dV[i];
        PhotoCoeffs.psi[i][nu][n] = (RMp-(aMp*PhotoCoeffs.psi[i][nu][n-1] +
				bMp*PhotoCoeffs.psi[i+1][nu][n-1] +
				fMp*PhotoCoeffs.psi[i+1][nu][n])) / dMp;

	if (PhotoCoeffs.psi[i][nu][n] < 0.0){
	    mu_0 = -sqrt(1. - pow(r[i]/r[i+1],2.0)*(1.-mu[n]*mu[n]));
	    nlo = (int) (floor((mu_0+1.)/dmup));
	    nhi	= nlo+1;
	    psi0 = 1./dmu[nlo]*(PhotoCoeffs.psi[i+1][nu][nlo]*(mu[nhi]-mu_0) 
			    + PhotoCoeffs.psi[i+1][nu][nhi]*(mu_0-mu[nlo]));
	    theta_p = acos(fabs(mu[n]))-acos(fabs(mu_0));
	    len = r[i]*sin(theta_p)/sqrt(1.-mu_0*mu_0);
	    PhotoCoeffs.psi[i][nu][n] = psi0*exp(-alpha[i][nu]*len) 
		    	+ SelfSpec.eps_nu[k][j][i][nu]/alpha[i][nu]*(1.0-exp(-alpha[i][nu]*len));
	}

	/* psi should not be negative, but still ... */
	PhotoCoeffs.psi[i][nu][n] = MAX(PhotoCoeffs.psi[i][nu][n], 0.0);

     }
   }
 }

 #ifdef PARALLEL
 if (!grid[IDIR].lbound){
   for (nu=0; nu<Nnu-1; nu++)
     MPI_Send(PhotoCoeffs.psi[IBEG-1+nghost][nu], NRAY/2, MPI_DOUBLE, prank-1, nu, MPI_COMM_WORLD); 
 }
 #endif

 /* Applying the spherical symmetry condition at r = 0 + a possible source term at centre */
 #ifdef PARALLEL
 if (grid[IDIR].lbound){
   for (nu=0; nu<Nnu-1; nu++)
     for (n=NRAY/2; n<NRAY; n++)
       PhotoCoeffs.psi[0][nu][n] = PhotoCoeffs.psi[0][nu][NRAY-1-n] + additional_source[nu];
 }else{
   for (nu=0; nu<Nnu-1; nu++)
     MPI_Recv((PhotoCoeffs.psi[0][nu]+NRAY/2), NRAY/2, MPI_DOUBLE, prank-1,1000*nu, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
 }
 #else
 for (nu=0; nu<Nnu-1; nu++)
   for (n=NRAY/2; n<NRAY; n++)
       PhotoCoeffs.psi[0][nu][n] = PhotoCoeffs.psi[0][nu][NRAY-1-n] + additional_source[nu];
 #endif

 /* Solving for the mu>0 rays */
 for (nu=0; nu<Nnu-1; nu++)
   for (n=NRAY/2; n<NRAY; n++){
     for (i=1; i<NX1_TOT; i++){
	aMp = aM[i-1][n-1] + alpha[i-1][nu]*dmu[n-1]/2.*B[i-1];
	bMp = bM[i-1][n-1] + alpha[i-1][nu]*dmu[n-1]/2.*Bbar[i-1];
	dMp = dM[i-1][n-1] + alpha[i-1][nu]*dmu[n-1]/2.*B[i-1];
	fMp = fM[i-1][n-1] + alpha[i-1][nu]*dmu[n-1]/2.*Bbar[i-1];
	RMp = SelfSpec.eps_nu[k][j][i-1][nu]*dmu[n-1]*dV[i-1];
        PhotoCoeffs.psi[i][nu][n] = (RMp-(aMp*PhotoCoeffs.psi[i-1][nu][n-1]
				+ bMp*PhotoCoeffs.psi[i][nu][n-1]
				+ dMp*PhotoCoeffs.psi[i-1][nu][n]))/fMp;

	/* For the rays that do not pass thourgh another cell */
	mu_crit = sqrt(1.-pow(r[i-1]/r[i],2.0));
        if (mu[n] < mu_crit){
            PhotoCoeffs.psi[i][nu][n] = PhotoCoeffs.psi[i][nu][NRAY-n-1]*exp(-alpha[i-1][nu]*2*r[i]*mu[n])
		          + SelfSpec.eps_nu[k][j][i-1][nu]/alpha[i-1][nu]*(1.-exp(-alpha[i-1][nu]*2*r[i]*mu[n]));
        }

	if (PhotoCoeffs.psi[i][nu][n]<0.0 & n<NRAY-1){
	  mu_0 = sqrt(1. - pow(r[i]/r[i-1],2.0)*(1.-mu[n]*mu[n]));
	  nlo  = (int) (floor((mu_0+1.)/dmup));
	  nhi  = nlo+1;
	  psi0 = 1./dmu[nlo]*(PhotoCoeffs.psi[i-1][nu][nlo]*(mu[nhi]-mu_0) 
			  + PhotoCoeffs.psi[i-1][nu][nhi]*(mu_0-mu[nlo]));
	  theta_p = acos(fabs(mu_0))-acos(fabs(mu[n]));
	  len = r[i]*sin(theta_p)/sqrt(1.-mu_0*mu_0);
	  PhotoCoeffs.psi[i][nu][n] = psi0*exp(-alpha[i-1][nu]*len)
		  + SelfSpec.eps_nu[k][j][i-1][nu]/alpha[i-1][nu]*(1.0-exp(-alpha[i-1][nu]*len));
	}else if (PhotoCoeffs.psi[i][nu][n]<0.0 & n==NRAY-1){
          PhotoCoeffs.psi[i][nu][n] = PhotoCoeffs.psi[i-1][nu][n]*exp(-alpha[i-1][nu]*dr[i-1])
                  + SelfSpec.eps_nu[k][j][i-1][nu]/alpha[i-1][nu]*(1.0-exp(-alpha[i-1][nu]*dr[i-1]));
	}else{};

	/* psi should not be negative, but still ... */
	PhotoCoeffs.psi[i][nu][n] = MAX(PhotoCoeffs.psi[i][nu][n], 0.0);
     }
 } /* end of outward integration */

 #ifdef PARALLEL
 if (!grid[IDIR].rbound){
   for (nu=0; nu<Nnu-1; nu++)
     MPI_Send((PhotoCoeffs.psi[IEND+1-nghost][nu]+NRAY/2), NRAY/2, MPI_DOUBLE, prank+1, 1000*nu, MPI_COMM_WORLD);
 }

 MPI_Barrier(MPI_COMM_WORLD);
 #endif

/*
if (prank==0){
 e_time = clock();
 print("step=%ld, time taken for solving RTE = %2.6f\n", g_stepNumber,
		 ((double)(e_time - s_time) / CLOCKS_PER_SEC));
}
*/

 /* Calculating the angle averaged intensity and others */
 if (g_stepNumber == 0)
   fprintf(ftrans, "\n\n\n # Transport examples\n # r(pc)   psi[7][0]   psi[9][0]   psi[7][NRAY-1] psi[9][NRAY-1] Jnu[i][9]   Fnu[i][9]\n");

 double nphot;

// IDOM_LOOP(i){
 ITOT_LOOP(i){
   radforce = 0.0;
   SelfSpec.N_totphot[i] = SelfSpec.N_uvH[i] = SelfSpec.N_uvHe[i] = 0.0;
   for (nu=0; nu<Nnu-1; nu++){
     Jnu = Jnu1 = 0.0;
     Fnu = Fnu1 = 0.0;
     for (n=0; n<NRAY-1; n++){
       Jnu  += dmu[n]/4.*(PhotoCoeffs.psi[i][nu][n]+PhotoCoeffs.psi[i][nu][n+1]);
       Fnu  += 2*CONST_PI*(A[n]*PhotoCoeffs.psi[i][nu][n] - Abar[n]*PhotoCoeffs.psi[i][nu][n+1]);
       if (i<NX1_TOT-1){
         Jnu1 += dmu[n]/4.*(PhotoCoeffs.psi[i+1][nu][n]+PhotoCoeffs.psi[i+1][nu][n+1]);
         Fnu1 += 2*CONST_PI*(A[n]*PhotoCoeffs.psi[i+1][nu][n] 
		- Abar[n]*PhotoCoeffs.psi[i+1][nu][n+1]);
       }
     }

     /* calculating HI ionising and non-ionising photons */
     nphot = Fnu*PhotoCoeffs.dnu[nu]/(CONST_h*PhotoCoeffs.nuc[nu]);
     SelfSpec.N_totphot[i]        	+= nphot;
     if (nu>=9) SelfSpec.N_uvH[i]	+= nphot;
     if (nu>=15) SelfSpec.N_uvHe[i]	+= nphot;

     /* Calculating cell averaged quantities */
     Jnu = 1./(4.*dr[i]*(r[i+1]*r[i+1]*r[i+1]-r[i]*r[i]*r[i]))
          * ( Jnu1*(3*pow(r[i+1],4.)+pow(r[i],4.)-4*r[i]*pow(r[i+1],3.))
                + Jnu*(pow(r[i+1],4.)+3*pow(r[i],4.)-4*r[i+1]*pow(r[i],3.)) );
     Fnu = 1./(4.*dr[i]*(r[i+1]*r[i+1]*r[i+1]-r[i]*r[i]*r[i]))
          * ( Fnu1*(3*pow(r[i+1],4.)+pow(r[i],4.)-4*r[i]*pow(r[i+1],3.))
                + Fnu*(pow(r[i+1],4.)+3*pow(r[i],4.)-4*r[i+1]*pow(r[i],3.)) );

     PhotoCoeffs.totJnu[i][nu] = MAX(Jnu, 0.0); //Jnu should not be negative by construction, still ...
     PhotoCoeffs.Fnu[i][nu] = Fnu;

     alpha_scat = Dust.albedo[nu]*SelfSpec.opac_dust_ext[k][j][i][nu] ;
     alpha_abs  = SelfSpec.opac_dust_ext[k][j][i][nu]-alpha_scat ;
     alpha_pr	= (SelfSpec.opac_ions[k][j][i][nu]+alpha_abs) 
	     	+ (1.-Dust.costheta[nu])*alpha_scat ; 

     radforce += alpha_pr*Fnu*PhotoCoeffs.dnu[nu];
//     radforce += alpha[i][nu]*Fnu*PhotoCoeffs.dnu[nu];
   }

   PhotoCoeffs.radforce[i] = radforce/CONST_c;  // radiation force per unit mass

   if (g_stepNumber ==0){
     fprintf(ftrans, "%2.4e : %2.4e  %2.4e  %2.4e  %2.4e  %2.4e  %2.4e\n", 
		     r[i]/CONST_pc, PhotoCoeffs.psi[i][7][0], PhotoCoeffs.psi[i][9][0],
		     PhotoCoeffs.psi[i][7][NRAY-1], PhotoCoeffs.psi[i][9][NRAY-1],
		     PhotoCoeffs.totJnu[i][9], PhotoCoeffs.Fnu[i][9] );
   }

 }  /* end of ITOT_LOOP: Jnu calculated at all r's */

 if (g_stepNumber==0) fclose(ftrans);
 /* ------------------------ RTE solved and Jnu obtained at every location ----------- */

}

