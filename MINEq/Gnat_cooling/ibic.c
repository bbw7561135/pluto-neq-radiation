#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void RunCloudy(int, int);
void RunCloudyCIE(int);

char *elem_name[]  = {"Hydrogen", "Helium", "Lithium", "Beryllium", "Boron", "Carbon",
         	"Nitrogen", "Oxygen", "Fluorine", "Neon", "Sodium", "Magnesium",
	       	"Aluminium", "Silicon", "Phosphorus", "Sulphur", "Chlorine", "Argon",
	        "Potassium", "Calcium", "Scandium", "Titanium",	"Vanadium", "Chromium",
	        "Manganese", "Iron"};

int main(int argc, char*argv[])
{
 int Z, ion, Zbeg, Zend;
 double T;

 Zbeg = atoi(argv[1]);
 Zend = atoi(argv[2]);

 FILE *fcl;
 for (Z=Zbeg; Z<=Zend; Z++){
     for (ion=0; ion<=Z; ion++){
         RunCloudy(Z, ion);
	 printf ("> completed for Z=%d, ion=%d\n", Z, ion);
     }
     RunCloudyCIE(Z);
 }

 return 0;
}

void RunCloudy (int Z, int ion)
{
 int i;
 char comm1[128];
 FILE *fcl = fopen("cloudyfile.in","w");

 fprintf (fcl, "interpolate (10.0  -40.00)\n");
 fprintf (fcl, "continue (15.0 -40.0)\n");
 fprintf (fcl, "continue (20.0 -40.0)\n");
 fprintf (fcl, "F(nu) -40.0 at 15.5\n");
 fprintf (fcl, "constant temperature 4.0 vary\n");
 fprintf (fcl, "grid from 3.5 to 8.5 in 0.05 dex steps\n");
 fprintf (fcl, "set trim -15\n");
 fprintf (fcl, "hden -10.0\n");
 fprintf (fcl, "stop zone 1\n");
 fprintf (fcl, "set dr 0\n");
 fprintf (fcl, "abundances all -15\n");
 fprintf (fcl, "set eden 0.0\n");
 fprintf (fcl, "element %s abundance   10.0\n", elem_name[Z-1]);
 fprintf (fcl, "element %s ionization ", elem_name[Z-1]);
 for (i=0; i<=Z; i++){
     if (i==ion) fprintf (fcl, "0.0 ");
     else fprintf (fcl, "-10.0 ");
 }
 fprintf (fcl, "\n");
 fprintf (fcl, "no molecules\n");
 fprintf (fcl, "iterate to converge\n");
 fprintf (fcl, "set save prefix \"data%d_ion%d\"\n", Z, ion);
 fprintf (fcl, "save cooling \".cool\" no hash last\n");

 fclose (fcl);

 sprintf(comm1,"/home/sarkar/softwares/cloudy17.01/source/cloudy.exe -r cloudyfile");
 system(comm1);

}

void RunCloudyCIE (int Z)
{
 int i;
 char comm1[128];
 FILE *fcl = fopen("cloudyfile.in","w");

 fprintf (fcl, "interpolate (10.0  -40.00)\n");
 fprintf (fcl, "continue (15.0 -40.0)\n");
 fprintf (fcl, "continue (20.0 -40.0)\n");
 fprintf (fcl, "F(nu) -40.0 at 15.5\n");
 fprintf (fcl, "constant temperature 4.0 vary\n");
 fprintf (fcl, "grid from 3.5 to 8.5 in 0.05 dex steps\n");
 fprintf (fcl, "set trim -15\n");
 fprintf (fcl, "hden -10.0\n");
 fprintf (fcl, "stop zone 1\n");
 fprintf (fcl, "set dr 0\n");
 fprintf (fcl, "abundances all -15\n");
 fprintf (fcl, "set eden 0.0\n");
 fprintf (fcl, "element %s abundance   10.0\n", elem_name[Z-1]);
/* fprintf (fcl, "element %s ionization ", elem_name[Z-1]);
 for (i=0; i<=Z; i++){
     if (i==ion) fprintf (fcl, "0.0 ");
     else fprintf (fcl, "-10.0 ");
 }
 fprintf (fcl, "\n"); */
 fprintf (fcl, "no molecules\n");
 fprintf (fcl, "iterate to converge\n");
 fprintf (fcl, "set save prefix \"data%d_tot\"\n", Z);
 fprintf (fcl, "save cooling \".cool\" no hash last\n");

 fclose (fcl);

 sprintf(comm1,"/home/sarkar/softwares/cloudy17.01/source/cloudy.exe -r cloudyfile");
 system(comm1);
}
