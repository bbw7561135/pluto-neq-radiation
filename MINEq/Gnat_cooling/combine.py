#!/usr/bin/python3.5
# This file is to combine the ion-by-ion cooling data for an element to a single file

def readcoolfile(Z, ion):
    T = [];
    Lambda = [];

    infile = "data%d_ion%d.cool" % (Z, ion)
    f = open(infile,"r")
    d = f.readlines()
    for a_line in d:
        the_line = a_line.split()
        if (the_line[0] == "#depth"):
            continue
        T.append(the_line[1])
        Lambda.append(the_line[3])

    return T, Lambda
    f.close()

def readcoolfileCIE(Z):
    T = [];
    Lambda = [];

    infile = "data%d_tot.cool" % Z
    f = open(infile,"r")
    d = f.readlines()
    for a_line in d:
        the_line = a_line.split()
        if (the_line[0] == "#depth"):
            continue
        T.append(the_line[1])
        Lambda.append(the_line[3])

    return T, Lambda
    f.close()

def gather_elem(Z):
    LAMBDA = [];
    for ion in range(0, Z+1):
        temp, lam = readcoolfile (Z, ion);
        LAMBDA.append(lam)
    
    temp, lam = readcoolfileCIE (Z);
    LAMBDA.append(lam)

    outfile = "data%d.txt" % Z
    fout = open(outfile,"w")

    for i in range (0,len(temp)):
        fout.write("%s " % temp[i])
        for ion in range (0, Z+2):
            fout.write("%s " % LAMBDA[ion][i])
        fout.write("\n");
    fout.close()


for elem in range(2,27):
    gather_elem(elem)

