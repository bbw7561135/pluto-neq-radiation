/* ############################################################
      
     FILE:     cooling.h

     PURPOSE:  contains common definitions for the 
               whole CODE

     Notice: the order is absolutely important and MUST NOT
             be changed !!!
   ############################################################ */


#define C_IONS  7   /* in [1,5] */
#define N_IONS  8   /* in [1,5] */
#define O_IONS  9   /* in [1,5] */
#define Ne_IONS 11   /* in [1,5] */
#define Mg_IONS 13   /* in [1,5] */
#define Si_IONS 15
#define S_IONS  17   /* in [1,5] */
#define Fe_IONS 27   /* in [0,3] */

#if C_IONS == 0
 #define C_EXPAND(a,b,c,d,e,f,g)  
#elif C_IONS == 1       
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a
#elif C_IONS == 2     
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b
#elif C_IONS == 3       
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c 
#elif C_IONS == 4
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d
#elif C_IONS == 5      
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d,e
#elif C_IONS == 6      
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d,e,f
#elif C_IONS == 7
 #define C_EXPAND(a,b,c,d,e,f,g)  ,a,b,c,d,e,f,g
#endif

#if N_IONS == 0
 #define N_EXPAND(a,b,c,d,e,f,g,h)  
#elif N_IONS == 1      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a
#elif N_IONS == 2     
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b
#elif N_IONS == 3       
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c 
#elif N_IONS == 4
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d
#elif N_IONS == 5      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e
#elif N_IONS == 6      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e,f
#elif N_IONS == 7      
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e,f,g
#elif N_IONS == 8
 #define N_EXPAND(a,b,c,d,e,f,g,h)  ,a,b,c,d,e,f,g,h
#endif

#if O_IONS == 0
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9)
#elif O_IONS == 1      
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1
#elif O_IONS == 2     
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2
#elif O_IONS == 3       
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3
#elif O_IONS == 4
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4
#elif O_IONS == 5      
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5
#elif O_IONS == 9
 #define O_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
#endif

#if Ne_IONS == 0
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) 
#elif Ne_IONS == 1      
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1
#elif Ne_IONS == 2     
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2
#elif Ne_IONS == 3       
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3
#elif Ne_IONS == 4
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4
#elif Ne_IONS == 5      
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5
#elif Ne_IONS == 10
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
#elif Ne_IONS == 11
 #define Ne_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11
#endif

#if Mg_IONS == 0
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) 
#elif Mg_IONS == 1      
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1
#elif Mg_IONS == 2     
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2
#elif Mg_IONS == 3       
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x2,x13) ,x1,x2,x3
#elif Mg_IONS == 4
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4
#elif Mg_IONS == 5      
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5
#elif Mg_IONS == 9
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9
#elif Mg_IONS == 10
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
#elif Mg_IONS == 13
 #define Mg_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13
#endif

#if S_IONS == 1      
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1
#elif S_IONS == 2     
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2
#elif S_IONS == 3       
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3
#elif S_IONS == 4
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4
#elif S_IONS == 5      
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5
#elif S_IONS == 10
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
#elif S_IONS == 15
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15
#elif S_IONS == 16
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16
#elif S_IONS == 17
 #define S_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17
#endif

#if Si_IONS == 1      
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1
#elif Si_IONS == 2     
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2
#elif Si_IONS == 3       
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3
#elif Si_IONS == 4
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4
#elif Si_IONS == 5      
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5
#elif Si_IONS == 7
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7
#elif Si_IONS == 10
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10
#elif Si_IONS == 13
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13
#elif Si_IONS == 15
 #define Si_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15
#endif

//#if Fe_IONS == 0
// #define Fe_EXPAND(a,b,c)  
//#elif Fe_IONS == 1      
// #define Fe_EXPAND(a,b,c)  ,a
//#elif Fe_IONS == 2     
// #define Fe_EXPAND(a,b,c)  ,a,b
//#elif Fe_IONS == 3       
// #define Fe_EXPAND(a,b,c)  ,a,b,c 
#if Fe_IONS == 27
 #define Fe_EXPAND(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) ,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27
#endif

/* **********************************************************************
     Ions are labeled progressively, depending on how many ionization 
     stages are effectively included in the network through the previous 
     X_EXPAND macros. 
     Elements are ordered as {H, He, C, N, O, Ne, S, Fe} and must be 
     carefully respected everywhere in the code. 
     Hydrogen and Helium are always included.
   ********************************************************************** */

enum {
  HI = NFLX, HeI, HeII, HeIII
  C_EXPAND(CI, CII, CIII, CIV, CV, CVI, CVII)
  N_EXPAND(NI, NII, NIII, NIV, NV, NVI, NVII, NVIII)
  O_EXPAND(OI, OII, OIII, OIV, OV, OVI, OVII, OVIII, OIX)
  Ne_EXPAND(NeI, NeII, NeIII, NeIV, NeV, NeVI, NeVII, NeVIII, NeIX, NeX, NeXI)
  Mg_EXPAND(MgI, MgII, MgIII, MgIV, MgV, MgVI, MgVII, MgVIII, MgIX, MgX, MgXI, MgXII, MgXIII)
  Si_EXPAND(SiI, SiII, SiIII, SiIV, SiV, SiVI, SiVII, SiVIII, SiIX, SiX, SiXI, SiXII, SiXIII, SiXIV, SiXV)
  S_EXPAND(SI, SII, SIII, SIV, SV, SVI, SVII, SVIII, SIX, SX, SXI, SXII, SXIII, SXIV, SXV, SXVI, SXVII)
  Fe_EXPAND(FeI, FeII, FeIII, FeIV, FeV, FeVI, FeVII, FeVIII, FeIX, FeX, FeXI, FeXII, FeXIII, FeXIV, FeXV, FeXVI, FeXVII, FeXVIII, FeXIX, FeXX, FeXXI, FeXXII, FeXXIII, FeXXIV, FeXXV, FeXXVI, FeXXVII)
};

#define NIONS  (4+C_IONS+N_IONS+O_IONS+Ne_IONS+Mg_IONS+Si_IONS+S_IONS+Fe_IONS)


/*
#define HI    (NFLX)
#define HeI   (NFLX + 1)
#define HeII  (NFLX + 2)
#define CI    (NFLX + 3)
#define CII   (NFLX + 4)
#define CIII  (NFLX + 5)
#define CIV   (NFLX + 6)
#define CV    (NFLX + 7)
#define NI    (NFLX + 8)
#define NII   (NFLX + 9)
#define NIII  (NFLX + 10)
#define NIV   (NFLX + 11)
#define NV    (NFLX + 12)
#define OI    (NFLX + 13)
#define OII   (NFLX + 14)
#define OIII  (NFLX + 15)
#define OIV   (NFLX + 16)
#define OV    (NFLX + 17)
#define NeI   (NFLX + 18)
#define NeII  (NFLX + 19)
#define NeIII (NFLX + 20)
#define NeIV  (NFLX + 21)
#define NeV   (NFLX + 22)
#define SI    (NFLX + 23)
#define SII   (NFLX + 24)
#define SIII  (NFLX + 25)
#define SIV   (NFLX + 26)
#define SV    (NFLX + 27)
#if INCLUDE_Fe == YES
 #define FeI    (NFLX + 28)
 #define FeII   (NFLX + 29)
 #define FeIII  (NFLX + 30)
#endif
*/

real GetMaxRate          (double *, double *, double);
real MeanMolecularWeight (double *);
double H_MassFrac (void);
real CompEquil            (double, double, double *);
real find_N_rho ();
void Radiat (double *, double *);

void CHECK_NORMALIZATION (double *, char *);
void NORMALIZE_IONS (double *);
/*
int Ros4_expl, Ros4_impl, Ros4_sup_dt;
*/

void get_ion(int, int *, int *);
int g_include_pi;		//kartick





