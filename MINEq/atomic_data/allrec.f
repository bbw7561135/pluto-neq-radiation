      
      subroutine allrec(T,e_den)
      implicit double precision (a-h,o-z)

      double precision T,e_den  ! temperature, electron density
      integer k
      LOGICAL DR_M,DR_G,DR_B,BUG,LRECOL
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     +  TE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     +  TE90,TE95
      COMMON/TEM32/TEM32
      COMMON/SQ3T/SQ3
      COMMON/ANNN/TE01,TE02,TE03,TE04,TE025,
     +   TE625,TE675,TE725,TE775,TE825,TE875,TE925,TE975
      COMMON/TE_L/TELOG
      COMMON/XFF11/XFF1
      COMMON/RECB/RE(300),REN(300),COLION(300)
      COMMON/REC1/RE1(300)
      COMMON/BUG/BUG,NZBUG,NPSBUG,LRECOL,DR_M,DR_G,DR_B
*
c --- Now running with Badnell DR rates:
      DR_M=.TRUE.   ! Hagai's new DR rates - should always be true.
      DR_B=.TRUE.   ! Bandell DR rates - choose this or Gu or none.
      DR_G=.FALSE.   ! Gu DR rates - choose this or Bandell or none.
*
      TE = T
      ANE = e_den

      call DEFTE
      call RECOMB(1)

      return
      end


      SUBROUTINE DEFTE
*
c      include 'optical_depth.inc'
*
      LOGICAL SOUND
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     +  TE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     +  TE90,TE95
      COMMON/TEM32/TEM32
      COMMON/SQ3T/SQ3
      COMMON/ANNN/TE01,TE02,TE03,TE04,TE025,
     +   TE625,TE675,TE725,TE775,TE825,TE875,TE925,TE975
      COMMON/TE_L/TELOG
      COMMON/XFF11/XFF1
*
      TELOG=ALOG10(TE)
      TEE=TE/10000.
      TEV=TE/11604.8
      SQTE=SQRT(TE)
      TEM32=1./TE/SQTE
      TE01=TEE**0.01
      TE02=TE01*TE01
      TE03=TE02*TE01
      TE04=TE03*TE01
      TE025=TEE**0.025
      TE05=TEE**0.05
      TE10=TE05*TE05
      TE15=TE10*TE05
      TE20=TE15*TE05
      TE25=TE15*TE10
      TE30=TE20*TE10
      TE35=TE25*TE10
      TE40=TE30*TE10
      TE45=TE35*TE10
      TE50=TE45*TE05
      SQ3=TEE*TE50
      TE55=TE45*TE10
      TE60=TE50*TE10
      TE625=TE60*TE025
      TE65=TE60*TE05
      TE675=TE65*TE025
      TE70=TE60*TE10
      TE725=TE70*TE025
      TE75=TE65*TE10
      TE775=TE75*TE025
      TE80=TE75*TE05
      TE825=TE80*TE025
      TE85=TE80*TE05
      TE875=TE85*TE025
      TE90=TE80*TE10
      TE925=TE90*TE025
      TE95=TE90*TE05
      TE975=TE95*TE025
*
      XFF1=157893.8/TE
*
      CN=8.629E-6*ANE/SQTE
*
      RETURN
      END

      SUBROUTINE RECOMB (NR)
C
C     CALCULATE RECOMBINATION AND COLLISIONAL IONIZATION RATES.
C
C     ORIGINAL DATA: ALDOVRANDI AND PEQUIGNOT 1973 (AST. AND AP. 25,137)
C     CORRECTION FOR GROUND LEVEL RECOMBINATION (GOULD 1978 AP. J. 219,250)
C     CHANGE IN DI-ELECTRONIC RECB. VALUES AFTER STOREY AND NUSSBAUMER 1983
C     AND SHULL AND VAN STEENBERG, 1982( AP.J.SUPP. 48,95)
*     June 1992: Arnaud and Raymond new recb. for Fe 15-26
C
C     THREE BODY RECOMBINATION (ROUTINE IIIBOD) FOR METALS  USING
C     STEVE COTA CODE (1987)
C
C     COLLISIONAL IONIZATION RATES FROM SHULL AND VAN STEENBERG 1982 WITH
C     CORRECTIONS, AS NEEDED, FROM ARNAUD AND ROTHENFLUG 1985 (AST.AP
C       SUPPL. 60, 425)
C
*changed August 1996: Rad. Rec. are all with Dima Verner's subroutine
*  rrfit to ensure compatibility with the opacity cross sections
*
      LOGICAL NOCOTA,BUG,LRECOL,ADDLOW,DR_M,DR_G,DR_B
*
      COMMON/LOGTE/ALOGTE,ALOGNE
      COMMON/HY/PAT,ALLEL,PPRL,PPRU,NSTEP,NTF 
      COMMON/LOGTE1/XLF,HUMMER
      COMMON/NSBIG/NSBIG
      COMMON/RCOTA/RCOTA(26),NOCOTA
      COMMON/BUG/BUG,NZBUG,NPSBUG,LRECOL,DR_M,DR_G,DR_B
      COMMON/SQ3T/SQ3
      COMMON/S2MAX/S2MAX
      COMMON/TEM32/TEM32
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      COMMON/ANNN/TE01,TE02,TE03,TE04,TE025,
     +   TE625,TE675,TE725,TE775,TE825,TE875,TE925,TE975
      COMMON/ZLION/ZTE,ZSQTE,ATE
      COMMON/RECB/RE(300),REN(300),COLION(300)
      COMMON/REC1/RE1(300)
      COMMON/REGHE/REGHE(26),REGLI(26)
      COMMON/ADDLOW/ADDLOW

      ALOGTE=ALOG10(TE)
      ALOGNE=ALOG10(ANE)
      ALOGT4=ALOGTE-4.
      ATE=0.1*TE
      ZTE=TE
      ZSQTE=SQTE
      SQ3=TEE*TE50
      TEM32=1./TE/SQTE
*
      NOCOTA=.TRUE.   ! kartick
      BUG=.FALSE.
      LRECOL=.FALSE.
*
*      suppression factor for dielectronic recombination
*      at high densities
*
      IF(ANE.GT.1E5)THEN
        S2=AMAX1(S2MAX , 1.818*(1.-0.09*ALOGNE) )
      ELSE
        S2=1.0
      ENDIF
*
      RE(1)=RB(TEE,1.,-0.493,0.504,0.857)
      IF(NR.GT.1)RE(1)=RB(TEE,1.,0.431,0.501,0.46)
C HELIUM 1
      IF(TEE.GE.0.65.AND.TEE.LE.4.0)GO TO 1
      RE(2)=RB(TEE,1.,0.431,0.501,0.46)+DIR(47.,9.4,0.19,0.3)
      GO TO 2
   1  RE(2)=4.904E-13*EXP(-0.127*TEE)/SQRT(TEE)
     A+DIR(47.,9.4,0.19,0.3)
   2  RE(3)=RB(TEE,2.,-0.493,0.504,0.857)
      IF(NR.GT.1)RE(3)=RB(TEE,2.,0.431,0.501,0.46)
C
C     OXYGEN
C
      iz=8
*
      S1=S2
      call rrfit(iz,8,te,rnew)
      RE1(4)=RNEW
      RE(4)=RE1(4)+S1*DIR(22.,0.,0.3,0.)
      call rrfit(iz,7,te,rnew)
      RE1(5)=RNEW
      RE(5)=RE1(5)+S1*DIR(20.,33.,0.51,0.18)
      call rrfit(iz,6,te,rnew)
      RE1(6)=RNEW
      RE(6)=RE1(6)+S1*DIR(24.1,28.3,1.48,.305)
      call rrfit(iz,5,te,rnew)
      RE1(7)=RNEW
      RE(7)=RE1(7)+S1*DIR(21.2,28.3,1.84,0.1)
      call rrfit(iz,4,te,rnew)
      RE1(8)=RNEW
      RE(8)=RE1(8)+S1*DIR(12.5,22.7,0.413,0.162)
      call rrfit(iz,3,te,rnew)
      RE1(9)=RNEW
      RE(9)=RE1(9)+S1*DIR(625.,112.,10.6,.34)
      call rrfit(iz,2,te,rnew)
      RE1(98)=RNEW
      RE(98)=RE1(98)+S1*DIR(701.,147.,6.23,.304)
      call rrfit(iz,1,te,rnew)
      RE1(99)=RNEW
      RE(99)=RE1(99)
****************************************
      IF(TEE.LT.0.1)GO TO 3
*      IF(TEE.GT.6.0.OR.TEE.LT.0.1)GO TO 3
      RE(4)=RE(4)+AD(0.0,0.0238,0.0659,0.0349,0.5334)
      RE(5)=RE(5)+AD(-0.0036,0.7519,1.5252,-0.0838,0.2769)
      RE(7)=RE(7)+AD(0.0061,0.2269,32.1419,1.9939,-0.0646)
      IF(TEE.GT.0.2)RE(6)=RE(6)+AD(0.,21.879,16.273,-0.702,1.1899)
      IF(TEE.GT.0.4)RE(8)=RE(8)+AD(-2.8425,0.2283,40.4072,-3.4959,
     A1.7558)
   3  CONTINUE
*     Ground level recombination for OVII (Verner private 1995)
      REGHE(8)=4.68E-12/TE50
      REGLI(8)=3.59E-12/TE50
C
C     CARBON
C
      iz=6
*
      call rrfit(iz,6,te,rnew)
      RE1(10)=RNEW
      RE(10)=RE1(10)+S1*DIR(15.7,37.4,.254,.044)
      call rrfit(iz,5,te,rnew)
      RE1(11)=RNEW
      RE(11)=RE1(11)+S1*DIR(14.1,14.1,.615,.0588)
      call rrfit(iz,4,te,rnew)
      RE1(12)=RNEW
      RE(12)=RE1(12)+S1*DIR(8.19,15.9,.162,.343)
      call rrfit(iz,3,te,rnew)
      RE1(13)=RNEW
      RE(13)=RE1(13)+S1*DIR(344.,58.7,4.78,.362)
      call rrfit(iz,2,te,rnew)
      RE1(14)=RNEW
      RE(14)=RE1(14)+S1*DIR(406.,83.1,3.22,.315)
      call rrfit(iz,1,te,rnew)
      RE1(15)=RNEW
      RE(15)=RE1(15)

*
      IF(TEE.LT.0.1)GO TO 10
      IF(TEE.GT.0.2)RE(10)=RE(10)+AD(0.0108,-0.1075,0.281,-0.0193,
     A-0.1127)
!      RE(10)=RE(10)+AD(0.0108,-0.1075,0.281,-0.0193,A-0.1127)
      RE(11)=RE(11)+AD(1.8267,4.1012,4.8443,0.2261,0.596)
      RE(12)=RE(12)+AD(2.3198,10.7328,6.883,-0.1824,0.4101)
  10  CONTINUE
*     Ground level recombination for CIV (Verner private 1995)
      REGHE(6)=2.51E-12/TE50
      REGLI(6)=1.56E-12/TE50
C
C     NITROGEN
C
      iz=7
*
      S1=S2
*
      call rrfit(iz,7,te,rnew)
      RE1(16)=RNEW
      RE(16)=RE1(16)+S1*DIR(22.,0.,.298,0.)
      call rrfit(iz,6,te,rnew)
      RE1(17)=RNEW
      RE(17)=RE1(17)+S1*DIR(20.1,7.37,.741,.0764)
      call rrfit(iz,5,te,rnew)
      RE1(18)=RNEW
      RE(18)=RE1(18)+S1*DIR(17.2,22.5,1.13,0.164)
      call rrfit(iz,4,te,rnew)
      RE1(19)=RNEW
      RE(19)=RE1(19)+S1*DIR(10.2,12.5,.262,.243)
      call rrfit(iz,3,te,rnew)
      RE1(20)=RNEW
      RE(20)=RE1(20)+S1*DIR(475.,83.5,7.5,.35)
      call rrfit(iz,2,te,rnew)
      RE1(21)=RNEW
      RE(21)=RE1(21)+S1*DIR(544.,114.,4.61,.309)
      call rrfit(iz,1,te,rnew)
      RE1(97)=RNEW
      RE(97)=RE1(97)
*
      IF(TEE.LT.0.1)GO TO 11
      RE(16)=RE(16)+AD(0.,0.631,0.199,-0.0197,0.4398)
      RE(17)=RE(17)+AD(0.032,-0.6624,4.3191,0.0003,0.5946)
      RE(18)=RE(18)+AD(-0.8806,11.2406,30.7066,-1.1721,0.6127)
      RE(19)=RE(19)+AD(0.4134,-4.6319,25.9172,-2.229,0.236)
  11  CONTINUE
*     Ground level recombination for NV (Verner private 1995)
      REGHE(7)=3.51E-12/TE50
      REGLI(7)=2.62E-12/TE60
C
C     IRON
C
C     VALUES FROM SARAZIN ET AL 1981 AP.J 249,399
C
*     June 1992 update Fe 15-26 after Arnaud and Raymond
*       these should (mor or less) account also for the 
*      low T dielectronic recb, accordind to Raymond)
*
      iz=26
*
      S1=1.0
* changed November 2000: no suppression at low densitiy for low-T recombination
*
      call rrfit(iz,26,te,rnew)
      RE1(22)=RNEW
      RE(22)=RE1(22)+
     +  S1*DIRAM(5.12,12.9,2.2E-4,1.0E-4)
      call rrfit(iz,25,te,rnew)
      RE1(23)=RNEW
      RE(23)=RE1(23)+
     +  S1*DIRAM(16.7,31.4,2.3E-3,2.7E-3)
      call rrfit(iz,24,te,rnew)
      RE1(24)=RNEW
      RE(24)=RE1(24)+
     +  S1*DIRAM(28.6,52.1,1.5E-2,4.7E-3)
      call rrfit(iz,23,te,rnew)
      RE1(25)=RNEW
      RE(25)=RE1(25)+
     +  S1*DIRAM(37.3,67.4,3.8E-2,1.6E-2)
      call rrfit(iz,22,te,rnew)
      RE1(26)=RNEW
      RE(26)=RE1(26)+
     +  S1*DIRAM(54.2,100.,8.0E-2,2.4E-2)
      call rrfit(iz,21,te,rnew)
      RE1(27)=RNEW
      RE(27)=RE1(27)+
     +  S1*DIRAM(45.5,36.,9.2E-2,4.1E-2)
      call rrfit(iz,20,te,rnew)
      RE1(28)=RNEW
      RE(28)=RE1(28)+
     +  S1*DIRAM(66.7,123.,0.16,3.6E-2)
      call rrfit(iz,19,te,rnew)
      RE1(29)=RNEW
      RE(29)=RE1(29)+
     +  S1*DIRAM(66.1,129.,0.18,7.0E-2)
      call rrfit(iz,18,te,rnew)
      RE1(30)=RNEW
      RE(30)=RE1(30)+
     +  S1*DIRAM(21.6,136.,0.14,0.26)
      call rrfit(iz,17,te,rnew)
      RE1(31)=RNEW
      RE(31)=RE1(31)+
     +  S1*DIRAM(22.2,144.,0.1,0.28)
      call rrfit(iz,16,te,rnew)
      RE1(32)=RNEW
      RE(32)=RE1(32)+
     +  S1*DIRAM(59.6,362.,0.225,0.231)
      call rrfit(iz,15,te,rnew)
      RE1(33)=RNEW
      RE(33)=RE1(33)+
     +  S1*DIRAM(75.,205.,0.24,0.17)
      call rrfit(iz,14,te,rnew)
      RE1(34)=RNEW
      RE(34)=RE1(34)+
     +  S1*DIRAM(36.3,193.,0.26,0.16)
      call rrfit(iz,13,te,rnew)
      RE1(35)=RNEW
      RE(35)=RE1(35) +
     +    S1*DIRAM(39.4,198.,0.19,9.0E-2)
      RE1(85)=1.68E-10/TEE**(0.602+0.0507*ALOGT4)
      RE(85)=RE1(85) +
     +    S1*DIRAM4(24.6,248.,560.,0.,0.12,0.12,0.6,0.)   
*
      IF(DR_M)THEN
*
*changed August 2003: add DR for many M-shell ions to represent 
*    delta(n)=0 transitions
*    "invented rates assume an additional terms (used in DIRAM3) 
*     of C*EXP(-E/TEV) peaking at 1e5K (TEV=8.67) that equals the radiative
*     recombination rate at that temp
*changed Sept 2005: Remove S1 (density suppresion) from those rates since these are
*    dn=0 transitions (the S1 factor was invented for dn>0 transitions)
*
      RE(27)=RE1(27)+
     +  DIRAM3(2.585, 45.5,36., 1.77e-4, 9.2E-2,4.1E-2)
*     +  S1*DIRAM3(8.67, 45.5,36., 2.96e-3, 9.2E-2,4.1E-2)
*     +  S1*DIRAM3(8.67, 45.5,36., 4.2e-4, 9.2E-2,4.1E-2)
      RE(28)=RE1(28)+
     +  DIRAM3(2.585, 66.7,123., 2.7e-4, 0.16,3.6E-2)
*     +  S1*DIRAM3(8.67, 66.7,123., 4.52e-3, 0.16,3.6E-2)
*     +  S1*DIRAM3(8.67, 66.7,123., 6.17e-4, 0.16,3.6E-2)
      RE(29)=RE1(29)+
     +  DIRAM3(2.585, 66.1,129., 3.80e-4, 0.18,7.0E-2)
*     +  S1*DIRAM3(8.67, 66.1,129., 6.5e-3, 0.18,7.0E-2)
*     +  S1*DIRAM3(8.67, 66.1,129., 8.4e-4, 0.18,7.0E-2)
      RE(30)=RE1(30)+
     +  DIRAM3(2.585, 21.6,136., 5.7e-4, 0.14,0.26)
*     +  S1*DIRAM3(8.67, 21.6,136., 8.5e-3, 0.14,0.26)
*     +  S1*DIRAM3(8.67, 21.6,136., 1.08e-3, 0.14,0.26)
      RE(31)=RE1(31)+
     +  DIRAM3(2.585, 22.2,144., 7.0e-4, 0.1,0.28)
*     +  S1*DIRAM3(8.67, 22.2,144., 1.12e-2, 0.1,0.28)
*     +  S1*DIRAM3(8.67, 22.2,144., 1.39e-3, 0.1,0.28)
      RE(32)=RE1(32)+
     +  DIRAM3(2.585, 59.6,362., 8.2e-4, 0.225,0.231)
*     +  S1*DIRAM3(8.67, 59.6,362., 1.37e-2, 0.225,0.231)
*     +  S1*DIRAM3(8.67, 59.6,362., 1.7e-3, 0.225,0.231)
      RE(33)=RE1(33)+
     +  DIRAM3(2.585, 75.,205., 1.0e-3, 0.24,0.17)
*     +  S1*DIRAM3(8.67, 75.,205., 1.68e-2, 0.24,0.17)
*     +  S1*DIRAM3(8.67, 75.,205., 2.07e-3, 0.24,0.17)
      RE(34)=RE1(34)+
     +  DIRAM3(2.585, 36.3,193., 1.23e-3, 0.26,0.16)
*     +  S1*DIRAM3(8.67, 36.3,193., 2.05e-2, 0.26,0.16)
*     +  S1*DIRAM3(8.67, 36.3,193., 2.51e-3, 0.26,0.16)
      RE(35)=RE1(35) +
     +  DIRAM3(2.585, 39.4,198., 1.1e-3, 0.19,9.0E-2)
*     +  S1*DIRAM3(8.67, 39.4,198., 1.82e-2, 0.19,9.0E-2)
*     +  S1*DIRAM3(8.67, 39.4,198., 2.81e-3, 0.19,9.0E-2)
*
      RE(85)=RE1(85) +
     +    S1*DIRAM4(24.6,248.,560.,0.,0.12,0.12,0.6,0.) +
     +    S1*DIRAM3(2.585,100.,200.,1.2e-3,0.0,0.0)
      endif
*
      RE1(86)=1.91E-10/TEE**(0.601+0.0510*ALOGT4) 
      RE(86)=RE1(86) +
     +    S1*DIRAM(560.,0.,1.23,0.)
      RE1(87)=2.24E-10/TEE**(0.579+0.0549*ALOGT4) 
      RE(87)=RE1(87) +
     +    S1*DIRAM4(22.5,117.,341.,683.,2.53E-3,3.36E-2,0.181,1.92)
      RE1(88)=2.59E-10/TEE**(0.567+0.0565*ALOGT4) 
      RE(88)=RE1(88) +
     +    S1*DIRAM4(16.2,96.,330.,729.,5.67E-3,7.82E-2,3.18e-2,1.26)
      RE1(89)=2.96E-10/TEE**(0.557+0.0579*ALOGT4) 
      RE(89)=RE1(89) +
     +    S1*DIRAM4(23.7,85.1,329.,787.,1.6E-2,7.17E-2,9.06e-2,0.739)
      RE1(90)=3.16E-10/TEE**(0.534+0.0602*ALOGT4) 
      RE(90)=RE1(90) +
     +    S1*DIRAM4(13.2,66.6,297.,714.,1.85E-2,9.53E-2,7.9e-2,1.23)
      RE1(91)=3.49E-10/TEE**(0.521+0.0622*ALOGT4) 
      RE(91)=RE1(91) +
     +    S1*DIRAM4(39.1,80.3,392.,919.,9.2E-4,0.129,0.192,0.912)
      RE1(92)=3.91E-10/TEE**(0.523+0.0615*ALOGT4) 
      RE(92)=RE1(92) +
     +    S1*DIRAM4(73.2,316.,877.,0.,0.131,8.49E-2,0.613,0.)
*
*      RE(93)=7.86E-10/TE70+S1*DIR(42.3,1070.,4.1,4.2)
      RE1(93)=4.33E-10/TEE**(0.531+0.0577*ALOGT4) 
      IF(TEE.GT.15.0)THEN
        RE(93)=RE1(93) +
     +    S1*DIRAM4(0.1,36.2,306.,928.,1.1E-2,4.88E-2,8.01e-2,0.529)
      ELSE
*      use Romanik ApJ 330, 1022 since previous is bad for low T
        RE(93)=RE1(93)+
     +    S1*DIRAM4(1.36,8.133,13.3,28.83,2.01e-3,
     +      1.57e-3,1.8e-2,1.12e-2)
      ENDIF
*
      RE1(94)=4.77E-10/TEE**(0.537+0.0549*ALOGT4) 
      IF(TEE.GT.15.0)THEN
        RE(94)=RE1(94) +
     +    S1*DIRAM(4.625E3,6.0E3,0.256,0.452)
      ELSE
*      use Romanik ApJ 330, 1022 since previous is bad for low T
        RE(94)=RE1(94) +
     +    S1*DIRAM4(9.18,16.32,24.48,36.45,8.53e-3,
     +       6.14e-3,8.78e-3,7.77e-3)
      ENDIF
      call rrfit(iz,2,te,rnew)
      RE1(95)=RNEW
      RE(95)=RE1(95) +
     +     S1*DIRAM(5.3E3,0.,0.43,0.)
*
      call rrfit(iz,1,te,rnew)
      RE1(96)=RNEW
      RE(96)=RE1(96)
*
*      print*,' ion   rad-recom.       DR          total'
*      do k=22,35
*        print444,k-21,re1(k),re(k)-re1(k),re(k)
*      enddo
*      do k=85,96
*        print444,k-70,re1(k),re(k)-re1(k),re(k)
*      enddo
* 444  format('Fe',i3,3x,5(1pe9.3,5x))
*
*     1.9.1992 add mean of dielectronic rec (mean of C,N,O) to those
*     elements not having them
*     Ferland idea after taking with Pradhan) see PASP 103, 1182
*     ADDLOW=.FALSE. turns this option off
*BUT (february 1997) not closed shells e.g. not Mg++ to Mg+
*
* September 2005: Note that the new Gu 2003 rates replace those rates for
*  Mg(3) and Mg(4)
*
      DMEAN1=0.
      DMEAN2=0.
      DMEAN3=0.
      DMEAN4=0.
      IF(ADDLOW)THEN
        IF(TEE.GT.0.15)THEN
          DMEAN1=
     +      AD(0.,0.631,0.199,-0.0197,0.4398)
     +     + AD(0.0,0.0238,0.0659,0.0349,0.5334)
     +     + AD(0.0108,-0.1075,0.281,-0.0193,-0.1127)
          DMEAN1=DMEAN1/3.
          DMEAN2=
     +     AD(0.032,-0.6624,4.3191,0.0003,0.5946)
     +     +AD(1.8267,4.1012,4.8443,0.2261,0.596)
     +     +AD(-0.0036,0.7519,1.5252,-0.0838,0.2769)
          DMEAN2=DMEAN2/3.
          DMEAN3=
     +     AD(-0.8806,11.2406,30.7066,-1.1721,0.6127)
     +     +AD(0.,21.879,16.273,-0.702,1.1899)
     +     +AD(2.3198,10.7328,6.883,-0.1824,0.4101)
          DMEAN3=DMEAN3/3.
          DMEAN4=
     +     AD(0.4134,-4.6319,25.9172,-2.229,0.236)
     +     +AD(0.0061,0.2269,32.1419,1.9939,-0.0646)
          DMEAN4=DMEAN4/2.
        ENDIF
      ENDIF
*     Ground level recombination for Fe24 (Verner private 1995)
      REGHE(26)=5.38E-11/TE50
      REGLI(26)=5.61E-11/TE50
C
C     MAGNESIUM
C
      iz=12
*
      S1=S2
*
      call rrfit(iz,12,te,rnew)
      RE1(36)=RNEW
      RE(36)=RE1(36)+S1*DIR(5.,2.8,0.045,0.021)
      call rrfit(iz,11,te,rnew)
      RE1(37)=RNEW
      RE(37)=RE1(37)+S1*DIR(60.6,144.,.195,.074)
      call rrfit(iz,10,te,rnew)
      RE1(38)=RNEW
      RE(38)=RE1(38)+S1*DIR(46.9,75.5,.512,.323)
      call rrfit(iz,9,te,rnew)
      RE1(39)=RNEW
      RE(39)=RE1(39)+S1*DIR(37.4,78.8,.774,.636)
      call rrfit(iz,8,te,rnew)
      RE1(40)=RNEW
      RE(40)=RE1(40)+S1*DIR(32.8,102.,1.17,.807)
      call rrfit(iz,7,te,rnew)
      RE1(41)=RNEW
      RE(41)=RE1(41)+S1*DIR(48.0,97.3,3.69,0.351)
      call rrfit(iz,6,te,rnew)
      RE1(42)=RNEW
      RE(42)=RE1(42)+S1*DIR(38.8,73.8,3.63,0.548)
      call rrfit(iz,5,te,rnew)
      RE1(43)=RNEW
      RE(43)=RE1(43)+S1*DIR(33.9,38.2,4.15,0.233)
      call rrfit(iz,4,te,rnew)
      RE1(44)=RNEW
      RE(44)=RE1(44)+S1*DIR(21.1,154.,0.886,0.318)
      call rrfit(iz,3,te,rnew)
      RE1(45)=RNEW
      RE(45)=RE1(45)+S1*DIR(1400.,264.,25.2,0.315)
      call rrfit(iz,2,te,rnew)
      RE1(46)=RNEW
      RE(46)=RE1(46)+S1*DIR(1500.,309.,14.4,0.291)
      call rrfit(iz,1,te,rnew)
      RE1(47)=RNEW
      RE(47)=RE1(47)
      IF(TEE.GT.0.1)THEN
        RE(36)=RE(36)+AD(1.2044,-4.6836,7.662,-0.593,1.626)
*changed February 1997 (closed shell i.e. not included)
*              RE(37)=RE(37)+DMEAN2
        RE(38)=RE(38)+DMEAN3
        RE(39)=RE(39)+DMEAN4
      ENDIF
*     Ground level recombination for MgX (Verner private 1995)
      REGHE(12)=1.10E-11/TE50
      REGLI(12)=9.78E-12/TE50
C
C     NEON
C
      iz=10
*
      S1=S2
*
      call rrfit(iz,10,te,rnew)
      RE1(48)=RNEW
      RE(48)=RE1(48)+S1*DIR(31.1,20.6,.0977,.073)
      call rrfit(iz,9,te,rnew)
      RE1(49)=RNEW
      RE(49)=RE1(49)+S1*DIR(28.4,30.7,.265,.242)
      call rrfit(iz,8,te,rnew)
      RE1(50)=RNEW
      RE(50)=RE1(50)+S1*DIR(22.4,29.4,.369,1.01)
      call rrfit(iz,7,te,rnew)
      RE1(51)=RNEW
      RE(51)=RE1(51)+S1*DIR(27.,55.,1.18,.391)
      call rrfit(iz,6,te,rnew)
      RE1(52)=RNEW
      RE(52)=RE1(52)+S1*DIR(30.9,99.1,2.44,2.52)
      call rrfit(iz,5,te,rnew)
      RE1(53)=RNEW
      RE(53)=RE1(53)+S1*DIR(28.3,173.,3.02,.445)
      call rrfit(iz,4,te,rnew)
      RE1(54)=RNEW
      RE(54)=RE1(54)+S1*DIR(16.8,61.3,.61,.254)
      call rrfit(iz,3,te,rnew)
      RE1(55)=RNEW
      RE(55)=RE1(55)+S1*DIR(1400.,180.,25.2,.304)
      call rrfit(iz,2,te,rnew)
      RE1(56)=RNEW
      RE(56)=RE1(56)+S1*DIR(1100.,224.,10.2,.296)
      call rrfit(iz,1,te,rnew)
      RE1(57)=RNEW
      RE(57)=RE1(57)
*
      IF(TEE.GT.0.1)THEN
        RE(49)=RE(49)+AD(.0129,-0.1779,.9353,-0.0682,.4516)
        RE(50)=RE(50)+AD(3.6781,14.1481,17.1175,-0.5017,.2313)
        RE(51)=RE(51)+AD(-0.0254,5.5365,17.0727,-0.7725,0.1702)
        RE(52)=RE(52)+AD(-0.0141,33.8479,43.1608,-1.6072,.1942)
        RE(53)=RE(53)+AD(19.928,235.0536,152.5096,9.1413,.1282)
        RE(54)=RE(54)+AD(5.4751,203.9751,86.9016,-7.4568,2.5145)
      END IF
*     Ground level recombination for NeVIII (Verner private 1995)
      REGHE(10)=7.49E-12/TE50
      REGLI(10)=6.14E-12/TE50
*
***  ALUMINUM
*
      iz=13
*
      S1=S2
*
*  Al1
      iii=1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(   4.9,  10.1,   .2000,  1.3400)
*  Al2
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(   6.7,   2.5,   .2350,   .1250)
*  Al3
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  79.0,  92.0,   .2970,   .3490)
*  Al4
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  84.2,  33.4,   .6970,   .1120)
*  Al5
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  42.9,  84.2,  1.2200,  1.2900)
*  Al6
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  37.2, 145.0,  1.8400,  1.3600)
*  Al7
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  41.7, 155.0,  3.3200,  1.2400) 
*  Al8
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  36.6, 141.0,  3.7800,   .8020)
*  Al9
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  35.9,  33.9,  5.1200,   .1720)
*  Al10
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(  23.1, 297.0,  1.1500,   .9430)
*  Al11
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(1610.0, 311.0, 28.5000,   .3120)  
*  Al12
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)+S1*DIR(1710.0, 191.0, 10.2000,   .1250)
*  Al13
      iii=iii+1
      iii1=14-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(130+iii)=RNEW
      RE(130+iii)=RE1(130+iii)  
* ground level scaled by z**2 to magnesium
      REGHE(13)=1.29E-11/TE50
      REGLI(13)=1.15E-11/TE50
*
C
C     SULPHUR
C
      iz=16
*
      S1=S2
*
      call rrfit(iz,16,te,rnew)
      RE1(58)=RNEW
      RE(58)=RE1(58)+S1*DIR(12.5,0.,.162,0.)
      call rrfit(iz,15,te,rnew)
      RE1(59)=RNEW
      RE(59)=RE1(59)+S1*DIR(19.2,1.8,1.09,.012)
*
*changed January 2001: add low-T ala Pequignot (fit to Nahar's data)
*   (note that his correction is relative to a larger alpha)
*
      IF(TEE.GT.0.25.AND.TEE.LT.1.0)THEN
        RE(59)=RE(59)*1.15*TE20
      ELSEIF(TEE.GE.1.0.AND.TEE.LT.2.0)THEN
        RE(59)=RE(59)*1.15*TE35
      ENDIF
      call rrfit(iz,14,te,rnew)
      RE1(60)=RNEW
      RE(60)=RE1(60)+S1*DIR(18.9,15.9,3.35,.066)
*changed January 2001: add low-T ala Pequignot (fit to Nahar's data)
      IF(TEE.GT.0.25.AND.TEE.LT.1.0)THEN
        RE(60)=RE(60)*6.6*TE40
      ELSEIF(TEE.GE.1.0.AND.TEE.LT.2.0)THEN
        RE(60)=RE(60)*6.6/TE05
      ENDIF
*
      call rrfit(iz,13,te,rnew)
      RE1(61)=RNEW
      RE(61)=RE1(61)+S1*DIR(16.8,8.04,3.14,0.069)
      call rrfit(iz,12,te,rnew)
      RE1(62)=RNEW
      RE(62)=RE1(62)+S1*DIR(13.8,17.1,1.27,.187)
      call rrfit(iz,11,te,rnew)
      RE1(63)=RNEW
      RE(63)=RE1(63)+S1*DIR(180.,175.,1.47,.129)
      call rrfit(iz,10,te,rnew)
      RE1(64)=RNEW
      RE(64)=RE1(64)+S1*DIR(69.0,215.,1.34,1.04)
      call rrfit(iz,9,te,rnew)
      RE1(65)=RNEW
      RE(65)=RE1(65)+S1*DIR(58.4,259.,2.38,1.12)
      call rrfit(iz,8,te,rnew)
      RE1(66)=RNEW
      RE(66)=RE1(66)+S1*DIR(51.7,291.,3.19,1.40)
      call rrfit(iz,7,te,rnew)
      RE1(67)=RNEW
      RE(67)=RE1(67)+S1*DIR(66.6,232.,7.13,1.00)
      call rrfit(iz,6,te,rnew)
      RE1(68)=RNEW
      RE(68)=RE1(68)+S1*DIR(60.0,241.,8.00,0.555)
      call rrfit(iz,5,te,rnew)
      RE1(69)=RNEW
      RE(69)=RE1(69)+S1*DIR(50.9,637.,7.96,1.63)
      call rrfit(iz,4,te,rnew)
      RE1(100)=RNEW
      RE(100)=RE1(100)+S1*DIR(29.1,104.,1.34,0.304)
      call rrfit(iz,3,te,rnew)
      RE1(101)=RNEW
      RE(101)=RE1(101)+S1*DIR(2410.,467.,40.2,0.298)
      call rrfit(iz,2,te,rnew)
      RE1(102)=RNEW
      RE(102)=RE1(102)+S1*DIR(2540.,530.,24.1,0.281)
      call rrfit(iz,1,te,rnew)
      RE1(103)=RNEW
      RE(103)=RE1(103)
*changed January 2001: new approach for sulphur - not the mean
*      RE(58)=RE(58)+DMEAN1
*      RE(59)=RE(59)+DMEAN2
*      RE(60)=RE(60)+DMEAN3
*      RE(61)=RE(61)+DMEAN4
*     Ground level recombination for SXIV (Verner private 1995)
      REGHE(16)=1.98E-11/TE50
      REGLI(16)=1.96E-11/TE50
C
C     SILICON
C
      iz=14
*
      S1=S2
*
      call rrfit(iz,14,te,rnew)
      RE1(70)=RNEW
      RE(70)=RE1(70)+S1*DIR(7.7,0.,.11,0.)
      call rrfit(iz,13,te,rnew)
      RE1(71)=RNEW
      RE(71)=RE1(71)+S1*DIR(9.63,6.46,.587,.753)
      call rrfit(iz,12,te,rnew)
      RE1(72)=RNEW
      RE(72)=RE1(72)+S1*DIR(8.75,4.71,.503,.188)
      call rrfit(iz,11,te,rnew)
      RE1(73)=RNEW
      RE(73)=RE1(73)+S1*DIR(105.,79.8,.543,.45)
      call rrfit(iz,10,te,rnew)
      RE1(74)=RNEW
      RE(74)=RE1(74)+S1*DIR(114.,0.,.886,0.)
      call rrfit(iz,9,te,rnew)
      RE1(75)=RNEW
      RE(75)=RE1(75)+S1*DIR(48.5,103.,1.68,1.80)
      call rrfit(iz,8,te,rnew)
      RE1(76)=RNEW
      RE(76)=RE1(76)+S1*DIR(41.5,191.,2.49,1.88)
      call rrfit(iz,7,te,rnew)
      RE1(77)=RNEW
      RE(77)=RE1(77)+S1*DIR(36.6,211.,3.13,2.01)
      call rrfit(iz,6,te,rnew)
      RE1(78)=RNEW
      RE(78)=RE1(78)+S1*DIR(36.3,214.,4.25,1.22)
      call rrfit(iz,5,te,rnew)
      RE1(79)=RNEW
      RE(79)=RE1(79)+S1*DIR(38.8,112.,6.18,0.303)
      call rrfit(iz,4,te,rnew)
      RE1(80)=RNEW
      RE(80)=RE1(80)+S1*DIR(25.1,393.,1.38,1.42)
      call rrfit(iz,3,te,rnew)
      RE1(81)=RNEW
      RE(81)=RE1(81)+S1*DIR(1880.,360.,32.7,0.306)
      call rrfit(iz,2,te,rnew)
      RE1(82)=RNEW
      RE(82)=RE1(82)+S1*DIR(1990.,414.,18.9,0.286)
      call rrfit(iz,1,te,rnew)
      RE1(83)=RNEW
      RE(83)=RE1(83)
      IF(TEE.GT.0.1)THEN
        RE(70)=RE(70)+AD(-0.0219,0.4364,0.0684,-0.0032,0.1342)
        RE(71)=RE(71)+AD(3.2163,-12.0571,16.2118,-0.5886,0.5613)
        RE(72)=RE(72)+AD(0.1203,-2.69,19.1943,-0.1479,0.1118)
      ENDIF
*     Ground level recombination for SiXII (Verner private 1995)
      REGHE(14)=1.51E-11/TE50
      REGLI(14)=1.43E-11/TE50
*
*     ARGON  (29.5.96)
C
      iz=18
*
      S1=S2
*
      call rrfit(iz,18,te,rnew)
      RE1(181)=RNEW
      RE(181)=RE1(181)+S1*DIR(32. , 31. , 0.1 , 5.e-3 )
      call rrfit(iz,17,te,rnew)
      RE1(182)=RNEW
      RE(182)=RE1(182)+S1*DIR(29. , 55. , 1.1 , 0.045 )
      call rrfit(iz,16,te,rnew)
      RE1(183)=RNEW
      RE(183)=RE1(183)+S1*DIR(23.9 , 60., 3.4 , 0.057 )
      call rrfit(iz,15,te,rnew)
      RE1(184)=RNEW
      RE(184)=RE1(184)+S1*DIR(25.6 , 38.1 , 6.85 , 0.087 )
      call rrfit(iz,14,te,rnew)
      RE1(185)=RNEW
      RE(185)=RE1(185)+S1*DIR(25. , 33. , 9. , 0.0769 )
*changed January 2001: correction ala Pequignot (low T di-recombination)
* after Nahar but note that his basic rates are larger)
      IF(TEE.GT.0.25.AND.TEE.LT.2.0)THEN
        RE(185)=RE(185)*9.88*TE50
      ENDIF
      call rrfit(iz,13,te,rnew)
      RE1(186)=RNEW
      RE(186)=RE1(186)+S1*DIR( 21. , 21.5 , 6.35, 0.14 )
      call rrfit(iz,12,te,rnew)
      RE1(187)=RNEW
      RE(187)=RE1(187)+S1*DIR( 18. , 21.5 , 2.6 , 0.12 )
      call rrfit(iz,11,te,rnew)
      RE1(188)=RNEW
      RE(188)=RE1(188)+S1*DIR( 270. , 330. , 1.7 , 0.1 )
      call rrfit(iz,10,te,rnew)
      RE1(189)=RNEW
      RE(189)=RE1(189)+S1*DIR( 83. , 350., 2.1 , 1.92 )
      call rrfit(iz,9,te,rnew)
      RE1(190)=RNEW
      RE(190)=RE1(190)
      call rrfit(iz,8,te,rnew)
      RE1(191)=RNEW
      RE(191)=RE1(191)
      call rrfit(iz,7,te,rnew)
      RE1(192)=RNEW
      RE(192)=RE1(192)
      call rrfit(iz,6,te,rnew)
      RE1(193)=RNEW
      RE(193)=RE1(193)
      call rrfit(iz,5,te,rnew)
      RE1(194)=RNEW
      RE(194)=RE1(194)
      call rrfit(iz,4,te,rnew)
      RE1(195)=RNEW
      RE(195)=RE1(195)
      call rrfit(iz,3,te,rnew)
      RE1(196)=RNEW
      RE(196)=RE1(196)
      call rrfit(iz,2,te,rnew)
      RE1(197)=RNEW
      RE(197)=RE1(197)
      call rrfit(iz,1,te,rnew)
      RE1(198)=RNEW
      RE(198)=RE1(198)
*
*     Add low-T dielectronic recombination
*
*      RE(181)=RE(181)+DMEAN1
*      RE(182)=RE(182)+DMEAN2
*      RE(183)=RE(183)+DMEAN3
*      RE(184)=RE(184)+DMEAN4
*     Ground level recombination for ArXVII based on Z**2 scaling
*      of the sulfur rate
      REGHE(18)=2.51E-11/TE50
      REGLI(18)=2.46E-11/TE50
*
*** CALCIUM
*
      iz=20
*
      S1=S2
*
*  Ca1
      iii=1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 3.5,   1.6,   .0328,   .0907)
*  Ca2
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 38.5,  24.5,  5.8400,   .1100)
*  Ca3
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 40.8,  42.7, 11.2000,   .0174 )
*  Ca4
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 38.2,  69.2, 13.2000,   .1320)
*  Ca5
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 35.3,  87.8, 13.3000,   .1140)
*  Ca6
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 31.9,  74.3, 12.6000,   .1620) 
*  Ca7
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 32.2,  69.9, 13.9000,   .0878)
*  Ca8
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 24.7,  44.3,  9.5500,   .2630)
*  Ca9
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 22.9,  28.1,  4.0200,   .0627)
*  Ca10
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 373.0, 584.0,  4.1900,   .0616)
*  Ca11
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 92.6, 489.0,  2.5700,  2.7700)
*  Ca12
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 79.6, 462.0,  4.4500,  2.2300)
*  Ca13
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 69.0, 452.0,  5.4800,  2.0000) 
*  Ca14
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 67.0, 332.0,  7.1300,  1.8200)
*  Ca15
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 70.0, 493.0, 10.9000,  1.7400)
*  Ca16
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 56.7, 441.0, 11.0000,   .2430)
*  Ca17
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR( 42.1, 227.0,  2.0500,   .1850)
*  Ca18
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR(3650.0, 725.0, 54.9000,   .2920)
*  Ca19
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)+S1*DIR(3740.0,  10.0, 26.8000,   .0000)
*  Ca20
      iii=iii+1
      iii1=21-iii
      call rrfit(iz,iii1,te,rnew)
      RE1(200+iii)=RNEW
      RE(200+iii)=RE1(200+iii)
* ground level scaled by z**2 to argon
      REGHE(20)=3.10E-11/TE50
      REGLI(20)=3.00E-11/TE50  
*
* Next are Gu 2003 dielectronic recombination - DR -coefficient
*
      IF(DR_G)THEN
        NZZ1=26
        DO KK=16,25
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE(70+KK)=RE1(70+KK)+S1*DRN0+DRN1
        ENDDO
*
        NZZ1=20
        DO KK=10,19
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE(200+KK)=RE1(200+KK)+S1*DRN0+DRN1
*          print*,nzz1,KK,re(200+kk)/re_old
        ENDDO
*
        NZZ1=18
        DO KK=8,17
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE_OLD=RE(180+KK)
          RE(180+KK)=RE1(180+KK)+S1*DRN0+DRN1
        ENDDO
*
        NZZ1=16
        DO KK=6,12
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE(57+KK)=RE1(57+KK)+S1*DRN0+DRN1
        ENDDO
*
        DO KK=13,15
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE(87+KK)=RE1(87+KK)+S1*DRN0+DRN1
        ENDDO
*
        NZZ1=14
        DO KK=4,13
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE(69+KK)=RE1(69+KK)+S1*DRN0+DRN1
        ENDDO
*
        NZZ1=12
        DO KK=2,11
          CALL DR_GU_2003(NZZ1,KK,DRN0,DRN1)
          RE(35+KK)=RE1(35+KK)+S1*DRN0+DRN1
        ENDDO
      ENDIF
*
* start new (2005) Nigel Badnell DR data
*
      IF(DR_B)THEN
*
* Carbon
*
        NZZ1=6
        DO KK=1,5
          RE_OLD=RE(9+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(9+KK)=RE1(9+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(9+kk)/re_old
        ENDDO
*
* Nitrogen
*
        NZZ1=7
        DO KK=1,6
          RE_OLD=RE(15+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(15+KK)=RE1(15+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(15+kk)/re_old
        ENDDO
*
* Oxygen
*
        NZZ1=8
        DO KK=1,6
          RE_OLD=RE(3+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(3+KK)=RE1(3+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(3+kk)/re_old
        ENDDO
        KK=7
        RE_OLD=RE(98)
        call dr_badnell(nzz1,nzz1-kk,dr_bad)
        RE(98)=RE1(98)+S1*DR_BAD
*        print*,nzz1,kk,re_old,re(98)/re_old
*
* Neon
*
        NZZ1=10
        DO KK=1,9
          RE_OLD=RE(47+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(47+KK)=RE1(47+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(47+kk)/re_old
        ENDDO
*
* Magnesium
*
        NZZ1=12
        DO KK=1,11
          RE_OLD=RE(35+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(35+KK)=RE1(35+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(35+kk)/re_old
        ENDDO
*
* Aluminum
*
        NZZ1=13
        DO KK=2,12
          RE_OLD=RE(130+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(130+KK)=RE1(130+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(130+kk)/re_old
        ENDDO
*
* Silicon
*
        NZZ1=14
        DO KK=3,13
          RE_OLD=RE(69+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(69+KK)=RE1(69+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(69+kk)/re_old
        ENDDO
*
* Sulphur
*
        NZZ1=16
        DO KK=5,12
          RE_OLD=RE(57+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(57+KK)=RE1(57+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(57+kk)/re_old
        ENDDO
        DO KK=13,15
          RE_OLD=RE(87+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(87+KK)=RE1(87+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(87+kk)/re_old
        ENDDO
*
* Argon
*
        NZZ1=18
        DO KK=7,17
          RE_OLD=RE(180+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(180+KK)=RE1(180+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(180+kk)/re_old
        ENDDO
*
* Calcium
*
        NZZ1=20
        DO KK=9,19
          RE_OLD=RE(200+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(200+KK)=RE1(200+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(200+kk)/re_old
        ENDDO
*
* Iron
*
        NZZ1=26
        DO KK=15,25
          RE_OLD=RE(70+KK)
          call dr_badnell(nzz1,nzz1-kk,dr_bad)
          RE(70+KK)=RE1(70+KK)+S1*DR_BAD
*          print*,nzz1,kk,re_old,re(70+kk)/re_old
        ENDDO
*
      ENDIF
*
      DO 4 I=1,300
        REN(I)=RE(I)
   4  CONTINUE
*       THIS WAS ALL RECB. THAT CONTRIBUTE TO COOLING
C
C     NOW ADD THREE BODY RECOMBINATION ALA COTA
C
      NSBIG=26
C
C     (I.E FROM X+15 TO X+14. THIS NUMBER CAN BE INCREASED UP TO 26
C
      IF(TE.GT.100.0)THEN              
        CALL IIIBOD
      ENDIF
*
*     previous loop added when a bug in Cota's routine DA was doscovered 1995
*
C
C     NEUTRALS FIRST
C
      RE(4)= RE(4) +RCOTA(1)
      RE(10)=RE(10)+RCOTA(1)
      RE(16)=RE(16)+RCOTA(1)
      RE(22)=RE(22)+RCOTA(1)
      RE(36)=RE(36)+RCOTA(1)
      RE(48)=RE(48)+RCOTA(1)
      RE(58)=RE(58)+RCOTA(1)
      RE(70)=RE(70)+RCOTA(1)
      RE(181)=RE(181)+RCOTA(1)
      RE(201)=RE(201)+RCOTA(1)
      RE(131)=RE(131)+RCOTA(1)
C
C     X+2 TO X+1
C
      RE(3)= RE(3) +RCOTA(2)
      RE(5)= RE(5) +RCOTA(2)
      RE(11)=RE(11)+RCOTA(2)
      RE(17)=RE(17)+RCOTA(2)
      RE(23)=RE(23)+RCOTA(2)
      RE(37)=RE(37)+RCOTA(2)
      RE(49)=RE(49)+RCOTA(2)
      RE(59)=RE(59)+RCOTA(2)
      RE(71)=RE(71)+RCOTA(2)
      RE(182)=RE(182)+RCOTA(2) 
      RE(202)=RE(202)+RCOTA(2)
      RE(132)=RE(132)+RCOTA(2) 
C
C     X+3 TO X+2
C
      RE(6)=RE(6)+RCOTA(3)
      RE(12)=RE(12)+RCOTA(3)
      RE(18)=RE(18)+RCOTA(3)
      RE(24)=RE(24)+RCOTA(3)
      RE(38)=RE(38)+RCOTA(3)
      RE(50)=RE(50)+RCOTA(3)
      RE(60)=RE(60)+RCOTA(3)
      RE(72)=RE(72)+RCOTA(3)
      RE(183)=RE(183)+RCOTA(3) 
      RE(203)=RE(203)+RCOTA(3)
      RE(133)=RE(133)+RCOTA(3) 
C
C     X+4 TO X+3
C
      RE(7)=RE(7)+RCOTA(4)
      RE(13)=RE(13)+RCOTA(4)
      RE(19)=RE(19)+RCOTA(4)
      RE(25)=RE(25)+RCOTA(4)
      RE(39)=RE(39)+RCOTA(4)
      RE(51)=RE(51)+RCOTA(4)
      RE(61)=RE(61)+RCOTA(4)
      RE(73)=RE(73)+RCOTA(4)
      RE(184)=RE(184)+RCOTA(4) 
      RE(204)=RE(204)+RCOTA(4)
      RE(134)=RE(134)+RCOTA(4) 
C
C     X+5 TO X+4
C
      RE(8)=RE(8)+RCOTA(5)
      RE(14)=RE(14)+RCOTA(5)
      RE(20)=RE(20)+RCOTA(5)
      RE(26)=RE(26)+RCOTA(5)
      RE(40)=RE(40)+RCOTA(5)
      RE(52)=RE(52)+RCOTA(5)
      RE(62)=RE(62)+RCOTA(5)
      RE(74)=RE(74)+RCOTA(5)
      RE(185)=RE(185)+RCOTA(5) 
      RE(205)=RE(205)+RCOTA(5)
      RE(135)=RE(135)+RCOTA(5) 
C
C     X+6 TO X+5
C
      RE(9)=RE(9)+RCOTA(6)
      RE(15)=RE(15)+RCOTA(6)
      RE(21)=RE(21)+RCOTA(6)
      RE(27)=RE(27)+RCOTA(6)
      RE(41)=RE(41)+RCOTA(6)
      RE(53)=RE(53)+RCOTA(6)
      RE(63)=RE(63)+RCOTA(6)
      RE(75)=RE(75)+RCOTA(6)
      RE(186)=RE(186)+RCOTA(6) 
      RE(206)=RE(206)+RCOTA(6)
      RE(136)=RE(136)+RCOTA(6) 
C
C     X+7 TO X+6
C
      RE(28)=RE(28)+RCOTA(7)
      RE(42)=RE(42)+RCOTA(7)
      RE(54)=RE(54)+RCOTA(7)
      RE(64)=RE(64)+RCOTA(7)
      RE(76)=RE(76)+RCOTA(7)
      RE(97)=RE(97)+RCOTA(7)
      RE(98)=RE(98)+RCOTA(7)
      RE(187)=RE(137)+RCOTA(7) 
      RE(207)=RE(207)+RCOTA(7)
      RE(137)=RE(137)+RCOTA(7) 
C
C     X+8 TO X+7
C
      RE(29)=RE(29)+RCOTA(8)
      RE(55)=RE(55)+RCOTA(8)
      RE(65)=RE(65)+RCOTA(8)
      RE(43)=RE(43)+RCOTA(8)
      RE(77)=RE(77)+RCOTA(8)
      RE(99)=RE(99)+RCOTA(8)
      RE(188)=RE(188)+RCOTA(8) 
      RE(208)=RE(208)+RCOTA(8)
      RE(138)=RE(138)+RCOTA(8) 
C
C     X+9 TO X+8 
C
      RE(30)=RE(30)+RCOTA(9)
      RE(44)=RE(44)+RCOTA(9)
      RE(56)=RE(56)+RCOTA(9)
      RE(66)=RE(66)+RCOTA(9)
      RE(78)=RE(78)+RCOTA(9)
      RE(189)=RE(189)+RCOTA(9) 
      RE(209)=RE(209)+RCOTA(9)
      RE(139)=RE(139)+RCOTA(9) 
*
C     X+10 TO X+ 9
*
      RE(31)=RE(31)+RCOTA(10)
      RE(45)=RE(45)+RCOTA(10)
      RE(57)=RE(57)+RCOTA(10)
      RE(67)=RE(67)+RCOTA(10)
      RE(79)=RE(79)+RCOTA(10)
      RE(190)=RE(190)+RCOTA(10) 
      RE(210)=RE(210)+RCOTA(10)
      RE(140)=RE(140)+RCOTA(10) 
*
C     X+11 TO X+10 
*
      RE(32)=RE(32)+RCOTA(11)
      RE(46)=RE(46)+RCOTA(11)
      RE(68)=RE(68)+RCOTA(11)
      RE(80)=RE(80)+RCOTA(11)
      RE(191)=RE(191)+RCOTA(11) 
      RE(211)=RE(211)+RCOTA(11)
      RE(141)=RE(141)+RCOTA(11) 
*
C     X+12 TO X+11 
*
      RE(33)=RE(33)+RCOTA(12)
      RE(47)=RE(47)+RCOTA(12)
      RE(69)=RE(69)+RCOTA(12)
      RE(81)=RE(81)+RCOTA(12)
      RE(192)=RE(192)+RCOTA(12) 
      RE(212)=RE(212)+RCOTA(12)
      RE(142)=RE(142)+RCOTA(12) 
*
C     X+13 TO X+12
*
      RE(34)=RE(34)+RCOTA(13)
      RE(82)=RE(82)+RCOTA(13)
      RE(100)=RE(100)+RCOTA(13)
      RE(193)=RE(193)+RCOTA(13) 
      RE(213)=RE(213)+RCOTA(13)
      RE(143)=RE(143)+RCOTA(13) 
*
C     X+14 TO X+13
*
      RE(35)=RE(35)+RCOTA(14)
      RE(83)=RE(83)+RCOTA(14)
      RE(101)=RE(101)+RCOTA(14)
      RE(194)=RE(194)+RCOTA(14) 
      RE(214)=RE(214)+RCOTA(14)
*
C     NOW ALL THE REST
*
      RE(85)=RE(85)+RCOTA(15)
      RE(102)=RE(102)+RCOTA(15)
      RE(195)=RE(195)+RCOTA(15) 
      RE(215)=RE(215)+RCOTA(15) 
*
      RE(86)=RE(86)+RCOTA(16)
      RE(103)=RE(103)+RCOTA(16)
      RE(196)=RE(196)+RCOTA(16) 
      RE(216)=RE(216)+RCOTA(16) 
*
      RE(197)=RE(197)+RCOTA(17) 
      RE(217)=RE(217)+RCOTA(17) 
      RE(87)=RE(87)+RCOTA(17)
*
      RE(198)=RE(198)+RCOTA(18) 
      RE(218)=RE(218)+RCOTA(18) 
      RE(88)=RE(88)+RCOTA(18)
*
      RE(89)=RE(89)+RCOTA(19)
      RE(219)=RE(219)+RCOTA(19) 
*
      RE(90)=RE(90)+RCOTA(20)
      RE(220)=RE(220)+RCOTA(20) 
*
      RE(91)=RE(91)+RCOTA(21)
      RE(92)=RE(92)+RCOTA(22)
      RE(93)=RE(93)+RCOTA(23)
      RE(94)=RE(94)+RCOTA(24)
      RE(95)=RE(95)+RCOTA(25)
      RE(96)=RE(96)+RCOTA(26)
C
C        COLLISIONAL  IONIZATION
*
*     ***  CARBON  ***
*
corly      iz=6
*
corly      in=iz
corly      call cfit(iz,in,te,colli)
corly      COLION(10)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(11)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(12)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(13)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(14)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(15)=COLLI
corly*
corly*     *** NITROGEN ***
corly*
corly      iz=7
corly*
corly      in=iz 
corly      call cfit(iz,in,te,colli) 
corly      COLION(16)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(17)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(18)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(19)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(20)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(21)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(97)=COLLI
corly*
corly*     ***  OXYGEN  ***
corly*
corly      iz=8
corly*
corly      in=iz  
corly      call cfit(iz,in,te,colli)  
corly      COLION(4)=COLLI  
corly      in=in-1  
corly      call cfit(iz,in,te,colli)
corly      COLION(5)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli) 
corly      COLION(6)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli) 
corly      COLION(7)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli) 
corly      COLION(8)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli) 
corly      COLION(9)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(98)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(99)=COLLI
corly* 
corly*     ***  NEON  ***
corly*
corly      iz=10
corly*
corly      in=iz
corly      call cfit(iz,in,te,colli)
corly      COLION(48)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(49)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(50)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(51)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(52)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(53)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(54)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(55)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(56)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(57)=COLLI
corly*
corly*
corly*     ***  MAGNESIUM  ***
corly*
corly      iz=12
corly*
corly      in=iz 
corly      call cfit(iz,in,te,colli)
corly      COLION(36)=COLLI   
corly      in=in-1  
corly      call cfit(iz,in,te,colli)
corly      COLION(37)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli)
corly      COLION(38)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli)
corly      COLION(39)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli)
corly      COLION(40)=COLLI 
corly      in=in-1  
corly      call cfit(iz,in,te,colli)
corly      COLION(41)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(42)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(43)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(44)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(45)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(46)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(47)=COLLI
corly*
corly***   ALUMINUM ***
corly*
corly      iz=13
corly*
corly* Al1
corly      in=iz
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al2
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al3
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al4
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al5
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI   
corly* Al6
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al7
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al8
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al9
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al10
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al11
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al12
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI
corly* Al13
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(130+iii)=COLLI   
corly*
corly*     ***  SILICON  **
corly*
corly      iz=14 
corly* 
corly      in=iz  
corly      call cfit(iz,in,te,colli)   
corly      COLION(70)=COLLI   
corly      in=in-1   
corly      call cfit(iz,in,te,colli) 
corly      COLION(71)=COLLI  
corly      in=in-1   
corly      call cfit(iz,in,te,colli)  
corly      COLION(72)=COLLI  
corly      in=in-1   
corly      call cfit(iz,in,te,colli)  
corly      COLION(73)=COLLI  
corly      in=in-1   
corly      call cfit(iz,in,te,colli)  
corly      COLION(74)=COLLI  
corly      in=in-1   
corly      call cfit(iz,in,te,colli)  
corly      COLION(75)=COLLI  
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(76)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(77)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(78)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(79)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(80)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(81)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(82)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(83)=COLLI
corly*
corly*     ***  SULPHUR
corly*
corly      iz=16
corly*
corly      in=iz
corly      call cfit(iz,in,te,colli)
corly      COLION(58)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(59)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(60)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(61)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(62)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(63)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(64)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(65)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(66)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(67)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(68)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(69)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(100)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(101)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(102)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(103)=COLLI
corly*
corly*     ***  ARGON ***
corly*
corly      iz=18
corly*
corly      in=iz
corly      call cfit(iz,in,te,colli)
corly      COLION(181)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(182)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(183)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(184)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(185)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(186)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(187)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(188)=COLLI 
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(189)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli)
corly      COLION(190)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(191)=COLLI
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(192)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(193)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(194)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(195)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(196)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(197)=COLLI 
corly      in=in-1 
corly      call cfit(iz,in,te,colli) 
corly      COLION(198)=COLLI 
corly*
corly***   CALCIUM ***
corly*
corly      iz=20
corly*
corly* Ca1
corly      in=iz
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca2
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca3
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca4
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca5
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca6
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca7
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca8
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca9
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca10
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca11
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca12
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca13
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca14
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca15
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca16
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI 
corly* Ca17
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca18
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca19
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI
corly* Ca20
corly      in=in-1
corly      iii=iz-in+1
corly      call cfit(iz,in,te,colli)
corly      COLION(200+iii)=COLLI 
corly*
corly*     ***  IRON  ***
corly*
corly      iz=26
corly*
corly      in=iz
corly      call cfit(iz,in,te,colli)
corly      COLION(22)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(23)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(24)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(25)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(26)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(27)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(28)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(29)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(30)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(31)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(32)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(33)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(34)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(35)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(85)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(86)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(87)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(88)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(89)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(90)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(91)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(92)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(93)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(94)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(95)=COLLI
corly      in=in-1
corly      call cfit(iz,in,te,colli)
corly      COLION(96)=COLLI
corly*
corly*     PRINT OUT DATA
corly*
corly      IF ( LRECOL.AND.NSTEP.LT.3 ) THEN
corly*
corly*       first: H and He+
*
      rec_h=recfer1(te,1.,1)
      rec_he2=recfer1(te,2.,1)

      re(1) = rec_h   ! carefull !!!!!!!!!!!!!!!!!!!! - mine
      re(3) = rec_he2 ! carefull !!!!!!!!!!!!!!!!!!!! - mine


corly        WRITE(60,'(1X,'' TE='',1PE9.3,5X,'' H='',e9.3,
corly     +   '' He+='',e9.3)')TE,rec_h,rec_he2
corly        WRITE(60,'(1X,'' TE='',1PE9.3,5X,''REC'',11X,''COLL'')')TE
corly        DO 20 I=10,15
corly  20    WRITE(60,200)RE(I),COLION(I)
corly        DO 21 I=16,21
corly  21    WRITE(60,201)RE(I),COLION(I)
corly        WRITE(60,201)RE(97),COLION(97)
corly        DO 22 I=4,9
corly  22    WRITE(60,202)RE(I),COLION(I)
corly        WRITE(60,202)RE(98),COLION(98)
corly        WRITE(60,202)RE(99),COLION(99)
corly        DO 23 I=48,57
corly  23    WRITE(60,203)RE(I),COLION(I)
corly        DO 24 I=36,47
corly  24    WRITE(60,204)RE(I),COLION(I)
corly        DO I=131,143
corly          WRITE(60,2013)RE(I),COLION(I)
corly        ENDDO
corly        DO 25 I=70,83
corly  25    WRITE(60,205)RE(I),COLION(I)
corly        DO 26 I=58,69
corly  26    WRITE(60,206)RE(I),COLION(I)
corly  10   WRITE(60,206)RE(100),COLION(100)
corly        WRITE(60,206)RE(101),COLION(101)
corly        WRITE(60,206)RE(102),COLION(102)
corly        WRITE(60,206)RE(103),COLION(103)
corly        DO 260 I=181,198
corly  260   WRITE(60,208)RE(I),COLION(I)
corly        DO I=201,220
corly          WRITE(60,2014)RE(I),COLION(I)
corly        ENDDO
corly        DO 27 I=22,35
corly  27    WRITE(60,207)RE(I),COLION(I)
corly        DO 277 I=85,96
corly  277    WRITE(60,207)RE(I),COLION(I)
corly        WRITE(60,'(1X,''RCOTA '',5(I3,1X,1PE8.2))')(I,RCOTA(I),I=1,5)
corly        WRITE(60,'(1X,''RCOTA '',5(I3,1X,1PE8.2))')(I,RCOTA(I),I=6,10)
corly        WRITE(60,'(1X,''RCOTA '',5(I3,1X,1PE8.2))')(I,RCOTA(I),I=11,15)
corly        WRITE(60,'(1X,''RCOTA '',5(I3,1X,1PE8.2))')(I,RCOTA(I),I=16,20)
corly        WRITE(60,'(1X,''RCOTA '',5(I3,1X,1PE8.2))')(I,RCOTA(I),I=21,25)
corly        WRITE(60,'(1X,''RCOTA '',5(I3,1X,1PE8.2))')(I,RCOTA(I),I=26,26)
corly      ENDIF
corly*
corly 200  FORMAT(2X,'C :',11X,1PE10.3,5X,E10.3)
corly 201  FORMAT(2X,'N :',11X,1PE10.3,5X,E10.3)
corly 202  FORMAT(2X,'O :',11X,1PE10.3,5X,E10.3)
corly 203  FORMAT(2X,'Ne:',11X,1PE10.3,5X,E10.3)
corly 204  FORMAT(2X,'Mg:',11X,1PE10.3,5X,E10.3)
corly 2013 FORMAT(2X,'Al:',11X,1PE10.3,5X,E10.3)
corly 2014 FORMAT(2X,'Ca:',11X,1PE10.3,5X,E10.3)
corly 205  FORMAT(2X,'Si:',11X,1PE10.3,5X,E10.3)
corly 206  FORMAT(2X,'S :',11X,1PE10.3,5X,E10.3)
corly 208  FORMAT(2X,'Ar:',11X,1PE10.3,5X,E10.3)
corly 207  FORMAT(2X,'Fe:',11X,1PE10.3,5X,E10.3)
*
      RETURN
      END

      SUBROUTINE DR_BADNELL(NZ,NE,DR_BAD)
      REAL DR_B_C(26,26,8),DR_B_E(26,26,8)
*
* October 2005: Include Ralph Badnell total DR rates as obtained from his site
*  (note several codes provided by him and modified ion ion/iondata to tabulate rates
*  Note also that if ionization level is IL then IL=NZ-NE where NE is the number of
*  electrons eg. for NZ=26 and NE=7 one gets DR rate for Fe19 in ION
*  largest number provided so far is NE=11
*
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      COMMON/TEM32/TEM32
*
* Helium
*
      DATA (DR_B_C( 2, 1,I),I=1,8)/
     +  5.966E-04, 1.613E-04,-2.223E-05, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E( 2, 1,I),I=1,8)/
     +  4.556E+05, 5.552E+05, 8.982E+05, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Carbon
*
      DATA (DR_B_C( 6, 1,I),I=1,8)/
     +  1.426E-03, 3.046E-02, 8.373E-04, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 6, 2,I),I=1,8)/
     +  2.646E-03, 1.762E-02,-7.843E-04, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 6, 3,I),I=1,8)/
     +  4.673E-07, 1.887E-05, 1.305E-05, 3.099E-03, 3.001E-04,
     +  2.553E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 6, 4,I),I=1,8)/
     +  3.489E-06, 2.222E-07, 1.954E-05, 4.212E-03, 2.037E-04,
     +  2.936E-04, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 6, 5,I),I=1,8)/
     +  6.559E-09, 4.281E-09, 3.947E-07, 1.274E-03, 3.965E-04,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E( 6, 1,I),I=1,8)/
     +  3.116E+06, 4.075E+06, 5.749E+06, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 6, 2,I),I=1,8)/
     +  2.804E+06, 3.485E+06, 4.324E+06, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 6, 3,I),I=1,8)/
     +  7.233E+02, 2.847E+03, 1.054E+04, 8.915E+04, 2.812E+05,
     +  3.254E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 6, 4,I),I=1,8)/
     +  2.660E+03, 3.756E+03, 2.566E+04, 1.400E+05, 1.801E+06,
     +  4.307E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 6, 5,I),I=1,8)/
     +  2.276E+01, 7.631E+01, 3.525E+03, 1.210E+05, 2.114E+05,
     +  0.000E+00, 0.000E+00, 0.000E+00/  
*
*******
*
* Nitrogen
*
      DATA (DR_B_C( 7, 1,I),I=1,8)/
     +  2.801E-03, 4.362E-02, 1.117E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 7, 2,I),I=1,8)/
     +  5.761E-03, 3.434E-02,-1.660E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 7, 3,I),I=1,8)/
     +  2.040E-06, 6.986E-05, 3.168E-04, 4.353E-03, 7.765E-04,
     +  5.101E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 7, 4,I),I=1,8)/
     +  3.386E-06, 3.036E-05, 5.945E-05, 1.195E-03, 6.462E-03,
     +  1.358E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 7, 5,I),I=1,8)/
     +  7.712E-08, 4.839E-08, 2.218E-06, 1.536E-03, 3.647E-03,
     +  4.234E-05, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 7, 6,I),I=1,8)/
     +  1.658E-08, 2.760E-08, 3.498E-08, 5.888E-04, 4.250E-04,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E( 7, 1,I),I=1,8)/
     +  4.198E+06, 5.516E+06, 8.050E+06, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 7, 2,I),I=1,8)/
     +  3.860E+06, 4.883E+06, 6.259E+06, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 7, 3,I),I=1,8)/
     +  3.084E+03, 1.332E+04, 6.475E+04, 1.181E+05, 6.687E+05,
     +  4.778E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 7, 4,I),I=1,8)/
     +  1.406E+03, 6.965E+03, 2.604E+04, 1.304E+05, 1.965E+05,
     +  4.466E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 7, 5,I),I=1,8)/
     +  7.113E+01, 2.765E+02, 1.439E+04, 1.347E+05, 2.496E+05,
     +  2.204E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 7, 6,I),I=1,8)/
     +  1.265E+01, 8.425E+01, 9.620E+03, 1.461E+05, 2.645E+05,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Oxygen
*
      DATA (DR_B_C( 8, 1,I),I=1,8)/
     +  4.925E-03, 5.837E-02, 1.359E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 8, 2,I),I=1,8)/
     +  6.135E-02, 1.968E-04, 0.000E+00, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 8, 3,I),I=1,8)/
     +  2.389E-05, 1.355E-04, 5.885E-03, 2.163E-03, 6.341E-04,
     +  1.348E-02, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 8, 4,I),I=1,8)/
     +  1.615E-05, 9.299E-06, 1.530E-04, 6.616E-04, 1.080E-02,
     +  7.503E-04, 2.892E-03, 0.000E+00/
      DATA (DR_B_C( 8, 5,I),I=1,8)/
     +  3.932E-07, 2.523E-07, 3.447E-05, 5.776E-03, 5.101E-03,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 8, 6,I),I=1,8)/
     +  1.627E-07, 1.262E-07, 6.663E-07, 3.925E-06, 2.406E-03,
     +  1.146E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C( 8, 7,I),I=1,8)/
     +  3.187E-08, 6.617E-04, 9.954E-05, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E( 8, 1,I),I=1,8)/
     +  5.440E+06, 7.170E+06, 1.152E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 8, 2,I),I=1,8)/
     +  6.113E+06, 3.656E+07, 0.000E+00, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 8, 3,I),I=1,8)/
     +  2.326E+04, 3.209E+04, 1.316E+05, 6.731E+05, 1.892E+06,
     +  6.150E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 8, 4,I),I=1,8)/
     +  7.569E+02, 3.659E+03, 1.984E+04, 8.429E+04, 2.294E+05,
     +  1.161E+06, 6.137E+06, 0.000E+00/
      DATA (DR_B_E( 8, 5,I),I=1,8)/
     +  1.509E+02, 6.211E+02, 1.562E+04, 1.936E+05, 4.700E+05,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 8, 6,I),I=1,8)/
     +  4.535E+01, 2.847E+02, 4.166E+03, 2.877E+04, 1.953E+05,
     +  3.646E+05, 0.000E+00, 0.000E+00/
      DATA (DR_B_E( 8, 7,I),I=1,8)/
     +  3.140E+04, 1.723E+05, 2.922E+05, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Neon
*
      DATA (DR_B_C(10, 1,I),I=1,8)/
     +  1.183E-02, 9.011E-02, 1.828E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(10, 2,I),I=1,8)/
     +  1.552E-02, 9.008E-02, 1.182E-02, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(10, 3,I),I=1,8)/
     +  2.399E-04, 3.532E-04, 8.928E-03, 5.427E-03, 5.342E-03,
     +  3.981E-02, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(10, 4,I),I=1,8)/
     +  8.460E-05, 1.817E-04, 1.176E-03, 1.397E-02, 5.566E-03,
     +  6.229E-03, 1.031E-02, 0.000E+00/
      DATA (DR_B_C(10, 5,I),I=1,8)/
     +  5.653E-06, 4.344E-05, 1.086E-04, 5.980E-04, 1.457E-02,
     +  1.601E-02, 5.365E-04, 0.000E+00/
      DATA (DR_B_C(10, 6,I),I=1,8)/
     +  2.922E-06, 7.144E-06, 2.836E-05, 9.820E-05, 8.379E-03,
     +  1.009E-02, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(10, 7,I),I=1,8)/
     +  2.763E-06, 1.053E-05, 4.453E-05, 6.244E-03, 3.146E-04,
     +  4.465E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(10, 8,I),I=1,8)/
     +  2.980E-08, 1.257E-07, 1.122E-06, 2.626E-03, 8.802E-04,
     +  1.231E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(10, 9,I),I=1,8)/
     +  4.152E-09, 4.656E-09, 1.310E-08, 1.417E-09, 7.968E-04,
     +  1.271E-05, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(10, 1,I),I=1,8)/
     +  8.405E+06, 1.111E+07, 1.812E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(10, 2,I),I=1,8)/
     +  7.845E+06, 9.803E+06, 1.209E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(10, 3,I),I=1,8)/
     +  2.536E+04, 4.800E+04, 1.770E+05, 1.030E+06, 1.859E+06,
     +  9.743E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(10, 4,I),I=1,8)/
     +  1.049E+03, 3.829E+03, 6.133E+04, 2.568E+05, 4.600E+05,
     +  1.324E+06, 9.353E+06, 0.000E+00/
      DATA (DR_B_E(10, 5,I),I=1,8)/
     +  6.280E+02, 2.812E+03, 1.324E+04, 8.064E+04, 3.052E+05,
     +  1.032E+06, 2.388E+06, 0.000E+00/
      DATA (DR_B_E(10, 6,I),I=1,8)/
     +  2.050E+02, 2.205E+03, 9.271E+03, 4.988E+04, 2.904E+05,
     +  8.782E+05, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(10, 7,I),I=1,8)/
     +  6.393E+02, 1.499E+03, 3.227E+04, 2.561E+05, 4.505E+05,
     +  7.934E+05, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(10, 8,I),I=1,8)/
     +  4.579E+01, 4.753E+02, 1.481E+04, 2.810E+05, 4.763E+05,
     +  4.677E+08, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(10, 9,I),I=1,8)/
     +  2.689E+01, 2.021E+02, 7.200E+02, 4.892E+04, 3.144E+05,
     +  6.738E+05, 0.000E+00, 0.000E+00/
******
*
* Magnesium
*
      DATA (DR_B_C(12, 1,I),I=1,8)/
     +  2.262E-02, 1.216E-01, 2.531E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(12, 2,I),I=1,8)/
     +  3.067E-02, 1.375E-01, 1.347E-02, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(12, 3,I),I=1,8)/
     +  2.582E-04, 5.033E-04, 4.561E-03, 8.754E-03, 2.734E-02,
     +  7.509E-02, 1.385E-02, 0.000E+00/
      DATA (DR_B_C(12, 4,I),I=1,8)/
     +  3.565E-05, 2.017E-05, 1.165E-03, 5.016E-03, 2.434E-02,
     +  2.508E-02, 2.475E-02, 7.087E-04/
      DATA (DR_B_C(12, 5,I),I=1,8)/
     +  5.451E-05, 6.999E-05, 3.928E-04, 7.483E-04, 1.851E-02,
     +  1.190E-02, 4.764E-02, 0.000E+00/
      DATA (DR_B_C(12, 6,I),I=1,8)/
     +  2.931E-05, 5.192E-05, 1.769E-04, 7.988E-04, 1.488E-02,
     +  4.546E-02, 1.505E-04, 0.000E+00/
      DATA (DR_B_C(12, 7,I),I=1,8)/
     +  2.407E-05, 1.187E-04, 3.845E-04, 1.333E-02, 2.804E-02,
     +  8.746E-04, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(12, 8,I),I=1,8)/
     +  2.292E-06, 6.894E-06, 2.639E-05, 4.887E-03, 4.189E-03,
     +  8.685E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(12, 9,I),I=1,8)/
     +  9.400E-08, 3.818E-07, 2.064E-07, 2.710E-05, 3.802E-03,
     +  3.086E-03, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(12,10,I),I=1,8)/
     +  9.477E-06, 8.507E-04, 3.064E-04, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(12,11,I),I=1,8)/
     +  3.871E-08, 4.732E-07, 1.599E-03, 2.628E-05, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(12, 1,I),I=1,8)/
     +  1.201E+07, 1.588E+07, 2.473E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(12, 2,I),I=1,8)/
     +  1.136E+07, 1.431E+07, 1.762E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(12, 3,I),I=1,8)/
     +  2.861E+04, 4.087E+04, 1.528E+05, 2.677E+05, 2.055E+06,
     +  1.298E+07, 1.852E+07, 0.000E+00/
      DATA (DR_B_E(12, 4,I),I=1,8)/
     +  1.759E+03, 1.237E+04, 5.651E+04, 1.680E+05, 4.007E+05,
     +  1.991E+06, 1.330E+07, 4.060E+07/
      DATA (DR_B_E(12, 5,I),I=1,8)/
     +  2.196E+02, 4.524E+03, 2.491E+04, 9.111E+04, 3.367E+05,
     +  8.316E+05, 1.817E+06, 0.000E+00/
      DATA (DR_B_E(12, 6,I),I=1,8)/
     +  7.667E+02, 3.444E+03, 2.409E+04, 1.190E+05, 3.999E+05,
     +  1.502E+06, 2.464E+07, 0.000E+00/
      DATA (DR_B_E(12, 7,I),I=1,8)/
     +  8.061E+03, 2.154E+04, 6.640E+04, 3.406E+05, 1.327E+06,
     +  3.347E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(12, 8,I),I=1,8)/
     +  5.394E+02, 1.783E+03, 1.739E+04, 1.726E+05, 3.809E+05,
     +  1.117E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(12, 9,I),I=1,8)/
     +  2.660E+02, 1.141E+03, 3.210E+03, 1.727E+05, 4.523E+05,
     +  9.105E+05, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(12,10,I),I=1,8)/
     +  3.803E+05, 5.329E+05, 6.984E+05, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(12,11,I),I=1,8)/
     +  8.415E+03, 1.682E+04, 5.000E+04, 2.759E+05, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Aluminum
*
      DATA (DR_B_C(13, 1,I),I=1,8)/
     +  3.004E-02, 1.306E-01, 7.786E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(13, 2,I),I=1,8)/
     +  4.093E-02, 1.618E-01, 1.332E-02, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(13, 3,I),I=1,8)/
     +  4.380E-04, 6.900E-04, 6.947E-03, 9.037E-03, 3.925E-02,
     +  1.153E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(13, 4,I),I=1,8)/
     +  1.049E-05, 5.683E-04, 2.734E-03, 6.968E-03, 2.668E-02,
     +  3.801E-02, 3.651E-02, 0.000E+00/
      DATA (DR_B_C(13, 5,I),I=1,8)/
     +  1.431E-05, 3.517E-05, 8.709E-04, 5.087E-03, 2.679E-02,
     +  8.922E-02, 2.483E-04, 0.000E+00/
      DATA (DR_B_C(13, 6,I),I=1,8)/
     +  3.236E-05, 7.821E-05, 3.899E-04, 2.106E-03, 2.015E-02,
     +  7.710E-02, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(13, 7,I),I=1,8)/
     +  1.460E-05, 9.728E-05, 6.830E-04, 1.230E-02, 9.660E-03,
     +  5.047E-02, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(13, 8,I),I=1,8)/
     +  1.096E-06, 7.112E-06, 1.379E-05, 7.521E-05, 4.704E-04,
     +  1.179E-02, 2.374E-02, 0.000E+00/
      DATA (DR_B_C(13, 9,I),I=1,8)/
     +  3.238E-07, 1.152E-06, 2.336E-06, 6.909E-06, 1.925E-03,
     +  5.081E-03, 9.317E-03, 0.000E+00/
      DATA (DR_B_C(13,10,I),I=1,8)/
     +  4.479E-05, 3.526E-03, 1.075E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(13,11,I),I=1,8)/
     +  1.629E-06, 3.584E-06, 1.244E-05, 4.155E-03, 2.904E-04,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(13, 1,I),I=1,8)/
     +  1.406E+07, 1.850E+07, 2.431E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(13, 2,I),I=1,8)/
     +  1.336E+07, 1.689E+07, 2.091E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(13, 3,I),I=1,8)/
     +  5.767E+03, 2.207E+04, 1.639E+05, 3.336E+05, 2.519E+06,
     +  1.617E+07, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(13, 4,I),I=1,8)/
     +  1.304E+04, 2.411E+04, 6.993E+04, 2.091E+05, 4.545E+05,
     +  2.349E+06, 1.598E+07, 0.000E+00/
      DATA (DR_B_E(13, 5,I),I=1,8)/
     +  1.122E+03, 6.431E+03, 5.139E+04, 1.820E+05, 4.936E+05,
     +  2.121E+06, 1.001E+08, 0.000E+00/
      DATA (DR_B_E(13, 6,I),I=1,8)/
     +  9.357E+02, 4.775E+03, 3.796E+04, 1.619E+05, 4.954E+05,
     +  1.885E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(13, 7,I),I=1,8)/
     +  4.671E+02, 1.234E+04, 4.178E+04, 3.345E+05, 6.942E+05,
     +  1.786E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(13, 8,I),I=1,8)/
     +  1.543E+03, 4.115E+03, 1.890E+04, 7.137E+04, 2.873E+05,
     +  5.406E+05, 1.418E+06, 0.000E+00/
      DATA (DR_B_E(13, 9,I),I=1,8)/
     +  2.488E+02, 1.793E+03, 9.919E+03, 4.367E+04, 4.012E+05,
     +  6.513E+05, 1.261E+06, 0.000E+00/
      DATA (DR_B_E(13,10,I),I=1,8)/
     +  5.170E+05, 8.021E+05, 1.185E+06, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(13,11,I),I=1,8)/
     +  3.809E+03, 5.120E+03, 2.166E+04, 7.635E+04, 8.477E+05,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Silicon
*
      DATA (DR_B_C(14, 1,I),I=1,8)/
     +  3.846E-02, 1.491E-01, 2.779E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14, 2,I),I=1,8)/
     +  5.318E-02, 1.874E-01, 1.227E-02, 7.173E-04, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14, 3,I),I=1,8)/
     +  1.205E-03, 1.309E-02, 5.333E-03, 2.858E-02, 3.195E-02,
     +  1.433E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14, 4,I),I=1,8)/
     +  2.214E-04, 1.260E-03, 3.832E-03, 1.158E-02, 2.871E-02,
     +  5.456E-02, 5.035E-02, 0.000E+00/
      DATA (DR_B_C(14, 5,I),I=1,8)/
     +  1.246E-04, 6.649E-04, 2.912E-03, 2.912E-02, 3.049E-02,
     +  1.056E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14, 6,I),I=1,8)/
     +  5.845E-04, 6.600E-04, 7.180E-04, 1.714E-02, 2.958E-02,
     +  1.075E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14, 7,I),I=1,8)/
     +  5.272E-05, 2.282E-04, 1.345E-03, 2.246E-02, 9.606E-02,
     +  8.366E-04, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14, 8,I),I=1,8)/
     +  2.086E-06, 9.423E-06, 3.423E-05, 3.950E-04, 1.535E-02,
     +  4.986E-02, 4.067E-04, 0.000E+00/
      DATA (DR_B_C(14, 9,I),I=1,8)/
     +  6.072E-07, 2.520E-06, 8.133E-07, 4.394E-05, 6.768E-03,
     +  1.421E-02, 1.291E-02, 0.000E+00/
      DATA (DR_B_C(14,10,I),I=1,8)/
     +  1.794E-04, 1.181E-02, 2.833E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(14,11,I),I=1,8)/
     +  3.819E-06, 2.421E-05, 2.283E-04, 8.604E-03, 2.617E-03,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(14, 1,I),I=1,8)/
     +  1.627E+07, 2.154E+07, 3.827E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14, 2,I),I=1,8)/
     +  1.552E+07, 1.969E+07, 2.532E+07, 2.696E+08, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14, 3,I),I=1,8)/
     +  4.824E+04, 2.137E+05, 5.492E+05, 2.460E+06, 3.647E+06,
     +  1.889E+07, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14, 4,I),I=1,8)/
     +  3.586E+03, 1.324E+04, 5.636E+04, 2.498E+05, 5.363E+05,
     +  2.809E+06, 1.854E+07, 0.000E+00/
      DATA (DR_B_E(14, 5,I),I=1,8)/
     +  1.167E+03, 9.088E+03, 5.332E+04, 4.219E+05, 1.571E+06,
     +  2.721E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14, 6,I),I=1,8)/
     +  9.856E+02, 6.577E+03, 4.281E+04, 3.944E+05, 1.296E+06,
     +  2.441E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14, 7,I),I=1,8)/
     +  2.829E+03, 2.617E+04, 1.374E+05, 4.520E+05, 2.072E+06,
     +  7.808E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14, 8,I),I=1,8)/
     +  3.708E+02, 3.870E+03, 2.226E+04, 1.318E+05, 5.285E+05,
     +  1.742E+06, 8.392E+06, 0.000E+00/
      DATA (DR_B_E(14, 9,I),I=1,8)/
     +  2.197E+02, 2.591E+03, 9.253E+03, 1.427E+05, 5.539E+05,
     +  1.308E+06, 1.788E+06, 0.000E+00/
      DATA (DR_B_E(14,10,I),I=1,8)/
     +  6.792E+05, 1.145E+06, 1.799E+06, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(14,11,I),I=1,8)/
     +  3.802E+03, 1.280E+04, 5.953E+04, 1.026E+05, 1.154E+06,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Sulphur
*
      DATA (DR_B_C(16, 1,I),I=1,8)/
     +  6.659E-02, 1.762E-01,-6.522E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(16, 2,I),I=1,8)/
     +  8.410E-02, 2.381E-01, 1.065E-02, 1.049E-03, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(16, 3,I),I=1,8)/
     +  1.173E-03, 1.718E-03, 1.657E-02, 7.474E-03, 9.698E-02,
     +  1.399E-01, 7.029E-02, 0.000E+00/
      DATA (DR_B_C(16, 4,I),I=1,8)/
     +  2.610E-04, 9.442E-04, 8.190E-03, 4.271E-02, 1.997E-02,
     +  9.590E-02, 8.444E-02, 0.000E+00/
      DATA (DR_B_C(16, 5,I),I=1,8)/
     +  9.126E-05, 1.691E-04, 3.050E-03, 2.604E-02, 3.245E-02,
     +  2.511E-01, 4.459E-04, 0.000E+00/
      DATA (DR_B_C(16, 6,I),I=1,8)/
     +  1.371E-04, 3.096E-04, 1.782E-03, 1.278E-02, 3.323E-02,
     +  2.593E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(16, 7,I),I=1,8)/
     +  1.830E-04, 1.551E-03, 1.717E-03, 2.798E-02, 6.933E-02,
     +  1.727E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(16, 8,I),I=1,8)/
     +  2.924E-05, 3.366E-04, 2.104E-04, 1.910E-02,-4.017E-04,
     +  6.541E-02, 9.546E-02, 0.000E+00/
      DATA (DR_B_C(16, 9,I),I=1,8)/
     +  2.585E-06, 9.517E-06, 5.194E-06, 3.715E-04, 1.553E-02,
     +  1.013E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(16,10,I),I=1,8)/
     +  6.243E-05, 5.059E-03, 6.043E-02, 2.541E-03, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(16,11,I),I=1,8)/
     +  2.816E-06, 3.172E-05, 1.832E-04, 4.360E-03, 1.618E-02,
     +  7.707E-03, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(16, 1,I),I=1,8)/
     +  2.122E+07, 2.897E+07, 5.786E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(16, 2,I),I=1,8)/
     +  2.032E+07, 2.592E+07, 3.206E+07, 2.016E+08, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(16, 3,I),I=1,8)/
     +  1.366E+04, 6.371E+04, 2.540E+05, 6.493E+05, 3.868E+06,
     +  2.186E+07, 2.983E+07, 0.000E+00/
      DATA (DR_B_E(16, 4,I),I=1,8)/
     +  5.114E+03, 1.750E+04, 1.061E+05, 4.691E+05, 1.821E+06,
     +  4.033E+06, 2.476E+07, 0.000E+00/
      DATA (DR_B_E(16, 5,I),I=1,8)/
     +  3.575E+03, 1.491E+04, 9.388E+04, 3.856E+05, 9.630E+05,
     +  3.490E+06, 1.743E+08, 0.000E+00/
      DATA (DR_B_E(16, 6,I),I=1,8)/
     +  1.505E+03, 1.100E+04, 6.720E+04, 3.364E+05, 9.686E+05,
     +  3.195E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(16, 7,I),I=1,8)/
     +  1.122E+04, 3.121E+04, 9.348E+04, 4.792E+05, 2.052E+06,
     +  3.291E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(16, 8,I),I=1,8)/
     +  7.192E+02, 7.490E+03, 4.510E+04, 5.586E+05, 3.830E+05,
     +  1.976E+06, 2.894E+06, 0.000E+00/
      DATA (DR_B_E(16, 9,I),I=1,8)/
     +  1.277E+03, 5.978E+03, 2.097E+04, 2.292E+05, 7.974E+05,
     +  2.376E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(16,10,I),I=1,8)/
     +  8.745E+05, 1.255E+06, 2.052E+06, 3.769E+06, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(16,11,I),I=1,8)/
     +  7.590E+03, 1.558E+04, 4.013E+04, 1.156E+05, 1.601E+05,
     +  1.839E+06, 0.000E+00, 0.000E+00/
******
*
* Argon
*
      DATA (DR_B_C(18, 1,I),I=1,8)/
     +  9.249E-02, 2.011E-01,-7.153E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(18, 2,I),I=1,8)/
     +  1.220E-01, 2.795E-01, 1.569E-02, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(18, 3,I),I=1,8)/
     +  9.046E-04, 3.613E-03, 1.984E-02, 9.058E-03, 1.497E-01,
     +  2.812E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(18, 4,I),I=1,8)/
     +  7.348E-04, 5.115E-03, 9.444E-03, 5.143E-02, 3.006E-02,
     +  1.566E-01, 1.309E-01, 0.000E+00/
      DATA (DR_B_C(18, 5,I),I=1,8)/
     +  1.885E-04, 3.131E-03, 5.019E-03, 1.104E-02, 4.690E-02,
     +  1.852E-01, 2.407E-01, 0.000E+00/
      DATA (DR_B_C(18, 6,I),I=1,8)/
     +  2.150E-04, 6.864E-04, 2.705E-03, 2.338E-02, 4.951E-02,
     +  4.399E-01, 8.728E-04, 0.000E+00/
      DATA (DR_B_C(18, 7,I),I=1,8)/
     +  7.762E-05, 2.947E-04, 2.841E-03, 2.215E-02, 3.892E-02,
     +  4.219E-01, 1.418E-02, 0.000E+00/
      DATA (DR_B_C(18, 8,I),I=1,8)/
     +  1.299E-05, 2.298E-04, 1.023E-03, 9.294E-03, 3.957E-02,
     +  3.174E-01, 3.450E-03, 0.000E+00/
      DATA (DR_B_C(18, 9,I),I=1,8)/
     +  6.816E-06, 3.234E-05, 2.038E-04, 2.187E-03, 3.042E-02,
     +  2.542E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(18,10,I),I=1,8)/
     +  1.527E-04, 2.438E-02, 1.667E-01, 4.030E-03, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(18,11,I),I=1,8)/
     +  1.016E-04, 7.473E-04, 3.394E-02, 2.398E-03, 2.530E-02,
     +  0.000E+00, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(18, 1,I),I=1,8)/
     +  2.677E+07, 3.653E+07, 4.729E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(18, 2,I),I=1,8)/
     +  2.576E+07, 3.288E+07, 4.360E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(18, 3,I),I=1,8)/
     +  1.409E+04, 6.954E+04, 2.936E+05, 6.472E+05, 4.759E+06,
     +  2.984E+07, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(18, 4,I),I=1,8)/
     +  5.053E+03, 2.167E+04, 1.280E+05, 5.220E+05, 2.161E+06,
     +  5.084E+06, 3.089E+07, 0.000E+00/
      DATA (DR_B_E(18, 5,I),I=1,8)/
     +  4.613E+03, 2.698E+04, 8.987E+04, 3.045E+05, 7.081E+05,
     +  3.524E+06, 5.235E+06, 0.000E+00/
      DATA (DR_B_E(18, 6,I),I=1,8)/
     +  2.258E+03, 1.495E+04, 9.067E+04, 4.780E+05, 1.538E+06,
     +  4.274E+06, 4.681E+07, 0.000E+00/
      DATA (DR_B_E(18, 7,I),I=1,8)/
     +  2.836E+03, 1.624E+04, 8.427E+04, 4.223E+05, 1.125E+06,
     +  3.906E+06, 5.900E+06, 0.000E+00/
      DATA (DR_B_E(18, 8,I),I=1,8)/
     +  1.535E+03, 1.464E+04, 6.005E+04, 4.186E+05, 1.213E+06,
     +  3.562E+06, 5.367E+06, 0.000E+00/
      DATA (DR_B_E(18, 9,I),I=1,8)/
     +  2.083E+03, 1.096E+04, 4.999E+04, 4.238E+05, 1.218E+06,
     +  3.344E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(18,10,I),I=1,8)/
     +  1.171E+06, 1.810E+06, 3.039E+06, 6.100E+06, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(18,11,I),I=1,8)/
     +  1.513E+04, 4.784E+04, 1.926E+05, 6.904E+05, 2.715E+06,
     +  0.000E+00, 0.000E+00, 0.000E+00/
******
*
* Calcium
*
      DATA (DR_B_C(20, 1,I),I=1,8)/
     +  1.210E-01, 2.224E-01,-6.832E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(20, 2,I),I=1,8)/
     +  1.659E-01, 3.186E-01, 1.801E-02, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(20, 3,I),I=1,8)/
     +  2.311E-03, 2.019E-03, 1.789E-02, 2.139E-02, 2.090E-01,
     +  3.375E-01, 1.415E-02, 0.000E+00/
      DATA (DR_B_C(20, 4,I),I=1,8)/
     +  2.892E-03, 7.663E-03, 2.689E-02, 5.718E-02, 2.681E-01,
     +  1.709E-01, 2.262E-02, 0.000E+00/
      DATA (DR_B_C(20, 5,I),I=1,8)/
     +  1.861E-04, 2.067E-03, 1.079E-02, 5.961E-02, 1.290E-01,
     +  5.155E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(20, 6,I),I=1,8)/
     +  2.456E-04, 1.305E-03, 5.715E-03, 3.863E-02, 1.100E-01,
     +  6.152E-01, 1.463E-03, 0.000E+00/
      DATA (DR_B_C(20, 7,I),I=1,8)/
     +  1.162E-04, 2.105E-03, 4.080E-03, 3.587E-02, 8.080E-02,
     +  7.038E-01, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(20, 8,I),I=1,8)/
     +  3.984E-05, 9.865E-04, 5.092E-04, 2.040E-02, 9.188E-02,
     +  5.563E-01, 1.205E-03, 0.000E+00/
      DATA (DR_B_C(20, 9,I),I=1,8)/
     +  1.420E-05, 5.390E-05, 9.249E-05, 5.689E-03, 5.330E-02,
     +  4.953E-01, 1.608E-03, 0.000E+00/
      DATA (DR_B_C(20,10,I),I=1,8)/
     +  2.150E-04, 8.630E-03, 1.030E-01, 2.889E-01, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(20,11,I),I=1,8)/
     +  6.508E-05, 8.795E-04, 6.576E-03, 4.780E-02, 5.167E-02,
     +  1.859E-02, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(20, 1,I),I=1,8)/
     +  3.296E+07, 4.485E+07, 6.312E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(20, 2,I),I=1,8)/
     +  3.186E+07, 4.085E+07, 5.068E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(20, 3,I),I=1,8)/
     +  1.806E+04, 4.286E+04, 2.292E+05, 6.275E+05, 5.896E+06,
     +  3.559E+07, 9.425E+07, 0.000E+00/
      DATA (DR_B_E(20, 4,I),I=1,8)/
     +  1.990E+04, 8.624E+04, 3.180E+05, 8.480E+05, 5.735E+06,
     +  3.300E+07, 7.268E+07, 0.000E+00/
      DATA (DR_B_E(20, 5,I),I=1,8)/
     +  8.140E+03, 3.409E+04, 1.535E+05, 6.411E+05, 3.094E+06,
     +  6.161E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(20, 6,I),I=1,8)/
     +  3.806E+03, 2.310E+04, 1.194E+05, 6.171E+05, 2.656E+06,
     +  5.600E+06, 8.556E+07, 0.000E+00/
      DATA (DR_B_E(20, 7,I),I=1,8)/
     +  3.547E+03, 2.672E+04, 9.898E+04, 5.104E+05, 1.930E+06,
     +  5.204E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(20, 8,I),I=1,8)/
     +  2.038E+03, 1.787E+04, 8.803E+04, 6.079E+05, 2.162E+06,
     +  4.852E+06, 2.461E+08, 0.000E+00/
      DATA (DR_B_E(20, 9,I),I=1,8)/
     +  4.047E+03, 1.614E+04, 7.385E+04, 5.331E+05, 1.746E+06,
     +  4.406E+06, 9.285E+07, 0.000E+00/
      DATA (DR_B_E(20,10,I),I=1,8)/
     +  1.485E+06, 1.977E+06, 2.925E+06, 4.351E+06, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(20,11,I),I=1,8)/
     +  3.711E+03, 4.421E+04, 1.189E+05, 2.603E+05, 3.099E+06,
     +  5.016E+06, 0.000E+00, 0.000E+00/
******
*
* Iron
*
      DATA (DR_B_C(26, 1,I),I=1,8)/
     +  1.984E-01, 2.676E-01,-2.293E-03, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(26, 2,I),I=1,8)/
     +  2.676E-01, 4.097E-01, 2.990E-02, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(26, 3,I),I=1,8)/
     +  7.882E-03, 1.636E-02, 4.868E-02, 4.230E-02, 4.151E-01,
     +  5.339E-01, 4.544E-03, 0.000E+00/
      DATA (DR_B_C(26, 4,I),I=1,8)/
     +  2.325E-03, 1.017E-02, 3.572E-02, 9.882E-02, 1.156E-01,
     +  5.792E-01, 3.344E-01, 0.000E+00/
      DATA (DR_B_C(26, 5,I),I=1,8)/
     +  8.382E-03, 7.897E-03, 3.157E-02, 1.159E-01, 3.919E-01,
     +  1.017E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(26, 6,I),I=1,8)/
     +  2.565E-03, 1.685E-02, 1.827E-02, 6.957E-02, 3.254E-01,
     +  5.101E-01, 7.325E-01, 0.000E+00/
      DATA (DR_B_C(26, 7,I),I=1,8)/
     +  2.106E-03, 6.569E-03, 1.532E-02, 3.799E-02, 7.669E-02,
     +  6.701E-01, 1.298E+00, 0.000E+00/
      DATA (DR_B_C(26, 8,I),I=1,8)/
     +  2.033E-04, 1.159E-03, 5.567E-03, 5.482E-02, 3.370E-01,
     +  1.518E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(26, 9,I),I=1,8)/
     +  8.207E-05, 2.766E-04, 1.897E-03, 2.842E-02, 4.022E-01,
     +  1.434E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(26,10,I),I=1,8)/
     +  7.440E-04, 9.515E-02, 1.019E+00, 3.796E-01,-3.839E-02,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_C(26,11,I),I=1,8)/
     +  7.676E-04, 5.587E-03, 1.152E-01, 4.929E-02, 7.274E-01,
     +  7.347E-03, 0.000E+00, 0.000E+00/
*
      DATA (DR_B_E(26, 1,I),I=1,8)/
     +  5.552E+07, 7.475E+07, 1.236E+08, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(26, 2,I),I=1,8)/
     +  5.394E+07, 6.854E+07, 9.651E+07, 0.000E+00, 0.000E+00,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(26, 3,I),I=1,8)/
     +  5.977E+04, 2.074E+05, 6.170E+05, 3.789E+06, 1.086E+07,
     +  6.363E+07, 1.599E+08, 0.000E+00/
      DATA (DR_B_E(26, 4,I),I=1,8)/
     +  1.376E+04, 8.251E+04, 2.794E+05, 9.378E+05, 4.688E+06,
     +  1.106E+07, 6.543E+07, 0.000E+00/
      DATA (DR_B_E(26, 5,I),I=1,8)/
     +  5.297E+03, 5.829E+04, 2.454E+05, 9.663E+05, 5.580E+06,
     +  1.110E+07, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(26, 6,I),I=1,8)/
     +  6.553E+03, 4.351E+04, 2.059E+05, 9.250E+05, 4.610E+06,
     +  1.019E+07, 1.019E+07, 0.000E+00/
      DATA (DR_B_E(26, 7,I),I=1,8)/
     +  4.463E+03, 3.545E+04, 1.944E+05, 6.148E+05, 1.635E+06,
     +  6.100E+06, 1.026E+07, 0.000E+00/
      DATA (DR_B_E(26, 8,I),I=1,8)/
     +  3.502E+03, 3.557E+04, 2.177E+05, 1.078E+06, 4.515E+06,
     +  9.050E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(26, 9,I),I=1,8)/
     +  6.365E+03, 3.842E+04, 2.002E+05, 1.150E+06, 4.736E+06,
     +  8.891E+06, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(26,10,I),I=1,8)/
     +  2.672E+06, 3.864E+06, 6.853E+06, 1.289E+07, 3.221E+07,
     +  0.000E+00, 0.000E+00, 0.000E+00/
      DATA (DR_B_E(26,11,I),I=1,8)/
     +  2.935E+04, 8.158E+04, 3.591E+05, 1.735E+06, 7.545E+06,
     +  4.634E+07, 0.000E+00, 0.000E+00/
******
*
* calculate rates
*
      SUM=0.
      DO I=1,8
        sum=sum+DR_B_C(NZ,NE,I)*EXP(-DR_B_E(NZ,NE,I)/TE)
      ENDDO
*
      DR_BAD=sum*tem32
*
      RETURN
      END

      SUBROUTINE DR_GU_2003(NZ,NION,DR_NION_0,DR_NION_1)
*
*changed August 2003: DR rates from Gu 2003, ApJ 590 1131
* August 2003: iron only
* Sept 2005: all other elements
*    
      REAL EN010(10,8),CN010(10,8),EN110(10,8),CN110(10,8)
      REAL EN012(12,8),CN012(12,8),EN112(12,8),CN112(12,8)
      REAL EN014(14,8),CN014(14,8),EN114(14,8),CN114(14,8)
      REAL EN016(16,8),CN016(16,8),EN116(16,8),CN116(16,8)
      REAL EN018(18,8),CN018(18,8),EN118(18,8),CN118(18,8)
      REAL EN020(20,8),CN020(20,8),EN120(20,8),CN120(20,8)
      REAL EN026(26,8),CN026(26,8),EN126(26,8),CN126(26,8)
      COMMON/GU_E_C/ E(8),C(8)
*
* Mg
*
      DATA (EN012(11,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012(11,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112(11,I),I=1,8)/
     + 1.038E+03,1.350E+03,1.551E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN112(11,I),I=1,8)/
     + 4.677E-03,1.436E-02,3.579E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN012(10,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012(10,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112(10,I),I=1,8)/
     + 9.749E+02,1.200E+03,1.353E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN112(10,I),I=1,8)/
     + 6.774E-03,2.089E-02,1.116E-02,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN012( 9,I),I=1,8)/
     + 2.064E+00,2.610E+00,3.157E+00,6.982E+00,1.270E+01,
     + 1.929E+01,0.000E+00,0.000E+00/
      DATA (CN012( 9,I),I=1,8)/
     + 2.337E-02,4.968E-01,4.756E-01,2.504E-01,2.251E-01,
     + 6.165E-01,0.000E+00,0.000E+00/
      DATA (EN112( 9,I),I=1,8)/
     + 2.835E+01,7.163E+01,8.198E+01,1.418E+02,1.901E+02,
     + 2.466E+02,1.024E+03,1.274E+03/
      DATA (CN112( 9,I),I=1,8)/
     + 4.284E-09,6.750E-03,1.038E-02,2.438E-02,6.433E-02,
     + 5.624E-03,6.662E-03,1.058E-02/
*
      DATA (EN012( 8,I),I=1,8)/
     + 3.280E-02,8.783E-02,3.196E+00,8.997E+00,3.035E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012( 8,I),I=1,8)/
     + 9.733E+01,2.655E+01,5.235E-01,9.642E-01,9.103E-01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112( 8,I),I=1,8)/
     + 3.703E+00,7.182E+01,8.723E+01,1.310E+02,1.765E+02,
     + 2.250E+02,1.039E+03,1.255E+03/
      DATA (CN112( 8,I),I=1,8)/
     + 2.842E-11,1.944E-03,1.370E-02,1.793E-02,7.007E-02,
     + 1.154E-02,1.957E-03,3.306E-03/
*
      DATA (EN012( 7,I),I=1,8)/
     + 1.812E-02,1.133E-01,1.298E+00,5.689E+00,3.156E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012( 7,I),I=1,8)/
     + 1.146E+02,1.293E+01,1.389E+00,4.797E-01,9.436E-01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112( 7,I),I=1,8)/
     + 5.895E+01,7.966E+01,1.262E+02,1.630E+02,4.575E+02,
     + 1.224E+03,0.000E+00,0.000E+00/
      DATA (CN112( 7,I),I=1,8)/
     + 1.276E-03,2.580E-02,9.275E-02,1.193E-01,3.678E-04,
     + 4.689E-03,0.000E+00,0.000E+00/
*
      DATA (EN012( 6,I),I=1,8)/
     + 7.146E-03,7.240E-02,1.505E-01,8.442E-01,3.055E+00,
     + 1.444E+01,3.606E+01,0.000E+00/
      DATA (CN012( 6,I),I=1,8)/
     + 2.731E+01,6.401E+00,4.278E+00,3.975E-01,3.669E-01,
     + 2.119E-01,5.731E-01,0.000E+00/
      DATA (EN112( 6,I),I=1,8)/
     + 0.000E+00,3.850E+01,6.307E+01,8.512E+01,1.252E+02,
     + 1.613E+02,1.166E+03,1.305E+03/
      DATA (CN112( 6,I),I=1,8)/
     + 0.000E+00,4.722E-06,4.482E-03,4.089E-02,1.450E-01,
     + 4.336E-02,1.829E-03,2.272E-03/
*
      DATA (EN012( 5,I),I=1,8)/
     + 1.171E-01,7.081E-01,1.471E+00,3.974E+00,9.465E+00,
     + 2.970E+01,0.000E+00,0.000E+00/
      DATA (CN012( 5,I),I=1,8)/
     + 1.641E-02,3.516E-01,2.289E-01,3.764E-01,1.148E-01,
     + 5.739E-01,0.000E+00,0.000E+00/
      DATA (EN112( 5,I),I=1,8)/
     + 2.939E+00,5.337E+01,7.081E+01,9.443E+01,1.192E+02,
     + 1.494E+02,1.263E+03,0.000E+00/
      DATA (CN112( 5,I),I=1,8)/
     + 7.181E-11,5.450E-04,1.221E-02,4.768E-02,6.172E-02,
     + 1.030E-02,1.930E-03,0.000E+00/
*
      DATA (EN012( 4,I),I=1,8)/
     + 1.021E-02,9.071E-02,6.347E-01,3.139E+00,3.308E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012( 4,I),I=1,8)/
     + 1.740E+01,5.827E-01,8.626E-02,5.970E-02,3.025E-01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112( 4,I),I=1,8)/
     + 2.245E+00,4.037E+01,5.699E+01,7.732E+01,9.917E+01,
     + 1.291E+02,1.256E+03,0.000E+00/
      DATA (CN112( 4,I),I=1,8)/
     + 1.011E-10,2.908E-04,5.453E-03,4.180E-02,2.096E-02,
     + 2.004E-03,8.764E-04,0.000E+00/
*
      DATA (EN012( 3,I),I=1,8)/
     + 2.038E-02,6.697E-02,1.980E-01,1.900E+01,3.765E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012( 3,I),I=1,8)/
     + 1.829E-01,9.117E-02,4.714E-02,3.124E-03,1.052E-01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112( 3,I),I=1,8)/
     + 1.957E+00,3.845E+01,5.154E+01,6.534E+01,8.955E+01,
     + 2.075E+02,1.249E+03,0.000E+00/
      DATA (CN112( 3,I),I=1,8)/
     + 3.282E-11,4.128E-04,4.821E-03,2.901E-02,2.433E-03,
     + 8.120E-06,3.142E-04,0.000E+00/
*
      DATA (EN012( 2,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN012( 2,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN112( 2,I),I=1,8)/
     + 3.517E+01,4.357E+01,5.029E+01,7.794E+01,1.307E+03,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN112( 2,I),I=1,8)/
     + 8.160E-05,2.794E-03,1.567E-02,9.653E-05,3.270E-05,
     + 0.000E+00,0.000E+00,0.000E+00/
*
* Si
*
      DATA (EN014(13,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN014(13,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN114(13,I),I=1,8)/
     + 1.404E+03,1.827E+03,2.116E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN114(13,I),I=1,8)/
     + 4.891E-03,1.172E-02,2.688E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN014(12,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN014(12,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN114(12,I),I=1,8)/
     + 1.335E+03,1.662E+03,1.892E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN114(12,I),I=1,8)/
     + 7.827E-03,1.988E-02,6.850E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN014(11,I),I=1,8)/
     + 3.366E+00,4.388E+00,8.322E+00,1.481E+01,2.322E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN014(11,I),I=1,8)/
     + 3.590E-01,6.053E-01,3.170E-01,2.937E-01,6.265E-01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN114(11,I),I=1,8)/
     + 4.482E+00,9.563E+01,1.089E+02,1.958E+02,2.665E+02,
     + 3.433E+02,1.394E+03,1.775E+03/
      DATA (CN114(11,I),I=1,8)/
     + 6.013E-11,9.617E-03,1.301E-02,2.854E-02,8.578E-02,
     + 8.106E-03,7.954E-03,1.193E-02/
*
      DATA (EN014(10,I),I=1,8)/
     + 4.089E-02,9.832E-02,4.713E-01,1.699E+00,6.449E+00,
     + 3.315E+01,0.000E+00,0.000E+00/
      DATA (CN014(10,I),I=1,8)/
     + 6.878E+01,5.261E+01,1.210E+01,5.523E+00,2.132E+00,
     + 1.170E+00,0.000E+00,0.000E+00/
      DATA (EN114(10,I),I=1,8)/
     + 9.277E+01,1.123E+02,1.622E+02,2.390E+02,2.977E+02,
     + 1.391E+03,1.756E+03,0.000E+00/
      DATA (CN114(10,I),I=1,8)/
     + 1.474E-03,2.262E-02,1.629E-02,9.570E-02,3.757E-02,
     + 2.433E-03,7.072E-03,0.000E+00/
*
      DATA (EN014( 9,I),I=1,8)/
     + 1.459E-02,8.405E-02,3.945E-01,1.455E+00,5.260E+00,
     + 3.747E+01,0.000E+00,0.000E+00/
      DATA (CN014( 9,I),I=1,8)/
     + 1.993E+01,2.238E+01,1.283E+01,3.798E+00,1.573E+00,
     + 1.027E+00,0.000E+00,0.000E+00/
      DATA (EN114( 9,I),I=1,8)/
     + 7.762E+01,1.017E+02,1.346E+02,2.082E+02,2.611E+02,
     + 1.346E+03,1.741E+03,0.000E+00/
      DATA (CN114( 9,I),I=1,8)/
     + 8.037E-04,2.684E-02,3.779E-02,2.063E-01,6.907E-02,
     + 1.170E-03,5.988E-03,0.000E+00/
*
      DATA (EN014( 8,I),I=1,8)/
     + 1.977E-02,6.566E-02,2.250E-01,8.196E-01,6.105E+00,
     + 4.024E+01,0.000E+00,0.000E+00/
      DATA (CN014( 8,I),I=1,8)/
     + 3.176E+02,1.976E+02,7.116E+01,6.671E+00,5.143E-01,
     + 7.761E-01,0.000E+00,0.000E+00/
      DATA (EN114( 8,I),I=1,8)/
     + 7.634E+01,1.040E+02,1.493E+02,2.043E+02,2.886E+02,
     + 1.711E+03,0.000E+00,0.000E+00/
      DATA (CN114( 8,I),I=1,8)/
     + 1.138E-03,4.028E-02,8.255E-02,2.514E-01,1.272E-02,
     + 4.074E-03,0.000E+00,0.000E+00/
*
      DATA (EN014( 7,I),I=1,8)/
     + 1.690E-01,2.308E-01,3.604E-01,1.254E+00,2.740E+00,
     + 7.361E+00,1.742E+01,3.758E+01/
      DATA (CN014( 7,I),I=1,8)/
     + 7.381E-01,1.991E+00,9.059E-01,1.980E-01,2.902E-01,
     + 2.050E-01,1.432E-01,6.329E-01/
      DATA (EN114( 7,I),I=1,8)/
     + 4.321E+00,7.718E+01,1.020E+02,1.382E+02,1.807E+02,
     + 2.305E+02,1.743E+03,0.000E+00/
      DATA (CN114( 7,I),I=1,8)/
     + 3.359E-10,1.445E-03,2.950E-02,6.912E-02,1.667E-01,
     + 2.547E-02,2.065E-03,0.000E+00/
*
      DATA (EN014( 6,I),I=1,8)/
     + 1.383E-02,8.427E-02,3.112E-01,1.490E+00,8.091E+00,
     + 4.189E+01,0.000E+00,0.000E+00/
      DATA (CN014( 6,I),I=1,8)/
     + 3.262E+00,6.406E-01,5.515E-01,1.103E-01,6.878E-02,
     + 3.758E-01,0.000E+00,0.000E+00/
      DATA (EN114( 6,I),I=1,8)/
     + 5.717E+01,7.739E+01,1.067E+02,1.452E+02,1.904E+02,
     + 1.735E+03,0.000E+00,0.000E+00/
      DATA (CN114( 6,I),I=1,8)/
     + 2.542E-04,5.693E-03,4.815E-02,1.178E-01,3.219E-02,
     + 1.034E-03,0.000E+00,0.000E+00/
*
      DATA (EN014( 5,I),I=1,8)/
     + 2.828E-02,9.223E-02,4.217E-01,1.136E+00,1.164E+01,
     + 4.781E+01,0.000E+00,0.000E+00/
      DATA (CN014( 5,I),I=1,8)/
     + 4.121E-01,2.514E-01,1.995E-01,1.491E-01,8.128E-03,
     + 1.518E-01,0.000E+00,0.000E+00/
      DATA (EN114( 5,I),I=1,8)/
     + 3.176E+00,5.808E+01,8.032E+01,1.072E+02,1.389E+02,
     + 1.776E+02,1.726E+03,0.000E+00/
      DATA (CN114( 5,I),I=1,8)/
     + 1.014E-10,6.712E-04,9.937E-03,5.598E-02,3.852E-02,
     + 5.848E-03,4.237E-04,0.000E+00/
*
      DATA (EN014( 4,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN014( 4,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN114( 4,I),I=1,8)/
     + 4.788E+01,5.901E+01,8.348E+01,1.022E+02,1.354E+02,
     + 1.839E+03,0.000E+00,0.000E+00/
      DATA (CN114( 4,I),I=1,8)/
     + 3.230E-05,9.954E-04,1.929E-02,4.517E-02,8.746E-03,
     + 9.969E-05,0.000E+00,0.000E+00/
*
* S
*
      DATA (EN016(15,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN016(15,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN116(15,I),I=1,8)/
     + 1.826E+03,2.379E+03,2.774E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN116(15,I),I=1,8)/
     + 4.994E-03,9.669E-03,2.049E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN016(14,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN016(14,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN116(14,I),I=1,8)/
     + 1.749E+03,2.195E+03,2.520E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN116(14,I),I=1,8)/
     + 8.567E-03,1.746E-02,4.726E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN016(13,I),I=1,8)/
     + 5.723E-01,6.415E-01,1.017E+00,1.291E+00,5.014E+00,
     + 8.605E+00,1.683E+01,2.738E+01/
      DATA (CN016(13,I),I=1,8)/
     + 4.389E-02,4.323E-01,1.573E+00,5.373E+00,5.463E-01,
     + 5.051E-01,3.931E-01,6.232E-01/
      DATA (EN116(13,I),I=1,8)/
     + 9.864E+01,1.251E+02,1.446E+02,2.658E+02,3.628E+02,
     + 4.846E+02,1.817E+03,2.333E+03/
      DATA (CN116(13,I),I=1,8)/
     + 3.151E-05,1.774E-02,1.150E-02,3.745E-02,9.332E-02,
     + 5.600E-03,8.088E-03,1.017E-02/
*
      DATA (EN016(12,I),I=1,8)/
     + 2.202E-02,3.530E-01,1.265E+00,8.368E+00,3.701E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN016(12,I),I=1,8)/
     + 4.061E+01,6.575E+00,5.054E+00,2.438E+00,1.292E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN116(12,I),I=1,8)/
     + 1.210E+02,1.444E+02,2.134E+02,3.171E+02,3.980E+02,
     + 1.803E+03,2.304E+03,0.000E+00/
      DATA (CN116(12,I),I=1,8)/
     + 2.696E-03,3.347E-02,1.915E-02,1.123E-01,5.083E-02,
     + 2.925E-03,6.859E-03,0.000E+00/
*
      DATA (EN016(11,I),I=1,8)/
     + 1.586E-02,1.284E-01,5.914E-01,1.861E+00,1.031E+01,
     + 4.449E+01,0.000E+00,0.000E+00/
      DATA (CN016(11,I),I=1,8)/
     + 3.883E+01,3.434E+00,2.302E+00,7.350E-01,1.113E+00,
     + 1.063E+00,0.000E+00,0.000E+00/
      DATA (EN116(11,I),I=1,8)/
     + 1.044E+02,1.341E+02,1.765E+02,2.798E+02,3.505E+02,
     + 1.634E+03,2.267E+03,0.000E+00/
      DATA (CN116(11,I),I=1,8)/
     + 1.594E-03,4.324E-02,4.248E-02,2.327E-01,1.181E-01,
     + 1.061E-03,6.292E-03,0.000E+00/
*
      DATA (EN016(10,I),I=1,8)/
     + 1.450E-02,8.243E-02,2.267E-01,1.182E+00,8.096E+00,
     + 4.556E+01,0.000E+00,0.000E+00/
      DATA (CN016(10,I),I=1,8)/
     + 6.048E+01,3.295E+01,1.163E+01,1.591E+00,9.220E-01,
     + 8.468E-01,0.000E+00,0.000E+00/
      DATA (EN116(10,I),I=1,8)/
     + 1.047E+02,1.404E+02,2.097E+02,2.872E+02,4.070E+02,
     + 2.240E+03,0.000E+00,0.000E+00/
      DATA (CN116(10,I),I=1,8)/
     + 2.812E-03,7.358E-02,1.035E-01,3.090E-01,1.483E-02,
     + 3.939E-03,0.000E+00,0.000E+00/
*
      DATA (EN016( 9,I),I=1,8)/
     + 1.136E-01,1.701E-01,3.541E-01,1.089E+00,2.692E+00,
     + 6.056E+00,1.696E+01,4.491E+01/
      DATA (CN016( 9,I),I=1,8)/
     + 4.941E+00,1.679E+00,1.371E+00,2.035E+00,2.375E+00,
     + 7.139E-01,2.520E-01,6.931E-01/
      DATA (EN116( 9,I),I=1,8)/
     + 3.187E+01,1.041E+02,1.367E+02,1.890E+02,2.550E+02,
     + 3.308E+02,2.300E+03,0.000E+00/
      DATA (CN116( 9,I),I=1,8)/
     + 4.071E-09,2.859E-03,4.972E-02,8.309E-02,2.601E-01,
     + 3.869E-02,2.009E-03,0.000E+00/
*
      DATA (EN016( 8,I),I=1,8)/
     + 1.879E-02,4.330E-02,4.894E-01,1.042E+00,7.896E+00,
     + 5.008E+01,0.000E+00,0.000E+00/
      DATA (CN016( 8,I),I=1,8)/
     + 4.244E+01,1.285E+01,4.589E+00,1.662E+00,8.811E-02,
     + 4.258E-01,0.000E+00,0.000E+00/
      DATA (EN116( 8,I),I=1,8)/
     + 8.179E+01,1.132E+02,1.519E+02,2.175E+02,2.837E+02,
     + 2.290E+03,0.000E+00,0.000E+00/
      DATA (CN116( 8,I),I=1,8)/
     + 7.030E-04,1.723E-02,8.659E-02,2.283E-01,5.468E-02,
     + 1.069E-03,0.000E+00,0.000E+00/
*
      DATA (EN016( 7,I),I=1,8)/
     + 5.532E-02,1.052E-01,3.096E-01,8.876E-01,1.525E+01,
     + 5.814E+01,0.000E+00,0.000E+00/
      DATA (CN016( 7,I),I=1,8)/
     + 1.237E-01,3.956E-01,2.034E-01,1.351E-01,2.117E-02,
     + 1.792E-01,0.000E+00,0.000E+00/
      DATA (EN116( 7,I),I=1,8)/
     + 4.520E+00,8.343E+01,1.208E+02,1.672E+02,2.228E+02,
     + 2.987E+02,9.110E+02,2.278E+03/
      DATA (CN116( 7,I),I=1,8)/
     + 8.061E-10,1.661E-03,3.479E-02,1.154E-01,1.032E-01,
     + 4.777E-03,1.402E-07,4.686E-04/
*
      DATA (EN016( 6,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN016( 6,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN116( 6,I),I=1,8)/
     + 1.070E+01,7.740E+01,1.081E+02,1.402E+02,1.812E+02,
     + 2.222E+02,2.465E+03,0.000E+00/
      DATA (CN116( 6,I),I=1,8)/
     + 6.791E-11,8.156E-04,1.431E-02,6.205E-02,8.339E-02,
     + 3.212E-02,1.601E-04,0.000E+00/
*
* Ar
*
      DATA (EN018(17,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN018(17,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN118(17,I),I=1,8)/
     + 2.304E+03,3.004E+03,3.525E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN118(17,I),I=1,8)/
     + 4.992E-03,7.950E-03,1.636E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN018(16,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN018(16,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN118(16,I),I=1,8)/
     + 2.218E+03,2.799E+03,3.228E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN118(16,I),I=1,8)/
     + 8.956E-03,1.471E-02,3.653E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN018(15,I),I=1,8)/
     + 6.772E-01,1.012E+00,1.244E+00,4.448E+00,9.371E+00,
     + 1.845E+01,3.171E+01,0.000E+00/
      DATA (CN018(15,I),I=1,8)/
     + 2.096E-01,1.038E+00,3.797E+00,1.529E+00,6.257E-01,
     + 4.984E-01,6.496E-01,0.000E+00/
      DATA (EN118(15,I),I=1,8)/
     + 7.042E+00,1.539E+02,1.739E+02,3.331E+02,4.602E+02,
     + 6.000E+02,2.284E+03,2.942E+03/
      DATA (CN118(15,I),I=1,8)/
     + 7.197E-11,1.666E-02,1.835E-02,3.660E-02,9.512E-02,
     + 1.004E-02,7.667E-03,7.175E-03/
*
      DATA (EN018(14,I),I=1,8)/
     + 1.528E-01,2.311E-01,3.719E-01,1.311E+00,2.342E+00,
     + 7.658E+00,1.890E+01,5.101E+01/
      DATA (CN018(14,I),I=1,8)/
     + 1.314E+01,1.164E+01,1.822E+01,1.437E+01,5.856E+00,
     + 1.455E+00,1.188E+00,8.888E-01/
      DATA (EN118(14,I),I=1,8)/
     + 1.517E+02,1.799E+02,2.672E+02,4.010E+02,5.161E+02,
     + 2.246E+03,2.861E+03,0.000E+00/
      DATA (CN118(14,I),I=1,8)/
     + 3.827E-03,4.327E-02,2.025E-02,1.108E-01,5.784E-02,
     + 3.055E-03,4.947E-03,0.000E+00/
*
      DATA (EN018(13,I),I=1,8)/
     + 2.465E-02,1.180E-01,5.184E-01,2.116E+00,7.426E+00,
     + 4.758E+01,0.000E+00,0.000E+00/
      DATA (CN018(13,I),I=1,8)/
     + 1.191E+02,3.740E+01,7.893E+00,7.755E+00,2.376E+00,
     + 1.252E+00,0.000E+00,0.000E+00/
      DATA (EN118(13,I),I=1,8)/
     + 1.432E+02,1.802E+02,3.129E+02,4.241E+02,8.560E+02,
     + 2.723E+03,0.000E+00,0.000E+00/
      DATA (CN118(13,I),I=1,8)/
     + 8.648E-03,7.863E-02,1.361E-01,2.749E-01,1.857E-03,
     + 5.086E-03,0.000E+00,0.000E+00/
*
      DATA (EN018(12,I),I=1,8)/
     + 1.369E-02,9.753E-02,3.720E-01,1.697E+00,9.005E+00,
     + 5.257E+01,0.000E+00,0.000E+00/
      DATA (CN018(12,I),I=1,8)/
     + 1.427E+02,8.648E+00,1.720E+01,2.790E+00,9.488E-01,
     + 8.928E-01,0.000E+00,0.000E+00/
      DATA (EN118(12,I),I=1,8)/
     + 1.337E+02,1.768E+02,2.645E+02,3.706E+02,4.920E+02,
     + 2.737E+03,0.000E+00,0.000E+00/
      DATA (CN118(12,I),I=1,8)/
     + 4.074E-03,9.792E-02,1.047E-01,3.815E-01,4.157E-02,
     + 2.467E-03,0.000E+00,0.000E+00/
*
      DATA (EN018(11,I),I=1,8)/
     + 1.279E-01,2.053E-01,4.183E-01,9.327E-01,2.582E+00,
     + 8.247E+00,2.075E+01,5.253E+01/
      DATA (CN018(11,I),I=1,8)/
     + 1.018E+00,3.242E+00,1.467E+00,8.781E-01,8.031E-01,
     + 9.952E-01,2.995E-01,7.241E-01/
      DATA (EN118(11,I),I=1,8)/
     + 1.316E+02,1.704E+02,2.261E+02,3.312E+02,4.244E+02,
     + 2.918E+03,0.000E+00,0.000E+00/
      DATA (CN118(11,I),I=1,8)/
     + 2.528E-03,6.748E-02,9.006E-02,3.482E-01,8.557E-02,
     + 1.902E-03,0.000E+00,0.000E+00/
*
      DATA (EN018(10,I),I=1,8)/
     + 2.904E-02,6.746E-02,2.791E-01,9.292E-01,2.709E+00,
     + 9.703E+00,5.889E+01,0.000E+00/
      DATA (CN018(10,I),I=1,8)/
     + 1.511E+00,1.709E+00,9.834E-01,1.160E+00,9.011E-01,
     + 3.464E-01,4.474E-01,0.000E+00/
      DATA (EN118(10,I),I=1,8)/
     + 1.083E+02,1.506E+02,1.975E+02,2.944E+02,3.828E+02,
     + 2.902E+03,0.000E+00,0.000E+00/
      DATA (CN118(10,I),I=1,8)/
     + 1.143E-03,3.108E-02,1.173E-01,3.096E-01,9.264E-02,
     + 1.055E-03,0.000E+00,0.000E+00/
*
      DATA (EN018( 9,I),I=1,8)/
     + 8.101E-02,1.506E-01,3.621E-01,8.794E-01,2.204E+00,
     + 5.522E+00,3.378E+01,7.086E+01/
      DATA (CN018( 9,I),I=1,8)/
     + 6.601E-02,5.289E-01,2.352E-01,1.699E-01,1.621E-01,
     + 1.037E-01,2.746E-02,1.816E-01/
      DATA (EN118( 9,I),I=1,8)/
     + 3.282E+01,1.079E+02,1.510E+02,2.000E+02,2.800E+02,
     + 3.618E+02,2.905E+03,0.000E+00/
      DATA (CN118( 9,I),I=1,8)/
     + 3.502E-09,1.812E-03,3.994E-02,1.013E-01,2.369E-01,
     + 5.365E-02,4.810E-04,0.000E+00/
*
      DATA (EN018( 8,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN018( 8,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN118( 8,I),I=1,8)/
     + 5.178E+01,1.050E+02,1.493E+02,1.971E+02,2.636E+02,
     + 3.285E+02,3.182E+03,0.000E+00/
      DATA (CN118( 8,I),I=1,8)/
     + 4.549E-08,1.656E-03,3.577E-02,9.786E-02,1.818E-01,
     + 4.772E-02,2.103E-04,0.000E+00/
*
* Ca
*
      DATA (EN020(19,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN020(19,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN120(19,I),I=1,8)/
     + 2.838E+03,3.703E+03,4.372E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN120(19,I),I=1,8)/
     + 4.878E-03,6.595E-03,1.294E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN020(18,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN020(18,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN120(18,I),I=1,8)/
     + 2.741E+03,3.469E+03,4.014E+03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN120(18,I),I=1,8)/
     + 8.927E-03,1.202E-02,3.095E-03,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN020(17,I),I=1,8)/
     + 1.020E+00,1.413E+00,1.706E+00,3.514E+00,9.323E+00,
     + 2.013E+01,3.644E+01,0.000E+00/
      DATA (CN020(17,I),I=1,8)/
     + 3.434E-01,2.165E+00,7.387E+00,1.264E+00,1.001E+00,
     + 6.266E-01,6.655E-01,0.000E+00/
      DATA (EN120(17,I),I=1,8)/
     + 8.467E+00,1.879E+02,2.114E+02,4.138E+02,5.737E+02,
     + 7.472E+02,2.816E+03,3.650E+03/
      DATA (CN120(17,I),I=1,8)/
     + 6.604E-11,1.959E-02,2.113E-02,3.798E-02,9.315E-02,
     + 1.144E-02,7.294E-03,6.149E-03/
*
      DATA (EN020(16,I),I=1,8)/
     + 8.471E-02,5.363E-01,1.940E+00,1.029E+01,4.632E+01,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN020(16,I),I=1,8)/
     + 1.782E+01,1.290E+00,9.889E+00,2.973E+00,1.447E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN120(16,I),I=1,8)/
     + 1.851E+02,2.180E+02,3.188E+02,4.900E+02,6.375E+02,
     + 2.725E+03,3.532E+03,0.000E+00/
      DATA (CN120(16,I),I=1,8)/
     + 4.690E-03,5.251E-02,1.908E-02,1.140E-01,7.486E-02,
     + 3.160E-03,5.002E-03,0.000E+00/
*
      DATA (EN020(15,I),I=1,8)/
     + 3.094E-01,5.315E-01,1.021E+00,2.521E+00,5.289E+00,
     + 1.222E+01,3.228E+01,6.752E+01/
      DATA (CN020(15,I),I=1,8)/
     + 2.265E+00,9.084E-01,1.211E+00,2.422E+00,1.014E+00,
     + 1.333E+00,6.903E-01,7.799E-01/
      DATA (EN120(15,I),I=1,8)/
     + 1.662E+02,2.084E+02,2.663E+02,4.425E+02,5.715E+02,
     + 2.480E+03,3.480E+03,0.000E+00/
      DATA (CN120(15,I),I=1,8)/
     + 3.281E-03,7.788E-02,4.842E-02,2.309E-01,1.847E-01,
     + 1.246E-03,4.147E-03,0.000E+00/
*
      DATA (EN020(14,I),I=1,8)/
     + 8.999E-02,1.578E-01,3.553E-01,9.498E-01,1.918E+00,
     + 6.880E+00,2.512E+01,6.881E+01/
      DATA (CN020(14,I),I=1,8)/
     + 2.054E+00,9.680E+00,6.436E+00,7.052E+00,2.593E+00,
     + 1.424E+00,6.129E-01,6.885E-01/
      DATA (EN120(14,I),I=1,8)/
     + 1.689E+02,2.193E+02,3.455E+02,4.810E+02,6.530E+02,
     + 3.390E+03,0.000E+00,0.000E+00/
      DATA (CN120(14,I),I=1,8)/
     + 7.148E-03,1.285E-01,1.304E-01,3.879E-01,3.258E-02,
     + 2.506E-03,0.000E+00,0.000E+00/
*
      DATA (EN020(13,I),I=1,8)/
     + 3.629E-02,7.742E-02,3.552E-01,2.241E+00,1.013E+01,
     + 5.460E+01,0.000E+00,0.000E+00/
      DATA (CN020(13,I),I=1,8)/
     + 2.877E+02,2.239E+02,7.412E+00,5.398E+00,1.363E+00,
     + 9.144E-01,0.000E+00,0.000E+00/
      DATA (EN120(13,I),I=1,8)/
     + 1.675E+02,2.161E+02,2.967E+02,4.335E+02,5.592E+02,
     + 3.631E+03,0.000E+00,0.000E+00/
      DATA (CN120(13,I),I=1,8)/
     + 5.499E-03,1.086E-01,1.043E-01,4.022E-01,8.258E-02,
     + 1.754E-03,0.000E+00,0.000E+00/
*
      DATA (EN020(12,I),I=1,8)/
     + 6.155E-02,1.306E-01,4.261E-01,6.894E-01,2.015E+00,
     + 6.533E+00,2.658E+01,7.151E+01/
      DATA (CN020(12,I),I=1,8)/
     + 2.911E-01,1.861E+00,5.929E+00,8.040E+00,9.527E-01,
     + 1.498E-01,1.631E-01,4.242E-01/
      DATA (EN120(12,I),I=1,8)/
     + 1.392E+02,1.945E+02,2.545E+02,3.909E+02,5.086E+02,
     + 3.617E+03,0.000E+00,0.000E+00/
      DATA (CN120(12,I),I=1,8)/
     + 1.921E-03,5.633E-02,1.341E-01,4.043E-01,1.046E-01,
     + 1.009E-03,0.000E+00,0.000E+00/
*
      DATA (EN020(11,I),I=1,8)/
     + 2.168E-01,3.170E-01,6.789E-01,1.429E+00,2.919E+00,
     + 1.793E+01,4.345E+01,8.305E+01/
      DATA (CN020(11,I),I=1,8)/
     + 4.735E-02,4.208E-01,2.109E-01,1.507E-01,1.147E-01,
     + 4.332E-02,3.865E-02,1.841E-01/
      DATA (EN120(11,I),I=1,8)/
     + 1.325E+02,1.841E+02,2.417E+02,3.617E+02,4.682E+02,
     + 3.558E+03,0.000E+00,0.000E+00/
      DATA (CN120(11,I),I=1,8)/
     + 1.547E-03,4.320E-02,1.335E-01,3.354E-01,9.950E-02,
     + 4.881E-04,0.000E+00,0.000E+00/
*
      DATA (EN020(10,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (CN020(10,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     + 0.000E+00,0.000E+00,0.000E+00/
      DATA (EN120(10,I),I=1,8)/
     + 1.143E+02,1.422E+02,1.987E+02,2.697E+02,3.609E+02,
     + 4.560E+02,3.990E+03,0.000E+00/
      DATA (CN120(10,I),I=1,8)/
     + 1.007E-04,3.923E-03,7.758E-02,1.357E-01,2.886E-01,
     + 4.961E-02,2.451E-04,0.000E+00/
*
* Fe
*
      DATA (EN026(25,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (CN026(25,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (EN126(25,I),I=1,8)/
     + 4.780E+03,6.225E+03,7.425E+03,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (CN126(25,I),I=1,8)/
     + 3.782E-03,3.826E-03,7.810E-04,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN026(24,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (CN026(24,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (EN126(24,I),I=1,8)/
     + 4.641E+03,5.797E+03,6.740E+03,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (CN126(24,I),I=1,8)/
     + 6.870E-03,5.870E-03,2.877E-03,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
*
      DATA (EN026(23,I),I=1,8)/
     + 4.701E+00,5.360E+00,1.077E+01,1.744E+01,3.465E+01,
     +  5.778E+01,0.000E+00,0.000E+00/
      DATA (CN026(23,I),I=1,8)/
     + 1.784E+00,3.378E+00,4.807E-01,1.015E+00,7.487E-01,
     +  5.162E-01,0.000E+00,0.000E+00/
      DATA (EN126(23,I),I=1,8)/
     + 1.176E+01,3.108E+02,3.470E+02,7.126E+02,9.973E+02,
     +  1.323E+03,4.780E+03,6.220E+03/
      DATA (CN126(23,I),I=1,8)/
     + 6.450E-11,2.535E-02,2.845E-02,3.870E-02,7.931E-02,
     +  1.107E-02,5.925E-03,4.564E-03/
*
      DATA (EN026(22,I),I=1,8)/
     + 4.425E-01,6.601E-01,1.185E+00,3.512E+00,8.418E+00,
     +  1.475E+01,3.681E+01,8.463E+01/
      DATA (CN026(22,I),I=1,8)/
     + 2.264E+00,1.301E+00,1.303E+01,1.948E+00,1.793E+00,
     +  2.507E+00,1.184E+00,8.119E-01/
      DATA (EN126(22,I),I=1,8)/
     + 3.020E+02,3.533E+02,4.946E+02,8.116E+02,1.101E+03,
     +  4.374E+03,5.933E+03,0.000E+00/
      DATA (CN126(22,I),I=1,8)/
     + 5.613E-03,7.835E-02,1.732E-02,1.116E-01,8.529E-02,
     +  2.572E-03,4.315E-03,0.000E+00/
*
      DATA (EN026(21,I),I=1,8)/
     + 2.099E-01,2.933E-01,5.167E-01,1.268E+00,4.111E+00,
     +  1.192E+01,3.514E+01,9.096E+01/
      DATA (CN026(21,I),I=1,8)/
     + 1.654E+01,9.708E+01,1.276E+02,4.474E+00,3.250E+00,
     +  3.283E+00,1.285E+00,9.591E-01/
      DATA (EN126(21,I),I=1,8)/
     + 1.546E+01,2.985E+02,3.624E+02,6.156E+02,8.712E+02,
     +  9.665E+02,1.253E+03,5.631E+03/
      DATA (CN126(21,I),I=1,8)/
     + 1.994E-11,1.621E-02,1.542E-01,8.845E-02,2.259E-01,
     +  7.139E-02,2.507E-02,4.608E-03/
*
      DATA (EN026(20,I),I=1,8)/
     + 1.006E-01,2.570E-01,6.130E-01,2.380E+00,5.742E+00,
     +  2.005E+01,8.853E+01,0.000E+00/
      DATA (CN026(20,I),I=1,8)/
     + 3.880E+01,3.773E+01,1.624E+01,1.719E+01,7.479E+00,
     +  1.585E+00,8.681E-01,0.000E+00/
      DATA (EN126(20,I),I=1,8)/
     + 2.355E+02,3.131E+02,3.749E+02,6.582E+02,9.025E+02,
     +  1.279E+03,5.776E+03,0.000E+00/
      DATA (CN126(20,I),I=1,8)/
     + 2.042E-04,4.745E-02,1.767E-01,1.763E-01,3.120E-01,
     +  1.653E-02,2.367E-03,0.000E+00/
*
      DATA (EN026(19,I),I=1,8)/
     + 4.477E-03,4.785E-02,2.833E-01,1.088E+00,3.041E+00,
     +  1.498E+01,7.697E+01,0.000E+00/
      DATA (CN026(19,I),I=1,8)/
     + 2.267E+03,1.368E+02,2.573E+01,1.709E+01,6.707E+00,
     +  2.544E+00,9.987E-01,0.000E+00/
      DATA (EN126(19,I),I=1,8)/
     + 2.812E+02,3.610E+02,4.937E+02,7.560E+02,9.965E+02,
     +  6.087E+03,0.000E+00,0.000E+00/
      DATA (CN126(19,I),I=1,8)/
     + 1.143E-02,2.079E-01,1.079E-01,4.170E-01,1.449E-01,
     +  9.867E-04,0.000E+00,0.000E+00/
*
      DATA (EN026(18,I),I=1,8)/
     + 6.682E-03,6.191E-02,2.301E-01,1.291E+00,4.986E+00,
     +  2.301E+01,9.812E+01,0.000E+00/
      DATA (CN026(18,I),I=1,8)/
     + 2.096E+02,2.711E+01,1.965E+00,2.462E+00,1.051E+00,
     +  4.864E-01,5.003E-01,0.000E+00/
      DATA (EN126(18,I),I=1,8)/
     + 2.516E+02,3.424E+02,4.388E+02,7.093E+02,9.376E+02,
     +  5.988E+03,0.000E+00,0.000E+00/
      DATA (CN126(18,I),I=1,8)/
     + 5.668E-03,1.596E-01,1.647E-01,4.558E-01,1.693E-01,
     +  6.548E-04,0.000E+00,0.000E+00/
*
      DATA (EN026(17,I),I=1,8)/
     + 2.014E-01,3.854E-01,5.807E-01,2.135E+00,5.978E+00,
     +  1.769E+01,5.147E+01,1.206E+02/
      DATA (CN026(17,I),I=1,8)/
     + 1.732E-01,4.610E-01,1.178E+00,3.114E-01,2.005E-01,
     +  1.745E-01,7.209E-02,1.963E-01/
      DATA (EN126(17,I),I=1,8)/
     + 2.417E+02,3.356E+02,4.346E+02,6.875E+02,9.060E+02,
     +  6.017E+03,0.000E+00,0.000E+00/
      DATA (CN126(17,I),I=1,8)/
     + 4.545E-03,1.409E-01,1.829E-01,4.703E-01,1.461E-01,
     +  3.317E-04,0.000E+00,0.000E+00/
*
      DATA (EN026(16,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (CN026(16,I),I=1,8)/
     + 0.000E+00,0.000E+00,0.000E+00,0.000E+00,0.000E+00,
     +  0.000E+00,0.000E+00,0.000E+00/
      DATA (EN126(16,I),I=1,8)/
     + 2.307E+02,3.245E+02,4.177E+02,6.600E+02,8.621E+02,
     +  6.898E+03,0.000E+00,0.000E+00/
      DATA (CN126(16,I),I=1,8)/
     + 2.820E-03,8.001E-02,1.645E-01,4.476E-01,1.503E-01,
     +  2.775E-04,0.000E+00,0.000E+00/
*
      if(nz.eq.10)then
        DO I=1,8
          E(I)=EN010(NION,I)
          C(I)=1.0E-10*CN010(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN110(NION,I)
          C(I)=1.0E-10*CN110(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
      elseif(nz.eq.12)then
        DO I=1,8
          E(I)=EN012(NION,I)
          C(I)=1.0E-10*CN012(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN112(NION,I)
          C(I)=1.0E-10*CN112(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
      elseif(nz.eq.14)then
        DO I=1,8
          E(I)=EN014(NION,I)
          C(I)=1.0E-10*CN014(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN114(NION,I)
          C(I)=1.0E-10*CN114(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
      elseif(nz.eq.16)then
        DO I=1,8
          E(I)=EN016(NION,I)
          C(I)=1.0E-10*CN016(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN116(NION,I)
          C(I)=1.0E-10*CN116(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
      elseif(nz.eq.18)then
        DO I=1,8
          E(I)=EN018(NION,I)
          C(I)=1.0E-10*CN018(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN118(NION,I)
          C(I)=1.0E-10*CN118(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
      elseif(nz.eq.20)then
        DO I=1,8
          E(I)=EN020(NION,I)
          C(I)=1.0E-10*CN020(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN120(NION,I)
          C(I)=1.0E-10*CN120(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
      elseif(nz.eq.26)then
        DO I=1,8
          E(I)=EN026(NION,I)
          C(I)=1.0E-10*CN026(NION,I)
        ENDDO
        DR_NION_0=DR_GU(NION)
        DO I=1,8
          E(I)=EN126(NION,I)
          C(I)=1.0E-10*CN126(NION,I)
        ENDDO
        DR_NION_1=DR_GU(NION)
*
      endif
*
      RETURN
      END

      FUNCTION DR_GU(NION)
*
*August 2003: new DR coefficients for Fe 17-25 for deltan=0
*
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      COMMON/TEM32/TEM32
      COMMON/GU_E_C/ E(8),C(8)
*
      DR=0.
      DO I=1,8
        ET=E(I)/TEV
        EX=EXP(-ET) 
        DR=DR+C(I)*ET**(1.5)*EX
      ENDDO
      DR_GU=DR
*
      RETURN
      END

      FUNCTION DIR(T01,T11,A1,B1)
      COMMON/SQ3T/SQ3
      COMMON/ANN/CN,ANE,TE,Y,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      T0=T01
      T1=T11
      A=A1
      B=B1
C     CALCULATE DIELECTRONIC RECOMBINATION COEFFICIENTS IN THE BURGES
C     METHOD (GOOD FOR HIGH TEMPERATURES) USING PEQUINOX AND ALDROVANDI
C     1973 TABULATIONS
      T0Y=T0/Y
      IF(T0Y.GT.30.)T0Y=30.
      T1Y=T1/Y
      IF(T1Y.GT.30.)T1Y=30.
      E1=EXP(-T0Y)/SQ3
      E2=EXP(-T1Y)*B
      DIR=1.0E-8*A*E1*(1.+E2)
      RETURN
      END
 
      FUNCTION DIRAM ( E1, E2, C1, C2 )
*
*     Dielectronic recombination coefficients ala Arnoud and
*      Raymond, 1992, ApJ 398, 394 with two coefficients
*
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      COMMON/TEM32/TEM32
*
      E1V=E1/TEV
      E2V=E2/TEV
      DIRAM = TEM32 * ( C1*EXP(-E1V) + C2*EXP(-E2V) )
      RETURN
      END
                  
      FUNCTION DIRAM3 ( E1,E2,E3,C1,C2,C3)
*
*     Dielectronic recombination coefficients with three coefficient
*
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      COMMON/TEM32/TEM32
*
      E1V=E1/TEV
      E2V=E2/TEV
      E3V=E3/TEV
      DIRAM3 = TEM32 *
     +   ( C1*EXP(-E1V)+C2*EXP(-E2V)+C3*EXP(-E3V)  )
      RETURN
      END
 
      FUNCTION DIRAM4 ( E1,E2,E3,E4, C1,C2,C3,C4 )
*
*     Dielectronic recombination coefficients ala Arnoud and
*      Raymond, 1992, ApJ 398, 394 with four coefficients
*
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      COMMON/TEM32/TEM32
*
      E1V=E1/TEV
      E2V=E2/TEV
      E3V=E3/TEV
      E4V=E4/TEV
      DIRAM4 = TEM32 *
     +   ( C1*EXP(-E1V)+C2*EXP(-E2V)+C3*EXP(-E3V)+C4*EXP(-E4V) )
      RETURN
      END   



      FUNCTION RB(T,Z1,A1,B1,C1)
      Z=Z1
      A=A1
      B=B1
      C=C1
      D=5.197E-14
      TT=T/15.789
      Y=Z**2/TT
      S=A+B*ALOG(Y)+C*Y**(-0.333)
      RB=D*Z*SQRT(Y)*S
      RETURN
      END        

*
      FUNCTION AD(A1,B1,C1,D1,F1)
      COMMON/SQ3T/SQ3
      COMMON/ANN/CN,ANE,TE,TEE,TEV,SQTE,TE05,TE10,TE15,TE20,TE25,TE30,
     ATE35,TE40,TE45,TE50,TE55,TE60,TE65,TE70,TE75,TE80,TE85,
     ATE90,TE95
      A=A1
      B=B1
      C=C1
      D=D1
      F=F1
      T=TEE
C     CALCULATE DIELECTRONIC RECOMBINATION COEFFICIENTS AT
C     LOW TEMPERATURES , AFTER STOREY AND NUSSBAUMER 1983
C     TEE IS ELEC. TEMP IN 1E4
C
      tmax=5.0
*
      if(tee.lt.tmax)then
        Z1=EXP(-F/T)
        A2=A/T+B+C*T+D*T**2
        AD=1.0E-12*Z1*A2/SQ3
      else
        t=tmax
        sq3max=tmax*sqrt(tmax)
        Z1=EXP(-F/T)
        A2=A/T+B+C*T+D*T**2
        AD1=1.0E-12*Z1*A2/SQ3max
        ad=ad1*(tmax/tee)**3
      endif
      RETURN
      END          



      subroutine rrfit(iz,in,t,r)
******************************************************************************
*** Version 3a. August 19, 1996.
*** This subroutine calculates rates of radiative recombination for all ions
*** of all elements from H through Zn by use of the following fits:
*** H-like, He-like, Li-like, Na-like - Verner & Ferland, 1996, ApJS, 103, 467
*** Other ions of C, N, O, Ne - Pequignot et al. 1991, A&A, 251, 680,
***    refitted by Verner & Ferland formula to ensure correct asymptotes
*** Fe XIV-XV and Fe XVII-XXIII - Arnaud & Raymond, 1992, ApJ, 398, 394
*** Other ions of Mg, Si, S, Ar, Ca, Fe, Ni - 
***                      - Shull & Van Steenberg, 1982, ApJS, 48, 95
*** Other ions of Na, Al - Landini & Monsignori Fossi, 1990, A&AS, 82, 229
*** Other ions of F, P, Cl, K, Ti, Cr, Mn, Co (excluding Ti I-II, Cr I-IV,
*** Mn I-V, Co I)        - Landini & Monsignori Fossi, 1991, A&AS, 91, 183
*** All other species    - interpolations of the power-law fits
*** Input parameters:  iz - atomic number 
***                    in - number of electrons from 1 to iz 
***                    t  - temperature, K
*** Output parameter:  r  - rate coefficient, cm^3 s^(-1)
******************************************************************************
      implicit none
      double precision rrec,rnew,fe,t,r,tt
      integer iz,in
      common/rrec/rrec(2,30,30)
      common/rnew/rnew(4,30,30)
      common/ferr/fe(3,13)
      r=0.0

!      if(iz.lt.1.or.iz.gt.30)then
!         write(6,'('' rrfit called with insane atomic number, ='',i4
!     &   )') iz
!         stop
!      endif
!      if(in.lt.1.or.in.gt.iz)then
!         write(6,'('' rrfit called with insane number elec ='',i4
!     &   )') in
!         print *,'atomin num is: ',iz
!        stop
!      endif
      if(in.le.3.or.in.eq.11.or.(iz.gt.5.and.iz.lt.9).or.iz.eq.10)then
         tt=sqrt(t/rnew(3,iz,in))
         r=rnew(1,iz,in)/(tt*(tt+1.0)**(1.0-rnew(2,iz,in))*
     &        (1.0+sqrt(t/rnew(4,iz,in)))**(1.0+rnew(2,iz,in)))
      else
         tt=t*1.0e-04
         if(iz.eq.26.and.in.le.13)then
            r=fe(1,in)/tt**(fe(2,in)+fe(3,in)*log10(tt))
         else
            r=rrec(1,iz,in)/tt**rrec(2,iz,in)
         endif
      endif
      return 
      end
*********************************************
      block data radrec
      common/rrec/rrec(2,30,30)
      common/rnew/rnew(4,30,30)
      common/ferr/fe(3,13)
      data(rrec(i, 4, 4),i=1,2)/4.500e-13,0.6480/
      data(rrec(i, 5, 4),i=1,2)/2.680e-12,0.7250/
      data(rrec(i, 6, 4),i=1,2)/4.900e-12,0.8030/
      data(rrec(i, 7, 4),i=1,2)/9.400e-12,0.7650/
      data(rrec(i, 8, 4),i=1,2)/1.590e-11,0.7590/
      data(rrec(i, 9, 4),i=1,2)/1.919e-11,0.7849/
      data(rrec(i,10, 4),i=1,2)/2.800e-11,0.7710/
      data(rrec(i,11, 4),i=1,2)/4.030e-11,0.7873/
      data(rrec(i,12, 4),i=1,2)/6.830e-11,0.7650/
      data(rrec(i,13, 4),i=1,2)/8.343e-11,0.8291/
      data(rrec(i,14, 4),i=1,2)/1.430e-10,0.8230/
      data(rrec(i,15, 4),i=1,2)/1.460e-10,0.8391/
      data(rrec(i,16, 4),i=1,2)/2.000e-10,0.8060/
      data(rrec(i,17, 4),i=1,2)/2.091e-10,0.8691/
      data(rrec(i,18, 4),i=1,2)/3.070e-10,0.8190/
      data(rrec(i,19, 4),i=1,2)/3.210e-10,0.8934/
      data(rrec(i,20, 4),i=1,2)/4.610e-10,0.8330/
      data(rrec(i,21, 4),i=1,2)/4.870e-10,0.8060/
      data(rrec(i,22, 4),i=1,2)/5.139e-10,0.7781/
      data(rrec(i,23, 4),i=1,2)/5.850e-10,0.7570/
      data(rrec(i,24, 4),i=1,2)/6.556e-10,0.7359/
      data(rrec(i,25, 4),i=1,2)/7.238e-10,0.7242/
      data(rrec(i,27, 4),i=1,2)/8.404e-10,0.7167/
      data(rrec(i,28, 4),i=1,2)/1.360e-09,0.8420/
      data(rrec(i,29, 4),i=1,2)/1.880e-09,0.8420/
      data(rrec(i,30, 4),i=1,2)/2.400e-09,0.8420/
      data(rrec(i, 5, 5),i=1,2)/4.600e-13,0.6360/
      data(rrec(i, 6, 5),i=1,2)/2.300e-12,0.6450/
      data(rrec(i, 7, 5),i=1,2)/5.000e-12,0.6760/
      data(rrec(i, 8, 5),i=1,2)/9.600e-12,0.6700/
      data(rrec(i, 9, 5),i=1,2)/1.558e-11,0.6816/
      data(rrec(i,10, 5),i=1,2)/2.300e-11,0.7040/
      data(rrec(i,11, 5),i=1,2)/3.253e-11,0.7075/
      data(rrec(i,12, 5),i=1,2)/4.600e-11,0.7110/
      data(rrec(i,13, 5),i=1,2)/5.951e-11,0.7125/
      data(rrec(i,14, 5),i=1,2)/7.700e-11,0.7140/
      data(rrec(i,15, 5),i=1,2)/1.042e-10,0.7330/
      data(rrec(i,16, 5),i=1,2)/1.400e-10,0.7550/
      data(rrec(i,17, 5),i=1,2)/1.760e-10,0.7682/
      data(rrec(i,18, 5),i=1,2)/2.140e-10,0.7740/
      data(rrec(i,19, 5),i=1,2)/2.629e-10,0.7772/
      data(rrec(i,20, 5),i=1,2)/3.240e-10,0.7800/
      data(rrec(i,21, 5),i=1,2)/3.970e-10,0.7840/
      data(rrec(i,22, 5),i=1,2)/4.700e-10,0.7873/
      data(rrec(i,23, 5),i=1,2)/5.500e-10,0.7910/
      data(rrec(i,24, 5),i=1,2)/6.301e-10,0.7952/
      data(rrec(i,25, 5),i=1,2)/7.058e-10,0.7982/
      data(rrec(i,27, 5),i=1,2)/8.252e-10,0.8004/
      data(rrec(i,28, 5),i=1,2)/8.710e-10,0.8000/
      data(rrec(i,29, 5),i=1,2)/9.170e-10,0.8000/
      data(rrec(i,30, 5),i=1,2)/9.630e-10,0.7990/
      data(rrec(i, 6, 6),i=1,2)/4.700e-13,0.6240/
      data(rrec(i, 7, 6),i=1,2)/2.200e-12,0.6390/
      data(rrec(i, 8, 6),i=1,2)/5.100e-12,0.6600/
      data(rrec(i, 9, 6),i=1,2)/9.171e-12,0.6757/
      data(rrec(i,10, 6),i=1,2)/1.500e-11,0.6840/
      data(rrec(i,11, 6),i=1,2)/2.191e-11,0.6875/
      data(rrec(i,12, 6),i=1,2)/3.200e-11,0.6910/
      data(rrec(i,13, 6),i=1,2)/4.308e-11,0.6970/
      data(rrec(i,14, 6),i=1,2)/5.800e-11,0.7030/
      data(rrec(i,15, 6),i=1,2)/7.316e-11,0.7027/
      data(rrec(i,16, 6),i=1,2)/9.200e-11,0.7140/
      data(rrec(i,17, 6),i=1,2)/1.198e-10,0.7508/
      data(rrec(i,18, 6),i=1,2)/1.580e-10,0.7900/
      data(rrec(i,19, 6),i=1,2)/2.048e-10,0.8032/
      data(rrec(i,20, 6),i=1,2)/2.600e-10,0.8000/
      data(rrec(i,21, 6),i=1,2)/3.280e-10,0.7990/
      data(rrec(i,22, 6),i=1,2)/3.966e-10,0.7973/
      data(rrec(i,23, 6),i=1,2)/4.760e-10,0.8000/
      data(rrec(i,24, 6),i=1,2)/5.547e-10,0.8027/
      data(rrec(i,25, 6),i=1,2)/6.313e-10,0.8058/
      data(rrec(i,27, 6),i=1,2)/7.503e-10,0.8085/
      data(rrec(i,28, 6),i=1,2)/7.940e-10,0.8080/
      data(rrec(i,29, 6),i=1,2)/8.380e-10,0.8080/
      data(rrec(i,30, 6),i=1,2)/8.810e-10,0.8070/
      data(rrec(i, 7, 7),i=1,2)/4.100e-13,0.6080/
      data(rrec(i, 8, 7),i=1,2)/2.000e-12,0.6460/
      data(rrec(i, 9, 7),i=1,2)/5.231e-12,0.6615/
      data(rrec(i,10, 7),i=1,2)/9.100e-12,0.6680/
      data(rrec(i,11, 7),i=1,2)/1.447e-11,0.6814/
      data(rrec(i,12, 7),i=1,2)/2.300e-11,0.6950/
      data(rrec(i,13, 7),i=1,2)/3.145e-11,0.6915/
      data(rrec(i,14, 7),i=1,2)/4.300e-11,0.6880/
      data(rrec(i,15, 7),i=1,2)/5.659e-11,0.7023/
      data(rrec(i,16, 7),i=1,2)/7.400e-11,0.7160/
      data(rrec(i,17, 7),i=1,2)/9.561e-11,0.7102/
      data(rrec(i,18, 7),i=1,2)/1.230e-10,0.7020/
      data(rrec(i,19, 7),i=1,2)/1.587e-10,0.7105/
      data(rrec(i,20, 7),i=1,2)/2.040e-10,0.7300/
      data(rrec(i,21, 7),i=1,2)/2.630e-10,0.7490/
      data(rrec(i,22, 7),i=1,2)/3.220e-10,0.7683/
      data(rrec(i,23, 7),i=1,2)/3.950e-10,0.7830/
      data(rrec(i,24, 7),i=1,2)/4.671e-10,0.7967/
      data(rrec(i,25, 7),i=1,2)/5.407e-10,0.8058/
      data(rrec(i,27, 7),i=1,2)/6.611e-10,0.8121/
      data(rrec(i,28, 7),i=1,2)/7.080e-10,0.8110/
      data(rrec(i,29, 7),i=1,2)/7.550e-10,0.8100/
      data(rrec(i,30, 7),i=1,2)/8.020e-10,0.8090/
      data(rrec(i, 8, 8),i=1,2)/3.100e-13,0.6780/
      data(rrec(i, 9, 8),i=1,2)/1.344e-12,0.6708/
      data(rrec(i,10, 8),i=1,2)/4.400e-12,0.6750/
      data(rrec(i,11, 8),i=1,2)/7.849e-12,0.6952/
      data(rrec(i,12, 8),i=1,2)/1.400e-11,0.7160/
      data(rrec(i,13, 8),i=1,2)/2.049e-11,0.7090/
      data(rrec(i,14, 8),i=1,2)/3.000e-11,0.7020/
      data(rrec(i,15, 8),i=1,2)/4.125e-11,0.6965/
      data(rrec(i,16, 8),i=1,2)/5.500e-11,0.7110/
      data(rrec(i,17, 8),i=1,2)/7.280e-11,0.7518/
      data(rrec(i,18, 8),i=1,2)/9.550e-11,0.7930/
      data(rrec(i,19, 8),i=1,2)/1.235e-10,0.8052/
      data(rrec(i,20, 8),i=1,2)/1.580e-10,0.8000/
      data(rrec(i,21, 8),i=1,2)/2.060e-10,0.7990/
      data(rrec(i,22, 8),i=1,2)/2.537e-10,0.7977/
      data(rrec(i,23, 8),i=1,2)/3.190e-10,0.8020/
      data(rrec(i,24, 8),i=1,2)/3.844e-10,0.8071/
      data(rrec(i,25, 8),i=1,2)/4.564e-10,0.8124/
      data(rrec(i,27, 8),i=1,2)/5.842e-10,0.8168/
      data(rrec(i,28, 8),i=1,2)/6.380e-10,0.8160/
      data(rrec(i,29, 8),i=1,2)/6.920e-10,0.8150/
      data(rrec(i,30, 8),i=1,2)/7.460e-10,0.8140/
      data(rrec(i, 9, 9),i=1,2)/6.273e-13,0.6798/
      data(rrec(i,10, 9),i=1,2)/1.500e-12,0.6930/
      data(rrec(i,11, 9),i=1,2)/3.399e-12,0.7054/
      data(rrec(i,12, 9),i=1,2)/7.700e-12,0.7180/
      data(rrec(i,13, 9),i=1,2)/1.275e-11,0.7170/
      data(rrec(i,14, 9),i=1,2)/2.110e-11,0.7160/
      data(rrec(i,15, 9),i=1,2)/2.975e-11,0.6945/
      data(rrec(i,16, 9),i=1,2)/4.000e-11,0.6960/
      data(rrec(i,17, 9),i=1,2)/5.281e-11,0.7491/
      data(rrec(i,18, 9),i=1,2)/6.920e-11,0.8110/
      data(rrec(i,19, 9),i=1,2)/9.044e-11,0.8251/
      data(rrec(i,20, 9),i=1,2)/1.180e-10,0.8100/
      data(rrec(i,21, 9),i=1,2)/1.580e-10,0.8040/
      data(rrec(i,22, 9),i=1,2)/1.983e-10,0.7980/
      data(rrec(i,23, 9),i=1,2)/2.570e-10,0.8040/
      data(rrec(i,24, 9),i=1,2)/3.154e-10,0.8101/
      data(rrec(i,25, 9),i=1,2)/3.837e-10,0.8183/
      data(rrec(i,27, 9),i=1,2)/5.147e-10,0.8253/
      data(rrec(i,28, 9),i=1,2)/5.750e-10,0.8240/
      data(rrec(i,29, 9),i=1,2)/6.350e-10,0.8230/
      data(rrec(i,30, 9),i=1,2)/6.960e-10,0.8210/
      data(rrec(i,10,10),i=1,2)/2.200e-13,0.7590/
      data(rrec(i,11,10),i=1,2)/8.775e-13,0.7467/
      data(rrec(i,12,10),i=1,2)/3.500e-12,0.7340/
      data(rrec(i,13,10),i=1,2)/6.481e-12,0.7345/
      data(rrec(i,14,10),i=1,2)/1.200e-11,0.7350/
      data(rrec(i,15,10),i=1,2)/1.834e-11,0.7285/
      data(rrec(i,16,10),i=1,2)/2.700e-11,0.7330/
      data(rrec(i,17,10),i=1,2)/3.711e-11,0.7641/
      data(rrec(i,18,10),i=1,2)/4.900e-11,0.8010/
      data(rrec(i,19,10),i=1,2)/6.444e-11,0.8175/
      data(rrec(i,20,10),i=1,2)/8.510e-11,0.8200/
      data(rrec(i,21,10),i=1,2)/1.170e-10,0.8220/
      data(rrec(i,22,10),i=1,2)/1.494e-10,0.8242/
      data(rrec(i,23,10),i=1,2)/2.010e-10,0.8280/
      data(rrec(i,24,10),i=1,2)/2.525e-10,0.8311/
      data(rrec(i,25,10),i=1,2)/3.177e-10,0.8341/
      data(rrec(i,27,10),i=1,2)/4.552e-10,0.8364/
      data(rrec(i,28,10),i=1,2)/5.250e-10,0.8360/
      data(rrec(i,29,10),i=1,2)/5.950e-10,0.8360/
      data(rrec(i,30,10),i=1,2)/6.650e-10,0.8350/
      data(rrec(i,12,12),i=1,2)/1.400e-13,0.8550/
      data(rrec(i,13,12),i=1,2)/7.197e-13,0.7697/
      data(rrec(i,14,12),i=1,2)/3.700e-12,0.6930/
      data(rrec(i,15,12),i=1,2)/7.980e-12,0.6829/
      data(rrec(i,16,12),i=1,2)/1.200e-11,0.7010/
      data(rrec(i,17,12),i=1,2)/1.800e-11,0.7232/
      data(rrec(i,18,12),i=1,2)/2.690e-11,0.7440/
      data(rrec(i,19,12),i=1,2)/3.748e-11,0.7628/
      data(rrec(i,20,12),i=1,2)/5.040e-11,0.7800/
      data(rrec(i,21,12),i=1,2)/7.240e-11,0.7950/
      data(rrec(i,22,12),i=1,2)/9.440e-11,0.8107/
      data(rrec(i,23,12),i=1,2)/1.350e-10,0.8220/
      data(rrec(i,24,12),i=1,2)/1.751e-10,0.8340/
      data(rrec(i,25,12),i=1,2)/2.298e-10,0.8417/
      data(rrec(i,27,12),i=1,2)/3.461e-10,0.8469/
      data(rrec(i,28,12),i=1,2)/4.030e-10,0.8460/
      data(rrec(i,29,12),i=1,2)/4.600e-10,0.8450/
      data(rrec(i,30,12),i=1,2)/5.170e-10,0.8440/
      data(rrec(i,13,13),i=1,2)/3.980e-13,0.8019/
      data(rrec(i,14,13),i=1,2)/1.000e-12,0.7860/
      data(rrec(i,15,13),i=1,2)/2.558e-12,0.7629/
      data(rrec(i,16,13),i=1,2)/5.700e-12,0.7550/
      data(rrec(i,17,13),i=1,2)/1.011e-11,0.7703/
      data(rrec(i,18,13),i=1,2)/1.580e-11,0.7930/
      data(rrec(i,19,13),i=1,2)/2.448e-11,0.8052/
      data(rrec(i,20,13),i=1,2)/3.760e-11,0.8100/
      data(rrec(i,21,13),i=1,2)/5.870e-11,0.8150/
      data(rrec(i,22,13),i=1,2)/7.972e-11,0.8206/
      data(rrec(i,23,13),i=1,2)/1.130e-10,0.8270/
      data(rrec(i,24,13),i=1,2)/1.470e-10,0.8325/
      data(rrec(i,25,13),i=1,2)/1.908e-10,0.8372/
      data(rrec(i,27,13),i=1,2)/2.976e-10,0.8406/
      data(rrec(i,28,13),i=1,2)/3.630e-10,0.8400/
      data(rrec(i,29,13),i=1,2)/4.280e-10,0.8390/
      data(rrec(i,30,13),i=1,2)/4.940e-10,0.8390/
      data(rrec(i,14,14),i=1,2)/5.900e-13,0.6010/
      data(rrec(i,15,14),i=1,2)/1.294e-12,0.6766/
      data(rrec(i,16,14),i=1,2)/2.700e-12,0.7450/
      data(rrec(i,17,14),i=1,2)/5.165e-12,0.7893/
      data(rrec(i,18,14),i=1,2)/9.120e-12,0.8110/
      data(rrec(i,19,14),i=1,2)/1.513e-11,0.8186/
      data(rrec(i,20,14),i=1,2)/2.400e-11,0.8200/
      data(rrec(i,21,14),i=1,2)/3.960e-11,0.8220/
      data(rrec(i,22,14),i=1,2)/5.518e-11,0.8245/
      data(rrec(i,23,14),i=1,2)/8.370e-11,0.8280/
      data(rrec(i,24,14),i=1,2)/1.123e-10,0.8313/
      data(rrec(i,25,14),i=1,2)/1.525e-10,0.8342/
      data(rrec(i,26,14),i=1,2)/2.000e-10,0.8360/
      data(rrec(i,27,14),i=1,2)/2.537e-10,0.8364/
      data(rrec(i,28,14),i=1,2)/3.160e-10,0.8360/
      data(rrec(i,29,14),i=1,2)/3.780e-10,0.8360/
      data(rrec(i,30,14),i=1,2)/4.410e-10,0.8350/
      data(rrec(i,15,15),i=1,2)/9.761e-13,0.6209/
      data(rrec(i,16,15),i=1,2)/1.800e-12,0.6860/
      data(rrec(i,17,15),i=1,2)/3.320e-12,0.7579/
      data(rrec(i,18,15),i=1,2)/6.030e-12,0.8120/
      data(rrec(i,19,15),i=1,2)/1.063e-11,0.8269/
      data(rrec(i,20,15),i=1,2)/1.800e-11,0.8200/
      data(rrec(i,21,15),i=1,2)/3.130e-11,0.8180/
      data(rrec(i,22,15),i=1,2)/4.451e-11,0.8153/
      data(rrec(i,23,15),i=1,2)/6.830e-11,0.8200/
      data(rrec(i,24,15),i=1,2)/9.206e-11,0.8246/
      data(rrec(i,25,15),i=1,2)/1.250e-10,0.8302/
      data(rrec(i,26,15),i=1,2)/1.640e-10,0.8340/
      data(rrec(i,27,15),i=1,2)/2.093e-10,0.8349/
      data(rrec(i,28,15),i=1,2)/2.630e-10,0.8340/
      data(rrec(i,29,15),i=1,2)/3.170e-10,0.8330/
      data(rrec(i,30,15),i=1,2)/3.700e-10,0.8320/
      data(rrec(i,16,16),i=1,2)/4.100e-13,0.6300/
      data(rrec(i,17,16),i=1,2)/1.248e-12,0.7663/
      data(rrec(i,18,16),i=1,2)/3.230e-12,0.8690/
      data(rrec(i,19,16),i=1,2)/6.384e-12,0.8790/
      data(rrec(i,20,16),i=1,2)/1.070e-11,0.8400/
      data(rrec(i,21,16),i=1,2)/1.920e-11,0.8210/
      data(rrec(i,22,16),i=1,2)/2.765e-11,0.8012/
      data(rrec(i,23,16),i=1,2)/4.650e-11,0.8060/
      data(rrec(i,24,16),i=1,2)/6.539e-11,0.8099/
      data(rrec(i,25,16),i=1,2)/9.539e-11,0.8202/
      data(rrec(i,26,16),i=1,2)/1.330e-10,0.8280/
      data(rrec(i,27,16),i=1,2)/1.769e-10,0.8299/
      data(rrec(i,28,16),i=1,2)/2.290e-10,0.8280/
      data(rrec(i,29,16),i=1,2)/2.810e-10,0.8260/
      data(rrec(i,30,16),i=1,2)/3.330e-10,0.8240/
      data(rrec(i,17,17),i=1,2)/1.010e-12,0.7380/
      data(rrec(i,18,17),i=1,2)/1.950e-12,0.7520/
      data(rrec(i,19,17),i=1,2)/3.766e-12,0.7662/
      data(rrec(i,20,17),i=1,2)/7.080e-12,0.7800/
      data(rrec(i,21,17),i=1,2)/1.430e-11,0.7920/
      data(rrec(i,22,17),i=1,2)/2.152e-11,0.8038/
      data(rrec(i,23,17),i=1,2)/3.740e-11,0.8120/
      data(rrec(i,24,17),i=1,2)/5.335e-11,0.8207/
      data(rrec(i,25,17),i=1,2)/7.807e-11,0.8260/
      data(rrec(i,26,17),i=1,2)/1.090e-10,0.8290/
      data(rrec(i,27,17),i=1,2)/1.459e-10,0.8296/
      data(rrec(i,28,17),i=1,2)/1.910e-10,0.8290/
      data(rrec(i,29,17),i=1,2)/2.360e-10,0.8280/
      data(rrec(i,30,17),i=1,2)/2.810e-10,0.8280/
      data(rrec(i,18,18),i=1,2)/3.770e-13,0.6510/
      data(rrec(i,19,18),i=1,2)/1.304e-12,0.6753/
      data(rrec(i,20,18),i=1,2)/3.960e-12,0.7000/
      data(rrec(i,21,18),i=1,2)/1.130e-11,0.7240/
      data(rrec(i,22,18),i=1,2)/1.857e-11,0.7484/
      data(rrec(i,23,18),i=1,2)/3.170e-11,0.7680/
      data(rrec(i,24,18),i=1,2)/4.479e-11,0.7883/
      data(rrec(i,25,18),i=1,2)/6.106e-11,0.8020/
      data(rrec(i,26,18),i=1,2)/8.130e-11,0.8100/
      data(rrec(i,27,18),i=1,2)/1.098e-10,0.8118/
      data(rrec(i,28,18),i=1,2)/1.500e-10,0.8100/
      data(rrec(i,29,18),i=1,2)/1.900e-10,0.8080/
      data(rrec(i,30,18),i=1,2)/2.300e-10,0.8060/
      data(rrec(i,19,19),i=1,2)/2.762e-13,0.8023/
      data(rrec(i,20,19),i=1,2)/6.780e-13,0.8000/
      data(rrec(i,21,19),i=1,2)/2.330e-12,0.7980/
      data(rrec(i,22,19),i=1,2)/3.983e-12,0.7955/
      data(rrec(i,23,19),i=1,2)/1.150e-11,0.7940/
      data(rrec(i,24,19),i=1,2)/1.906e-11,0.7919/
      data(rrec(i,25,19),i=1,2)/3.620e-11,0.7907/
      data(rrec(i,26,19),i=1,2)/6.050e-11,0.7900/
      data(rrec(i,27,19),i=1,2)/8.818e-11,0.7898/
      data(rrec(i,28,19),i=1,2)/1.190e-10,0.7900/
      data(rrec(i,29,19),i=1,2)/1.500e-10,0.7900/
      data(rrec(i,30,19),i=1,2)/1.810e-10,0.7900/
      data(rrec(i,20,20),i=1,2)/1.120e-13,0.9000/
      data(rrec(i,21,20),i=1,2)/6.540e-13,0.8670/
      data(rrec(i,22,20),i=1,2)/1.196e-12,0.8344/
      data(rrec(i,23,20),i=1,2)/5.330e-12,0.8090/
      data(rrec(i,24,20),i=1,2)/9.471e-12,0.7846/
      data(rrec(i,25,20),i=1,2)/2.169e-11,0.7683/
      data(rrec(i,26,20),i=1,2)/4.120e-11,0.7590/
      data(rrec(i,27,20),i=1,2)/6.409e-11,0.7570/
      data(rrec(i,28,20),i=1,2)/8.910e-11,0.7590/
      data(rrec(i,29,20),i=1,2)/1.140e-10,0.7610/
      data(rrec(i,30,20),i=1,2)/1.390e-10,0.7630/
      data(rrec(i,21,21),i=1,2)/1.170e-13,0.8980/
      data(rrec(i,22,21),i=1,2)/5.330e-12,0.8640/
      data(rrec(i,23,21),i=1,2)/1.060e-11,0.8300/
      data(rrec(i,24,21),i=1,2)/1.580e-11,0.7960/
      data(rrec(i,25,21),i=1,2)/2.100e-11,0.7620/
      data(rrec(i,26,21),i=1,2)/2.620e-11,0.7280/
      data(rrec(i,27,21),i=1,2)/2.822e-11,0.7280/
      data(rrec(i,28,21),i=1,2)/3.040e-11,0.7280/
      data(rrec(i,29,21),i=1,2)/3.260e-11,0.7280/
      data(rrec(i,30,21),i=1,2)/3.480e-11,0.7280/
      data(rrec(i,22,22),i=1,2)/1.220e-13,0.8970/
      data(rrec(i,23,22),i=1,2)/3.870e-12,0.8480/
      data(rrec(i,24,22),i=1,2)/7.610e-12,0.7980/
      data(rrec(i,25,22),i=1,2)/1.140e-11,0.7480/
      data(rrec(i,26,22),i=1,2)/1.510e-11,0.6990/
      data(rrec(i,27,22),i=1,2)/1.626e-11,0.6990/
      data(rrec(i,28,22),i=1,2)/1.750e-11,0.6990/
      data(rrec(i,29,22),i=1,2)/1.870e-11,0.6990/
      data(rrec(i,30,22),i=1,2)/2.000e-11,0.6990/
      data(rrec(i,23,23),i=1,2)/1.270e-13,0.8950/
      data(rrec(i,24,23),i=1,2)/2.680e-12,0.8240/
      data(rrec(i,25,23),i=1,2)/5.240e-12,0.7530/
      data(rrec(i,26,23),i=1,2)/7.800e-12,0.6820/
      data(rrec(i,27,23),i=1,2)/8.402e-12,0.6820/
      data(rrec(i,28,23),i=1,2)/9.050e-12,0.6820/
      data(rrec(i,29,23),i=1,2)/9.700e-12,0.6820/
      data(rrec(i,30,23),i=1,2)/1.030e-11,0.6820/
      data(rrec(i,24,24),i=1,2)/1.320e-13,0.8940/
      data(rrec(i,25,24),i=1,2)/1.730e-12,0.8200/
      data(rrec(i,26,24),i=1,2)/3.320e-12,0.7460/
      data(rrec(i,27,24),i=1,2)/3.575e-12,0.7460/
      data(rrec(i,28,24),i=1,2)/3.580e-12,0.7460/
      data(rrec(i,29,24),i=1,2)/3.580e-12,0.7460/
      data(rrec(i,30,24),i=1,2)/3.590e-12,0.7460/
      data(rrec(i,25,25),i=1,2)/1.370e-13,0.8920/
      data(rrec(i,26,25),i=1,2)/1.020e-12,0.8430/
      data(rrec(i,27,25),i=1,2)/1.278e-12,0.7682/
      data(rrec(i,28,25),i=1,2)/1.600e-12,0.7000/
      data(rrec(i,29,25),i=1,2)/1.920e-12,0.7000/
      data(rrec(i,30,25),i=1,2)/2.240e-12,0.7000/
      data(rrec(i,26,26),i=1,2)/1.420e-13,0.8910/
      data(rrec(i,27,26),i=1,2)/4.459e-13,0.7897/
      data(rrec(i,28,26),i=1,2)/1.400e-12,0.7000/
      data(rrec(i,29,26),i=1,2)/1.500e-12,0.7000/
      data(rrec(i,30,26),i=1,2)/1.600e-12,0.7000/
      data(rrec(i,27,27),i=1,2)/2.510e-13,0.7950/
      data(rrec(i,28,27),i=1,2)/1.000e-12,0.7000/
      data(rrec(i,29,27),i=1,2)/1.000e-12,0.7000/
      data(rrec(i,30,27),i=1,2)/1.100e-12,0.7000/
      data(rrec(i,28,28),i=1,2)/3.600e-13,0.7000/
      data(rrec(i,29,28),i=1,2)/3.600e-13,0.7000/
      data(rrec(i,30,28),i=1,2)/3.600e-13,0.7000/
      data(rrec(i,29,29),i=1,2)/3.600e-13,0.7000/
      data(rrec(i,30,29),i=1,2)/3.600e-13,0.7000/
      data(rrec(i,30,30),i=1,2)/3.600e-13,0.7000/
      data(rnew(i, 1, 1),i=1,4)/7.982e-11,0.7480,3.148e+00,7.036e+05/
      data(rnew(i, 2, 1),i=1,4)/1.891e-10,0.7524,9.370e+00,2.774e+06/
      data(rnew(i, 3, 1),i=1,4)/3.039e-10,0.7539,1.871e+01,6.209e+06/
      data(rnew(i, 4, 1),i=1,4)/4.290e-10,0.7557,3.000e+01,1.093e+07/
      data(rnew(i, 5, 1),i=1,4)/5.437e-10,0.7560,4.576e+01,1.706e+07/
      data(rnew(i, 6, 1),i=1,4)/6.556e-10,0.7567,6.523e+01,2.446e+07/
      data(rnew(i, 7, 1),i=1,4)/7.586e-10,0.7563,9.015e+01,3.338e+07/
      data(rnew(i, 8, 1),i=1,4)/8.616e-10,0.7563,1.191e+02,4.352e+07/
      data(rnew(i, 9, 1),i=1,4)/9.712e-10,0.7566,1.499e+02,5.498e+07/
      data(rnew(i,10, 1),i=1,4)/1.085e-09,0.7570,1.834e+02,6.776e+07/
      data(rnew(i,11, 1),i=1,4)/1.163e-09,0.7558,2.328e+02,8.262e+07/
      data(rnew(i,12, 1),i=1,4)/1.317e-09,0.7574,2.585e+02,9.769e+07/
      data(rnew(i,13, 1),i=1,4)/1.419e-09,0.7578,3.057e+02,1.143e+08/
      data(rnew(i,14, 1),i=1,4)/1.517e-09,0.7574,3.601e+02,1.329e+08/
      data(rnew(i,15, 1),i=1,4)/1.586e-09,0.7560,4.327e+02,1.534e+08/
      data(rnew(i,16, 1),i=1,4)/1.729e-09,0.7568,4.725e+02,1.746e+08/
      data(rnew(i,17, 1),i=1,4)/1.791e-09,0.7565,5.591e+02,1.972e+08/
      data(rnew(i,18, 1),i=1,4)/1.913e-09,0.7567,6.175e+02,2.212e+08/
      data(rnew(i,19, 1),i=1,4)/2.033e-09,0.7569,6.797e+02,2.463e+08/
      data(rnew(i,20, 1),i=1,4)/2.129e-09,0.7570,7.591e+02,2.739e+08/
      data(rnew(i,21, 1),i=1,4)/2.262e-09,0.7578,8.186e+02,3.000e+08/
      data(rnew(i,22, 1),i=1,4)/2.370e-09,0.7574,9.002e+02,3.307e+08/
      data(rnew(i,23, 1),i=1,4)/2.415e-09,0.7565,1.032e+03,3.635e+08/
      data(rnew(i,24, 1),i=1,4)/2.537e-09,0.7571,1.108e+03,3.954e+08/
      data(rnew(i,25, 1),i=1,4)/2.618e-09,0.7565,1.225e+03,4.307e+08/
      data(rnew(i,26, 1),i=1,4)/2.735e-09,0.7568,1.314e+03,4.659e+08/
      data(rnew(i,27, 1),i=1,4)/2.809e-09,0.7565,1.444e+03,5.042e+08/
      data(rnew(i,28, 1),i=1,4)/3.002e-09,0.7581,1.467e+03,5.409e+08/
      data(rnew(i,29, 1),i=1,4)/3.022e-09,0.7564,1.666e+03,5.855e+08/
      data(rnew(i,30, 1),i=1,4)/3.127e-09,0.7567,1.779e+03,6.246e+08/
      data(rnew(i, 2, 2),i=1,4)/9.356e-10,0.7892,4.266e-02,4.677e+06/
      data(rnew(i, 3, 2),i=1,4)/1.112e-10,0.6926,2.437e+01,8.323e+06/
      data(rnew(i, 4, 2),i=1,4)/1.317e-10,0.6691,8.473e+01,1.412e+07/
      data(rnew(i, 5, 2),i=1,4)/1.922e-10,0.6717,1.272e+02,1.975e+07/
      data(rnew(i, 6, 2),i=1,4)/2.765e-10,0.6858,1.535e+02,2.556e+07/
      data(rnew(i, 7, 2),i=1,4)/3.910e-10,0.6988,1.611e+02,3.271e+07/
      data(rnew(i, 8, 2),i=1,4)/4.897e-10,0.7048,1.906e+02,4.093e+07/
      data(rnew(i, 9, 2),i=1,4)/5.602e-10,0.7052,2.476e+02,5.077e+07/
      data(rnew(i,10, 2),i=1,4)/6.161e-10,0.7029,3.274e+02,6.243e+07/
      data(rnew(i,11, 2),i=1,4)/6.833e-10,0.7018,4.060e+02,7.491e+07/
      data(rnew(i,12, 2),i=1,4)/7.510e-10,0.7020,4.921e+02,8.643e+07/
      data(rnew(i,13, 2),i=1,4)/8.182e-10,0.7008,5.875e+02,1.007e+08/
      data(rnew(i,14, 2),i=1,4)/8.722e-10,0.6996,7.098e+02,1.155e+08/
      data(rnew(i,15, 2),i=1,4)/9.142e-10,0.6961,8.682e+02,1.335e+08/
      data(rnew(i,16, 2),i=1,4)/9.692e-10,0.6945,1.017e+03,1.517e+08/
      data(rnew(i,17, 2),i=1,4)/1.021e-09,0.6932,1.184e+03,1.695e+08/
      data(rnew(i,18, 2),i=1,4)/1.087e-09,0.6936,1.329e+03,1.880e+08/
      data(rnew(i,19, 2),i=1,4)/1.145e-09,0.6921,1.503e+03,2.098e+08/
      data(rnew(i,20, 2),i=1,4)/1.179e-09,0.6893,1.757e+03,2.344e+08/
      data(rnew(i,21, 2),i=1,4)/1.265e-09,0.6902,1.877e+03,2.555e+08/
      data(rnew(i,22, 2),i=1,4)/1.322e-09,0.6885,2.092e+03,2.829e+08/
      data(rnew(i,23, 2),i=1,4)/1.375e-09,0.6885,2.321e+03,3.056e+08/
      data(rnew(i,24, 2),i=1,4)/1.422e-09,0.6874,2.589e+03,3.336e+08/
      data(rnew(i,25, 2),i=1,4)/1.488e-09,0.6867,2.802e+03,3.623e+08/
      data(rnew(i,26, 2),i=1,4)/1.542e-09,0.6859,3.073e+03,3.926e+08/
      data(rnew(i,27, 2),i=1,4)/1.589e-09,0.6846,3.373e+03,4.267e+08/
      data(rnew(i,28, 2),i=1,4)/1.676e-09,0.6861,3.530e+03,4.538e+08/
      data(rnew(i,29, 2),i=1,4)/1.686e-09,0.6824,4.031e+03,4.948e+08/
      data(rnew(i,30, 2),i=1,4)/1.758e-09,0.6834,4.254e+03,5.258e+08/
      data(rnew(i, 3, 3),i=1,4)/1.036e-11,0.3880,1.077e+02,1.177e+07/
      data(rnew(i, 4, 3),i=1,4)/2.338e-11,0.4211,3.647e+02,1.215e+07/
      data(rnew(i, 5, 3),i=1,4)/4.487e-11,0.4644,5.371e+02,1.465e+07/
      data(rnew(i, 6, 3),i=1,4)/8.540e-11,0.5247,5.014e+02,1.479e+07/
      data(rnew(i, 7, 3),i=1,4)/1.169e-10,0.5470,6.793e+02,1.650e+07/
      data(rnew(i, 8, 3),i=1,4)/2.053e-10,0.6019,4.772e+02,1.711e+07/
      data(rnew(i, 9, 3),i=1,4)/2.739e-10,0.6188,5.033e+02,2.064e+07/
      data(rnew(i,10, 3),i=1,4)/3.200e-10,0.6198,6.329e+02,2.616e+07/
      data(rnew(i,11, 3),i=1,4)/3.873e-10,0.6295,7.000e+02,2.989e+07/
      data(rnew(i,12, 3),i=1,4)/4.284e-10,0.6287,8.748e+02,3.586e+07/
      data(rnew(i,13, 3),i=1,4)/4.881e-10,0.6326,9.941e+02,4.085e+07/
      data(rnew(i,14, 3),i=1,4)/5.373e-10,0.6337,1.164e+03,4.677e+07/
      data(rnew(i,15, 3),i=1,4)/5.876e-10,0.6354,1.341e+03,5.292e+07/
      data(rnew(i,16, 3),i=1,4)/6.571e-10,0.6400,1.452e+03,5.796e+07/
      data(rnew(i,17, 3),i=1,4)/7.076e-10,0.6397,1.653e+03,6.555e+07/
      data(rnew(i,18, 3),i=1,4)/7.538e-10,0.6388,1.889e+03,7.306e+07/
      data(rnew(i,19, 3),i=1,4)/8.182e-10,0.6411,2.044e+03,8.057e+07/
      data(rnew(i,20, 3),i=1,4)/8.577e-10,0.6403,2.334e+03,8.850e+07/
      data(rnew(i,21, 3),i=1,4)/9.162e-10,0.6413,2.543e+03,9.690e+07/
      data(rnew(i,22, 3),i=1,4)/9.844e-10,0.6440,2.708e+03,1.044e+08/
      data(rnew(i,23, 3),i=1,4)/1.020e-09,0.6427,3.057e+03,1.140e+08/
      data(rnew(i,24, 3),i=1,4)/1.091e-09,0.6445,3.225e+03,1.229e+08/
      data(rnew(i,25, 3),i=1,4)/1.151e-09,0.6451,3.461e+03,1.334e+08/
      data(rnew(i,26, 3),i=1,4)/1.198e-09,0.6443,3.789e+03,1.437e+08/
      data(rnew(i,27, 3),i=1,4)/1.211e-09,0.6406,4.357e+03,1.572e+08/
      data(rnew(i,28, 3),i=1,4)/1.288e-09,0.6440,4.506e+03,1.651e+08/
      data(rnew(i,29, 3),i=1,4)/1.372e-09,0.6472,4.627e+03,1.740e+08/
      data(rnew(i,30, 3),i=1,4)/1.412e-09,0.6454,5.053e+03,1.891e+08/
      data(rnew(i,11,11),i=1,4)/5.641e-12,0.1749,3.077e+02,2.617e+06/
      data(rnew(i,12,11),i=1,4)/1.920e-11,0.3028,4.849e+02,5.890e+06/
      data(rnew(i,13,11),i=1,4)/3.753e-11,0.3585,6.848e+02,9.035e+06/
      data(rnew(i,14,11),i=1,4)/5.942e-11,0.3930,8.962e+02,1.213e+07/
      data(rnew(i,15,11),i=1,4)/1.721e-10,0.5429,2.848e+02,3.975e+07/
      data(rnew(i,16,11),i=1,4)/3.502e-10,0.6266,1.532e+02,1.755e+07/
      data(rnew(i,17,11),i=1,4)/2.502e-10,0.5580,5.303e+02,4.558e+07/
      data(rnew(i,18,11),i=1,4)/2.862e-10,0.5621,7.002e+02,4.885e+07/
      data(rnew(i,19,11),i=1,4)/2.757e-10,0.5364,1.204e+03,7.013e+07/
      data(rnew(i,20,11),i=1,4)/5.273e-10,0.6281,5.329e+02,3.188e+07/
      data(rnew(i,21,11),i=1,4)/3.890e-10,0.5645,1.391e+03,6.295e+07/
      data(rnew(i,22,11),i=1,4)/4.207e-10,0.5646,1.688e+03,6.872e+07/
      data(rnew(i,23,11),i=1,4)/4.605e-10,0.5659,1.949e+03,7.419e+07/
      data(rnew(i,24,11),i=1,4)/4.975e-10,0.5655,2.257e+03,8.072e+07/
      data(rnew(i,25,11),i=1,4)/5.349e-10,0.5658,2.577e+03,8.710e+07/
      data(rnew(i,26,11),i=1,4)/7.688e-10,0.6173,1.653e+03,6.161e+07/
      data(rnew(i,27,11),i=1,4)/5.850e-10,0.5598,3.538e+03,1.052e+08/
      data(rnew(i,28,11),i=1,4)/6.347e-10,0.5631,3.780e+03,1.116e+08/
      data(rnew(i,29,11),i=1,4)/6.619e-10,0.5602,4.322e+03,1.210e+08/
      data(rnew(i,30,11),i=1,4)/7.002e-10,0.5612,4.726e+03,1.287e+08/
      data(rnew(i, 6, 4),i=1,4)/2.020E-09,0.7798,6.690E-01,2.425E+06/
      data(rnew(i, 7, 4),i=1,4)/1.595E-11,0.3529,9.870E+03,2.584E+07/
      data(rnew(i, 8, 4),i=1,4)/2.008E-11,0.3567,1.520E+04,3.843E+07/
      data(rnew(i,10, 4),i=1,4)/2.793E-11,0.3533,3.017E+04,7.872E+07/
      data(rnew(i, 6, 5),i=1,4)/8.577E-10,0.7837,7.286E-01,1.140E+07/
      data(rnew(i, 7, 5),i=1,4)/7.039E-10,0.8607,2.203E+00,3.029E+06/
      data(rnew(i, 8, 5),i=1,4)/1.542E-10,0.6712,1.775E+02,1.535E+08/
      data(rnew(i,10, 5),i=1,4)/2.515E-10,0.7011,3.028E+02,8.903E+06/
      data(rnew(i, 6, 6),i=1,4)/7.651E-09,0.8027,1.193E-03,9.334E+12/
      data(rnew(i, 7, 6),i=1,4)/5.989E-10,0.7560,1.377E+00,1.517E+09/
      data(rnew(i, 8, 6),i=1,4)/3.672E-09,0.7676,2.900E-01,8.521E+07/
      data(rnew(i,10, 6),i=1,4)/9.353E-11,0.6270,9.031E+02,2.387E+07/
      data(rnew(i, 7, 7),i=1,4)/1.243E-08,0.7022,1.136E-03,1.015E+13/
      data(rnew(i, 8, 7),i=1,4)/1.816E-08,0.7170,6.717E-03,3.286E+12/
      data(rnew(i,10, 7),i=1,4)/4.227E-11,0.5395,1.687E+03,1.491E+17/
      data(rnew(i, 8, 8),i=1,4)/1.341E-10,0.6159,1.673E+00,6.366E+16/
      data(rnew(i,10, 8),i=1,4)/9.563E-12,0.3067,9.768E+03,4.851E+17/
      data(rnew(i,10, 9),i=1,4)/5.417E-08,0.6930,1.179E-03,1.060E+07/
      data(rnew(i,10,10),i=1,4)/5.023E-12,0.2420,3.181E+02,1.450E+18/
      data(fe(1,i),i=4,13)/4.33e-10,3.91e-10,3.49e-10,3.16e-10,
     &2.96e-10,2.59e-10,2.24e-10,1.91e-10,1.68e-10,1.46e-10/
      data(fe(2,i),i=4,13)/0.531,0.523,0.521,0.534,
     &0.557,0.567,0.579,0.601,0.602,0.597/
      data(fe(3,i),i=4,13)/5.77e-02,6.15e-02,6.22e-02,6.02e-02,
     &5.79e-02,5.65e-02,5.49e-02,5.10e-02,5.07e-02,5.22e-02/
      end





c      subroutine cfit(iz,in,t,c)
* Version 2, March 24, 1997
******************************************************************************
*** This subroutine calculates rates of direct collisional ionization 
*** for all ionization stages of all elements from H to Ni (Z=28)
*** by use of the fits from G. S. Voronov, 1997, ADNDT, 65, 1
*** Input parameters:  iz - atomic number 
***                    in - number of electrons from 1 to iz 
***                    t  - temperature, K
*** Output parameter:  c  - rate coefficient, cm^3 s^(-1)
******************************************************************************
c      common/CF/CF(5,28,28)
c      c=0.0
c      if(iz.lt.1.or.iz.gt.28)return
c      if(in.lt.1.or.in.gt.iz)return
c      te=t*8.617385e-05
c      u=cf(1,iz,in)/te
c      if(u.gt.80.0)return
c      c=cf(3,iz,in)*(1.0+cf(2,iz,in)*sqrt(u))/(cf(4,iz,in)+u)*
c     *u**cf(5,iz,in)*exp(-u)
c      return
c      end
c*********************************************
c      block data cifit
c      common/CF/CF(5,28,28)
c      DATA(CF(I, 1, 1),I=1,5)/   13.6,0.,2.91E-08,0.2320,0.39/
c      DATA(CF(I, 2, 2),I=1,5)/   24.6,0.,1.75E-08,0.1800,0.35/
c      DATA(CF(I, 2, 1),I=1,5)/   54.4,1.,2.05E-09,0.2650,0.25/
c      DATA(CF(I, 3, 3),I=1,5)/    5.4,0.,1.39E-07,0.4380,0.41/
c      DATA(CF(I, 3, 2),I=1,5)/   75.6,1.,2.01E-09,0.2090,0.23/
c      DATA(CF(I, 3, 1),I=1,5)/  122.4,1.,9.60E-10,0.5820,0.17/
c      DATA(CF(I, 4, 4),I=1,5)/    9.3,0.,1.02E-07,0.3750,0.27/
c      DATA(CF(I, 4, 3),I=1,5)/   18.2,1.,2.08E-08,0.4390,0.21/
c      DATA(CF(I, 4, 2),I=1,5)/  153.9,0.,2.67E-09,0.6120,0.27/
c      DATA(CF(I, 4, 1),I=1,5)/  217.7,1.,4.27E-10,0.6580,0.15/
c      DATA(CF(I, 5, 5),I=1,5)/    8.3,0.,6.49E-08,0.2000,0.26/
c      DATA(CF(I, 5, 4),I=1,5)/   25.2,1.,1.24E-08,0.2670,0.22/
c      DATA(CF(I, 5, 3),I=1,5)/   37.9,1.,3.27E-09,0.2950,0.23/
c      DATA(CF(I, 5, 2),I=1,5)/  259.4,1.,4.95E-10,0.4890,0.09/
c      DATA(CF(I, 5, 1),I=1,5)/  340.2,1.,2.19E-10,0.6570,0.15/
c      DATA(CF(I, 6, 6),I=1,5)/   11.3,0.,6.85E-08,0.1930,0.25/
c      DATA(CF(I, 6, 5),I=1,5)/   24.4,1.,1.86E-08,0.2860,0.24/
c      DATA(CF(I, 6, 4),I=1,5)/   47.9,1.,6.35E-09,0.4270,0.21/
c      DATA(CF(I, 6, 3),I=1,5)/   64.5,1.,1.50E-09,0.4160,0.13/
c      DATA(CF(I, 6, 2),I=1,5)/  392.1,1.,2.99E-10,0.6660,0.02/
c      DATA(CF(I, 6, 1),I=1,5)/  490.0,1.,1.23E-10,0.6200,0.16/
c      DATA(CF(I, 7, 7),I=1,5)/   14.5,0.,4.82E-08,0.0652,0.42/
c      DATA(CF(I, 7, 6),I=1,5)/   29.6,0.,2.98E-08,0.3100,0.30/
c      DATA(CF(I, 7, 5),I=1,5)/   47.5,1.,8.10E-09,0.3500,0.24/
c      DATA(CF(I, 7, 4),I=1,5)/   77.5,1.,3.71E-09,0.5490,0.18/
c      DATA(CF(I, 7, 3),I=1,5)/   97.9,0.,1.51E-09,0.0167,0.74/
c      DATA(CF(I, 7, 2),I=1,5)/  552.1,0.,3.71E-10,0.5460,0.29/
c      DATA(CF(I, 7, 1),I=1,5)/  667.0,1.,7.77E-11,0.6240,0.16/
c      DATA(CF(I, 8, 8),I=1,5)/   13.6,0.,3.59E-08,0.0730,0.34/
c      DATA(CF(I, 8, 7),I=1,5)/   35.1,1.,1.39E-08,0.2120,0.22/
c      DATA(CF(I, 8, 6),I=1,5)/   54.9,1.,9.31E-09,0.2700,0.27/
c      DATA(CF(I, 8, 5),I=1,5)/   77.4,0.,1.02E-08,0.6140,0.27/
c      DATA(CF(I, 8, 4),I=1,5)/  113.9,1.,2.19E-09,0.6300,0.17/
c      DATA(CF(I, 8, 3),I=1,5)/  138.1,0.,1.95E-09,0.3600,0.54/
c      DATA(CF(I, 8, 2),I=1,5)/  739.3,0.,2.12E-10,0.3960,0.35/
c      DATA(CF(I, 8, 1),I=1,5)/  871.4,1.,5.21E-11,0.6290,0.16/
c      DATA(CF(I, 9, 9),I=1,5)/   17.4,1.,7.00E-08,0.1780,0.29/
c      DATA(CF(I, 9, 8),I=1,5)/   35.0,0.,5.41E-08,0.5710,0.27/
c      DATA(CF(I, 9, 7),I=1,5)/   62.7,1.,9.37E-09,0.3190,0.20/
c      DATA(CF(I, 9, 6),I=1,5)/   87.1,1.,4.92E-09,0.3230,0.24/
c      DATA(CF(I, 9, 5),I=1,5)/  114.2,0.,7.06E-09,0.6840,0.27/
c      DATA(CF(I, 9, 4),I=1,5)/  157.2,1.,1.28E-09,0.6480,0.16/
c      DATA(CF(I, 9, 3),I=1,5)/  185.2,1.,5.61E-10,0.7380,0.16/
c      DATA(CF(I, 9, 2),I=1,5)/  953.9,0.,1.66E-10,0.5420,0.29/
c      DATA(CF(I, 9, 1),I=1,5)/ 1103.1,1.,3.74E-11,0.6590,0.15/
c      DATA(CF(I,10,10),I=1,5)/   21.6,1.,1.50E-08,0.0329,0.43/
c      DATA(CF(I,10, 9),I=1,5)/   41.0,0.,1.98E-08,0.2950,0.20/
c      DATA(CF(I,10, 8),I=1,5)/   63.5,1.,7.03E-09,0.0677,0.39/
c      DATA(CF(I,10, 7),I=1,5)/   97.1,1.,4.24E-09,0.0482,0.58/
c      DATA(CF(I,10, 6),I=1,5)/  126.2,1.,2.79E-09,0.3050,0.25/
c      DATA(CF(I,10, 5),I=1,5)/  157.9,0.,3.45E-09,0.5810,0.28/
c      DATA(CF(I,10, 4),I=1,5)/  207.3,1.,9.56E-10,0.7490,0.14/
c      DATA(CF(I,10, 3),I=1,5)/  239.1,1.,4.73E-10,0.9920,0.04/
c      DATA(CF(I,10, 2),I=1,5)/ 1196.0,1.,3.92E-11,0.2620,0.20/
c      DATA(CF(I,10, 1),I=1,5)/ 1360.6,1.,2.77E-11,0.6610,0.13/
c      DATA(CF(I,11,11),I=1,5)/    5.1,1.,1.01E-07,0.2750,0.23/
c      DATA(CF(I,11,10),I=1,5)/   47.3,1.,7.35E-09,0.0560,0.35/
c      DATA(CF(I,11, 9),I=1,5)/   71.6,1.,8.10E-09,0.1480,0.32/
c      DATA(CF(I,11, 8),I=1,5)/   98.9,0.,1.14E-08,0.5530,0.28/
c      DATA(CF(I,11, 7),I=1,5)/  138.4,1.,2.63E-09,0.2300,0.29/
c      DATA(CF(I,11, 6),I=1,5)/  172.2,1.,1.85E-09,0.3630,0.22/
c      DATA(CF(I,11, 5),I=1,5)/  208.5,0.,2.82E-09,0.6740,0.27/
c      DATA(CF(I,11, 4),I=1,5)/  264.2,1.,6.72E-10,0.7520,0.14/
c      DATA(CF(I,11, 3),I=1,5)/  299.9,1.,2.80E-10,0.7810,0.15/
c      DATA(CF(I,11, 2),I=1,5)/ 1465.1,1.,4.63E-11,0.5580,0.16/
c      DATA(CF(I,11, 1),I=1,5)/ 1648.7,1.,2.16E-11,0.7430,0.13/
c      DATA(CF(I,12,12),I=1,5)/    7.6,0.,6.21E-07,0.5920,0.39/
c      DATA(CF(I,12,11),I=1,5)/   15.2,0.,1.92E-08,0.0027,0.85/
c      DATA(CF(I,12,10),I=1,5)/   80.1,1.,5.56E-09,0.1070,0.30/
c      DATA(CF(I,12, 9),I=1,5)/  109.3,1.,4.35E-09,0.1590,0.31/
c      DATA(CF(I,12, 8),I=1,5)/  141.3,0.,7.10E-09,0.6580,0.25/
c      DATA(CF(I,12, 7),I=1,5)/  186.5,1.,1.70E-09,0.2420,0.28/
c      DATA(CF(I,12, 6),I=1,5)/  224.9,1.,1.22E-09,0.3430,0.23/
c      DATA(CF(I,12, 5),I=1,5)/  266.0,0.,2.20E-09,0.8970,0.22/
c      DATA(CF(I,12, 4),I=1,5)/  328.2,1.,4.86E-10,0.7510,0.14/
c      DATA(CF(I,12, 3),I=1,5)/  367.5,1.,2.35E-10,1.0300,0.10/
c      DATA(CF(I,12, 2),I=1,5)/ 1761.8,1.,2.06E-11,0.1960,0.25/
c      DATA(CF(I,12, 1),I=1,5)/ 1962.7,1.,1.75E-11,0.8350,0.11/
c      DATA(CF(I,13,13),I=1,5)/    6.0,1.,2.28E-07,0.3870,0.25/
c      DATA(CF(I,13,12),I=1,5)/   18.8,0.,1.18E-07,2.2100,0.25/
c      DATA(CF(I,13,11),I=1,5)/   28.5,1.,4.40E-09,0.1060,0.24/
c      DATA(CF(I,13,10),I=1,5)/  120.0,0.,1.75E-08,0.8720,0.22/
c      DATA(CF(I,13, 9),I=1,5)/  153.8,1.,2.61E-09,0.1590,0.31/
c      DATA(CF(I,13, 8),I=1,5)/  198.5,1.,1.85E-09,0.1520,0.36/
c      DATA(CF(I,13, 7),I=1,5)/  241.4,1.,1.14E-09,0.2280,0.29/
c      DATA(CF(I,13, 6),I=1,5)/  284.6,1.,8.00E-10,0.4170,0.16/
c      DATA(CF(I,13, 5),I=1,5)/  390.2,1.,5.83E-10,0.4970,0.23/
c      DATA(CF(I,13, 4),I=1,5)/  399.4,0.,4.93E-10,0.7060,0.16/
c      DATA(CF(I,13, 3),I=1,5)/  442.0,1.,9.77E-11,0.2780,0.17/
c      DATA(CF(I,13, 2),I=1,5)/ 2086.6,0.,3.94E-11,0.2860,0.36/
c      DATA(CF(I,13, 1),I=1,5)/ 2304.1,1.,1.38E-11,0.8350,0.11/
c      DATA(CF(I,14,14),I=1,5)/    8.2,1.,1.88E-07,0.3760,0.25/
c      DATA(CF(I,14,13),I=1,5)/   16.4,1.,6.43E-08,0.6320,0.20/
c      DATA(CF(I,14,12),I=1,5)/   33.5,1.,2.01E-08,0.4730,0.22/
c      DATA(CF(I,14,11),I=1,5)/   54.0,1.,4.94E-09,0.1720,0.23/
c      DATA(CF(I,14,10),I=1,5)/  166.8,1.,1.76E-09,0.1020,0.31/
c      DATA(CF(I,14, 9),I=1,5)/  205.3,1.,1.74E-09,0.1800,0.29/
c      DATA(CF(I,14, 8),I=1,5)/  246.5,1.,1.23E-09,0.5180,0.07/
c      DATA(CF(I,14, 7),I=1,5)/  303.5,1.,8.27E-10,0.2390,0.28/
c      DATA(CF(I,14, 6),I=1,5)/  351.1,1.,6.01E-10,0.3050,0.25/
c      DATA(CF(I,14, 5),I=1,5)/  401.4,1.,4.65E-10,0.6660,0.04/
c      DATA(CF(I,14, 4),I=1,5)/  476.4,1.,2.63E-10,0.6660,0.16/
c      DATA(CF(I,14, 3),I=1,5)/  523.5,1.,1.18E-10,0.7340,0.16/
c      DATA(CF(I,14, 2),I=1,5)/ 2437.7,0.,3.36E-11,0.3360,0.37/
c      DATA(CF(I,14, 1),I=1,5)/ 2673.2,1.,1.19E-11,0.9890,0.08/
c      DATA(CF(I,15,15),I=1,5)/   10.5,1.,1.99E-07,0.5350,0.24/
c      DATA(CF(I,15,14),I=1,5)/   19.8,1.,5.88E-08,0.5370,0.21/
c      DATA(CF(I,15,13),I=1,5)/   30.2,1.,2.96E-08,0.8650,0.16/
c      DATA(CF(I,15,12),I=1,5)/   51.4,1.,1.01E-08,0.5460,0.20/
c      DATA(CF(I,15,11),I=1,5)/   65.0,1.,2.36E-09,0.1920,0.17/
c      DATA(CF(I,15,10),I=1,5)/  220.4,0.,6.66E-09,1.0000,0.18/
c      DATA(CF(I,15, 9),I=1,5)/  263.2,1.,1.24E-09,0.2150,0.26/
c      DATA(CF(I,15, 8),I=1,5)/  309.4,0.,2.27E-09,0.7340,0.23/
c      DATA(CF(I,15, 7),I=1,5)/  371.7,1.,6.14E-10,0.2560,0.27/
c      DATA(CF(I,15, 6),I=1,5)/  424.5,1.,4.69E-10,0.3420,0.23/
c      DATA(CF(I,15, 5),I=1,5)/  479.6,0.,6.14E-10,0.3340,0.39/
c      DATA(CF(I,15, 4),I=1,5)/  560.4,0.,3.22E-10,0.8500,0.12/
c      DATA(CF(I,15, 3),I=1,5)/  611.9,1.,9.32E-11,0.7340,0.16/
c      DATA(CF(I,15, 2),I=1,5)/ 2816.9,0.,3.79E-11,0.8050,0.22/
c      DATA(CF(I,15, 1),I=1,5)/ 3069.9,1.,9.73E-12,0.9910,0.08/
c      DATA(CF(I,16,16),I=1,5)/   10.4,1.,5.49E-08,0.1000,0.25/
c      DATA(CF(I,16,15),I=1,5)/   23.3,1.,6.81E-08,0.6930,0.21/
c      DATA(CF(I,16,14),I=1,5)/   34.8,1.,2.14E-08,0.3530,0.24/
c      DATA(CF(I,16,13),I=1,5)/   47.3,1.,1.66E-08,1.0300,0.14/
c      DATA(CF(I,16,12),I=1,5)/   72.6,1.,6.12E-09,0.5800,0.19/
c      DATA(CF(I,16,11),I=1,5)/   88.1,1.,1.33E-09,0.0688,0.35/
c      DATA(CF(I,16,10),I=1,5)/  280.9,0.,4.93E-09,1.1300,0.16/
c      DATA(CF(I,16, 9),I=1,5)/  328.2,1.,8.73E-10,0.1930,0.28/
c      DATA(CF(I,16, 8),I=1,5)/  379.1,0.,1.35E-09,0.4310,0.32/
c      DATA(CF(I,16, 7),I=1,5)/  447.1,1.,4.59E-10,0.2420,0.28/
c      DATA(CF(I,16, 6),I=1,5)/  504.8,1.,3.49E-10,0.3050,0.25/
c      DATA(CF(I,16, 5),I=1,5)/  564.7,0.,5.23E-10,0.4280,0.35/
c      DATA(CF(I,16, 4),I=1,5)/  651.6,0.,2.59E-10,0.8540,0.12/
c      DATA(CF(I,16, 3),I=1,5)/  707.2,1.,7.50E-11,0.7340,0.16/
c      DATA(CF(I,16, 2),I=1,5)/ 3223.9,0.,2.67E-11,0.5720,0.28/
c      DATA(CF(I,16, 1),I=1,5)/ 3494.2,1.,6.32E-12,0.5850,0.17/
c      DATA(CF(I,17,17),I=1,5)/   13.0,1.,1.69E-07,0.4300,0.24/
c      DATA(CF(I,17,16),I=1,5)/   23.8,1.,6.96E-08,0.6700,0.20/
c      DATA(CF(I,17,15),I=1,5)/   39.6,1.,3.40E-08,0.8650,0.18/
c      DATA(CF(I,17,14),I=1,5)/   53.5,1.,1.10E-08,0.3280,0.25/
c      DATA(CF(I,17,13),I=1,5)/   67.8,1.,1.11E-08,1.3700,0.10/
c      DATA(CF(I,17,12),I=1,5)/   97.0,1.,3.17E-09,0.3300,0.24/
c      DATA(CF(I,17,11),I=1,5)/  114.2,1.,1.01E-09,0.1960,0.16/
c      DATA(CF(I,17,10),I=1,5)/  348.3,0.,2.11E-09,0.3130,0.37/
c      DATA(CF(I,17, 9),I=1,5)/  400.1,1.,6.32E-10,0.1730,0.30/
c      DATA(CF(I,17, 8),I=1,5)/  455.6,0.,9.48E-10,0.3440,0.36/
c      DATA(CF(I,17, 7),I=1,5)/  529.3,1.,3.69E-10,0.2730,0.26/
c      DATA(CF(I,17, 6),I=1,5)/  592.0,1.,2.85E-10,0.3430,0.23/
c      DATA(CF(I,17, 5),I=1,5)/  656.7,0.,4.81E-10,0.6580,0.27/
c      DATA(CF(I,17, 4),I=1,5)/  749.8,1.,1.31E-10,0.6230,0.16/
c      DATA(CF(I,17, 3),I=1,5)/  809.4,1.,6.13E-11,0.7360,0.16/
c      DATA(CF(I,17, 2),I=1,5)/ 3658.4,0.,1.90E-11,0.3790,0.36/
c      DATA(CF(I,17, 1),I=1,5)/ 3946.3,1.,5.14E-12,0.5530,0.18/
c      DATA(CF(I,18,18),I=1,5)/   15.8,1.,5.99E-08,0.1360,0.26/
c      DATA(CF(I,18,17),I=1,5)/   27.6,1.,6.07E-08,0.5440,0.21/
c      DATA(CF(I,18,16),I=1,5)/   40.9,1.,3.43E-08,0.8340,0.17/
c      DATA(CF(I,18,15),I=1,5)/   52.3,0.,3.00E-08,1.0300,0.25/
c      DATA(CF(I,18,14),I=1,5)/   75.0,1.,8.73E-09,0.3660,0.31/
c      DATA(CF(I,18,13),I=1,5)/   91.0,1.,5.78E-09,0.3140,0.34/
c      DATA(CF(I,18,12),I=1,5)/  124.3,1.,2.98E-09,0.7030,0.16/
c      DATA(CF(I,18,11),I=1,5)/  143.5,1.,7.25E-10,0.2070,0.15/
c      DATA(CF(I,18,10),I=1,5)/  422.4,1.,1.40E-09,0.6960,0.13/
c      DATA(CF(I,18, 9),I=1,5)/  478.7,1.,4.78E-10,0.1640,0.31/
c      DATA(CF(I,18, 8),I=1,5)/  539.0,0.,8.02E-10,0.4390,0.32/
c      DATA(CF(I,18, 7),I=1,5)/  618.3,1.,2.88E-10,0.2590,0.27/
c      DATA(CF(I,18, 6),I=1,5)/  686.1,1.,2.32E-10,0.3620,0.22/
c      DATA(CF(I,18, 5),I=1,5)/  755.7,0.,3.33E-10,0.4120,0.36/
c      DATA(CF(I,18, 4),I=1,5)/  854.8,1.,1.27E-10,0.9100,0.13/
c      DATA(CF(I,18, 3),I=1,5)/  918.0,1.,5.21E-11,0.7810,0.15/
c      DATA(CF(I,18, 2),I=1,5)/ 4120.7,0.,1.66E-11,0.4350,0.33/
c      DATA(CF(I,18, 1),I=1,5)/ 4426.2,1.,4.32E-12,0.5540,0.18/
c      DATA(CF(I,19,19),I=1,5)/    4.3,1.,2.02E-07,0.2720,0.31/
c      DATA(CF(I,19,18),I=1,5)/   31.6,1.,4.01E-08,0.3710,0.22/
c      DATA(CF(I,19,17),I=1,5)/   45.8,1.,1.50E-08,0.4330,0.21/
c      DATA(CF(I,19,16),I=1,5)/   60.9,1.,1.94E-08,0.8890,0.16/
c      DATA(CF(I,19,15),I=1,5)/   82.7,1.,6.95E-09,0.4940,0.18/
c      DATA(CF(I,19,14),I=1,5)/   99.4,1.,4.11E-09,0.5400,0.17/
c      DATA(CF(I,19,13),I=1,5)/  117.6,1.,2.23E-09,0.5190,0.16/
c      DATA(CF(I,19,12),I=1,5)/  154.7,1.,2.15E-09,0.8280,0.14/
c      DATA(CF(I,19,11),I=1,5)/  175.8,0.,1.61E-09,0.6420,0.13/
c      DATA(CF(I,19,10),I=1,5)/  504.0,1.,1.07E-09,0.6950,0.13/
c      DATA(CF(I,19, 9),I=1,5)/  564.7,1.,3.78E-10,0.1730,0.30/
c      DATA(CF(I,19, 8),I=1,5)/  629.4,0.,6.24E-10,0.4180,0.33/
c      DATA(CF(I,19, 7),I=1,5)/  714.6,1.,2.29E-10,0.2450,0.28/
c      DATA(CF(I,19, 6),I=1,5)/  786.6,1.,1.86E-10,0.3440,0.23/
c      DATA(CF(I,19, 5),I=1,5)/  861.1,0.,2.69E-10,0.3960,0.37/
c      DATA(CF(I,19, 4),I=1,5)/  968.0,1.,1.06E-10,0.9120,0.13/
c      DATA(CF(I,19, 3),I=1,5)/ 1053.4,1.,4.24E-11,0.7370,0.16/
c      DATA(CF(I,19, 2),I=1,5)/ 4610.9,0.,1.38E-11,0.4160,0.34/
c      DATA(CF(I,19, 1),I=1,5)/ 4934.1,1.,3.67E-12,0.5550,0.18/
c      DATA(CF(I,20,20),I=1,5)/    6.1,0.,4.40E-07,0.8480,0.33/
c      DATA(CF(I,20,19),I=1,5)/   11.9,0.,5.22E-08,0.1510,0.34/
c      DATA(CF(I,20,18),I=1,5)/   50.9,1.,2.06E-08,0.4180,0.20/
c      DATA(CF(I,20,17),I=1,5)/   67.3,1.,1.72E-08,0.6380,0.19/
c      DATA(CF(I,20,16),I=1,5)/   84.5,1.,1.26E-08,1.0100,0.14/
c      DATA(CF(I,20,15),I=1,5)/  108.8,1.,4.72E-09,0.5260,0.17/
c      DATA(CF(I,20,14),I=1,5)/  127.2,1.,2.89E-09,0.5480,0.17/
c      DATA(CF(I,20,13),I=1,5)/  147.2,1.,1.64E-09,0.5520,0.15/
c      DATA(CF(I,20,12),I=1,5)/  188.3,1.,1.57E-09,0.7990,0.14/
c      DATA(CF(I,20,11),I=1,5)/  211.3,1.,4.32E-10,0.2320,0.14/
c      DATA(CF(I,20,10),I=1,5)/  591.9,0.,9.47E-10,0.3110,0.38/
c      DATA(CF(I,20, 9),I=1,5)/  657.2,1.,2.98E-10,0.1630,0.31/
c      DATA(CF(I,20, 8),I=1,5)/  726.6,0.,4.78E-10,0.3590,0.36/
c      DATA(CF(I,20, 7),I=1,5)/  817.6,1.,1.86E-10,0.2440,0.28/
c      DATA(CF(I,20, 6),I=1,5)/  894.5,1.,1.56E-10,0.3640,0.22/
c      DATA(CF(I,20, 5),I=1,5)/  974.0,0.,2.16E-10,0.3570,0.39/
c      DATA(CF(I,20, 4),I=1,5)/ 1087.0,1.,7.70E-11,0.6550,0.15/
c      DATA(CF(I,20, 3),I=1,5)/ 1157.0,1.,3.58E-11,0.7360,0.16/
c      DATA(CF(I,20, 2),I=1,5)/ 5128.9,0.,1.28E-11,0.5200,0.30/
c      DATA(CF(I,20, 1),I=1,5)/ 5469.9,1.,3.08E-12,0.5280,0.19/
c      DATA(CF(I,21,21),I=1,5)/    6.6,1.,3.16E-07,0.2040,0.28/
c      DATA(CF(I,21,20),I=1,5)/   12.8,1.,8.61E-08,0.1810,0.25/
c      DATA(CF(I,21,19),I=1,5)/   24.8,1.,5.08E-08,0.3570,0.24/
c      DATA(CF(I,21,18),I=1,5)/   73.5,1.,1.00E-08,0.4530,0.15/
c      DATA(CF(I,21,17),I=1,5)/   91.9,1.,6.76E-09,0.4600,0.15/
c      DATA(CF(I,21,16),I=1,5)/  110.7,1.,5.27E-09,0.5610,0.17/
c      DATA(CF(I,21,15),I=1,5)/  138.0,1.,3.40E-09,0.5600,0.16/
c      DATA(CF(I,21,14),I=1,5)/  158.1,1.,2.18E-09,0.6120,0.15/
c      DATA(CF(I,21,13),I=1,5)/  180.0,1.,1.26E-09,0.6100,0.14/
c      DATA(CF(I,21,12),I=1,5)/  225.1,1.,1.24E-09,0.8520,0.13/
c      DATA(CF(I,21,11),I=1,5)/  249.8,1.,3.62E-10,0.3490,0.05/
c      DATA(CF(I,21,10),I=1,5)/  687.4,1.,5.52E-10,0.3750,0.28/
c      DATA(CF(I,21, 9),I=1,5)/  756.7,1.,5.64E-10,0.8730,0.15/
c      DATA(CF(I,21, 8),I=1,5)/  830.8,1.,4.50E-10,1.0500,0.13/
c      DATA(CF(I,21, 7),I=1,5)/  927.5,1.,2.73E-10,0.8660,0.15/
c      DATA(CF(I,21, 6),I=1,5)/ 1009.0,1.,1.56E-10,0.7150,0.17/
c      DATA(CF(I,21, 5),I=1,5)/ 1094.0,0.,1.81E-10,1.1400,0.36/
c      DATA(CF(I,21, 4),I=1,5)/ 1213.0,1.,4.29E-11,0.7840,0.15/
c      DATA(CF(I,21, 3),I=1,5)/ 1288.0,0.,2.21E-11,0.0270,0.82/
c      DATA(CF(I,21, 2),I=1,5)/ 5674.9,1.,4.51E-12,0.9180,0.04/
c      DATA(CF(I,21, 1),I=1,5)/ 6033.8,0.,2.03E-12,0.0170,0.70/
c      DATA(CF(I,22,22),I=1,5)/    6.8,1.,1.60E-07,0.3600,0.28/
c      DATA(CF(I,22,21),I=1,5)/   13.6,0.,2.14E-07,0.8800,0.28/
c      DATA(CF(I,22,20),I=1,5)/   27.5,1.,2.85E-08,0.2270,0.21/
c      DATA(CF(I,22,19),I=1,5)/   43.3,1.,3.48E-08,0.3900,0.23/
c      DATA(CF(I,22,18),I=1,5)/   99.3,1.,1.00E-08,0.5790,0.18/
c      DATA(CF(I,22,17),I=1,5)/  119.5,1.,7.01E-09,0.6380,0.17/
c      DATA(CF(I,22,16),I=1,5)/  140.8,1.,4.95E-09,0.7170,0.16/
c      DATA(CF(I,22,15),I=1,5)/  170.4,1.,2.99E-09,0.6930,0.17/
c      DATA(CF(I,22,14),I=1,5)/  192.1,1.,2.10E-09,0.7220,0.16/
c      DATA(CF(I,22,13),I=1,5)/  215.9,1.,1.62E-09,0.7650,0.14/
c      DATA(CF(I,22,12),I=1,5)/  265.0,1.,1.11E-09,0.8850,0.12/
c      DATA(CF(I,22,11),I=1,5)/  291.5,0.,9.09E-10,0.9720,0.06/
c      DATA(CF(I,22,10),I=1,5)/  787.8,1.,4.41E-10,0.3590,0.29/
c      DATA(CF(I,22, 9),I=1,5)/  863.1,1.,4.39E-10,0.7810,0.17/
c      DATA(CF(I,22, 8),I=1,5)/  941.9,1.,3.73E-10,1.0500,0.13/
c      DATA(CF(I,22, 7),I=1,5)/ 1044.0,1.,2.28E-10,0.8580,0.15/
c      DATA(CF(I,22, 6),I=1,5)/ 1131.0,1.,1.34E-10,0.7570,0.16/
c      DATA(CF(I,22, 5),I=1,5)/ 1221.0,0.,1.55E-10,1.1500,0.36/
c      DATA(CF(I,22, 4),I=1,5)/ 1346.0,1.,3.80E-11,0.8350,0.14/
c      DATA(CF(I,22, 3),I=1,5)/ 1426.0,0.,1.89E-11,0.0280,0.82/
c      DATA(CF(I,22, 2),I=1,5)/ 6249.1,1.,4.01E-12,0.9680,0.03/
c      DATA(CF(I,22, 1),I=1,5)/ 6625.0,1.,1.62E-12,0.6570,0.14/
c      DATA(CF(I,23,23),I=1,5)/    6.7,0.,8.82E-07,0.3590,0.32/
c      DATA(CF(I,23,22),I=1,5)/   14.7,0.,3.11E-07,0.4320,0.29/
c      DATA(CF(I,23,21),I=1,5)/   29.3,1.,3.50E-08,0.2470,0.25/
c      DATA(CF(I,23,20),I=1,5)/   46.7,0.,5.32E-08,1.1100,0.16/
c      DATA(CF(I,23,19),I=1,5)/   65.3,1.,8.98E-09,0.1400,0.37/
c      DATA(CF(I,23,18),I=1,5)/  128.1,1.,5.87E-09,0.5170,0.17/
c      DATA(CF(I,23,17),I=1,5)/  150.6,1.,5.11E-09,0.6790,0.16/
c      DATA(CF(I,23,16),I=1,5)/  173.4,1.,3.71E-09,0.7610,0.15/
c      DATA(CF(I,23,15),I=1,5)/  205.8,1.,2.24E-09,0.7110,0.17/
c      DATA(CF(I,23,14),I=1,5)/  230.5,1.,1.65E-09,0.7640,0.15/
c      DATA(CF(I,23,13),I=1,5)/  256.0,1.,1.26E-09,0.7620,0.14/
c      DATA(CF(I,23,12),I=1,5)/  308.0,1.,8.86E-10,0.8860,0.12/
c      DATA(CF(I,23,11),I=1,5)/  336.3,0.,3.89E-10,0.1420,0.39/
c      DATA(CF(I,23,10),I=1,5)/  896.0,1.,3.80E-10,0.4090,0.27/
c      DATA(CF(I,23, 9),I=1,5)/  976.0,0.,4.84E-10,0.1730,0.57/
c      DATA(CF(I,23, 8),I=1,5)/ 1060.0,1.,2.49E-10,0.6500,0.14/
c      DATA(CF(I,23, 7),I=1,5)/ 1168.0,0.,5.91E-10,1.6100,0.18/
c      DATA(CF(I,23, 6),I=1,5)/ 1260.0,0.,5.02E-10,2.1200,0.15/
c      DATA(CF(I,23, 5),I=1,5)/ 1355.0,1.,5.38E-11,0.1370,0.40/
c      DATA(CF(I,23, 4),I=1,5)/ 1486.0,1.,5.56E-11,0.7080,0.10/
c      DATA(CF(I,23, 3),I=1,5)/ 1571.0,0.,2.84E-11,0.0240,0.79/
c      DATA(CF(I,23, 2),I=1,5)/ 6851.3,0.,2.54E-11,2.9200,0.09/
c      DATA(CF(I,23, 1),I=1,5)/ 7246.1,0.,1.32E-11,3.5100,0.07/
c      DATA(CF(I,24,24),I=1,5)/    6.8,1.,1.03E-07,0.2170,0.27/
c      DATA(CF(I,24,23),I=1,5)/   16.5,0.,2.45E-07,0.3810,0.32/
c      DATA(CF(I,24,22),I=1,5)/   31.0,0.,1.09E-07,0.5180,0.27/
c      DATA(CF(I,24,21),I=1,5)/   49.1,1.,1.52E-08,0.1820,0.30/
c      DATA(CF(I,24,20),I=1,5)/   69.5,0.,3.25E-08,1.3600,0.13/
c      DATA(CF(I,24,19),I=1,5)/   90.6,1.,5.50E-09,0.1430,0.37/
c      DATA(CF(I,24,18),I=1,5)/  160.2,1.,5.13E-09,0.6570,0.16/
c      DATA(CF(I,24,17),I=1,5)/  184.7,1.,3.85E-09,0.7220,0.15/
c      DATA(CF(I,24,16),I=1,5)/  209.3,1.,2.81E-09,0.7590,0.15/
c      DATA(CF(I,24,15),I=1,5)/  244.4,1.,1.76E-09,0.7320,0.16/
c      DATA(CF(I,24,14),I=1,5)/  271.0,1.,1.30E-09,0.7640,0.15/
c      DATA(CF(I,24,13),I=1,5)/  298.0,1.,1.02E-09,0.8100,0.13/
c      DATA(CF(I,24,12),I=1,5)/  354.8,1.,7.19E-10,0.8870,0.12/
c      DATA(CF(I,24,11),I=1,5)/  384.2,1.,1.61E-10,0.1500,0.22/
c      DATA(CF(I,24,10),I=1,5)/ 1011.0,1.,4.64E-10,0.9710,0.12/
c      DATA(CF(I,24, 9),I=1,5)/ 1097.0,1.,3.31E-10,0.9240,0.14/
c      DATA(CF(I,24, 8),I=1,5)/ 1185.0,1.,2.49E-10,0.9310,0.15/
c      DATA(CF(I,24, 7),I=1,5)/ 1299.0,1.,1.68E-10,0.9100,0.14/
c      DATA(CF(I,24, 6),I=1,5)/ 1396.0,1.,1.01E-10,0.8050,0.15/
c      DATA(CF(I,24, 5),I=1,5)/ 1496.0,0.,1.17E-10,1.2100,0.35/
c      DATA(CF(I,24, 4),I=1,5)/ 1634.0,1.,2.91E-11,0.8840,0.13/
c      DATA(CF(I,24, 3),I=1,5)/ 1721.0,0.,1.45E-11,0.0350,0.80/
c      DATA(CF(I,24, 2),I=1,5)/ 7482.0,1.,3.07E-12,0.9670,0.03/
c      DATA(CF(I,24, 1),I=1,5)/ 7894.8,1.,1.46E-12,0.1830,0.39/
c      DATA(CF(I,25,25),I=1,5)/    7.4,1.,8.56E-08,0.1320,0.26/
c      DATA(CF(I,25,24),I=1,5)/   15.6,0.,1.18E-07,0.3590,0.19/
c      DATA(CF(I,25,23),I=1,5)/   33.7,0.,8.54E-08,0.3970,0.32/
c      DATA(CF(I,25,22),I=1,5)/   51.2,1.,1.80E-08,0.2720,0.18/
c      DATA(CF(I,25,21),I=1,5)/   72.4,1.,8.22E-09,0.1610,0.32/
c      DATA(CF(I,25,20),I=1,5)/   95.0,0.,2.15E-08,1.5400,0.11/
c      DATA(CF(I,25,19),I=1,5)/  119.3,1.,3.65E-09,0.1470,0.37/
c      DATA(CF(I,25,18),I=1,5)/  194.5,1.,3.91E-09,0.6990,0.15/
c      DATA(CF(I,25,17),I=1,5)/  221.8,1.,2.92E-09,0.7190,0.15/
c      DATA(CF(I,25,16),I=1,5)/  248.3,1.,2.23E-09,0.8060,0.14/
c      DATA(CF(I,25,15),I=1,5)/  286.0,1.,1.39E-09,0.7350,0.16/
c      DATA(CF(I,25,14),I=1,5)/  314.4,1.,1.04E-09,0.7610,0.15/
c      DATA(CF(I,25,13),I=1,5)/  343.6,1.,8.28E-10,0.8090,0.13/
c      DATA(CF(I,25,12),I=1,5)/  403.0,1.,5.60E-10,0.7870,0.14/
c      DATA(CF(I,25,11),I=1,5)/  435.2,1.,1.52E-10,0.2990,0.08/
c      DATA(CF(I,25,10),I=1,5)/ 1133.0,1.,4.03E-10,1.0400,0.11/
c      DATA(CF(I,25, 9),I=1,5)/ 1244.0,1.,2.74E-10,0.9230,0.14/
c      DATA(CF(I,25, 8),I=1,5)/ 1317.0,1.,2.18E-10,0.9900,0.14/
c      DATA(CF(I,25, 7),I=1,5)/ 1437.0,1.,1.49E-10,0.9680,0.13/
c      DATA(CF(I,25, 6),I=1,5)/ 1539.0,1.,8.70E-11,0.8020,0.15/
c      DATA(CF(I,25, 5),I=1,5)/ 1644.0,0.,1.02E-10,1.2200,0.35/
c      DATA(CF(I,25, 4),I=1,5)/ 1788.0,1.,2.54E-11,0.8830,0.13/
c      DATA(CF(I,25, 3),I=1,5)/ 1880.0,0.,1.28E-11,0.0330,0.81/
c      DATA(CF(I,25, 2),I=1,5)/ 8141.0,1.,2.77E-12,1.0100,0.02/
c      DATA(CF(I,25, 1),I=1,5)/ 8571.9,1.,1.32E-12,0.2190,0.37/
c      DATA(CF(I,26,26),I=1,5)/    7.9,0.,2.52E-07,0.7010,0.25/
c      DATA(CF(I,26,25),I=1,5)/   16.2,1.,2.21E-08,0.0330,0.45/
c      DATA(CF(I,26,24),I=1,5)/   30.6,0.,4.10E-08,0.3660,0.17/
c      DATA(CF(I,26,23),I=1,5)/   54.8,0.,3.53E-08,0.2430,0.39/
c      DATA(CF(I,26,22),I=1,5)/   75.0,1.,1.04E-08,0.2850,0.17/
c      DATA(CF(I,26,21),I=1,5)/   99.0,1.,1.23E-08,0.4110,0.21/
c      DATA(CF(I,26,20),I=1,5)/  125.0,1.,9.47E-09,0.4580,0.21/
c      DATA(CF(I,26,19),I=1,5)/  151.1,1.,4.71E-09,0.2800,0.28/
c      DATA(CF(I,26,18),I=1,5)/  233.6,1.,3.02E-09,0.6970,0.15/
c      DATA(CF(I,26,17),I=1,5)/  262.1,1.,2.34E-09,0.7640,0.14/
c      DATA(CF(I,26,16),I=1,5)/  290.0,1.,1.76E-09,0.8050,0.14/
c      DATA(CF(I,26,15),I=1,5)/  331.0,1.,1.14E-09,0.7730,0.15/
c      DATA(CF(I,26,14),I=1,5)/  361.0,1.,8.66E-10,0.8050,0.14/
c      DATA(CF(I,26,13),I=1,5)/  392.0,1.,6.61E-10,0.7620,0.14/
c      DATA(CF(I,26,12),I=1,5)/  457.0,1.,4.41E-10,0.6980,0.16/
c      DATA(CF(I,26,11),I=1,5)/  489.3,1.,1.18E-10,0.2110,0.15/
c      DATA(CF(I,26,10),I=1,5)/ 1262.0,1.,3.61E-10,1.1600,0.09/
c      DATA(CF(I,26, 9),I=1,5)/ 1360.0,1.,2.45E-10,0.9780,0.13/
c      DATA(CF(I,26, 8),I=1,5)/ 1470.0,1.,1.87E-10,0.9880,0.14/
c      DATA(CF(I,26, 7),I=1,5)/ 1582.0,1.,1.33E-10,1.0300,0.12/
c      DATA(CF(I,26, 6),I=1,5)/ 1690.0,1.,7.84E-11,0.8480,0.14/
c      DATA(CF(I,26, 5),I=1,5)/ 1800.0,0.,8.90E-11,1.2000,0.35/
c      DATA(CF(I,26, 4),I=1,5)/ 1960.0,1.,2.29E-11,0.9360,0.12/
c      DATA(CF(I,26, 3),I=1,5)/ 2046.0,0.,1.12E-11,0.0340,0.81/
c      DATA(CF(I,26, 2),I=1,5)/ 8828.0,1.,2.46E-12,1.0200,0.02/
c      DATA(CF(I,26, 1),I=1,5)/ 9277.7,1.,9.79E-13,0.6640,0.14/
c      DATA(CF(I,27,27),I=1,5)/    7.9,1.,8.89E-08,0.1270,0.24/
c      DATA(CF(I,27,26),I=1,5)/   17.1,1.,5.65E-08,0.1940,0.23/
c      DATA(CF(I,27,25),I=1,5)/   33.5,1.,3.06E-08,0.2010,0.22/
c      DATA(CF(I,27,24),I=1,5)/   51.3,0.,2.27E-08,0.5740,0.10/
c      DATA(CF(I,27,23),I=1,5)/   79.5,0.,1.93E-08,0.1950,0.42/
c      DATA(CF(I,27,22),I=1,5)/  102.0,0.,1.27E-08,0.1260,0.47/
c      DATA(CF(I,27,21),I=1,5)/  129.0,1.,3.58E-09,0.1940,0.29/
c      DATA(CF(I,27,20),I=1,5)/  158.0,0.,1.17E-08,1.9800,0.07/
c      DATA(CF(I,27,19),I=1,5)/  186.1,1.,1.78E-09,0.1120,0.42/
c      DATA(CF(I,27,18),I=1,5)/  275.0,1.,2.41E-09,0.7390,0.14/
c      DATA(CF(I,27,17),I=1,5)/  305.0,1.,1.86E-09,0.7620,0.14/
c      DATA(CF(I,27,16),I=1,5)/  336.0,1.,1.41E-09,0.8040,0.14/
c      DATA(CF(I,27,15),I=1,5)/  379.0,1.,9.54E-10,0.8130,0.14/
c      DATA(CF(I,27,14),I=1,5)/  411.0,1.,7.12E-10,0.8030,0.14/
c      DATA(CF(I,27,13),I=1,5)/  444.0,1.,5.34E-10,0.7180,0.15/
c      DATA(CF(I,27,12),I=1,5)/  512.0,1.,3.62E-10,0.6580,0.17/
c      DATA(CF(I,27,11),I=1,5)/  546.6,1.,1.05E-10,0.2520,0.12/
c      DATA(CF(I,27,10),I=1,5)/ 1397.0,1.,3.10E-10,1.1700,0.09/
c      DATA(CF(I,27, 9),I=1,5)/ 1486.0,1.,1.56E-10,0.5720,0.15/
c      DATA(CF(I,27, 8),I=1,5)/ 1603.0,1.,1.32E-10,0.6820,0.13/
c      DATA(CF(I,27, 7),I=1,5)/ 1735.0,1.,9.08E-11,0.5110,0.17/
c      DATA(CF(I,27, 6),I=1,5)/ 1846.0,0.,3.45E-10,2.8400,0.11/
c      DATA(CF(I,27, 5),I=1,5)/ 1962.0,1.,5.01E-11,0.7140,0.11/
c      DATA(CF(I,27, 4),I=1,5)/ 2119.0,1.,1.92E-11,0.1170,0.42/
c      DATA(CF(I,27, 3),I=1,5)/ 2219.0,1.,1.95E-11,1.2000,0.00/
c      DATA(CF(I,27, 2),I=1,5)/ 9544.0,0.,1.68E-11,3.5200,0.05/
c      DATA(CF(I,27, 1),I=1,5)/10012.1,1.,1.45E-12,0.6350,0.15/
c      DATA(CF(I,28,28),I=1,5)/    7.6,0.,1.65E-07,0.4520,0.28/
c      DATA(CF(I,28,27),I=1,5)/   18.2,0.,8.42E-08,0.6190,0.16/
c      DATA(CF(I,28,26),I=1,5)/   35.3,1.,1.89E-08,0.2200,0.21/
c      DATA(CF(I,28,25),I=1,5)/   54.9,1.,1.48E-08,0.2160,0.21/
c      DATA(CF(I,28,24),I=1,5)/   76.0,0.,1.13E-08,0.5180,0.09/
c      DATA(CF(I,28,23),I=1,5)/  108.0,0.,1.16E-08,0.1850,0.44/
c      DATA(CF(I,28,22),I=1,5)/  133.0,0.,8.68E-09,0.1380,0.46/
c      DATA(CF(I,28,21),I=1,5)/  162.0,1.,2.45E-09,0.1630,0.32/
c      DATA(CF(I,28,20),I=1,5)/  193.0,0.,9.24E-09,2.2500,0.05/
c      DATA(CF(I,28,19),I=1,5)/  225.0,0.,2.41E-09,0.0270,0.79/
c      DATA(CF(I,28,18),I=1,5)/  321.0,1.,1.92E-09,0.7380,0.14/
c      DATA(CF(I,28,17),I=1,5)/  352.0,1.,1.50E-09,0.7610,0.14/
c      DATA(CF(I,28,16),I=1,5)/  384.0,1.,1.16E-09,0.8030,0.14/
c      DATA(CF(I,28,15),I=1,5)/  430.0,1.,8.08E-10,0.8560,0.13/
c      DATA(CF(I,28,14),I=1,5)/  464.0,1.,6.09E-10,0.8500,0.13/
c      DATA(CF(I,28,13),I=1,5)/  499.0,1.,4.48E-10,0.7180,0.15/
c      DATA(CF(I,28,12),I=1,5)/  571.3,1.,3.00E-10,0.6220,0.18/
c      DATA(CF(I,28,11),I=1,5)/  607.0,1.,7.90E-11,0.1600,0.19/
c      DATA(CF(I,28,10),I=1,5)/ 1541.0,1.,2.78E-10,1.2500,0.08/
c      DATA(CF(I,28, 9),I=1,5)/ 1648.0,1.,1.92E-10,1.0400,0.12/
c      DATA(CF(I,28, 8),I=1,5)/ 1756.0,1.,1.51E-10,1.1100,0.12/
c      DATA(CF(I,28, 7),I=1,5)/ 1894.0,1.,1.05E-10,1.0900,0.11/
c      DATA(CF(I,28, 6),I=1,5)/ 2011.0,1.,6.04E-11,0.8490,0.14/
c      DATA(CF(I,28, 5),I=1,5)/ 2131.0,0.,6.91E-11,1.2100,0.35/
c      DATA(CF(I,28, 4),I=1,5)/ 2295.0,1.,1.84E-11,0.9910,0.11/
c      DATA(CF(I,28, 3),I=1,5)/ 2399.0,0.,9.03E-12,0.0420,0.79/
c      DATA(CF(I,28, 2),I=1,5)/10290.0,1.,2.61E-12,0.5680,0.16/
c      DATA(CF(I,28, 1),I=1,5)/10775.3,1.,1.39E-12,0.7290,0.13/
c      end






*
      FUNCTION RECFER (T,Z,N)
      PARAMETER ( NT=9 )
      double precision A(NT),B(NT),C(NT),D(NT),E(NT),F(NT),G(NT),
     +   H(NT),AI(NT)
*
*     Hydrogenic Recombination coefficients from Ferland etal
*     ApJ 1992, 387, 95
*     1 - 6 are real levels 7:7-10  8:11-20 9:21-1000
*
      DATA (A(I),I=1,9)/-10.7815, -11.0434, -11.2031, -11.3145,
     +    -11.4062, -11.4846, -11.0213, -10.8751, -10.2096/
      DATA (B(I),I=1,9)/-0.388896, -0.393514, -0.424576, -0.439393,
     +    -0.436467, -0.442457, -0.442776, -0.450383, -0.048286/
      DATA (C(I),I=1,9)/4.68469, 4.84334, 5.25370, 5.45398,
     +     5.46028, 5.56951, 5.37057, 5.39988, 0.85490/
      DATA (D(I),I=1,9)/0.0640438, 0.0692118, 0.0819237, 0.0853755,
     +     0.0863856, 0.0899765, 0.0943866, 0.1022072, 0.0453455/
      DATA (E(I),I=1,9)/-0.874235, -0.956856, -1.126131, -1.166601,
     +    -1.184309, -1.241026, -1.252699, -1.341072, -0.474956/
      DATA (F(I),I=1,9)/-5.10248E-3,-5.49930E-3,-6.81708E-3,-7.40060E-3,
     +    -7.37118E-3,-7.54870E-3,-7.44688E-3,-7.17252E-3,-1.01818E-3/
      DATA (G(I),I=1,9)/0.081411, 0.092159, 0.114221, 0.118057,
     +     0.119145, 0.125443, 0.125435, 0.129801, 0.00/
      DATA (H(I),I=1,9)/2.47761E-4,3.06273E-4,4.11653E-4,4.03664E-4,
     +     4.12342E-4,4.48713E-4,4.88629E-4, 5.52029E-4, 0.0/
      DATA(AI(I),I=1,9)/-3.87713E-3,-5.02483E-3,-6.77511E-3,-6.48848E-3,
     +    -6.75160E-3, -7.53579E-3, -8.30273E-3, -9.92535E-3, 0.0/
*
      TZ=T/Z**2
      X=ALOG10(TZ)
      I=N
*
      FUP=(A(I)+X*(C(I)+X*(E(I)+X*(G(I)+AI(I)*X))))
      FLO=(1.0 +X*(B(I)+X*(D(I)+X*(F(I)+H(I)*X ))))
      FTN=FUP/FLO
      RECFER=(Z*10.**FTN)/TZ
*
      RETURN
      END

      FUNCTION RECFER1 (T,Z,N1)
      PARAMETER ( NT=9 )
*
*     Sum recombination coefficients using the Ferland etal 1992 data
*     in function RECFER
*     N1 is the lowest level number, i.e. alpha-B means N1=2
*
      SUM=0.
      DO 10 I=N1,NT
        SUM=SUM+RECFER(T,Z,I)
  10  CONTINUE
      RECFER1=SUM
*
      RETURN
      END


corly      FUNCTION RECFER2(T,Z,N)
corly*recombination coefficient for hydrogen
corly
corly* received from J. Ferguson 25june97
corly*
corly      integer n
corly*
corly*     local variables
corly      real eps, x,fac
corly*
corly      real a(15),b(15),c(15),d(15),e(15),f(15),g(15),h(15),i(15)
corly*
corly      data a/-10.7814536, -11.0434026, -11.2031246, -11.3145225,
corly     1 -11.4062195, -11.4846095,
corly     2 -11.4646746 , -11.5032599, -11.71,
corly     3 -11.610978, -11.992763, -11.7151303, -11.6625192, -11.6988845,
corly     4 -11.715368/
corly*
corly      data b/ -0.38889639, -0.39351422,-0.42457562,-0.4393928,
corly     1-0.43646701,-0.44245653,
corly     2 0.032705359, -0.17837447, 8.0767024, -0.16696997,
corly     3  4.7646025, -0.146717, 0.027221784, 0.0289227, 0.023116295/
corly*
corly      data c/ 4.684694732, 4.843339724, 5.25369788, 5.453982338,
corly     1 5.460274938, 5.569510445,
corly     2 -0.07601504, 2.303021561, -92.517282 ,
corly     3 2.2047166, -54.299096, 2.02503782, -0.28687758, -0.28812,
corly     4 -0.28765391/
corly*
corly      data d/ 0.064043815, 0.069211833, 0.081923668, 0.085375549,
corly     1 0.086385567, 0.089975575,
corly     2 0.006599021, 0.053925364, -1.4691482, 0.059152556,-0.88170929,
corly     3 0.062525821, 0.093677105, 0.090329188, 0.11801512/
corly      
corly*
corly      data e/ -0.87423486, -0.95685641, -1.12613095, -1.16660095,
corly     1 -1.18430859, -1.24102597,
corly     2 0.02975201, -0.53186795, 18.562078, -.59020955, 10.944953,
corly     3 -.63482507, -0.71040608, -0.695660318, -0.94380361/
corly*
corly      data f/ -0.0051024762,-0.0054992961,-0.0068170788,-0.0074005994,
corly     1-0.00737118,-0.0075487022,
corly     2 0.010321731, -0.00119851, 0.5103495, -0.0013545537, 0.33187099,
corly     3 -0.00146163, 0.000462095, 0.001726288, -0.0022470643/
corly*
corly      data g/ 0.081410787, 0.092158621, 0.114220651, 0.11805714,
corly     1 0.119144754, 0.125442906,
corly     2 -0.10863166, 0.0, -4.9301946, 0.0, -3.2247619,
corly     3 0.0, -0.0548276, -0.06497062, -0.037080131/
corly*
corly      data h/ 0.00024776095,0.00030627271,0.00041165267,0.00040366383,
corly     1 0.00041234231,0.00044871329,
corly     2 -0.00025303, 0.0, -0.012089006, 0.0, -0.0079305392,
corly     3 0.0, -1.3652e-5, -4.8413e-5, 4.68229e-5/
corly*
corly      data i/ -0.0038771298,-0.0050248303,-0.0067751069,-0.0064884839,
corly     1 -0.0067516033,-0.0075357895, 0.0, 0.0, 0.0, 0.0, 0.0,
corly     2 0.0,0.0,0.0,0.0/
corly*
corly      TZ=T/Z**2
corly      X=ALOG10(TZ)
corly*
corly      if(n.gt.15) then
corly        eps = 157807./n/n/tz
corly        expx=exp(-eps)
corly        alogx=alog(eps)
corly        te32=tz**(1.5)
corly        rec = (3.262e-6 /n**3 /te32) * exp(eps) *
corly     +       EI(eps,1,expx,alogx)
corly        recfer2=z*rec
corly      else
corly         FUP=(A(n)+X*(C(n)+X*(E(n)+X*(G(n)+I(n)*X))))
corly         FLO=(1.0 +X*(B(n)+X*(D(n)+X*(F(n)+H(n)*X ))))
corly         FAC=FUP/FLO
corly         RECFER2 = z*(10.**fac)/tz
corly      endif
corly*
corly      end


      SUBROUTINE IIIBOD
      COMMON/NSBIG/ NSBIG
      COMMON/RCOTA/ RCOTA(26),NOCOTA
      COMMON/BUG/BUG,NZNBUG,NPSBUG,LRECOL,DR_M,DR_G
      LOGICAL NOCOTA , BUG
*
      IF( NOCOTA ) THEN
         DO 1 I=1,26
 1       RCOTA(I) = 0.
         NSBIG = 0
         RETURN
      ENDIF
*
      IF( NSBIG.EQ.0 ) THEN
         IUP = 26
      ELSE
         IUP = MIN( 26 , NSBIG )
      ENDIF
*
      DO 2 I=1,IUP
  2   RCOTA(I) = DA( FLOAT(I) )
      NSBIG = 0
!      IF(BUG) WRITE(60,'(5X,''3BOD RATE='',1P,14E8.1)')(RCOTA(I),I=1,14)
!	kartick omitted the write command
*
      RETURN
      END

      FUNCTION XINVRS ( Y , A , B , U , V  , IFAIL )
      DATA ITMAX / 10 /
C
C INVERTS EQUATION OF THE FORM
C
C             Y = A + B * X ** U - V * LOG ( X )
C
      IFAIL   = 0
      XLOG    = ( ( A - Y ) / V )
      X       = 10. ** XLOG
C
      xinvrs=0.
*     added July 1995 because of a compiler warning
*
      DO 100 I = 1 , ITMAX
        BXU     = B * X ** U
        FX      = Y - A - BXU + V * XLOG
        DFX     = V * .4342945 - BXU * U
C
        XX      = X - X * FX / DFX
        IF ( ( ABS ( XX - X ) /  X ) .LT. 1.E-4 ) THEN
          XINVRS = XX
          RETURN
        ELSE
          X    = XX
          XLOG = LOG10 ( X )
        ENDIF
 100  CONTINUE
      IFAIL = 1
      RETURN
      END


      FUNCTION XMAP ( X , Y , X0 )
C
C PARABOLIC INTERPOLATION.
C
      double precision X ( 3 ) , Y ( 3 )
C
      Y1   = Y ( 1 )
      X1   = X ( 1 )
      X2   = X ( 2 )
      X3   = X ( 3 )
      X13M = X1 - X3
      X12M = X1 - X2
      Y13M = Y1 - Y ( 3 )
      X3   = ( X1 + X3 ) * X13M
      X2   = ( X1 + X2 ) * X12M
      B    = ( ( Y1 - Y ( 2 ) ) * X3 - Y13M * X2 )
     *               / (  X12M * X3 - X13M * X2 )
      A    = ( Y13M - X13M * B ) / X3
      C    = Y1 - A * X1 * X1 - B * X1
C
      XMAP  = A * X0 * X0 + B * X0 + C
C
      RETURN
      END

      FUNCTION DA( Z )
*
* WRITTEN BY S. A. COTA, 2/87
*
      PARAMETER ( MAXZ = 28 )
*
* MAXZ IS THE MAXIMUM EFFECTIVE NUCLEAR CHARGE ( = IONIC CHARGE + 1 )
* WHICH THE DIMENSION STATEMENTS ACCOMODATE.
*
* IT IS USED ONLY FOR THE ARRAY ZLOG7 ( = 7 * LOG ( Z ) )
* AND THE ARRAY ZLOG2 ( = 2 * LOG ( Z ) ) .   THESE ARRAYS
* CONTAIN EASILY CALCULATED VALUES, WHICH HAVE BEEN STORED
* TO SAVE TIME IN EXECUTION.
*
* IF MAXZ IS EXCEEDED, THIS PROGRAM SIMPLY CALCULATES THE
* LOGS INSTEAD OF LOOKING THEM UP.
*
*
      COMMON/LOGTE/ALOGTE,ALOGNE
      COMMON/LOGTE1/XLF,HUMMER
      COMMON/DALERR/ILT, ILTLN, ILTHN, IHTHN, IFAIL
*
      COMMON/PARMS/ TZ(83), ZLOG7(MAXZ), ZLOG2(MAXZ)
      COMMON /DAX/ A0( 83), B0( 83), Y0( 83)
      COMMON /XCX/ A1( 83), B1( 83), X1( 83)
      COMMON /XNX/ A2(21: 83), B2(21: 83), X2(21: 83)
      COMMON/ COEFF / C, D, U, V, A, B, X, Y, XNC
*
      double precision XX(3), YYA(3), YYB(3), YYX(3), YYY(3)
      LOGICAL INTERP
*

      INTERP = .TRUE.
      NZ = Z + .1
      IF ( NZ .GT. MAXZ ) THEN
        ZLOG = ALOG10 ( Z )
        ALOGT = ALOGTE - 2. * ZLOG
        ALOGN = ALOGNE - 7. * ZLOG
      ELSE
        ALOGT = ALOGTE - ZLOG2 (NZ)
        ALOGN = ALOGNE - ZLOG7 (NZ)
      ENDIF
*
* CHECK IF PARAMETERS ARE WITHIN BOUNDS.  IF NOT, INCREMENT
*       APPROPRIATE ERROR COUNTER AND SET TO BOUNDARY IF
*       NECESSARY:
*
* DEFINITION OF ERROR COUNTERS:
*
*     ILT    : LOW T
*     ILTLN  : LOW T , LOW  N
*     ILTHN  : LOW T , HIGH N
*     IHTHN  : HIGH T , HIGH N
*
      IF  ( ALOGT .LT. 0. ) THEN
        ILT = ILT + 1
        ALOGT = 0.
      ENDIF
*
      IF  ( ALOGT .LE. 2.1760913 ) THEN
        IF  ( ALOGN .LT. ( 3.5 * ALOGT - 8. ) ) THEN
          ILTLN = ILTLN + 1
        ELSE IF ( ALOGN .GT. ( 3.5 * ALOGT - 2. ) ) THEN
          ILTHN = ILTHN + 1
          ALOGN = 3.5 * ALOGT - 2.
        ENDIF
*
      ELSE IF ( ALOGT .LE. 2.4771213 ) THEN
        IF ( ALOGN .GT. 9. ) THEN
          ILTHN = ILTHN + 1
          ALOGN = 9.
        ENDIF
*
      ELSE IF ( ALOGT .LE. 5.1139434 ) THEN
        IF ( ALOGN .GT. 13. ) THEN
          IHTHN = IHTHN + 1
          ALOGN = 13.
        ENDIF
*
      ELSE
        DA  = 0.
        RETURN
      ENDIF
*
* LOCATE POSITION IN ARRAYS
*
      IF ( ALOGT .LE. 2. ) THEN
        NT  = 9.9657843 * ALOGT + 1.
      ELSE
        NT = 19.931568 * ALOGT - 19.
      ENDIF
      NT = MAX( 1 , MIN( 83 , NT ) )
*
* CENTER UP SINCE ARRAY VALUES ARE ROUNDED
*
      IF ( ABS ( ALOGT - TZ ( NT ) ) .GE.
     *     ABS ( ALOGT - TZ ( MIN(83,NT+1) ) ) ) THEN
        NT = MIN( 83 , NT + 1 )
      ELSE IF ( ABS ( ALOGT - TZ ( NT  ) ) .GT.
     *          ABS ( ALOGT - TZ ( MAX(1,NT - 1) ) ) ) THEN
        NT  = MAX( 1 , NT - 1 )
      ENDIF
*
* CHECK IF INTERPOLATION IS NEEDED AND PROCEED IF NOT.
*
      IF ( ABS ( ALOGT - TZ ( NT ) ) .LT. 0.00001 ) THEN
        IF ( Z .NE. 1.0 ) THEN
          C = A1 ( NT )
          D = B1 ( NT )
          U = X1 ( NT )
          V = 8.90
        ELSE
          NT = MIN( 83 , MAX( 21 , NT ) )
          C = A2 ( NT )
          D = B2 ( NT )
          U = X2 ( NT )
          V = 9.40
        ENDIF
*

        XNC    = XINVRS ( ALOGN , C , D , U , V , JFAIL )
        ALOGNC = ALOG10 ( XNC )
*
        A      = A0 ( NT )
        B      = B0 ( NT )
        X      = -2.45
        Y      = Y0 ( NT )
        NT0    = NT - 1
*
*   IF INTERPOLATION WAS REQUIRED,
*   CHECK THAT NT IS NOT ON THE EDGE OF A DISCONTINUITY,
*   EITHER AT END OF ARRAYS OR WITHIN THEM,
*   WHERE VALUES CHANGE ABRUPTLY.
*
      ELSE
        IF ( ( NT .LE. 21 ) .AND. ( Z .EQ. 1.0 ) ) THEN
          NT = 22
        ELSE IF ( NT .LE. 1 ) THEN
          NT = 2
        ELSE IF ( NT .GE. 83 ) THEN
          NT = 82
        ELSE IF ( NT .EQ. 24 ) THEN
          IF ( ALOGT .LE. 2.1760913 ) THEN
            NT = 23
          ELSE
            NT = 25
          ENDIF
        ELSE IF ( NT .EQ. 30 ) THEN
          IF ( ALOGT .LE. 2.4771213 ) THEN
            NT = 29
          ELSE
            NT = 31
          ENDIF
        ENDIF
*
        NT0 = NT - 1
        NT1 = NT + 1
        XX ( 1 ) = TZ ( NT0 )
        XX ( 2 ) = TZ  ( NT )
        XX ( 3 ) = TZ ( NT1 )
*
        IF ( Z .NE. 1.0 ) THEN
          IF ( NT0 .EQ. 24 ) THEN
            YYA ( 1 ) = 17.2880135
            YYB ( 1 ) = 6.93410742E+03
            YYX ( 1 ) = -3.75
          ELSE IF ( NT0 .EQ. 30 ) THEN
            YYA ( 1 ) = 17.4317989
            YYB ( 1 ) = 1.36653821E+03
            YYX ( 1 ) = -3.40
          ELSE
            YYA ( 1 ) = A1( NT0 )
            YYB ( 1 ) = B1( NT0 )
            YYX ( 1 ) = X1( NT0 )
          ENDIF
*
          YYA ( 2 ) = A1( NT  )
          YYA ( 3 ) = A1( NT1 )
          C       =  XMAP ( XX , YYA , ALOGT )
          YYB ( 2 ) = B1( NT  )
          YYB ( 3 ) = B1( NT1 )
          D       = XMAP ( XX , YYB , ALOGT )
          YYX ( 2 ) = X1( NT  )
          YYX ( 3 ) = X1( NT1 )
          U        = XMAP ( XX , YYX , ALOGT )
          V        = 8.90
*
        ELSE
          IF ( NT0 .EQ. 24 ) THEN
            YYA ( 1 ) = 20.1895161
            YYB ( 1 ) = 2.25774918E+01
            YYX ( 1 ) = -1.66
          ELSE IF ( NT0 .EQ. 30 ) THEN
            YYA ( 1 ) = 19.8647804
            YYB ( 1 ) = 6.70408707E+02
            YYX ( 1 ) = -2.12
          ELSE
            YYA ( 1 ) = A2( NT0 )
            YYB ( 1 ) = B2( NT0 )
            YYX ( 1 ) = X2( NT0 )
          ENDIF
*
          YYA ( 2 ) = A2( NT  )
          YYA ( 3 ) = A2( NT1 )
          C       =  XMAP ( XX , YYA , ALOGT )
          YYB ( 2 ) = B2( NT  )
          YYB ( 3 ) = B2( NT1 )
          D       = XMAP ( XX , YYB , ALOGT )
          YYX ( 2 ) = X2( NT  )
          YYX ( 3 ) = X2( NT1 )
          U        = XMAP ( XX , YYX , ALOGT )
          V        = 9.40
        ENDIF
*
        XNC    = XINVRS ( ALOGN , C , D , U , V , JFAIL )
        ALOGNC = ALOG10 ( XNC )
*
        IF ( NT0 .EQ. 24 ) THEN
          YYA ( 1 ) = -8.04963875
          YYB ( 1 ) = 1.07205127E+03
          YYY ( 1 ) = 2.05
        ELSE IF ( NT0 .EQ. 30 ) THEN
          YYA ( 1 ) = -8.54721069
          YYB ( 1 ) = 4.70450195E+02
          YYY ( 1 ) = 2.05
        ELSE
          YYA ( 1 ) = A0 ( NT0 )
          YYB ( 1 ) = B0 ( NT0 )
          YYY ( 1 ) = Y0 ( NT0 )
        ENDIF
*
        YYA ( 2 ) = A0 ( NT  )
        YYA ( 3 ) = A0 ( NT1 )
        A       = XMAP ( XX , YYA , ALOGT )
        YYB ( 2 ) = B0 ( NT  )
        YYB ( 3 ) = B0 ( NT1 )
        B       = XMAP ( XX , YYB , ALOGT )
        X         = -2.45
        YYY ( 2 ) = Y0 ( NT  )
        YYY ( 3 ) = Y0 ( NT1 )
        Y         = XMAP ( XX , YYY, ALOGT )
      ENDIF
*
      DA  = Z * 10. ** ( A - Y * ALOGNC + B * XNC ** X )
      IFAIL = IFAIL + JFAIL
*
      RETURN
      END



      BLOCK DATA DALPHA
C
      PARAMETER ( MAXZ = 28 )
C
C BLOCK DATA FOR FUNCTION DA.
C
C     S. A. COTA, 2/1987
C
      COMMON/PARMS/ TZ( 83), ZLOG7(28), ZLOG2(28)
      COMMON/DAX/ A0( 83),B0( 83),X0( 83)
      COMMON/XCX/ A1( 83),B1( 83),X1( 83)
      COMMON/XNX/ A2( 21: 83),B2( 21: 83),X2( 21: 83)
      double precision BA0 ( 79 ) , BB0 ( 4 ) , BA1 ( 79 ), BB1 ( 4 )
      EQUIVALENCE ( BA0 ( 1 ) , B0 ( 1 ) ) , ( BB0 ( 1 ) , B0 ( 80 ) )
      EQUIVALENCE ( BA1 ( 1 ) , B1 ( 1 ) ) , ( BB1 ( 1 ) , B1 ( 80 ) )
      DATA ZLOG7/
     * 0.00000E+00, 2.10721E+00, 3.33985E+00, 4.21442E+00, 4.89279E+00,
     * 5.44706E+00, 5.91569E+00, 6.32163E+00, 6.67970E+00, 7.00000E+00,
     * 7.28975E+00, 7.55427E+00, 7.79760E+00, 8.02290E+00, 8.23264E+00,
     * 8.42884E+00, 8.61314E+00, 8.78691E+00, 8.95128E+00, 9.10721E+00,
     * 9.25554E+00, 9.39696E+00, 9.53209E+00, 9.66148E+00, 9.78558E+00,
     * 9.90481E+00,10.01954E+00,10.13010E+00/
      DATA ZLOG2/
     * 0.00000E+00, 6.02060E-01, 9.54243E-01, 1.20412E+00, 1.39794E+00,
     * 1.55630E+00, 1.69020E+00, 1.80618E+00, 1.90849E+00, 2.00000E+00,
     * 2.08279E+00, 2.15836E+00, 2.22789E+00, 2.29226E+00, 2.35218E+00,
     * 2.40824E+00, 2.46090E+00, 2.51055E+00, 2.55751E+00, 2.60206E+00,
     * 2.64444E+00, 2.68485E+00, 2.72346E+00, 2.76042E+00, 2.79588E+00,
     * 2.82995E+00, 2.86272E+00, 2.89431E+00/
      DATA TZ /
     *0.     ,0.09691,0.17609,0.30103,0.39794,
     *0.47712,0.60206,0.69897,0.77815,0.90309,
     *1.00000,1.07918,1.20412,1.30103,1.39794,
     *1.47712,1.60206,1.69897,1.77815,1.90309,
     *2.00000,2.06070,2.09691,2.17609,2.20412,
     *2.24304,2.30103,2.35218,2.39794,2.47712,
     *2.51188,2.54407,2.60206,2.65321,2.69897,
     *2.75967,2.81291,2.86034,2.91645,2.95424,
     *3.00000,3.07918,3.11394,3.17609,3.20412,
     *3.25527,3.30103,3.36173,3.39794,3.46240,
     *3.51188,3.56820,3.60206,3.66276,3.72016,
     *3.76343,3.81291,3.86034,3.90309,3.95424,
     *4.02119,4.06070,4.11394,4.16137,4.20412,
     *4.25527,4.31175,4.36173,4.41497,4.46240,
     *4.51521,4.56526,4.61542,4.66605,4.71600,
     *4.76343,4.81624,4.86629,4.91645,4.96614,
     *5.02119,5.06726,5.11394/
      DATA A0 /
     * -4.31396484, -4.56640625, -4.74560547, -4.98535156, -5.15373850,
     * -5.28123093, -5.48215008, -5.63811255, -5.76573515, -5.96755028,
     * -6.12449837, -6.25304174, -6.45615673, -6.61384058, -6.77161551,
     * -6.90069818, -7.10470295, -7.26322412, -7.39289951, -7.59792519,
     * -7.75725508, -7.85722494, -7.91697407, -8.04758644, -8.09447479,
     * -8.15859795, -8.25424385, -8.33880615, -8.41452408, -8.54581165,
     * -8.60400581, -8.65751839, -8.75414848, -8.83946800, -8.91589737,
     * -9.01741695, -9.10663033, -9.18621922, -9.28059292, -9.34430218,
     * -9.42154408, -9.55562973, -9.61459446, -9.72023010, -9.76802444,
     * -9.85540199, -9.93374062,-10.03800774,-10.10044670,-10.21178055,
     *-10.29757786,-10.39561272,-10.45469666,-10.56102180,-10.66205502,
     *-10.73780537,-10.82557774,-10.91007328,-10.98659325,-11.07857418,
     *-11.19975281,-11.27170753,-11.36930943,-11.45675945,-11.53620148,
     *-11.63198853,-11.73875237,-11.83400822,-11.93677044,-12.02933311,
     *-12.13374519,-12.23410702,-12.33664989,-12.44163322,-12.54730415,
     *-12.64975739,-12.76682186,-12.88185978,-13.00052643,-13.12289810,
     *-13.26689529,-13.39390945,-30.00000000/
C  -13.53340816/
      DATA BA0 /        4.53776000E+05, 3.48304000E+05, 2.80224000E+05,
     * 1.98128000E+05, 1.51219797E+05, 1.21113266E+05, 8.52812109E+04,
     * 6.49598125E+04, 5.20075781E+04, 3.66190977E+04, 2.79060723E+04,
     * 2.23634102E+04, 1.57683135E+04, 1.20284307E+04, 9.17755273E+03,
     * 7.36044873E+03, 5.19871680E+03, 3.97240796E+03, 3.18934326E+03,
     * 2.25737622E+03, 1.72767114E+03, 1.46202722E+03, 1.32456628E+03,
     * 1.06499792E+03, 9.92735291E+02, 8.91604858E+02, 7.59411560E+02,
     * 6.59120056E+02, 5.80688965E+02, 4.66602264E+02, 4.27612854E+02,
     * 3.91531494E+02, 3.34516968E+02, 2.91021820E+02, 2.56853912E+02,
     * 2.17598007E+02, 1.88145462E+02, 1.65329865E+02, 1.41960342E+02,
     * 1.28181686E+02, 1.13336761E+02, 9.17785034E+01, 8.36242981E+01,
     * 7.08843536E+01, 6.58346100E+01, 5.75790634E+01, 5.11293755E+01,
     * 4.37563019E+01, 3.99226875E+01, 3.39562836E+01, 3.00413170E+01,
     * 2.61871891E+01, 2.41310368E+01, 2.08853607E+01, 1.82632275E+01,
     * 1.60007000E+01, 1.42617064E+01, 1.27951088E+01, 1.16221066E+01,
     * 1.03779335E+01, 8.97864914E+00, 8.25593281E+00, 7.39339924E+00,
     * 6.70784378E+00, 6.16084862E+00, 5.57818031E+00, 5.01341105E+00,
     * 4.55679178E+00, 4.13692093E+00, 3.80004382E+00, 3.46328306E+00,
     * 3.17340493E+00, 2.93525696E+00, 2.69083858E+00, 2.46588683E+00,
     * 2.26083040E+00, 2.04337358E+00, 1.89027369E+00, 1.69208312E+00/
      DATA BB0 /
     * 1.48992336E+00, 1.32466662E+00, 1.10697949E+00,  9.29813862E-01/
      DATA X0 /
     *2.12597656,2.08984375,2.06958008,2.05444336,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      ,2.05      ,2.05      ,
     *2.05      ,2.05      ,2.05      /
      DATA A1 /
     * 16.23337936, 16.27946854, 16.31696320, 16.37597656, 16.42210960,
     * 16.45996284, 16.51994896, 16.56644440, 16.60460854, 16.66510773,
     * 16.71198654, 16.75038719, 16.81106949, 16.85778809, 16.90416527,
     * 16.94209099, 17.00195694, 17.04838943, 17.08633804, 17.14627838,
     * 17.19270515, 17.22186279, 17.23933601, 17.27728271, 17.30161858,
     * 17.32085800, 17.34928894, 17.37349129, 17.39528084, 17.43282318,
     * 17.44827652, 17.46357536, 17.49082375, 17.51517677, 17.53697205,
     * 17.56587219, 17.59125519, 17.61410332, 17.64081383, 17.65900803,
     * 17.68086433, 17.71843529, 17.73512840, 17.76512146, 17.77873421,
     * 17.80340767, 17.82530022, 17.85470963, 17.87210464, 17.90334511,
     * 17.92751503, 17.95458603, 17.97117233, 18.00062943, 18.02842712,
     * 18.04934502, 18.07340050, 18.09639168, 18.11732864, 18.14218903,
     * 18.17465591, 18.19370079, 18.21962166, 18.24237251, 18.26305962,
     * 18.28767967, 18.31531525, 18.33900452, 18.36478043, 18.38741112,
     * 18.41271973, 18.43644333, 18.46075630, 18.48509216, 18.50897980,
     * 18.53143501, 18.55570030, 18.58008003, 18.60348320, 18.62536430,
     * 18.65199852, 18.67623520, 18.70072174/
      DATA BA1 /       1.09462866E+10, 9.32986675E+09, 6.15947008E+09,
     * 1.54486170E+09, 1.00812454E+09, 7.00559552E+08, 6.25999232E+08,
     * 3.50779968E+08, 3.11956288E+08, 3.74866016E+08, 2.47019424E+08,
     * 1.73169776E+08, 1.01753168E+08, 6.81861920E+07, 4.61764000E+07,
     * 3.31671360E+07, 2.03160540E+07, 1.40249480E+07, 1.02577860E+07,
     * 3.53822650E+06, 1.32563388E+06, 9.14284688E+05, 1.25230388E+06,
     * 3.17865156E+05, 4.76750244E+03, 4.81107031E+03, 4.88406152E+03,
     * 4.80611279E+03, 4.78843652E+03, 4.65988477E+03, 1.26723059E+03,
     * 1.20825342E+03, 8.66052612E+02, 7.76661316E+02, 7.05279358E+02,
     * 6.21722656E+02, 5.46207581E+02, 4.96247742E+02, 4.26340118E+02,
     * 3.96090424E+02, 3.48429657E+02, 2.37949142E+02, 2.14678406E+02,
     * 1.81019180E+02, 1.68923676E+02, 1.45979385E+02, 1.25311127E+02,
     * 1.05205528E+02, 9.39378357E+01, 7.75339966E+01, 6.68987427E+01,
     * 5.53580055E+01, 5.00100212E+01, 4.14198608E+01, 3.46289063E+01,
     * 3.00775223E+01, 2.60294399E+01, 2.26602840E+01, 2.02123032E+01,
     * 1.76353855E+01, 1.47198439E+01, 1.33078461E+01, 1.17181997E+01,
     * 1.04125805E+01, 9.45785904E+00, 8.42799950E+00, 7.62769842E+00,
     * 6.85484743E+00, 6.25903368E+00, 5.75135279E+00, 5.28468180E+00,
     * 4.87669659E+00, 4.57353973E+00, 4.30108690E+00, 4.05412245E+00,
     * 3.83283114E+00, 3.57902861E+00, 3.43705726E+00, 3.26563096E+00/
      DATA BB1/        3.07498097E+00, 2.96334076E+00, 2.92890000E+00,
     *                 2.89550042E+00/
      DATA X1/
     *-5.46,-5.51,-5.49,-5.30,-5.29,-5.28,-5.37,-5.33,-5.38,-5.55,
     *-5.55,-5.55,-5.55,-5.55,-5.55,-5.55,-5.55,-5.55,-5.55,-5.38,
     *-5.19,-5.14,-5.27,-4.93,-3.64,-3.68,-3.74,-3.78,-3.82,-3.88,
     *-3.40,-3.41,-3.32,-3.32,-3.32,-3.32,-3.31,-3.31,-3.29,-3.29,
     *-3.27,-3.16,-3.14,-3.11,-3.10,-3.07,-3.03,-2.99,-2.96,-2.91,
     *-2.87,-2.81,-2.78,-2.72,-2.66,-2.61,-2.56,-2.51,-2.47,-2.42,
     *-2.35,-2.31,-2.26,-2.21,-2.17,-2.12,-2.08,-2.03,-1.99,-1.95,
     *-1.91,-1.87,-1.84,-1.81,-1.78,-1.75,-1.71,-1.69,-1.66,-1.62,
     *-1.60,-1.60,-1.60/
      DATA A2 /
     * 20.30049515, 20.28500366, 20.25300407, 20.16626740, 20.15743256,
     * 20.11256981, 20.04818344, 19.99261856, 19.94472885, 19.86478043,
     * 19.83321571, 19.80185127, 19.74884224, 19.70136070, 19.65981102,
     * 19.60598755, 19.56017494, 19.52042389, 19.47429657, 19.44413757,
     * 19.40796280, 19.34819984, 19.32203293, 19.27634430, 19.25627136,
     * 19.22009087, 19.18853378, 19.14809799, 19.12456703, 19.08409119,
     * 19.05431557, 19.02083015, 19.00176430, 18.96817970, 18.93762589,
     * 18.91706085, 18.89299583, 18.87085915, 18.85210609, 18.83035851,
     * 18.80403900, 18.78901100, 18.77099228, 18.75540161, 18.74287033,
     * 18.72928810, 18.71601868, 18.70474434, 18.69515800, 18.68782425,
     * 18.68120766, 18.67630005, 18.67357445, 18.67129898, 18.67042351,
     * 18.67090988, 18.67313004, 18.67636490, 18.68120003, 18.68803024,
     * 18.69487381, 18.70458412, 18.71205139/
      DATA B2 /
     * 1.01078403E+00, 1.97956896E+00, 3.14605665E+00, 6.46874905E+00,
     * 3.16406364E+01, 3.74927673E+01, 4.75353088E+01, 5.27809143E+01,
     * 5.86515846E+01, 6.70408707E+01, 1.14904137E+02, 1.03133133E+02,
     * 1.26508728E+02, 1.03827606E+02, 8.79508896E+01, 7.18328934E+01,
     * 6.19807892E+01, 5.51255455E+01, 4.87156143E+01, 4.58579826E+01,
     * 4.19952011E+01, 4.08252220E+01, 3.78243637E+01, 3.34573860E+01,
     * 3.19036102E+01, 2.92026005E+01, 2.74482193E+01, 2.54643936E+01,
     * 2.46636391E+01, 2.33054180E+01, 2.23069897E+01, 2.12891216E+01,
     * 2.06667900E+01, 1.96430798E+01, 1.87381802E+01, 1.76523514E+01,
     * 1.69235287E+01, 1.62647285E+01, 1.56806908E+01, 1.50346069E+01,
     * 1.42240467E+01, 1.37954988E+01, 1.31949224E+01, 1.27211905E+01,
     * 1.22885675E+01, 1.17868662E+01, 1.12577572E+01, 1.08565578E+01,
     * 1.04121590E+01, 1.00410652E+01, 9.64534473E+00, 9.29232121E+00,
     * 8.92519569E+00, 8.60898972E+00, 8.31234550E+00, 8.04089737E+00,
     * 7.74343491E+00, 7.48133039E+00, 7.21957016E+00, 6.94726801E+00,
     * 6.71931219E+00, 6.45107985E+00, 6.28593779E+00/
      DATA X2/
     *-0.43,-0.75,-0.93,-1.20,-1.78,-1.85,-1.95,-2.00,-2.05,-2.12,
     *-2.34,-2.31,-2.42,-2.36,-2.31,-2.25,-2.21,-2.18,-2.15,-2.14,
     *-2.12,-2.14,-2.12,-2.09,-2.08,-2.06,-2.05,-2.04,-2.04,-2.04,
     *-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,
     *-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,
     *-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,-2.04,
     *-2.04,-2.04,-2.04/
      END


