
c	program calculate
c	implicit none
	


	subroutine getrate(temperature, el_id, ion_id, rate)
	implicit none

c	program get_rate
c	implicit none

c	define universal constants
	double precision k_B, kpc2cm
	double precision   m_H        ! proton mass (gr)


c	declare functions/subroutines to be used
	external colion, ct_rec, ct_ion, allrec
	double precision colion, ct_rec, ct_ion
	external he_ct_r, he_ct_i
	double precision he_ct_r, he_ct_i
	external He_alpha1, He_alphaB, H_alphaB,HeII_alphaB
	double precision He_alpha1, He_alphaB, H_alphaB,HeII_alphaB
	external recfer1
	double precision recfer1
	external Auger_pop_rate
	double precision Auger_pop_rate
	double precision allrec_mat(1:30, 0:30)

c	declare common variables/arrays
	COMMON/RECB/re(300),unused(300),unused2(300)
	double precision re, unused, unused2
	common/spectrum/nu(10000),Jnu(10000),VecSize
	double precision nu, Jnu
	integer Vecsize

	common/model/PoverK,T_HIM,nH,currentT,old_time,new_time,epsl,D,
     + H_HE_case,HeIIcase
	double precision PoverK   ! pressure in units of P/k_B (cm^-3 K).
	double precision T_HIM    ! initial temperature.
	double precision nH       ! (cm^-3) H den 
	double precision currentT ! present temperature
	double precision epsl
	double precision old_time
	double precision new_time
	double precision D        ! cloud size
	integer H_HE_case,HeIIcase

	common/Gas/X_He, X_C, X_N, X_O, X_Si, X_S, X_Ne, X_Fe, X_Mg
	double precision X_He, X_C, X_N, X_O, X_Si, X_S !abundances relative to H
	double precision X_Ne, X_Fe, X_Mg


c	variables 
	double precision xHII, xHeIII, xCVII, xNVIII, xOIX, xSiXV, xSXVII
	double precision xNeXI, xFeXXVII, xMgXIII
	integer H_ , He_, C_, N_, O_, Ne_, Mg_, Si_, S_, Fe_
	integer I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII
	integer XIV,XV,XVI,XVII
	integer XVIII,XIX,XX,XXI,XXII,XXIII,XXIV,XXV,XXVI,XXVII

	integer H_I
	integer He_I, He_II
	integer C_I, C_II, C_III, C_IV, C_V, C_VI
	integer N_I, N_II, N_III, N_IV, N_V, N_VI, N_VII
	integer O_I, O_II, O_III, O_IV, O_V, O_VI, O_VII, O_VIII
	integer Ne_I, Ne_II, Ne_III, Ne_IV, Ne_V, Ne_VI, Ne_VII, Ne_VIII
	integer Ne_IX, Ne_X
	integer Mg_I, Mg_II, Mg_III, Mg_IV, Mg_V, Mg_VI, Mg_VII, Mg_VIII
	integer Mg_IX, Mg_X, Mg_XI, Mg_XII
	integer Si_I,Si_II,Si_III,Si_IV,Si_V,Si_VI,Si_VII,Si_VIII,Si_IX
	integer Si_X,Si_XI,Si_XII,Si_XIII,Si_XIV
	integer S_I, S_II, S_III, S_IV, S_V, S_VI, S_VII, S_VIII, S_IX
	integer S_X, S_XI, S_XII, S_XIII, S_XIV, S_XV, S_XVI
	integer Fe_I, Fe_II, Fe_III, Fe_IV, Fe_V, Fe_VI, Fe_VII, Fe_VIII
	integer Fe_IX, Fe_X, Fe_XI, Fe_XII, Fe_XIII, Fe_XIV, Fe_XV, Fe_XVI
	integer Fe_XVII, Fe_XVIII, Fe_XIX, Fe_XX, Fe_XXI, Fe_XXII
	integer Fe_XXIII, Fe_XXIV, Fe_XXV, Fe_XXVI

	double precision temperature, e_den, rate(0:6)
        integer el_id, ion_id

c	initialise variables
	parameter (H_=1,He_=2,C_=6,N_=7,O_=8,Ne_=10,Mg_=12,Si_=14)
	parameter (S_=16,Fe_=26)
	parameter (I=0,II=1,III=2,IV=3,V=4,VI=5,VII=6,VIII=7)
	parameter (IX=8,X=9,XI=10,XII=11,XIII=12,XIV=13,XV=14)
	parameter (XVI=15,XVII=16)
	parameter (XVIII=17,XIX=18,XX=19,XXI=20,XXII=21,XXIII=22)
	parameter (XXIV=23,XXV=24,XXVI=25,XXVII=26)

c --- define meaningfull names for indices in vector n:
	parameter (H_I= 1)
	parameter (He_I= 2, He_II= 3)
	parameter (C_I=4 , C_II= 5, C_III= 6, C_IV= 7, C_V= 8, C_VI= 9)
	parameter (N_I= 10, N_II= 11, N_III= 12, N_IV= 13, N_V= 14)
	parameter (N_VI= 15, N_VII= 16)
	parameter (O_I= 17, O_II= 18, O_III= 19, O_IV= 20, O_V= 21)
	parameter (O_VI= 22, O_VII= 23, O_VIII= 24)
	parameter (Si_I= 25, Si_II= 26, Si_III= 27, Si_IV= 28, Si_V= 29)
	parameter (Si_VI= 30, Si_VII= 31, Si_VIII= 32, Si_IX= 33)
	parameter (Si_X= 34, Si_XI= 35, Si_XII= 36, Si_XIII= 37)
	parameter (Si_XIV= 38)
	parameter (S_I= 39, S_II= 40, S_III= 41, S_IV= 42, S_V= 43)
	parameter (S_VI= 44)
	parameter (S_VII= 45, S_VIII= 46, S_IX= 47, S_X= 48, S_XI= 49)
	parameter (S_XII= 50, S_XIII= 51, S_XIV= 52, S_XV= 53, S_XVI= 54)
	parameter (Ne_I=55,Ne_II=56,Ne_III=57,Ne_IV=58,Ne_V=59,Ne_VI=60)
	parameter (Ne_VII=61,Ne_VIII=62,Ne_IX=63,Ne_X=64)
	parameter (Fe_I=65,Fe_II=66,Fe_III=67,Fe_IV=68,Fe_V=69,Fe_VI=70)
	parameter (Fe_VII=71,Fe_VIII=72,Fe_IX=73,Fe_X=74,Fe_XI=75)
	parameter (Fe_XII=76,Fe_XIII=77,Fe_XIV=78,Fe_XV=79,Fe_XVI=80)
	parameter (Fe_XVII=81,Fe_XVIII=82,Fe_XIX=83,Fe_XX=84,Fe_XXI=85)
	parameter (Fe_XXII=86,Fe_XXIII=87,Fe_XXIV=88,Fe_XXV=89,Fe_XXVI=90)
	parameter (Mg_I=91,Mg_II=92,Mg_III=93,Mg_IV=94,Mg_V=95,Mg_VI=96)
        parameter (Mg_VII=97,Mg_VIII=98,Mg_IX=99,Mg_X=100,Mg_XI=101)
        parameter (Mg_XII=102)

        parameter (kpc2cm=3.08e21,k_B=1.38e-16,m_H=1.6726e-24)

        parameter (e_den=1.0)


c	calculating the rate coefficients
        call allrec(temperature, e_den)                      
c	now common vector re() contains data
        call allrec_mat_set(re,allrec_mat)     
c 	now allrec_mat(C,IV) is the CIV->CIII rec. coefficient.


c	print *,'total recombinatione rate', allrec_mat(C_, IV)
c	print *, 'ct-rec with HI', ct_rec(C_, IV, temperature)
c	print *, 'ct-ion with HII', ct_ion(C_, IV, temperature)
c	print *, 'ct-rec with HeI', he_ct_r(C_, IV, temperature)
c	print *, 'ct-ion with HeII', he_ct_i(C_, IV, temperature)
c	print *, 'coll-ion', colion(C_, IV, temperature)

        if (ion_id < el_id) then
        rate(0) = colion(el_id, ion_id, temperature)
        rate(1) = allrec_mat(el_id, ion_id+1)
        rate(2) = ct_ion(el_id, ion_id, temperature)
        rate(3) = ct_rec(el_id, ion_id+1, temperature)
        rate(4) = he_ct_r(el_id, ion_id+1, temperature)
        rate(5) = he_ct_i(el_id, ion_id, temperature)
        rate(6) = 0.0
        else
        rate(0) = 0.0
        rate(1) = 0.0
        rate(2) = 0.0
        rate(3) = 0.0
        rate(4) = 0.0
        rate(5) = 0.0
        rate(6) = 0.0
        end if

c	end program get_rate
        end


c  =========================================================================
      subroutine allrec_mat_set(re,allrec_vec)
c  =========================================================================
      implicit none

c     organizes the vector re() set by Hagai's recombination code
c     in a matrix such that (e.g.) allrec_mat(C_,IV) is the CIV->CIII
c     recombination coefficient.

      double precision re(300), allrec_vec(1:30,0:30)

      integer H_, He_, C_, N_, O_, Si_, S_ !for atomic numbers
      integer Ne_, Mg_, Al_, Ar_, Ca_, Fe_

      integer I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII
      integer XIV,XV,XVI,XVII  !for number of electrons
      integer XVIII, XIX, XX, XXI, XXII, XXIII, XXIV, XXV
      integer XXVI, XXVII, XXVIII, XXIX, XXX, XXXI

      integer j,k !indices for loops


      H_  = 1
      He_ = 2 
      C_  = 6
      N_  = 7
      O_  = 8
      Ne_ = 10
      Mg_ = 12
      Al_ = 13
      Si_ = 14
      S_  = 16
      Ar_ = 18
      Ca_ = 20
      Fe_ = 26

      I      = 0
      II     = 1
      III    = 2
      IV     = 3
      V      = 4
      VI     = 5
      VII    = 6
      VIII   = 7
      IX     = 8
      X      = 9
      XI     = 10
      XII    = 11
      XIII   = 12
      XIV    = 13
      XV     = 14
      XVI    = 15
      XVII   = 16
      XVIII  = 17
      XIX    = 18
      XX     = 19
      XXI    = 20
      XXII   = 21
      XXIII  = 22
      XXIV   = 23
      XXV    = 24
      XXVI   = 25
      XXVII  = 26
      XXVIII = 27
      XXIX   = 28
      XXX    = 29
      XXXI   = 30


c     initialize vector:
      do k=1,30
         do j=0,30
            allrec_vec(k,j) = 0
         enddo
      enddo

c     set values:
c     (no particular logic, thats just the way it is in Hagai's code)
      
c     Hydrogen
      allrec_vec(H_,II)     = RE(1)

c     Helium
      allrec_vec(He_,II)    = RE(2)
      allrec_vec(He_,III)   = RE(3)

c     Carbon
      allrec_vec(C_,II)     = RE(10)
      allrec_vec(C_,III)    = RE(11)
      allrec_vec(C_,IV)     = RE(12)
      allrec_vec(C_,V)      = RE(13)
      allrec_vec(C_,VI)     = RE(14)
      allrec_vec(C_,VII)    = RE(15)

c     Nitrogen
      allrec_vec(N_,II)     = RE(16)
      allrec_vec(N_,III)    = RE(17)
      allrec_vec(N_,IV)     = RE(18)
      allrec_vec(N_,V)      = RE(19)
      allrec_vec(N_,VI)     = RE(20)
      allrec_vec(N_,VII)    = RE(21)
      allrec_vec(N_,VIII)   = RE(97)

c     Oxygen
      allrec_vec(O_,II)     = RE(4)
      allrec_vec(O_,III)    = RE(5)
      allrec_vec(O_,IV)     = RE(6)
      allrec_vec(O_,V)      = RE(7)
      allrec_vec(O_,VI)     = RE(8)
      allrec_vec(O_,VII)    = RE(9)
      allrec_vec(O_,VIII)   = RE(98)
      allrec_vec(O_,IX)     = RE(99)

c     Neon
      allrec_vec(Ne_,II)    = RE(48)
      allrec_vec(Ne_,III)   = RE(49)
      allrec_vec(Ne_,IV)    = RE(50)
      allrec_vec(Ne_,V)     = RE(51)
      allrec_vec(Ne_,VI)    = RE(52)
      allrec_vec(Ne_,VII)   = RE(53)
      allrec_vec(Ne_,VIII)  = RE(54)
      allrec_vec(Ne_,IX)    = RE(55)
      allrec_vec(Ne_,X)     = RE(56)
      allrec_vec(Ne_,XI)    = RE(57)

c     Magnesiu
      allrec_vec(Mg_,II)    = RE(36)
      allrec_vec(Mg_,III)   = RE(37)
      allrec_vec(Mg_,IV)    = RE(38)
      allrec_vec(Mg_,V)     = RE(39)
      allrec_vec(Mg_,VI)    = RE(40)
      allrec_vec(Mg_,VII)   = RE(41)
      allrec_vec(Mg_,VIII)  = RE(42)
      allrec_vec(Mg_,IX)    = RE(43)
      allrec_vec(Mg_,X)     = RE(44)
      allrec_vec(Mg_,XI)    = RE(45)
      allrec_vec(Mg_,XII)   = RE(46)
      allrec_vec(Mg_,XIII)  = RE(47)

c     Aluminum
      allrec_vec(Al_,II)    = RE(131)
      allrec_vec(Al_,III)   = RE(132)
      allrec_vec(Al_,IV)    = RE(133)
      allrec_vec(Al_,V)     = RE(134)
      allrec_vec(Al_,VI)    = RE(135)
      allrec_vec(Al_,VII)   = RE(136)
      allrec_vec(Al_,VIII)  = RE(137)
      allrec_vec(Al_,IX)    = RE(138)
      allrec_vec(Al_,X)     = RE(139)
      allrec_vec(Al_,XI)    = RE(140)
      allrec_vec(Al_,XII)   = RE(141)
      allrec_vec(Al_,XIII)  = RE(142)
      allrec_vec(Al_,XIV)   = RE(143)

c     Silicon
      allrec_vec(Si_,II)    = RE(70)
      allrec_vec(Si_,III)   = RE(71)
      allrec_vec(Si_,IV)    = RE(72)
      allrec_vec(Si_,V)     = RE(73)
      allrec_vec(Si_,VI)    = RE(74)
      allrec_vec(Si_,VII)   = RE(75)
      allrec_vec(Si_,VIII)  = RE(76)
      allrec_vec(Si_,IX)    = RE(77)
      allrec_vec(Si_,X)     = RE(78)
      allrec_vec(Si_,XI)    = RE(79)
      allrec_vec(Si_,XII)   = RE(80)
      allrec_vec(Si_,XIII)  = RE(81)
      allrec_vec(Si_,XIV)   = RE(82)
      allrec_vec(Si_,XV)    = RE(83)

c     Sulfur
      allrec_vec(S_,II)     = RE(58)
      allrec_vec(S_,III)    = RE(59)
      allrec_vec(S_,IV)     = RE(60)
      allrec_vec(S_,V)      = RE(61)
      allrec_vec(S_,VI)     = RE(62)
      allrec_vec(S_,VII)    = RE(63)
      allrec_vec(S_,VIII)   = RE(64)
      allrec_vec(S_,IX)     = RE(65)
      allrec_vec(S_,X)      = RE(66)
      allrec_vec(S_,XI)     = RE(67)
      allrec_vec(S_,XII)    = RE(68)
      allrec_vec(S_,XIII)   = RE(69)
      allrec_vec(S_,XIV)    = RE(100)
      allrec_vec(S_,XV)     = RE(101)
      allrec_vec(S_,XVI)    = RE(102)
      allrec_vec(S_,XVII)   = RE(103)

c     Argon
      allrec_vec(Ar_,II)    = RE(181)
      allrec_vec(Ar_,III)   = RE(182)
      allrec_vec(Ar_,IV)    = RE(183)
      allrec_vec(Ar_,V)     = RE(184)
      allrec_vec(Ar_,VI)    = RE(185)
      allrec_vec(Ar_,VII)   = RE(186)
      allrec_vec(Ar_,VIII)  = RE(187)
      allrec_vec(Ar_,IX)    = RE(188)
      allrec_vec(Ar_,X)     = RE(189)
      allrec_vec(Ar_,XI)    = RE(190)
      allrec_vec(Ar_,XII)   = RE(191)
      allrec_vec(Ar_,XIII)  = RE(192)
      allrec_vec(Ar_,XIV)   = RE(193)
      allrec_vec(Ar_,XV)    = RE(194)
      allrec_vec(Ar_,XVI)   = RE(195)
      allrec_vec(Ar_,XVII)  = RE(196)
      allrec_vec(Ar_,XVIII) = RE(197)
      allrec_vec(Ar_,XIX)   = RE(198)


c     Calcium
      allrec_vec(Ca_,II)    = RE(201)
      allrec_vec(Ca_,III)   = RE(202)
      allrec_vec(Ca_,IV)    = RE(203)
      allrec_vec(Ca_,V)     = RE(204)
      allrec_vec(Ca_,VI)    = RE(205)
      allrec_vec(Ca_,VII)   = RE(206)
      allrec_vec(Ca_,VIII)  = RE(207)
      allrec_vec(Ca_,IX)    = RE(208)
      allrec_vec(Ca_,X)     = RE(209)
      allrec_vec(Ca_,XI)    = RE(210)
      allrec_vec(Ca_,XII)   = RE(211)
      allrec_vec(Ca_,XIII)  = RE(212)
      allrec_vec(Ca_,XIV)   = RE(213)
      allrec_vec(Ca_,XV)    = RE(214)
      allrec_vec(Ca_,XVI)   = RE(215)
      allrec_vec(Ca_,XVII)  = RE(216)
      allrec_vec(Ca_,XVIII) = RE(217)
      allrec_vec(Ca_,XIX)   = RE(218)
      allrec_vec(Ca_,XX)    = RE(219)
      allrec_vec(Ca_,XXI)   = RE(220)

c     Iron
      allrec_vec(Fe_,II)    = RE(22)
      allrec_vec(Fe_,III)   = RE(23)
      allrec_vec(Fe_,IV)    = RE(24)
      allrec_vec(Fe_,V)     = RE(25)
      allrec_vec(Fe_,VI)    = RE(26)
      allrec_vec(Fe_,VII)   = RE(27)
      allrec_vec(Fe_,VIII)  = RE(28)
      allrec_vec(Fe_,IX)    = RE(29)
      allrec_vec(Fe_,X)     = RE(30)
      allrec_vec(Fe_,XI)    = RE(31)
      allrec_vec(Fe_,XII)   = RE(32)
      allrec_vec(Fe_,XIII)  = RE(33)
      allrec_vec(Fe_,XIV)   = RE(34)
      allrec_vec(Fe_,XV)    = RE(35)
      allrec_vec(Fe_,XVI)   = RE(85)
      allrec_vec(Fe_,XVII)  = RE(86)
      allrec_vec(Fe_,XVIII) = RE(87)
      allrec_vec(Fe_,XIX)   = RE(88)
      allrec_vec(Fe_,XX)    = RE(89)
      allrec_vec(Fe_,XXI)   = RE(90)
      allrec_vec(Fe_,XXII)  = RE(91)
      allrec_vec(Fe_,XXIII) = RE(92)
      allrec_vec(Fe_,XXIV)  = RE(93)
      allrec_vec(Fe_,XXV)   = RE(94)
      allrec_vec(Fe_,XXVI)  = RE(95)
      allrec_vec(Fe_,XXVII) = RE(96)

      return
      end
