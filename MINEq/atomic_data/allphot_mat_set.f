c  =========================================================================
      subroutine allphot_mat_set(allphot_vec)
c  =========================================================================
      implicit none

c     organizes the vector re() set by Hagai's recombination code
c     in a matrix such that (e.g.) allrec_mat(C_,IV) is the CIV->CIII
c     recombination coefficient.

      double precision allphot_vec(1:30,0:30,0:7)

      integer H_, He_, C_, N_, O_, Si_, S_ !for atomic numbers
      integer Ne_, Mg_, Al_, Ar_, Ca_, Fe_

      integer I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII
      integer XIV,XV,XVI,XVII  !for number of electrons
      integer XVIII, XIX, XX, XXI, XXII, XXIII, XXIV, XXV
      integer XXVI, XXVII, XXVIII, XXIX, XXX, XXXI

      integer lall_,l1s_,l2s_,l2p_,l3s_,l3p_,l3d_,l4s_

      integer j,k,sh !indices for loops

      external Gamma_i
      double precision Gamma_i


      H_  = 1
      He_ = 2 
      C_  = 6
      N_  = 7
      O_  = 8
      Ne_ = 10
      Mg_ = 12
      Al_ = 13
      Si_ = 14
      S_  = 16
      Ar_ = 18
      Ca_ = 20
      Fe_ = 26

      I      = 0
      II     = 1
      III    = 2
      IV     = 3
      V      = 4
      VI     = 5
      VII    = 6
      VIII   = 7
      IX     = 8
      X      = 9
      XI     = 10
      XII    = 11
      XIII   = 12
      XIV    = 13
      XV     = 14
      XVI    = 15
      XVII   = 16
      XVIII  = 17
      XIX    = 18
      XX     = 19
      XXI    = 20
      XXII   = 21
      XXIII  = 22
      XXIV   = 23
      XXV    = 24
      XXVI   = 25
      XXVII  = 26
      XXVIII = 27
      XXIX   = 28
      XXX    = 29
      XXXI   = 30

      lall_ = 0
      l1s_ = 1
      l2s_ = 2
      l2p_ = 3
      l3s_ = 4
      l3p_ = 5
      l3d_ = 6
      l4s_ = 7

c     initialize vector:
      do k=1,30
         do j=0,30
            do sh=0,7
               allphot_vec(k,j,sh) = 0
            enddo
         enddo
      enddo

c Hydrogen

      allphot_vec(H_,I,l1s_)  = Gamma_i(H_,I,l1s_)
      allphot_vec(H_,I,l2s_)  = Gamma_i(H_,I,l2s_)
      allphot_vec(H_,I,l2p_)  = Gamma_i(H_,I,l2p_)
      allphot_vec(H_,I,l3s_)  = Gamma_i(H_,I,l3s_)
      allphot_vec(H_,I,l3p_)  = Gamma_i(H_,I,l3p_)
      allphot_vec(H_,I,l3d_)  = Gamma_i(H_,I,l3d_)
      allphot_vec(H_,I,l4s_)  = Gamma_i(H_,I,l4s_)
      allphot_vec(H_,I,lall_) = allphot_vec(H_,I,l1s_) +
     +                allphot_vec(H_,I,l2s_) + allphot_vec(H_,I,l2p_) +
     +                allphot_vec(H_,I,l3s_) + allphot_vec(H_,I,l3p_) +
     +                allphot_vec(H_,I,l3d_) + allphot_vec(H_,I,l4s_) 

c Helium

      allphot_vec(He_,I,l1s_)  = Gamma_i(He_,I,l1s_)
      allphot_vec(He_,I,l2s_)  = Gamma_i(He_,I,l2s_)
      allphot_vec(He_,I,l2p_)  = Gamma_i(He_,I,l2p_)
      allphot_vec(He_,I,l3s_)  = Gamma_i(He_,I,l3s_)
      allphot_vec(He_,I,l3p_)  = Gamma_i(He_,I,l3p_)
      allphot_vec(He_,I,l3d_)  = Gamma_i(He_,I,l3d_)
      allphot_vec(He_,I,l4s_)  = Gamma_i(He_,I,l4s_)
      allphot_vec(He_,I,lall_) = allphot_vec(He_,I,l1s_) +
     +              allphot_vec(He_,I,l2s_) + allphot_vec(He_,I,l2p_) +
     +              allphot_vec(He_,I,l3s_) + allphot_vec(He_,I,l3p_) +
     +              allphot_vec(He_,I,l3d_) + allphot_vec(He_,I,l4s_) 

      allphot_vec(He_,II,l1s_)  = Gamma_i(He_,II,l1s_)
      allphot_vec(He_,II,l2s_)  = Gamma_i(He_,II,l2s_)
      allphot_vec(He_,II,l2p_)  = Gamma_i(He_,II,l2p_)
      allphot_vec(He_,II,l3s_)  = Gamma_i(He_,II,l3s_)
      allphot_vec(He_,II,l3p_)  = Gamma_i(He_,II,l3p_)
      allphot_vec(He_,II,l3d_)  = Gamma_i(He_,II,l3d_)
      allphot_vec(He_,II,l4s_)  = Gamma_i(He_,II,l4s_)
      allphot_vec(He_,II,lall_) = allphot_vec(He_,II,l1s_) +
     +             allphot_vec(He_,II,l2s_) + allphot_vec(He_,II,l2p_) +
     +             allphot_vec(He_,II,l3s_) + allphot_vec(He_,II,l3p_) +
     +             allphot_vec(He_,II,l3d_) + allphot_vec(He_,II,l4s_) 


c Carbon

      allphot_vec(C_,I,l1s_)  = Gamma_i(C_,I,l1s_)
      allphot_vec(C_,I,l2s_)  = Gamma_i(C_,I,l2s_)
      allphot_vec(C_,I,l2p_)  = Gamma_i(C_,I,l2p_)
      allphot_vec(C_,I,l3s_)  = Gamma_i(C_,I,l3s_)
      allphot_vec(C_,I,l3p_)  = Gamma_i(C_,I,l3p_)
      allphot_vec(C_,I,l3d_)  = Gamma_i(C_,I,l3d_)
      allphot_vec(C_,I,l4s_)  = Gamma_i(C_,I,l4s_)
      allphot_vec(C_,I,lall_) = allphot_vec(C_,I,l1s_) +
     +              allphot_vec(C_,I,l2s_) + allphot_vec(C_,I,l2p_) +
     +              allphot_vec(C_,I,l3s_) + allphot_vec(C_,I,l3p_) +
     +              allphot_vec(C_,I,l3d_) + allphot_vec(C_,I,l4s_) 

      allphot_vec(C_,II,l1s_)  = Gamma_i(C_,II,l1s_)
      allphot_vec(C_,II,l2s_)  = Gamma_i(C_,II,l2s_)
      allphot_vec(C_,II,l2p_)  = Gamma_i(C_,II,l2p_)
      allphot_vec(C_,II,l3s_)  = Gamma_i(C_,II,l3s_)
      allphot_vec(C_,II,l3p_)  = Gamma_i(C_,II,l3p_)
      allphot_vec(C_,II,l3d_)  = Gamma_i(C_,II,l3d_)
      allphot_vec(C_,II,l4s_)  = Gamma_i(C_,II,l4s_)
      allphot_vec(C_,II,lall_) = allphot_vec(C_,II,l1s_) +
     +              allphot_vec(C_,II,l2s_) + allphot_vec(C_,II,l2p_) +
     +              allphot_vec(C_,II,l3s_) + allphot_vec(C_,II,l3p_) +
     +              allphot_vec(C_,II,l3d_) + allphot_vec(C_,II,l4s_) 

      allphot_vec(C_,III,l1s_)  = Gamma_i(C_,III,l1s_)
      allphot_vec(C_,III,l2s_)  = Gamma_i(C_,III,l2s_)
      allphot_vec(C_,III,l2p_)  = Gamma_i(C_,III,l2p_)
      allphot_vec(C_,III,l3s_)  = Gamma_i(C_,III,l3s_)
      allphot_vec(C_,III,l3p_)  = Gamma_i(C_,III,l3p_)
      allphot_vec(C_,III,l3d_)  = Gamma_i(C_,III,l3d_)
      allphot_vec(C_,III,l4s_)  = Gamma_i(C_,III,l4s_)
      allphot_vec(C_,III,lall_) = allphot_vec(C_,III,l1s_) +
     +            allphot_vec(C_,III,l2s_) + allphot_vec(C_,III,l2p_) +
     +            allphot_vec(C_,III,l3s_) + allphot_vec(C_,III,l3p_) +
     +            allphot_vec(C_,III,l3d_) + allphot_vec(C_,III,l4s_) 

      allphot_vec(C_,IV,l1s_)  = Gamma_i(C_,IV,l1s_)
      allphot_vec(C_,IV,l2s_)  = Gamma_i(C_,IV,l2s_)
      allphot_vec(C_,IV,l2p_)  = Gamma_i(C_,IV,l2p_)
      allphot_vec(C_,IV,l3s_)  = Gamma_i(C_,IV,l3s_)
      allphot_vec(C_,IV,l3p_)  = Gamma_i(C_,IV,l3p_)
      allphot_vec(C_,IV,l3d_)  = Gamma_i(C_,IV,l3d_)
      allphot_vec(C_,IV,l4s_)  = Gamma_i(C_,IV,l4s_)
      allphot_vec(C_,IV,lall_) = allphot_vec(C_,IV,l1s_) +
     +              allphot_vec(C_,IV,l2s_) + allphot_vec(C_,IV,l2p_) +
     +              allphot_vec(C_,IV,l3s_) + allphot_vec(C_,IV,l3p_) +
     +              allphot_vec(C_,IV,l3d_) + allphot_vec(C_,IV,l4s_) 

      allphot_vec(C_,V,l1s_)  = Gamma_i(C_,V,l1s_)
      allphot_vec(C_,V,l2s_)  = Gamma_i(C_,V,l2s_)
      allphot_vec(C_,V,l2p_)  = Gamma_i(C_,V,l2p_)
      allphot_vec(C_,V,l3s_)  = Gamma_i(C_,V,l3s_)
      allphot_vec(C_,V,l3p_)  = Gamma_i(C_,V,l3p_)
      allphot_vec(C_,V,l3d_)  = Gamma_i(C_,V,l3d_)
      allphot_vec(C_,V,l4s_)  = Gamma_i(C_,V,l4s_)
      allphot_vec(C_,V,lall_) = allphot_vec(C_,V,l1s_) +
     +              allphot_vec(C_,V,l2s_) + allphot_vec(C_,V,l2p_) +
     +              allphot_vec(C_,V,l3s_) + allphot_vec(C_,V,l3p_) +
     +              allphot_vec(C_,V,l3d_) + allphot_vec(C_,V,l4s_) 

      allphot_vec(C_,VI,l1s_)  = Gamma_i(C_,VI,l1s_)
      allphot_vec(C_,VI,l2s_)  = Gamma_i(C_,VI,l2s_)
      allphot_vec(C_,VI,l2p_)  = Gamma_i(C_,VI,l2p_)
      allphot_vec(C_,VI,l3s_)  = Gamma_i(C_,VI,l3s_)
      allphot_vec(C_,VI,l3p_)  = Gamma_i(C_,VI,l3p_)
      allphot_vec(C_,VI,l3d_)  = Gamma_i(C_,VI,l3d_)
      allphot_vec(C_,VI,l4s_)  = Gamma_i(C_,VI,l4s_)
      allphot_vec(C_,VI,lall_) = allphot_vec(C_,VI,l1s_) +
     +              allphot_vec(C_,VI,l2s_) + allphot_vec(C_,VI,l2p_) +
     +              allphot_vec(C_,VI,l3s_) + allphot_vec(C_,VI,l3p_) +
     +              allphot_vec(C_,VI,l3d_) + allphot_vec(C_,VI,l4s_) 


c Nitrogen

      allphot_vec(N_,I,l1s_)  = Gamma_i(N_,I,l1s_)
      allphot_vec(N_,I,l2s_)  = Gamma_i(N_,I,l2s_)
      allphot_vec(N_,I,l2p_)  = Gamma_i(N_,I,l2p_)
      allphot_vec(N_,I,l3s_)  = Gamma_i(N_,I,l3s_)
      allphot_vec(N_,I,l3p_)  = Gamma_i(N_,I,l3p_)
      allphot_vec(N_,I,l3d_)  = Gamma_i(N_,I,l3d_)
      allphot_vec(N_,I,l4s_)  = Gamma_i(N_,I,l4s_)
      allphot_vec(N_,I,lall_) = allphot_vec(N_,I,l1s_) +
     +              allphot_vec(N_,I,l2s_) + allphot_vec(N_,I,l2p_) +
     +              allphot_vec(N_,I,l3s_) + allphot_vec(N_,I,l3p_) +
     +              allphot_vec(N_,I,l3d_) + allphot_vec(N_,I,l4s_) 

      allphot_vec(N_,II,l1s_)  = Gamma_i(N_,II,l1s_)
      allphot_vec(N_,II,l2s_)  = Gamma_i(N_,II,l2s_)
      allphot_vec(N_,II,l2p_)  = Gamma_i(N_,II,l2p_)
      allphot_vec(N_,II,l3s_)  = Gamma_i(N_,II,l3s_)
      allphot_vec(N_,II,l3p_)  = Gamma_i(N_,II,l3p_)
      allphot_vec(N_,II,l3d_)  = Gamma_i(N_,II,l3d_)
      allphot_vec(N_,II,l4s_)  = Gamma_i(N_,II,l4s_)
      allphot_vec(N_,II,lall_) = allphot_vec(N_,II,l1s_) +
     +              allphot_vec(N_,II,l2s_) + allphot_vec(N_,II,l2p_) +
     +              allphot_vec(N_,II,l3s_) + allphot_vec(N_,II,l3p_) +
     +              allphot_vec(N_,II,l3d_) + allphot_vec(N_,II,l4s_) 

      allphot_vec(N_,III,l1s_)  = Gamma_i(N_,III,l1s_)
      allphot_vec(N_,III,l2s_)  = Gamma_i(N_,III,l2s_)
      allphot_vec(N_,III,l2p_)  = Gamma_i(N_,III,l2p_)
      allphot_vec(N_,III,l3s_)  = Gamma_i(N_,III,l3s_)
      allphot_vec(N_,III,l3p_)  = Gamma_i(N_,III,l3p_)
      allphot_vec(N_,III,l3d_)  = Gamma_i(N_,III,l3d_)
      allphot_vec(N_,III,l4s_)  = Gamma_i(N_,III,l4s_)
      allphot_vec(N_,III,lall_) = allphot_vec(N_,III,l1s_) +
     +            allphot_vec(N_,III,l2s_) + allphot_vec(N_,III,l2p_) +
     +            allphot_vec(N_,III,l3s_) + allphot_vec(N_,III,l3p_) +
     +            allphot_vec(N_,III,l3d_) + allphot_vec(N_,III,l4s_) 

      allphot_vec(N_,IV,l1s_)  = Gamma_i(N_,IV,l1s_)
      allphot_vec(N_,IV,l2s_)  = Gamma_i(N_,IV,l2s_)
      allphot_vec(N_,IV,l2p_)  = Gamma_i(N_,IV,l2p_)
      allphot_vec(N_,IV,l3s_)  = Gamma_i(N_,IV,l3s_)
      allphot_vec(N_,IV,l3p_)  = Gamma_i(N_,IV,l3p_)
      allphot_vec(N_,IV,l3d_)  = Gamma_i(N_,IV,l3d_)
      allphot_vec(N_,IV,l4s_)  = Gamma_i(N_,IV,l4s_)
      allphot_vec(N_,IV,lall_) = allphot_vec(N_,IV,l1s_) +
     +              allphot_vec(N_,IV,l2s_) + allphot_vec(N_,IV,l2p_) +
     +              allphot_vec(N_,IV,l3s_) + allphot_vec(N_,IV,l3p_) +
     +              allphot_vec(N_,IV,l3d_) + allphot_vec(N_,IV,l4s_) 

      allphot_vec(N_,V,l1s_)  = Gamma_i(N_,V,l1s_)
      allphot_vec(N_,V,l2s_)  = Gamma_i(N_,V,l2s_)
      allphot_vec(N_,V,l2p_)  = Gamma_i(N_,V,l2p_)
      allphot_vec(N_,V,l3s_)  = Gamma_i(N_,V,l3s_)
      allphot_vec(N_,V,l3p_)  = Gamma_i(N_,V,l3p_)
      allphot_vec(N_,V,l3d_)  = Gamma_i(N_,V,l3d_)
      allphot_vec(N_,V,l4s_)  = Gamma_i(N_,V,l4s_)
      allphot_vec(N_,V,lall_) = allphot_vec(N_,V,l1s_) +
     +              allphot_vec(N_,V,l2s_) + allphot_vec(N_,V,l2p_) +
     +              allphot_vec(N_,V,l3s_) + allphot_vec(N_,V,l3p_) +
     +              allphot_vec(N_,V,l3d_) + allphot_vec(N_,V,l4s_) 

      allphot_vec(N_,VI,l1s_)  = Gamma_i(N_,VI,l1s_)
      allphot_vec(N_,VI,l2s_)  = Gamma_i(N_,VI,l2s_)
      allphot_vec(N_,VI,l2p_)  = Gamma_i(N_,VI,l2p_)
      allphot_vec(N_,VI,l3s_)  = Gamma_i(N_,VI,l3s_)
      allphot_vec(N_,VI,l3p_)  = Gamma_i(N_,VI,l3p_)
      allphot_vec(N_,VI,l3d_)  = Gamma_i(N_,VI,l3d_)
      allphot_vec(N_,VI,l4s_)  = Gamma_i(N_,VI,l4s_)
      allphot_vec(N_,VI,lall_) = allphot_vec(N_,VI,l1s_) +
     +              allphot_vec(N_,VI,l2s_) + allphot_vec(N_,VI,l2p_) +
     +              allphot_vec(N_,VI,l3s_) + allphot_vec(N_,VI,l3p_) +
     +              allphot_vec(N_,VI,l3d_) + allphot_vec(N_,VI,l4s_) 

      allphot_vec(N_,VII,l1s_)  = Gamma_i(N_,VII,l1s_)
      allphot_vec(N_,VII,l2s_)  = Gamma_i(N_,VII,l2s_)
      allphot_vec(N_,VII,l2p_)  = Gamma_i(N_,VII,l2p_)
      allphot_vec(N_,VII,l3s_)  = Gamma_i(N_,VII,l3s_)
      allphot_vec(N_,VII,l3p_)  = Gamma_i(N_,VII,l3p_)
      allphot_vec(N_,VII,l3d_)  = Gamma_i(N_,VII,l3d_)
      allphot_vec(N_,VII,l4s_)  = Gamma_i(N_,VII,l4s_)
      allphot_vec(N_,VII,lall_) = allphot_vec(N_,VII,l1s_) +
     +             allphot_vec(N_,VII,l2s_) + allphot_vec(N_,VII,l2p_) +
     +             allphot_vec(N_,VII,l3s_) + allphot_vec(N_,VII,l3p_) +
     +             allphot_vec(N_,VII,l3d_) + allphot_vec(N_,VII,l4s_) 

c Oxygen

      allphot_vec(O_,I,l1s_)  = Gamma_i(O_,I,l1s_)
      allphot_vec(O_,I,l2s_)  = Gamma_i(O_,I,l2s_)
      allphot_vec(O_,I,l2p_)  = Gamma_i(O_,I,l2p_)
      allphot_vec(O_,I,l3s_)  = Gamma_i(O_,I,l3s_)
      allphot_vec(O_,I,l3p_)  = Gamma_i(O_,I,l3p_)
      allphot_vec(O_,I,l3d_)  = Gamma_i(O_,I,l3d_)
      allphot_vec(O_,I,l4s_)  = Gamma_i(O_,I,l4s_)
      allphot_vec(O_,I,lall_) = allphot_vec(O_,I,l1s_) +
     +              allphot_vec(O_,I,l2s_) + allphot_vec(O_,I,l2p_) +
     +              allphot_vec(O_,I,l3s_) + allphot_vec(O_,I,l3p_) +
     +              allphot_vec(O_,I,l3d_) + allphot_vec(O_,I,l4s_) 

      allphot_vec(O_,II,l1s_)  = Gamma_i(O_,II,l1s_)
      allphot_vec(O_,II,l2s_)  = Gamma_i(O_,II,l2s_)
      allphot_vec(O_,II,l2p_)  = Gamma_i(O_,II,l2p_)
      allphot_vec(O_,II,l3s_)  = Gamma_i(O_,II,l3s_)
      allphot_vec(O_,II,l3p_)  = Gamma_i(O_,II,l3p_)
      allphot_vec(O_,II,l3d_)  = Gamma_i(O_,II,l3d_)
      allphot_vec(O_,II,l4s_)  = Gamma_i(O_,II,l4s_)
      allphot_vec(O_,II,lall_) = allphot_vec(O_,II,l1s_) +
     +              allphot_vec(O_,II,l2s_) + allphot_vec(O_,II,l2p_) +
     +              allphot_vec(O_,II,l3s_) + allphot_vec(O_,II,l3p_) +
     +              allphot_vec(O_,II,l3d_) + allphot_vec(O_,II,l4s_) 

      allphot_vec(O_,III,l1s_)  = Gamma_i(O_,III,l1s_)
      allphot_vec(O_,III,l2s_)  = Gamma_i(O_,III,l2s_)
      allphot_vec(O_,III,l2p_)  = Gamma_i(O_,III,l2p_)
      allphot_vec(O_,III,l3s_)  = Gamma_i(O_,III,l3s_)
      allphot_vec(O_,III,l3p_)  = Gamma_i(O_,III,l3p_)
      allphot_vec(O_,III,l3d_)  = Gamma_i(O_,III,l3d_)
      allphot_vec(O_,III,l4s_)  = Gamma_i(O_,III,l4s_)
      allphot_vec(O_,III,lall_) = allphot_vec(O_,III,l1s_) +
     +            allphot_vec(O_,III,l2s_) + allphot_vec(O_,III,l2p_) +
     +            allphot_vec(O_,III,l3s_) + allphot_vec(O_,III,l3p_) +
     +            allphot_vec(O_,III,l3d_) + allphot_vec(O_,III,l4s_) 

      allphot_vec(O_,IV,l1s_)  = Gamma_i(O_,IV,l1s_)
      allphot_vec(O_,IV,l2s_)  = Gamma_i(O_,IV,l2s_)
      allphot_vec(O_,IV,l2p_)  = Gamma_i(O_,IV,l2p_)
      allphot_vec(O_,IV,l3s_)  = Gamma_i(O_,IV,l3s_)
      allphot_vec(O_,IV,l3p_)  = Gamma_i(O_,IV,l3p_)
      allphot_vec(O_,IV,l3d_)  = Gamma_i(O_,IV,l3d_)
      allphot_vec(O_,IV,l4s_)  = Gamma_i(O_,IV,l4s_)
      allphot_vec(O_,IV,lall_) = allphot_vec(O_,IV,l1s_) +
     +              allphot_vec(O_,IV,l2s_) + allphot_vec(O_,IV,l2p_) +
     +              allphot_vec(O_,IV,l3s_) + allphot_vec(O_,IV,l3p_) +
     +              allphot_vec(O_,IV,l3d_) + allphot_vec(O_,IV,l4s_) 

      allphot_vec(O_,V,l1s_)  = Gamma_i(O_,V,l1s_)
      allphot_vec(O_,V,l2s_)  = Gamma_i(O_,V,l2s_)
      allphot_vec(O_,V,l2p_)  = Gamma_i(O_,V,l2p_)
      allphot_vec(O_,V,l3s_)  = Gamma_i(O_,V,l3s_)
      allphot_vec(O_,V,l3p_)  = Gamma_i(O_,V,l3p_)
      allphot_vec(O_,V,l3d_)  = Gamma_i(O_,V,l3d_)
      allphot_vec(O_,V,l4s_)  = Gamma_i(O_,V,l4s_)
      allphot_vec(O_,V,lall_) = allphot_vec(O_,V,l1s_) +
     +              allphot_vec(O_,V,l2s_) + allphot_vec(O_,V,l2p_) +
     +              allphot_vec(O_,V,l3s_) + allphot_vec(O_,V,l3p_) +
     +              allphot_vec(O_,V,l3d_) + allphot_vec(O_,V,l4s_) 

      allphot_vec(O_,VI,l1s_)  = Gamma_i(O_,VI,l1s_)
      allphot_vec(O_,VI,l2s_)  = Gamma_i(O_,VI,l2s_)
      allphot_vec(O_,VI,l2p_)  = Gamma_i(O_,VI,l2p_)
      allphot_vec(O_,VI,l3s_)  = Gamma_i(O_,VI,l3s_)
      allphot_vec(O_,VI,l3p_)  = Gamma_i(O_,VI,l3p_)
      allphot_vec(O_,VI,l3d_)  = Gamma_i(O_,VI,l3d_)
      allphot_vec(O_,VI,l4s_)  = Gamma_i(O_,VI,l4s_)
      allphot_vec(O_,VI,lall_) = allphot_vec(O_,VI,l1s_) +
     +              allphot_vec(O_,VI,l2s_) + allphot_vec(O_,VI,l2p_) +
     +              allphot_vec(O_,VI,l3s_) + allphot_vec(O_,VI,l3p_) +
     +              allphot_vec(O_,VI,l3d_) + allphot_vec(O_,VI,l4s_) 

      allphot_vec(O_,VII,l1s_)  = Gamma_i(O_,VII,l1s_)
      allphot_vec(O_,VII,l2s_)  = Gamma_i(O_,VII,l2s_)
      allphot_vec(O_,VII,l2p_)  = Gamma_i(O_,VII,l2p_)
      allphot_vec(O_,VII,l3s_)  = Gamma_i(O_,VII,l3s_)
      allphot_vec(O_,VII,l3p_)  = Gamma_i(O_,VII,l3p_)
      allphot_vec(O_,VII,l3d_)  = Gamma_i(O_,VII,l3d_)
      allphot_vec(O_,VII,l4s_)  = Gamma_i(O_,VII,l4s_)
      allphot_vec(O_,VII,lall_) = allphot_vec(O_,VII,l1s_) +
     +             allphot_vec(O_,VII,l2s_) + allphot_vec(O_,VII,l2p_) +
     +             allphot_vec(O_,VII,l3s_) + allphot_vec(O_,VII,l3p_) +
     +             allphot_vec(O_,VII,l3d_) + allphot_vec(O_,VII,l4s_) 

      allphot_vec(O_,VIII,l1s_)  = Gamma_i(O_,VIII,l1s_)
      allphot_vec(O_,VIII,l2s_)  = Gamma_i(O_,VIII,l2s_)
      allphot_vec(O_,VIII,l2p_)  = Gamma_i(O_,VIII,l2p_)
      allphot_vec(O_,VIII,l3s_)  = Gamma_i(O_,VIII,l3s_)
      allphot_vec(O_,VIII,l3p_)  = Gamma_i(O_,VIII,l3p_)
      allphot_vec(O_,VIII,l3d_)  = Gamma_i(O_,VIII,l3d_)
      allphot_vec(O_,VIII,l4s_)  = Gamma_i(O_,VIII,l4s_)
      allphot_vec(O_,VIII,lall_) = allphot_vec(O_,VIII,l1s_) +
     +           allphot_vec(O_,VIII,l2s_) + allphot_vec(O_,VIII,l2p_) +
     +           allphot_vec(O_,VIII,l3s_) + allphot_vec(O_,VIII,l3p_) +
     +           allphot_vec(O_,VIII,l3d_) + allphot_vec(O_,VIII,l4s_) 


c Neon

      allphot_vec(Ne_,I,l1s_)  = Gamma_i(Ne_,I,l1s_)
      allphot_vec(Ne_,I,l2s_)  = Gamma_i(Ne_,I,l2s_)
      allphot_vec(Ne_,I,l2p_)  = Gamma_i(Ne_,I,l2p_)
      allphot_vec(Ne_,I,l3s_)  = Gamma_i(Ne_,I,l3s_)
      allphot_vec(Ne_,I,l3p_)  = Gamma_i(Ne_,I,l3p_)
      allphot_vec(Ne_,I,l3d_)  = Gamma_i(Ne_,I,l3d_)
      allphot_vec(Ne_,I,l4s_)  = Gamma_i(Ne_,I,l4s_)
      allphot_vec(Ne_,I,lall_) = allphot_vec(Ne_,I,l1s_) +
     +              allphot_vec(Ne_,I,l2s_) + allphot_vec(Ne_,I,l2p_) +
     +              allphot_vec(Ne_,I,l3s_) + allphot_vec(Ne_,I,l3p_) +
     +              allphot_vec(Ne_,I,l3d_) + allphot_vec(Ne_,I,l4s_) 

      allphot_vec(Ne_,II,l1s_)  = Gamma_i(Ne_,II,l1s_)
      allphot_vec(Ne_,II,l2s_)  = Gamma_i(Ne_,II,l2s_)
      allphot_vec(Ne_,II,l2p_)  = Gamma_i(Ne_,II,l2p_)
      allphot_vec(Ne_,II,l3s_)  = Gamma_i(Ne_,II,l3s_)
      allphot_vec(Ne_,II,l3p_)  = Gamma_i(Ne_,II,l3p_)
      allphot_vec(Ne_,II,l3d_)  = Gamma_i(Ne_,II,l3d_)
      allphot_vec(Ne_,II,l4s_)  = Gamma_i(Ne_,II,l4s_)
      allphot_vec(Ne_,II,lall_) = allphot_vec(Ne_,II,l1s_) +
     +             allphot_vec(Ne_,II,l2s_) + allphot_vec(Ne_,II,l2p_) +
     +             allphot_vec(Ne_,II,l3s_) + allphot_vec(Ne_,II,l3p_) +
     +             allphot_vec(Ne_,II,l3d_) + allphot_vec(Ne_,II,l4s_) 

      allphot_vec(Ne_,III,l1s_)  = Gamma_i(Ne_,III,l1s_)
      allphot_vec(Ne_,III,l2s_)  = Gamma_i(Ne_,III,l2s_)
      allphot_vec(Ne_,III,l2p_)  = Gamma_i(Ne_,III,l2p_)
      allphot_vec(Ne_,III,l3s_)  = Gamma_i(Ne_,III,l3s_)
      allphot_vec(Ne_,III,l3p_)  = Gamma_i(Ne_,III,l3p_)
      allphot_vec(Ne_,III,l3d_)  = Gamma_i(Ne_,III,l3d_)
      allphot_vec(Ne_,III,l4s_)  = Gamma_i(Ne_,III,l4s_)
      allphot_vec(Ne_,III,lall_) = allphot_vec(Ne_,III,l1s_) +
     +           allphot_vec(Ne_,III,l2s_) + allphot_vec(Ne_,III,l2p_) +
     +           allphot_vec(Ne_,III,l3s_) + allphot_vec(Ne_,III,l3p_) +
     +           allphot_vec(Ne_,III,l3d_) + allphot_vec(Ne_,III,l4s_) 

      allphot_vec(Ne_,IV,l1s_)  = Gamma_i(Ne_,IV,l1s_)
      allphot_vec(Ne_,IV,l2s_)  = Gamma_i(Ne_,IV,l2s_)
      allphot_vec(Ne_,IV,l2p_)  = Gamma_i(Ne_,IV,l2p_)
      allphot_vec(Ne_,IV,l3s_)  = Gamma_i(Ne_,IV,l3s_)
      allphot_vec(Ne_,IV,l3p_)  = Gamma_i(Ne_,IV,l3p_)
      allphot_vec(Ne_,IV,l3d_)  = Gamma_i(Ne_,IV,l3d_)
      allphot_vec(Ne_,IV,l4s_)  = Gamma_i(Ne_,IV,l4s_)
      allphot_vec(Ne_,IV,lall_) = allphot_vec(Ne_,IV,l1s_) +
     +             allphot_vec(Ne_,IV,l2s_) + allphot_vec(Ne_,IV,l2p_) +
     +             allphot_vec(Ne_,IV,l3s_) + allphot_vec(Ne_,IV,l3p_) +
     +             allphot_vec(Ne_,IV,l3d_) + allphot_vec(Ne_,IV,l4s_) 

      allphot_vec(Ne_,V,l1s_)  = Gamma_i(Ne_,V,l1s_)
      allphot_vec(Ne_,V,l2s_)  = Gamma_i(Ne_,V,l2s_)
      allphot_vec(Ne_,V,l2p_)  = Gamma_i(Ne_,V,l2p_)
      allphot_vec(Ne_,V,l3s_)  = Gamma_i(Ne_,V,l3s_)
      allphot_vec(Ne_,V,l3p_)  = Gamma_i(Ne_,V,l3p_)
      allphot_vec(Ne_,V,l3d_)  = Gamma_i(Ne_,V,l3d_)
      allphot_vec(Ne_,V,l4s_)  = Gamma_i(Ne_,V,l4s_)
      allphot_vec(Ne_,V,lall_) = allphot_vec(Ne_,V,l1s_) +
     +              allphot_vec(Ne_,V,l2s_) + allphot_vec(Ne_,V,l2p_) +
     +              allphot_vec(Ne_,V,l3s_) + allphot_vec(Ne_,V,l3p_) +
     +              allphot_vec(Ne_,V,l3d_) + allphot_vec(Ne_,V,l4s_) 

      allphot_vec(Ne_,VI,l1s_)  = Gamma_i(Ne_,VI,l1s_)
      allphot_vec(Ne_,VI,l2s_)  = Gamma_i(Ne_,VI,l2s_)
      allphot_vec(Ne_,VI,l2p_)  = Gamma_i(Ne_,VI,l2p_)
      allphot_vec(Ne_,VI,l3s_)  = Gamma_i(Ne_,VI,l3s_)
      allphot_vec(Ne_,VI,l3p_)  = Gamma_i(Ne_,VI,l3p_)
      allphot_vec(Ne_,VI,l3d_)  = Gamma_i(Ne_,VI,l3d_)
      allphot_vec(Ne_,VI,l4s_)  = Gamma_i(Ne_,VI,l4s_)
      allphot_vec(Ne_,VI,lall_) = allphot_vec(Ne_,VI,l1s_) +
     +             allphot_vec(Ne_,VI,l2s_) + allphot_vec(Ne_,VI,l2p_) +
     +             allphot_vec(Ne_,VI,l3s_) + allphot_vec(Ne_,VI,l3p_) +
     +             allphot_vec(Ne_,VI,l3d_) + allphot_vec(Ne_,VI,l4s_) 

      allphot_vec(Ne_,VII,l1s_)  = Gamma_i(Ne_,VII,l1s_)
      allphot_vec(Ne_,VII,l2s_)  = Gamma_i(Ne_,VII,l2s_)
      allphot_vec(Ne_,VII,l2p_)  = Gamma_i(Ne_,VII,l2p_)
      allphot_vec(Ne_,VII,l3s_)  = Gamma_i(Ne_,VII,l3s_)
      allphot_vec(Ne_,VII,l3p_)  = Gamma_i(Ne_,VII,l3p_)
      allphot_vec(Ne_,VII,l3d_)  = Gamma_i(Ne_,VII,l3d_)
      allphot_vec(Ne_,VII,l4s_)  = Gamma_i(Ne_,VII,l4s_)
      allphot_vec(Ne_,VII,lall_) = allphot_vec(Ne_,VII,l1s_) +
     +           allphot_vec(Ne_,VII,l2s_) + allphot_vec(Ne_,VII,l2p_) +
     +           allphot_vec(Ne_,VII,l3s_) + allphot_vec(Ne_,VII,l3p_) +
     +           allphot_vec(Ne_,VII,l3d_) + allphot_vec(Ne_,VII,l4s_) 

      allphot_vec(Ne_,VIII,l1s_)  = Gamma_i(Ne_,VIII,l1s_)
      allphot_vec(Ne_,VIII,l2s_)  = Gamma_i(Ne_,VIII,l2s_)
      allphot_vec(Ne_,VIII,l2p_)  = Gamma_i(Ne_,VIII,l2p_)
      allphot_vec(Ne_,VIII,l3s_)  = Gamma_i(Ne_,VIII,l3s_)
      allphot_vec(Ne_,VIII,l3p_)  = Gamma_i(Ne_,VIII,l3p_)
      allphot_vec(Ne_,VIII,l3d_)  = Gamma_i(Ne_,VIII,l3d_)
      allphot_vec(Ne_,VIII,l4s_)  = Gamma_i(Ne_,VIII,l4s_)
      allphot_vec(Ne_,VIII,lall_) = allphot_vec(Ne_,VIII,l1s_) +
     +         allphot_vec(Ne_,VIII,l2s_) + allphot_vec(Ne_,VIII,l2p_) +
     +         allphot_vec(Ne_,VIII,l3s_) + allphot_vec(Ne_,VIII,l3p_) +
     +         allphot_vec(Ne_,VIII,l3d_) + allphot_vec(Ne_,VIII,l4s_) 

      allphot_vec(Ne_,IX,l1s_)  = Gamma_i(Ne_,IX,l1s_)
      allphot_vec(Ne_,IX,l2s_)  = Gamma_i(Ne_,IX,l2s_)
      allphot_vec(Ne_,IX,l2p_)  = Gamma_i(Ne_,IX,l2p_)
      allphot_vec(Ne_,IX,l3s_)  = Gamma_i(Ne_,IX,l3s_)
      allphot_vec(Ne_,IX,l3p_)  = Gamma_i(Ne_,IX,l3p_)
      allphot_vec(Ne_,IX,l3d_)  = Gamma_i(Ne_,IX,l3d_)
      allphot_vec(Ne_,IX,l4s_)  = Gamma_i(Ne_,IX,l4s_)
      allphot_vec(Ne_,IX,lall_) = allphot_vec(Ne_,IX,l1s_) +
     +            allphot_vec(Ne_,IX,l2s_) + allphot_vec(Ne_,IX,l2p_) +
     +            allphot_vec(Ne_,IX,l3s_) + allphot_vec(Ne_,IX,l3p_) +
     +            allphot_vec(Ne_,IX,l3d_) + allphot_vec(Ne_,IX,l4s_) 

      allphot_vec(Ne_,X,l1s_)  = Gamma_i(Ne_,X,l1s_)
      allphot_vec(Ne_,X,l2s_)  = Gamma_i(Ne_,X,l2s_)
      allphot_vec(Ne_,X,l2p_)  = Gamma_i(Ne_,X,l2p_)
      allphot_vec(Ne_,X,l3s_)  = Gamma_i(Ne_,X,l3s_)
      allphot_vec(Ne_,X,l3p_)  = Gamma_i(Ne_,X,l3p_)
      allphot_vec(Ne_,X,l3d_)  = Gamma_i(Ne_,X,l3d_)
      allphot_vec(Ne_,X,l4s_)  = Gamma_i(Ne_,X,l4s_)
      allphot_vec(Ne_,X,lall_) = allphot_vec(Ne_,X,l1s_) +
     +              allphot_vec(Ne_,X,l2s_) + allphot_vec(Ne_,X,l2p_) +
     +              allphot_vec(Ne_,X,l3s_) + allphot_vec(Ne_,X,l3p_) +
     +              allphot_vec(Ne_,X,l3d_) + allphot_vec(Ne_,X,l4s_) 

c Magnesium

      allphot_vec(Mg_,I,l1s_)  = Gamma_i(Mg_,I,l1s_)
      allphot_vec(Mg_,I,l2s_)  = Gamma_i(Mg_,I,l2s_)
      allphot_vec(Mg_,I,l2p_)  = Gamma_i(Mg_,I,l2p_)
      allphot_vec(Mg_,I,l3s_)  = Gamma_i(Mg_,I,l3s_)
      allphot_vec(Mg_,I,l3p_)  = Gamma_i(Mg_,I,l3p_)
      allphot_vec(Mg_,I,l3d_)  = Gamma_i(Mg_,I,l3d_)
      allphot_vec(Mg_,I,l4s_)  = Gamma_i(Mg_,I,l4s_)
      allphot_vec(Mg_,I,lall_) = allphot_vec(Mg_,I,l1s_) +
     +              allphot_vec(Mg_,I,l2s_) + allphot_vec(Mg_,I,l2p_) +
     +              allphot_vec(Mg_,I,l3s_) + allphot_vec(Mg_,I,l3p_) +
     +              allphot_vec(Mg_,I,l3d_) + allphot_vec(Mg_,I,l4s_) 

      allphot_vec(Mg_,II,l1s_)  = Gamma_i(Mg_,II,l1s_)
      allphot_vec(Mg_,II,l2s_)  = Gamma_i(Mg_,II,l2s_)
      allphot_vec(Mg_,II,l2p_)  = Gamma_i(Mg_,II,l2p_)
      allphot_vec(Mg_,II,l3s_)  = Gamma_i(Mg_,II,l3s_)
      allphot_vec(Mg_,II,l3p_)  = Gamma_i(Mg_,II,l3p_)
      allphot_vec(Mg_,II,l3d_)  = Gamma_i(Mg_,II,l3d_)
      allphot_vec(Mg_,II,l4s_)  = Gamma_i(Mg_,II,l4s_)
      allphot_vec(Mg_,II,lall_) = allphot_vec(Mg_,II,l1s_) +
     +             allphot_vec(Mg_,II,l2s_) + allphot_vec(Mg_,II,l2p_) +
     +             allphot_vec(Mg_,II,l3s_) + allphot_vec(Mg_,II,l3p_) +
     +             allphot_vec(Mg_,II,l3d_) + allphot_vec(Mg_,II,l4s_) 

      allphot_vec(Mg_,III,l1s_)  = Gamma_i(Mg_,III,l1s_)
      allphot_vec(Mg_,III,l2s_)  = Gamma_i(Mg_,III,l2s_)
      allphot_vec(Mg_,III,l2p_)  = Gamma_i(Mg_,III,l2p_)
      allphot_vec(Mg_,III,l3s_)  = Gamma_i(Mg_,III,l3s_)
      allphot_vec(Mg_,III,l3p_)  = Gamma_i(Mg_,III,l3p_)
      allphot_vec(Mg_,III,l3d_)  = Gamma_i(Mg_,III,l3d_)
      allphot_vec(Mg_,III,l4s_)  = Gamma_i(Mg_,III,l4s_)
      allphot_vec(Mg_,III,lall_) = allphot_vec(Mg_,III,l1s_) +
     +           allphot_vec(Mg_,III,l2s_) + allphot_vec(Mg_,III,l2p_) +
     +           allphot_vec(Mg_,III,l3s_) + allphot_vec(Mg_,III,l3p_) +
     +           allphot_vec(Mg_,III,l3d_) + allphot_vec(Mg_,III,l4s_) 

      allphot_vec(Mg_,IV,l1s_)  = Gamma_i(Mg_,IV,l1s_)
      allphot_vec(Mg_,IV,l2s_)  = Gamma_i(Mg_,IV,l2s_)
      allphot_vec(Mg_,IV,l2p_)  = Gamma_i(Mg_,IV,l2p_)
      allphot_vec(Mg_,IV,l3s_)  = Gamma_i(Mg_,IV,l3s_)
      allphot_vec(Mg_,IV,l3p_)  = Gamma_i(Mg_,IV,l3p_)
      allphot_vec(Mg_,IV,l3d_)  = Gamma_i(Mg_,IV,l3d_)
      allphot_vec(Mg_,IV,l4s_)  = Gamma_i(Mg_,IV,l4s_)
      allphot_vec(Mg_,IV,lall_) = allphot_vec(Mg_,IV,l1s_) +
     +             allphot_vec(Mg_,IV,l2s_) + allphot_vec(Mg_,IV,l2p_) +
     +             allphot_vec(Mg_,IV,l3s_) + allphot_vec(Mg_,IV,l3p_) +
     +             allphot_vec(Mg_,IV,l3d_) + allphot_vec(Mg_,IV,l4s_) 

      allphot_vec(Mg_,V,l1s_)  = Gamma_i(Mg_,V,l1s_)
      allphot_vec(Mg_,V,l2s_)  = Gamma_i(Mg_,V,l2s_)
      allphot_vec(Mg_,V,l2p_)  = Gamma_i(Mg_,V,l2p_)
      allphot_vec(Mg_,V,l3s_)  = Gamma_i(Mg_,V,l3s_)
      allphot_vec(Mg_,V,l3p_)  = Gamma_i(Mg_,V,l3p_)
      allphot_vec(Mg_,V,l3d_)  = Gamma_i(Mg_,V,l3d_)
      allphot_vec(Mg_,V,l4s_)  = Gamma_i(Mg_,V,l4s_)
      allphot_vec(Mg_,V,lall_) = allphot_vec(Mg_,V,l1s_) +
     +              allphot_vec(Mg_,V,l2s_) + allphot_vec(Mg_,V,l2p_) +
     +              allphot_vec(Mg_,V,l3s_) + allphot_vec(Mg_,V,l3p_) +
     +              allphot_vec(Mg_,V,l3d_) + allphot_vec(Mg_,V,l4s_) 

      allphot_vec(Mg_,VI,l1s_)  = Gamma_i(Mg_,VI,l1s_)
      allphot_vec(Mg_,VI,l2s_)  = Gamma_i(Mg_,VI,l2s_)
      allphot_vec(Mg_,VI,l2p_)  = Gamma_i(Mg_,VI,l2p_)
      allphot_vec(Mg_,VI,l3s_)  = Gamma_i(Mg_,VI,l3s_)
      allphot_vec(Mg_,VI,l3p_)  = Gamma_i(Mg_,VI,l3p_)
      allphot_vec(Mg_,VI,l3d_)  = Gamma_i(Mg_,VI,l3d_)
      allphot_vec(Mg_,VI,l4s_)  = Gamma_i(Mg_,VI,l4s_)
      allphot_vec(Mg_,VI,lall_) = allphot_vec(Mg_,VI,l1s_) +
     +             allphot_vec(Mg_,VI,l2s_) + allphot_vec(Mg_,VI,l2p_) +
     +             allphot_vec(Mg_,VI,l3s_) + allphot_vec(Mg_,VI,l3p_) +
     +             allphot_vec(Mg_,VI,l3d_) + allphot_vec(Mg_,VI,l4s_) 

      allphot_vec(Mg_,VII,l1s_)  = Gamma_i(Mg_,VII,l1s_)
      allphot_vec(Mg_,VII,l2s_)  = Gamma_i(Mg_,VII,l2s_)
      allphot_vec(Mg_,VII,l2p_)  = Gamma_i(Mg_,VII,l2p_)
      allphot_vec(Mg_,VII,l3s_)  = Gamma_i(Mg_,VII,l3s_)
      allphot_vec(Mg_,VII,l3p_)  = Gamma_i(Mg_,VII,l3p_)
      allphot_vec(Mg_,VII,l3d_)  = Gamma_i(Mg_,VII,l3d_)
      allphot_vec(Mg_,VII,l4s_)  = Gamma_i(Mg_,VII,l4s_)
      allphot_vec(Mg_,VII,lall_) = allphot_vec(Mg_,VII,l1s_) +
     +           allphot_vec(Mg_,VII,l2s_) + allphot_vec(Mg_,VII,l2p_) +
     +           allphot_vec(Mg_,VII,l3s_) + allphot_vec(Mg_,VII,l3p_) +
     +           allphot_vec(Mg_,VII,l3d_) + allphot_vec(Mg_,VII,l4s_) 

      allphot_vec(Mg_,VIII,l1s_)  = Gamma_i(Mg_,VIII,l1s_)
      allphot_vec(Mg_,VIII,l2s_)  = Gamma_i(Mg_,VIII,l2s_)
      allphot_vec(Mg_,VIII,l2p_)  = Gamma_i(Mg_,VIII,l2p_)
      allphot_vec(Mg_,VIII,l3s_)  = Gamma_i(Mg_,VIII,l3s_)
      allphot_vec(Mg_,VIII,l3p_)  = Gamma_i(Mg_,VIII,l3p_)
      allphot_vec(Mg_,VIII,l3d_)  = Gamma_i(Mg_,VIII,l3d_)
      allphot_vec(Mg_,VIII,l4s_)  = Gamma_i(Mg_,VIII,l4s_)
      allphot_vec(Mg_,VIII,lall_) = allphot_vec(Mg_,VIII,l1s_) +
     +         allphot_vec(Mg_,VIII,l2s_) + allphot_vec(Mg_,VIII,l2p_) +
     +         allphot_vec(Mg_,VIII,l3s_) + allphot_vec(Mg_,VIII,l3p_) +
     +         allphot_vec(Mg_,VIII,l3d_) + allphot_vec(Mg_,VIII,l4s_) 

      allphot_vec(Mg_,IX,l1s_)  = Gamma_i(Mg_,IX,l1s_)
      allphot_vec(Mg_,IX,l2s_)  = Gamma_i(Mg_,IX,l2s_)
      allphot_vec(Mg_,IX,l2p_)  = Gamma_i(Mg_,IX,l2p_)
      allphot_vec(Mg_,IX,l3s_)  = Gamma_i(Mg_,IX,l3s_)
      allphot_vec(Mg_,IX,l3p_)  = Gamma_i(Mg_,IX,l3p_)
      allphot_vec(Mg_,IX,l3d_)  = Gamma_i(Mg_,IX,l3d_)
      allphot_vec(Mg_,IX,l4s_)  = Gamma_i(Mg_,IX,l4s_)
      allphot_vec(Mg_,IX,lall_) = allphot_vec(Mg_,IX,l1s_) +
     +             allphot_vec(Mg_,IX,l2s_) + allphot_vec(Mg_,IX,l2p_) +
     +             allphot_vec(Mg_,IX,l3s_) + allphot_vec(Mg_,IX,l3p_) +
     +             allphot_vec(Mg_,IX,l3d_) + allphot_vec(Mg_,IX,l4s_) 

      allphot_vec(Mg_,X,l1s_)  = Gamma_i(Mg_,X,l1s_)
      allphot_vec(Mg_,X,l2s_)  = Gamma_i(Mg_,X,l2s_)
      allphot_vec(Mg_,X,l2p_)  = Gamma_i(Mg_,X,l2p_)
      allphot_vec(Mg_,X,l3s_)  = Gamma_i(Mg_,X,l3s_)
      allphot_vec(Mg_,X,l3p_)  = Gamma_i(Mg_,X,l3p_)
      allphot_vec(Mg_,X,l3d_)  = Gamma_i(Mg_,X,l3d_)
      allphot_vec(Mg_,X,l4s_)  = Gamma_i(Mg_,X,l4s_)
      allphot_vec(Mg_,X,lall_) = allphot_vec(Mg_,X,l1s_) +
     +              allphot_vec(Mg_,X,l2s_) + allphot_vec(Mg_,X,l2p_) +
     +              allphot_vec(Mg_,X,l3s_) + allphot_vec(Mg_,X,l3p_) +
     +              allphot_vec(Mg_,X,l3d_) + allphot_vec(Mg_,X,l4s_) 

      allphot_vec(Mg_,XI,l1s_)  = Gamma_i(Mg_,XI,l1s_)
      allphot_vec(Mg_,XI,l2s_)  = Gamma_i(Mg_,XI,l2s_)
      allphot_vec(Mg_,XI,l2p_)  = Gamma_i(Mg_,XI,l2p_)
      allphot_vec(Mg_,XI,l3s_)  = Gamma_i(Mg_,XI,l3s_)
      allphot_vec(Mg_,XI,l3p_)  = Gamma_i(Mg_,XI,l3p_)
      allphot_vec(Mg_,XI,l3d_)  = Gamma_i(Mg_,XI,l3d_)
      allphot_vec(Mg_,XI,l4s_)  = Gamma_i(Mg_,XI,l4s_)
      allphot_vec(Mg_,XI,lall_) = allphot_vec(Mg_,XI,l1s_) +
     +             allphot_vec(Mg_,XI,l2s_) + allphot_vec(Mg_,XI,l2p_) +
     +             allphot_vec(Mg_,XI,l3s_) + allphot_vec(Mg_,XI,l3p_) +
     +             allphot_vec(Mg_,XI,l3d_) + allphot_vec(Mg_,XI,l4s_) 

      allphot_vec(Mg_,XII,l1s_)  = Gamma_i(Mg_,XII,l1s_)
      allphot_vec(Mg_,XII,l2s_)  = Gamma_i(Mg_,XII,l2s_)
      allphot_vec(Mg_,XII,l2p_)  = Gamma_i(Mg_,XII,l2p_)
      allphot_vec(Mg_,XII,l3s_)  = Gamma_i(Mg_,XII,l3s_)
      allphot_vec(Mg_,XII,l3p_)  = Gamma_i(Mg_,XII,l3p_)
      allphot_vec(Mg_,XII,l3d_)  = Gamma_i(Mg_,XII,l3d_)
      allphot_vec(Mg_,XII,l4s_)  = Gamma_i(Mg_,XII,l4s_)
      allphot_vec(Mg_,XII,lall_) = allphot_vec(Mg_,XII,l1s_) +
     +           allphot_vec(Mg_,XII,l2s_) + allphot_vec(Mg_,XII,l2p_) +
     +           allphot_vec(Mg_,XII,l3s_) + allphot_vec(Mg_,XII,l3p_) +
     +           allphot_vec(Mg_,XII,l3d_) + allphot_vec(Mg_,XII,l4s_) 


c Silicon

      allphot_vec(Si_,I,l1s_)  = Gamma_i(Si_,I,l1s_)
      allphot_vec(Si_,I,l2s_)  = Gamma_i(Si_,I,l2s_)
      allphot_vec(Si_,I,l2p_)  = Gamma_i(Si_,I,l2p_)
      allphot_vec(Si_,I,l3s_)  = Gamma_i(Si_,I,l3s_)
      allphot_vec(Si_,I,l3p_)  = Gamma_i(Si_,I,l3p_)
      allphot_vec(Si_,I,l3d_)  = Gamma_i(Si_,I,l3d_)
      allphot_vec(Si_,I,l4s_)  = Gamma_i(Si_,I,l4s_)
      allphot_vec(Si_,I,lall_) = allphot_vec(Si_,I,l1s_) +
     +              allphot_vec(Si_,I,l2s_) + allphot_vec(Si_,I,l2p_) +
     +              allphot_vec(Si_,I,l3s_) + allphot_vec(Si_,I,l3p_) +
     +              allphot_vec(Si_,I,l3d_) + allphot_vec(Si_,I,l4s_) 

      allphot_vec(Si_,II,l1s_)  = Gamma_i(Si_,II,l1s_)
      allphot_vec(Si_,II,l2s_)  = Gamma_i(Si_,II,l2s_)
      allphot_vec(Si_,II,l2p_)  = Gamma_i(Si_,II,l2p_)
      allphot_vec(Si_,II,l3s_)  = Gamma_i(Si_,II,l3s_)
      allphot_vec(Si_,II,l3p_)  = Gamma_i(Si_,II,l3p_)
      allphot_vec(Si_,II,l3d_)  = Gamma_i(Si_,II,l3d_)
      allphot_vec(Si_,II,l4s_)  = Gamma_i(Si_,II,l4s_)
      allphot_vec(Si_,II,lall_) = allphot_vec(Si_,II,l1s_) +
     +             allphot_vec(Si_,II,l2s_) + allphot_vec(Si_,II,l2p_) +
     +             allphot_vec(Si_,II,l3s_) + allphot_vec(Si_,II,l3p_) +
     +             allphot_vec(Si_,II,l3d_) + allphot_vec(Si_,II,l4s_) 

      allphot_vec(Si_,III,l1s_)  = Gamma_i(Si_,III,l1s_)
      allphot_vec(Si_,III,l2s_)  = Gamma_i(Si_,III,l2s_)
      allphot_vec(Si_,III,l2p_)  = Gamma_i(Si_,III,l2p_)
      allphot_vec(Si_,III,l3s_)  = Gamma_i(Si_,III,l3s_)
      allphot_vec(Si_,III,l3p_)  = Gamma_i(Si_,III,l3p_)
      allphot_vec(Si_,III,l3d_)  = Gamma_i(Si_,III,l3d_)
      allphot_vec(Si_,III,l4s_)  = Gamma_i(Si_,III,l4s_)
      allphot_vec(Si_,III,lall_) = allphot_vec(Si_,III,l1s_) +
     +           allphot_vec(Si_,III,l2s_) + allphot_vec(Si_,III,l2p_) +
     +           allphot_vec(Si_,III,l3s_) + allphot_vec(Si_,III,l3p_) +
     +           allphot_vec(Si_,III,l3d_) + allphot_vec(Si_,III,l4s_) 

      allphot_vec(Si_,IV,l1s_)  = Gamma_i(Si_,IV,l1s_)
      allphot_vec(Si_,IV,l2s_)  = Gamma_i(Si_,IV,l2s_)
      allphot_vec(Si_,IV,l2p_)  = Gamma_i(Si_,IV,l2p_)
      allphot_vec(Si_,IV,l3s_)  = Gamma_i(Si_,IV,l3s_)
      allphot_vec(Si_,IV,l3p_)  = Gamma_i(Si_,IV,l3p_)
      allphot_vec(Si_,IV,l3d_)  = Gamma_i(Si_,IV,l3d_)
      allphot_vec(Si_,IV,l4s_)  = Gamma_i(Si_,IV,l4s_)
      allphot_vec(Si_,IV,lall_) = allphot_vec(Si_,IV,l1s_) +
     +             allphot_vec(Si_,IV,l2s_) + allphot_vec(Si_,IV,l2p_) +
     +             allphot_vec(Si_,IV,l3s_) + allphot_vec(Si_,IV,l3p_) +
     +             allphot_vec(Si_,IV,l3d_) + allphot_vec(Si_,IV,l4s_) 

      allphot_vec(Si_,V,l1s_)  = Gamma_i(Si_,V,l1s_)
      allphot_vec(Si_,V,l2s_)  = Gamma_i(Si_,V,l2s_)
      allphot_vec(Si_,V,l2p_)  = Gamma_i(Si_,V,l2p_)
      allphot_vec(Si_,V,l3s_)  = Gamma_i(Si_,V,l3s_)
      allphot_vec(Si_,V,l3p_)  = Gamma_i(Si_,V,l3p_)
      allphot_vec(Si_,V,l3d_)  = Gamma_i(Si_,V,l3d_)
      allphot_vec(Si_,V,l4s_)  = Gamma_i(Si_,V,l4s_)
      allphot_vec(Si_,V,lall_) = allphot_vec(Si_,V,l1s_) +
     +              allphot_vec(Si_,V,l2s_) + allphot_vec(Si_,V,l2p_) +
     +              allphot_vec(Si_,V,l3s_) + allphot_vec(Si_,V,l3p_) +
     +              allphot_vec(Si_,V,l3d_) + allphot_vec(Si_,V,l4s_) 

      allphot_vec(Si_,VI,l1s_)  = Gamma_i(Si_,VI,l1s_)
      allphot_vec(Si_,VI,l2s_)  = Gamma_i(Si_,VI,l2s_)
      allphot_vec(Si_,VI,l2p_)  = Gamma_i(Si_,VI,l2p_)
      allphot_vec(Si_,VI,l3s_)  = Gamma_i(Si_,VI,l3s_)
      allphot_vec(Si_,VI,l3p_)  = Gamma_i(Si_,VI,l3p_)
      allphot_vec(Si_,VI,l3d_)  = Gamma_i(Si_,VI,l3d_)
      allphot_vec(Si_,VI,l4s_)  = Gamma_i(Si_,VI,l4s_)
      allphot_vec(Si_,VI,lall_) = allphot_vec(Si_,VI,l1s_) +
     +             allphot_vec(Si_,VI,l2s_) + allphot_vec(Si_,VI,l2p_) +
     +             allphot_vec(Si_,VI,l3s_) + allphot_vec(Si_,VI,l3p_) +
     +             allphot_vec(Si_,VI,l3d_) + allphot_vec(Si_,VI,l4s_) 

      allphot_vec(Si_,VII,l1s_)  = Gamma_i(Si_,VII,l1s_)
      allphot_vec(Si_,VII,l2s_)  = Gamma_i(Si_,VII,l2s_)
      allphot_vec(Si_,VII,l2p_)  = Gamma_i(Si_,VII,l2p_)
      allphot_vec(Si_,VII,l3s_)  = Gamma_i(Si_,VII,l3s_)
      allphot_vec(Si_,VII,l3p_)  = Gamma_i(Si_,VII,l3p_)
      allphot_vec(Si_,VII,l3d_)  = Gamma_i(Si_,VII,l3d_)
      allphot_vec(Si_,VII,l4s_)  = Gamma_i(Si_,VII,l4s_)
      allphot_vec(Si_,VII,lall_) = allphot_vec(Si_,VII,l1s_) +
     +           allphot_vec(Si_,VII,l2s_) + allphot_vec(Si_,VII,l2p_) +
     +           allphot_vec(Si_,VII,l3s_) + allphot_vec(Si_,VII,l3p_) +
     +           allphot_vec(Si_,VII,l3d_) + allphot_vec(Si_,VII,l4s_) 

      allphot_vec(Si_,VIII,l1s_)  = Gamma_i(Si_,VIII,l1s_)
      allphot_vec(Si_,VIII,l2s_)  = Gamma_i(Si_,VIII,l2s_)
      allphot_vec(Si_,VIII,l2p_)  = Gamma_i(Si_,VIII,l2p_)
      allphot_vec(Si_,VIII,l3s_)  = Gamma_i(Si_,VIII,l3s_)
      allphot_vec(Si_,VIII,l3p_)  = Gamma_i(Si_,VIII,l3p_)
      allphot_vec(Si_,VIII,l3d_)  = Gamma_i(Si_,VIII,l3d_)
      allphot_vec(Si_,VIII,l4s_)  = Gamma_i(Si_,VIII,l4s_)
      allphot_vec(Si_,VIII,lall_) = allphot_vec(Si_,VIII,l1s_) +
     +         allphot_vec(Si_,VIII,l2s_) + allphot_vec(Si_,VIII,l2p_) +
     +         allphot_vec(Si_,VIII,l3s_) + allphot_vec(Si_,VIII,l3p_) +
     +         allphot_vec(Si_,VIII,l3d_) + allphot_vec(Si_,VIII,l4s_) 

      allphot_vec(Si_,IX,l1s_)  = Gamma_i(Si_,IX,l1s_)
      allphot_vec(Si_,IX,l2s_)  = Gamma_i(Si_,IX,l2s_)
      allphot_vec(Si_,IX,l2p_)  = Gamma_i(Si_,IX,l2p_)
      allphot_vec(Si_,IX,l3s_)  = Gamma_i(Si_,IX,l3s_)
      allphot_vec(Si_,IX,l3p_)  = Gamma_i(Si_,IX,l3p_)
      allphot_vec(Si_,IX,l3d_)  = Gamma_i(Si_,IX,l3d_)
      allphot_vec(Si_,IX,l4s_)  = Gamma_i(Si_,IX,l4s_)
      allphot_vec(Si_,IX,lall_) = allphot_vec(Si_,IX,l1s_) +
     +            allphot_vec(Si_,IX,l2s_) + allphot_vec(Si_,IX,l2p_) +
     +            allphot_vec(Si_,IX,l3s_) + allphot_vec(Si_,IX,l3p_) +
     +            allphot_vec(Si_,IX,l3d_) + allphot_vec(Si_,IX,l4s_) 

      allphot_vec(Si_,X,l1s_)  = Gamma_i(Si_,X,l1s_)
      allphot_vec(Si_,X,l2s_)  = Gamma_i(Si_,X,l2s_)
      allphot_vec(Si_,X,l2p_)  = Gamma_i(Si_,X,l2p_)
      allphot_vec(Si_,X,l3s_)  = Gamma_i(Si_,X,l3s_)
      allphot_vec(Si_,X,l3p_)  = Gamma_i(Si_,X,l3p_)
      allphot_vec(Si_,X,l3d_)  = Gamma_i(Si_,X,l3d_)
      allphot_vec(Si_,X,l4s_)  = Gamma_i(Si_,X,l4s_)
      allphot_vec(Si_,X,lall_) = allphot_vec(Si_,X,l1s_) +
     +              allphot_vec(Si_,X,l2s_) + allphot_vec(Si_,X,l2p_) +
     +              allphot_vec(Si_,X,l3s_) + allphot_vec(Si_,X,l3p_) +
     +              allphot_vec(Si_,X,l3d_) + allphot_vec(Si_,X,l4s_) 

      allphot_vec(Si_,XI,l1s_)  = Gamma_i(Si_,XI,l1s_)
      allphot_vec(Si_,XI,l2s_)  = Gamma_i(Si_,XI,l2s_)
      allphot_vec(Si_,XI,l2p_)  = Gamma_i(Si_,XI,l2p_)
      allphot_vec(Si_,XI,l3s_)  = Gamma_i(Si_,XI,l3s_)
      allphot_vec(Si_,XI,l3p_)  = Gamma_i(Si_,XI,l3p_)
      allphot_vec(Si_,XI,l3d_)  = Gamma_i(Si_,XI,l3d_)
      allphot_vec(Si_,XI,l4s_)  = Gamma_i(Si_,XI,l4s_)
      allphot_vec(Si_,XI,lall_) = allphot_vec(Si_,XI,l1s_) +
     +             allphot_vec(Si_,XI,l2s_) + allphot_vec(Si_,XI,l2p_) +
     +             allphot_vec(Si_,XI,l3s_) + allphot_vec(Si_,XI,l3p_) +
     +             allphot_vec(Si_,XI,l3d_) + allphot_vec(Si_,XI,l4s_) 

      allphot_vec(Si_,XII,l1s_)  = Gamma_i(Si_,XII,l1s_)
      allphot_vec(Si_,XII,l2s_)  = Gamma_i(Si_,XII,l2s_)
      allphot_vec(Si_,XII,l2p_)  = Gamma_i(Si_,XII,l2p_)
      allphot_vec(Si_,XII,l3s_)  = Gamma_i(Si_,XII,l3s_)
      allphot_vec(Si_,XII,l3p_)  = Gamma_i(Si_,XII,l3p_)
      allphot_vec(Si_,XII,l3d_)  = Gamma_i(Si_,XII,l3d_)
      allphot_vec(Si_,XII,l4s_)  = Gamma_i(Si_,XII,l4s_)
      allphot_vec(Si_,XII,lall_) = allphot_vec(Si_,XII,l1s_) +
     +           allphot_vec(Si_,XII,l2s_) + allphot_vec(Si_,XII,l2p_) +
     +           allphot_vec(Si_,XII,l3s_) + allphot_vec(Si_,XII,l3p_) +
     +           allphot_vec(Si_,XII,l3d_) + allphot_vec(Si_,XII,l4s_) 

      allphot_vec(Si_,XIII,l1s_)  = Gamma_i(Si_,XIII,l1s_)
      allphot_vec(Si_,XIII,l2s_)  = Gamma_i(Si_,XIII,l2s_)
      allphot_vec(Si_,XIII,l2p_)  = Gamma_i(Si_,XIII,l2p_)
      allphot_vec(Si_,XIII,l3s_)  = Gamma_i(Si_,XIII,l3s_)
      allphot_vec(Si_,XIII,l3p_)  = Gamma_i(Si_,XIII,l3p_)
      allphot_vec(Si_,XIII,l3d_)  = Gamma_i(Si_,XIII,l3d_)
      allphot_vec(Si_,XIII,l4s_)  = Gamma_i(Si_,XIII,l4s_)
      allphot_vec(Si_,XIII,lall_) = allphot_vec(Si_,XIII,l1s_) +
     +         allphot_vec(Si_,XIII,l2s_) + allphot_vec(Si_,XIII,l2p_) +
     +         allphot_vec(Si_,XIII,l3s_) + allphot_vec(Si_,XIII,l3p_) +
     +         allphot_vec(Si_,XIII,l3d_) + allphot_vec(Si_,XIII,l4s_) 

      allphot_vec(Si_,XIV,l1s_)  = Gamma_i(Si_,XIV,l1s_)
      allphot_vec(Si_,XIV,l2s_)  = Gamma_i(Si_,XIV,l2s_)
      allphot_vec(Si_,XIV,l2p_)  = Gamma_i(Si_,XIV,l2p_)
      allphot_vec(Si_,XIV,l3s_)  = Gamma_i(Si_,XIV,l3s_)
      allphot_vec(Si_,XIV,l3p_)  = Gamma_i(Si_,XIV,l3p_)
      allphot_vec(Si_,XIV,l3d_)  = Gamma_i(Si_,XIV,l3d_)
      allphot_vec(Si_,XIV,l4s_)  = Gamma_i(Si_,XIV,l4s_)
      allphot_vec(Si_,XIV,lall_) = allphot_vec(Si_,XIV,l1s_) +
     +           allphot_vec(Si_,XIV,l2s_) + allphot_vec(Si_,XIV,l2p_) +
     +           allphot_vec(Si_,XIV,l3s_) + allphot_vec(Si_,XIV,l3p_) +
     +           allphot_vec(Si_,XIV,l3d_) + allphot_vec(Si_,XIV,l4s_) 

c Sulfur

      allphot_vec(S_,I,l1s_)  = Gamma_i(S_,I,l1s_)
      allphot_vec(S_,I,l2s_)  = Gamma_i(S_,I,l2s_)
      allphot_vec(S_,I,l2p_)  = Gamma_i(S_,I,l2p_)
      allphot_vec(S_,I,l3s_)  = Gamma_i(S_,I,l3s_)
      allphot_vec(S_,I,l3p_)  = Gamma_i(S_,I,l3p_)
      allphot_vec(S_,I,l3d_)  = Gamma_i(S_,I,l3d_)
      allphot_vec(S_,I,l4s_)  = Gamma_i(S_,I,l4s_)
      allphot_vec(S_,I,lall_) = allphot_vec(S_,I,l1s_) +
     +              allphot_vec(S_,I,l2s_) + allphot_vec(S_,I,l2p_) +
     +              allphot_vec(S_,I,l3s_) + allphot_vec(S_,I,l3p_) +
     +              allphot_vec(S_,I,l3d_) + allphot_vec(S_,I,l4s_) 

      allphot_vec(S_,II,l1s_)  = Gamma_i(S_,II,l1s_)
      allphot_vec(S_,II,l2s_)  = Gamma_i(S_,II,l2s_)
      allphot_vec(S_,II,l2p_)  = Gamma_i(S_,II,l2p_)
      allphot_vec(S_,II,l3s_)  = Gamma_i(S_,II,l3s_)
      allphot_vec(S_,II,l3p_)  = Gamma_i(S_,II,l3p_)
      allphot_vec(S_,II,l3d_)  = Gamma_i(S_,II,l3d_)
      allphot_vec(S_,II,l4s_)  = Gamma_i(S_,II,l4s_)
      allphot_vec(S_,II,lall_) = allphot_vec(S_,II,l1s_) +
     +              allphot_vec(S_,II,l2s_) + allphot_vec(S_,II,l2p_) +
     +              allphot_vec(S_,II,l3s_) + allphot_vec(S_,II,l3p_) +
     +              allphot_vec(S_,II,l3d_) + allphot_vec(S_,II,l4s_) 

      allphot_vec(S_,III,l1s_)  = Gamma_i(S_,III,l1s_)
      allphot_vec(S_,III,l2s_)  = Gamma_i(S_,III,l2s_)
      allphot_vec(S_,III,l2p_)  = Gamma_i(S_,III,l2p_)
      allphot_vec(S_,III,l3s_)  = Gamma_i(S_,III,l3s_)
      allphot_vec(S_,III,l3p_)  = Gamma_i(S_,III,l3p_)
      allphot_vec(S_,III,l3d_)  = Gamma_i(S_,III,l3d_)
      allphot_vec(S_,III,l4s_)  = Gamma_i(S_,III,l4s_)
      allphot_vec(S_,III,lall_) = allphot_vec(S_,III,l1s_) +
     +            allphot_vec(S_,III,l2s_) + allphot_vec(S_,III,l2p_) +
     +            allphot_vec(S_,III,l3s_) + allphot_vec(S_,III,l3p_) +
     +            allphot_vec(S_,III,l3d_) + allphot_vec(S_,III,l4s_) 

      allphot_vec(S_,IV,l1s_)  = Gamma_i(S_,IV,l1s_)
      allphot_vec(S_,IV,l2s_)  = Gamma_i(S_,IV,l2s_)
      allphot_vec(S_,IV,l2p_)  = Gamma_i(S_,IV,l2p_)
      allphot_vec(S_,IV,l3s_)  = Gamma_i(S_,IV,l3s_)
      allphot_vec(S_,IV,l3p_)  = Gamma_i(S_,IV,l3p_)
      allphot_vec(S_,IV,l3d_)  = Gamma_i(S_,IV,l3d_)
      allphot_vec(S_,IV,l4s_)  = Gamma_i(S_,IV,l4s_)
      allphot_vec(S_,IV,lall_) = allphot_vec(S_,IV,l1s_) +
     +              allphot_vec(S_,IV,l2s_) + allphot_vec(S_,IV,l2p_) +
     +              allphot_vec(S_,IV,l3s_) + allphot_vec(S_,IV,l3p_) +
     +              allphot_vec(S_,IV,l3d_) + allphot_vec(S_,IV,l4s_) 

      allphot_vec(S_,V,l1s_)  = Gamma_i(S_,V,l1s_)
      allphot_vec(S_,V,l2s_)  = Gamma_i(S_,V,l2s_)
      allphot_vec(S_,V,l2p_)  = Gamma_i(S_,V,l2p_)
      allphot_vec(S_,V,l3s_)  = Gamma_i(S_,V,l3s_)
      allphot_vec(S_,V,l3p_)  = Gamma_i(S_,V,l3p_)
      allphot_vec(S_,V,l3d_)  = Gamma_i(S_,V,l3d_)
      allphot_vec(S_,V,l4s_)  = Gamma_i(S_,V,l4s_)
      allphot_vec(S_,V,lall_) = allphot_vec(S_,V,l1s_) +
     +              allphot_vec(S_,V,l2s_) + allphot_vec(S_,V,l2p_) +
     +              allphot_vec(S_,V,l3s_) + allphot_vec(S_,V,l3p_) +
     +              allphot_vec(S_,V,l3d_) + allphot_vec(S_,V,l4s_) 

      allphot_vec(S_,VI,l1s_)  = Gamma_i(S_,VI,l1s_)
      allphot_vec(S_,VI,l2s_)  = Gamma_i(S_,VI,l2s_)
      allphot_vec(S_,VI,l2p_)  = Gamma_i(S_,VI,l2p_)
      allphot_vec(S_,VI,l3s_)  = Gamma_i(S_,VI,l3s_)
      allphot_vec(S_,VI,l3p_)  = Gamma_i(S_,VI,l3p_)
      allphot_vec(S_,VI,l3d_)  = Gamma_i(S_,VI,l3d_)
      allphot_vec(S_,VI,l4s_)  = Gamma_i(S_,VI,l4s_)
      allphot_vec(S_,VI,lall_) = allphot_vec(S_,VI,l1s_) +
     +              allphot_vec(S_,VI,l2s_) + allphot_vec(S_,VI,l2p_) +
     +              allphot_vec(S_,VI,l3s_) + allphot_vec(S_,VI,l3p_) +
     +              allphot_vec(S_,VI,l3d_) + allphot_vec(S_,VI,l4s_) 

      allphot_vec(S_,VII,l1s_)  = Gamma_i(S_,VII,l1s_)
      allphot_vec(S_,VII,l2s_)  = Gamma_i(S_,VII,l2s_)
      allphot_vec(S_,VII,l2p_)  = Gamma_i(S_,VII,l2p_)
      allphot_vec(S_,VII,l3s_)  = Gamma_i(S_,VII,l3s_)
      allphot_vec(S_,VII,l3p_)  = Gamma_i(S_,VII,l3p_)
      allphot_vec(S_,VII,l3d_)  = Gamma_i(S_,VII,l3d_)
      allphot_vec(S_,VII,l4s_)  = Gamma_i(S_,VII,l4s_)
      allphot_vec(S_,VII,lall_) = allphot_vec(S_,VII,l1s_) +
     +             allphot_vec(S_,VII,l2s_) + allphot_vec(S_,VII,l2p_) +
     +             allphot_vec(S_,VII,l3s_) + allphot_vec(S_,VII,l3p_) +
     +             allphot_vec(S_,VII,l3d_) + allphot_vec(S_,VII,l4s_) 

      allphot_vec(S_,VIII,l1s_)  = Gamma_i(S_,VIII,l1s_)
      allphot_vec(S_,VIII,l2s_)  = Gamma_i(S_,VIII,l2s_)
      allphot_vec(S_,VIII,l2p_)  = Gamma_i(S_,VIII,l2p_)
      allphot_vec(S_,VIII,l3s_)  = Gamma_i(S_,VIII,l3s_)
      allphot_vec(S_,VIII,l3p_)  = Gamma_i(S_,VIII,l3p_)
      allphot_vec(S_,VIII,l3d_)  = Gamma_i(S_,VIII,l3d_)
      allphot_vec(S_,VIII,l4s_)  = Gamma_i(S_,VIII,l4s_)
      allphot_vec(S_,VIII,lall_) = allphot_vec(S_,VIII,l1s_) +
     +           allphot_vec(S_,VIII,l2s_) + allphot_vec(S_,VIII,l2p_) +
     +           allphot_vec(S_,VIII,l3s_) + allphot_vec(S_,VIII,l3p_) +
     +           allphot_vec(S_,VIII,l3d_) + allphot_vec(S_,VIII,l4s_) 

      allphot_vec(S_,IX,l1s_)  = Gamma_i(S_,IX,l1s_)
      allphot_vec(S_,IX,l2s_)  = Gamma_i(S_,IX,l2s_)
      allphot_vec(S_,IX,l2p_)  = Gamma_i(S_,IX,l2p_)
      allphot_vec(S_,IX,l3s_)  = Gamma_i(S_,IX,l3s_)
      allphot_vec(S_,IX,l3p_)  = Gamma_i(S_,IX,l3p_)
      allphot_vec(S_,IX,l3d_)  = Gamma_i(S_,IX,l3d_)
      allphot_vec(S_,IX,l4s_)  = Gamma_i(S_,IX,l4s_)
      allphot_vec(S_,IX,lall_) = allphot_vec(S_,IX,l1s_) +
     +              allphot_vec(S_,IX,l2s_) + allphot_vec(S_,IX,l2p_) +
     +              allphot_vec(S_,IX,l3s_) + allphot_vec(S_,IX,l3p_) +
     +              allphot_vec(S_,IX,l3d_) + allphot_vec(S_,IX,l4s_) 

      allphot_vec(S_,X,l1s_)  = Gamma_i(S_,X,l1s_)
      allphot_vec(S_,X,l2s_)  = Gamma_i(S_,X,l2s_)
      allphot_vec(S_,X,l2p_)  = Gamma_i(S_,X,l2p_)
      allphot_vec(S_,X,l3s_)  = Gamma_i(S_,X,l3s_)
      allphot_vec(S_,X,l3p_)  = Gamma_i(S_,X,l3p_)
      allphot_vec(S_,X,l3d_)  = Gamma_i(S_,X,l3d_)
      allphot_vec(S_,X,l4s_)  = Gamma_i(S_,X,l4s_)
      allphot_vec(S_,X,lall_) = allphot_vec(S_,X,l1s_) +
     +              allphot_vec(S_,X,l2s_) + allphot_vec(S_,X,l2p_) +
     +              allphot_vec(S_,X,l3s_) + allphot_vec(S_,X,l3p_) +
     +              allphot_vec(S_,X,l3d_) + allphot_vec(S_,X,l4s_) 

      allphot_vec(S_,XI,l1s_)  = Gamma_i(S_,XI,l1s_)
      allphot_vec(S_,XI,l2s_)  = Gamma_i(S_,XI,l2s_)
      allphot_vec(S_,XI,l2p_)  = Gamma_i(S_,XI,l2p_)
      allphot_vec(S_,XI,l3s_)  = Gamma_i(S_,XI,l3s_)
      allphot_vec(S_,XI,l3p_)  = Gamma_i(S_,XI,l3p_)
      allphot_vec(S_,XI,l3d_)  = Gamma_i(S_,XI,l3d_)
      allphot_vec(S_,XI,l4s_)  = Gamma_i(S_,XI,l4s_)
      allphot_vec(S_,XI,lall_) = allphot_vec(S_,XI,l1s_) +
     +              allphot_vec(S_,XI,l2s_) + allphot_vec(S_,XI,l2p_) +
     +              allphot_vec(S_,XI,l3s_) + allphot_vec(S_,XI,l3p_) +
     +              allphot_vec(S_,XI,l3d_) + allphot_vec(S_,XI,l4s_) 

      allphot_vec(S_,XII,l1s_)  = Gamma_i(S_,XII,l1s_)
      allphot_vec(S_,XII,l2s_)  = Gamma_i(S_,XII,l2s_)
      allphot_vec(S_,XII,l2p_)  = Gamma_i(S_,XII,l2p_)
      allphot_vec(S_,XII,l3s_)  = Gamma_i(S_,XII,l3s_)
      allphot_vec(S_,XII,l3p_)  = Gamma_i(S_,XII,l3p_)
      allphot_vec(S_,XII,l3d_)  = Gamma_i(S_,XII,l3d_)
      allphot_vec(S_,XII,l4s_)  = Gamma_i(S_,XII,l4s_)
      allphot_vec(S_,XII,lall_) = allphot_vec(S_,XII,l1s_) +
     +             allphot_vec(S_,XII,l2s_) + allphot_vec(S_,XII,l2p_) +
     +             allphot_vec(S_,XII,l3s_) + allphot_vec(S_,XII,l3p_) +
     +             allphot_vec(S_,XII,l3d_) + allphot_vec(S_,XII,l4s_) 

      allphot_vec(S_,XIII,l1s_)  = Gamma_i(S_,XIII,l1s_)
      allphot_vec(S_,XIII,l2s_)  = Gamma_i(S_,XIII,l2s_)
      allphot_vec(S_,XIII,l2p_)  = Gamma_i(S_,XIII,l2p_)
      allphot_vec(S_,XIII,l3s_)  = Gamma_i(S_,XIII,l3s_)
      allphot_vec(S_,XIII,l3p_)  = Gamma_i(S_,XIII,l3p_)
      allphot_vec(S_,XIII,l3d_)  = Gamma_i(S_,XIII,l3d_)
      allphot_vec(S_,XIII,l4s_)  = Gamma_i(S_,XIII,l4s_)
      allphot_vec(S_,XIII,lall_) = allphot_vec(S_,XIII,l1s_) +
     +           allphot_vec(S_,XIII,l2s_) + allphot_vec(S_,XIII,l2p_) +
     +           allphot_vec(S_,XIII,l3s_) + allphot_vec(S_,XIII,l3p_) +
     +           allphot_vec(S_,XIII,l3d_) + allphot_vec(S_,XIII,l4s_) 

      allphot_vec(S_,XIV,l1s_)  = Gamma_i(S_,XIV,l1s_)
      allphot_vec(S_,XIV,l2s_)  = Gamma_i(S_,XIV,l2s_)
      allphot_vec(S_,XIV,l2p_)  = Gamma_i(S_,XIV,l2p_)
      allphot_vec(S_,XIV,l3s_)  = Gamma_i(S_,XIV,l3s_)
      allphot_vec(S_,XIV,l3p_)  = Gamma_i(S_,XIV,l3p_)
      allphot_vec(S_,XIV,l3d_)  = Gamma_i(S_,XIV,l3d_)
      allphot_vec(S_,XIV,l4s_)  = Gamma_i(S_,XIV,l4s_)
      allphot_vec(S_,XIV,lall_) = allphot_vec(S_,XIV,l1s_) +
     +             allphot_vec(S_,XIV,l2s_) + allphot_vec(S_,XIV,l2p_) +
     +             allphot_vec(S_,XIV,l3s_) + allphot_vec(S_,XIV,l3p_) +
     +             allphot_vec(S_,XIV,l3d_) + allphot_vec(S_,XIV,l4s_) 

      allphot_vec(S_,XV,l1s_)  = Gamma_i(S_,XV,l1s_)
      allphot_vec(S_,XV,l2s_)  = Gamma_i(S_,XV,l2s_)
      allphot_vec(S_,XV,l2p_)  = Gamma_i(S_,XV,l2p_)
      allphot_vec(S_,XV,l3s_)  = Gamma_i(S_,XV,l3s_)
      allphot_vec(S_,XV,l3p_)  = Gamma_i(S_,XV,l3p_)
      allphot_vec(S_,XV,l3d_)  = Gamma_i(S_,XV,l3d_)
      allphot_vec(S_,XV,l4s_)  = Gamma_i(S_,XV,l4s_)
      allphot_vec(S_,XV,lall_) = allphot_vec(S_,XV,l1s_) +
     +              allphot_vec(S_,XV,l2s_) + allphot_vec(S_,XV,l2p_) +
     +              allphot_vec(S_,XV,l3s_) + allphot_vec(S_,XV,l3p_) +
     +              allphot_vec(S_,XV,l3d_) + allphot_vec(S_,XV,l4s_) 

      allphot_vec(S_,XVI,l1s_)  = Gamma_i(S_,XVI,l1s_)
      allphot_vec(S_,XVI,l2s_)  = Gamma_i(S_,XVI,l2s_)
      allphot_vec(S_,XVI,l2p_)  = Gamma_i(S_,XVI,l2p_)
      allphot_vec(S_,XVI,l3s_)  = Gamma_i(S_,XVI,l3s_)
      allphot_vec(S_,XVI,l3p_)  = Gamma_i(S_,XVI,l3p_)
      allphot_vec(S_,XVI,l3d_)  = Gamma_i(S_,XVI,l3d_)
      allphot_vec(S_,XVI,l4s_)  = Gamma_i(S_,XVI,l4s_)
      allphot_vec(S_,XVI,lall_) = allphot_vec(S_,XVI,l1s_) +
     +             allphot_vec(S_,XVI,l2s_) + allphot_vec(S_,XVI,l2p_) +
     +             allphot_vec(S_,XVI,l3s_) + allphot_vec(S_,XVI,l3p_) +
     +             allphot_vec(S_,XVI,l3d_) + allphot_vec(S_,XVI,l4s_) 


c Iron

      allphot_vec(Fe_,I,l1s_)  = Gamma_i(Fe_,I,l1s_)
      allphot_vec(Fe_,I,l2s_)  = Gamma_i(Fe_,I,l2s_)
      allphot_vec(Fe_,I,l2p_)  = Gamma_i(Fe_,I,l2p_)
      allphot_vec(Fe_,I,l3s_)  = Gamma_i(Fe_,I,l3s_)
      allphot_vec(Fe_,I,l3p_)  = Gamma_i(Fe_,I,l3p_)
      allphot_vec(Fe_,I,l3d_)  = Gamma_i(Fe_,I,l3d_)
      allphot_vec(Fe_,I,l4s_)  = Gamma_i(Fe_,I,l4s_)
      allphot_vec(Fe_,I,lall_) = allphot_vec(Fe_,I,l1s_) +
     +              allphot_vec(Fe_,I,l2s_) + allphot_vec(Fe_,I,l2p_) +
     +              allphot_vec(Fe_,I,l3s_) + allphot_vec(Fe_,I,l3p_) +
     +              allphot_vec(Fe_,I,l3d_) + allphot_vec(Fe_,I,l4s_) 

      allphot_vec(Fe_,II,l1s_)  = Gamma_i(Fe_,II,l1s_)
      allphot_vec(Fe_,II,l2s_)  = Gamma_i(Fe_,II,l2s_)
      allphot_vec(Fe_,II,l2p_)  = Gamma_i(Fe_,II,l2p_)
      allphot_vec(Fe_,II,l3s_)  = Gamma_i(Fe_,II,l3s_)
      allphot_vec(Fe_,II,l3p_)  = Gamma_i(Fe_,II,l3p_)
      allphot_vec(Fe_,II,l3d_)  = Gamma_i(Fe_,II,l3d_)
      allphot_vec(Fe_,II,l4s_)  = Gamma_i(Fe_,II,l4s_)
      allphot_vec(Fe_,II,lall_) = allphot_vec(Fe_,II,l1s_) +
     +             allphot_vec(Fe_,II,l2s_) + allphot_vec(Fe_,II,l2p_) +
     +             allphot_vec(Fe_,II,l3s_) + allphot_vec(Fe_,II,l3p_) +
     +             allphot_vec(Fe_,II,l3d_) + allphot_vec(Fe_,II,l4s_) 

      allphot_vec(Fe_,III,l1s_)  = Gamma_i(Fe_,III,l1s_)
      allphot_vec(Fe_,III,l2s_)  = Gamma_i(Fe_,III,l2s_)
      allphot_vec(Fe_,III,l2p_)  = Gamma_i(Fe_,III,l2p_)
      allphot_vec(Fe_,III,l3s_)  = Gamma_i(Fe_,III,l3s_)
      allphot_vec(Fe_,III,l3p_)  = Gamma_i(Fe_,III,l3p_)
      allphot_vec(Fe_,III,l3d_)  = Gamma_i(Fe_,III,l3d_)
      allphot_vec(Fe_,III,l4s_)  = Gamma_i(Fe_,III,l4s_)
      allphot_vec(Fe_,III,lall_) = allphot_vec(Fe_,III,l1s_) +
     +           allphot_vec(Fe_,III,l2s_) + allphot_vec(Fe_,III,l2p_) +
     +           allphot_vec(Fe_,III,l3s_) + allphot_vec(Fe_,III,l3p_) +
     +           allphot_vec(Fe_,III,l3d_) + allphot_vec(Fe_,III,l4s_) 

      allphot_vec(Fe_,IV,l1s_)  = Gamma_i(Fe_,IV,l1s_)
      allphot_vec(Fe_,IV,l2s_)  = Gamma_i(Fe_,IV,l2s_)
      allphot_vec(Fe_,IV,l2p_)  = Gamma_i(Fe_,IV,l2p_)
      allphot_vec(Fe_,IV,l3s_)  = Gamma_i(Fe_,IV,l3s_)
      allphot_vec(Fe_,IV,l3p_)  = Gamma_i(Fe_,IV,l3p_)
      allphot_vec(Fe_,IV,l3d_)  = Gamma_i(Fe_,IV,l3d_)
      allphot_vec(Fe_,IV,l4s_)  = Gamma_i(Fe_,IV,l4s_)
      allphot_vec(Fe_,IV,lall_) = allphot_vec(Fe_,IV,l1s_) +
     +             allphot_vec(Fe_,IV,l2s_) + allphot_vec(Fe_,IV,l2p_) +
     +             allphot_vec(Fe_,IV,l3s_) + allphot_vec(Fe_,IV,l3p_) +
     +             allphot_vec(Fe_,IV,l3d_) + allphot_vec(Fe_,IV,l4s_) 

      allphot_vec(Fe_,V,l1s_)  = Gamma_i(Fe_,V,l1s_)
      allphot_vec(Fe_,V,l2s_)  = Gamma_i(Fe_,V,l2s_)
      allphot_vec(Fe_,V,l2p_)  = Gamma_i(Fe_,V,l2p_)
      allphot_vec(Fe_,V,l3s_)  = Gamma_i(Fe_,V,l3s_)
      allphot_vec(Fe_,V,l3p_)  = Gamma_i(Fe_,V,l3p_)
      allphot_vec(Fe_,V,l3d_)  = Gamma_i(Fe_,V,l3d_)
      allphot_vec(Fe_,V,l4s_)  = Gamma_i(Fe_,V,l4s_)
      allphot_vec(Fe_,V,lall_) = allphot_vec(Fe_,V,l1s_) +
     +              allphot_vec(Fe_,V,l2s_) + allphot_vec(Fe_,V,l2p_) +
     +              allphot_vec(Fe_,V,l3s_) + allphot_vec(Fe_,V,l3p_) +
     +              allphot_vec(Fe_,V,l3d_) + allphot_vec(Fe_,V,l4s_) 

      allphot_vec(Fe_,VI,l1s_)  = Gamma_i(Fe_,VI,l1s_)
      allphot_vec(Fe_,VI,l2s_)  = Gamma_i(Fe_,VI,l2s_)
      allphot_vec(Fe_,VI,l2p_)  = Gamma_i(Fe_,VI,l2p_)
      allphot_vec(Fe_,VI,l3s_)  = Gamma_i(Fe_,VI,l3s_)
      allphot_vec(Fe_,VI,l3p_)  = Gamma_i(Fe_,VI,l3p_)
      allphot_vec(Fe_,VI,l3d_)  = Gamma_i(Fe_,VI,l3d_)
      allphot_vec(Fe_,VI,l4s_)  = Gamma_i(Fe_,VI,l4s_)
      allphot_vec(Fe_,VI,lall_) = allphot_vec(Fe_,VI,l1s_) +
     +             allphot_vec(Fe_,VI,l2s_) + allphot_vec(Fe_,VI,l2p_) +
     +             allphot_vec(Fe_,VI,l3s_) + allphot_vec(Fe_,VI,l3p_) +
     +             allphot_vec(Fe_,VI,l3d_) + allphot_vec(Fe_,VI,l4s_) 

      allphot_vec(Fe_,VII,l1s_)  = Gamma_i(Fe_,VII,l1s_)
      allphot_vec(Fe_,VII,l2s_)  = Gamma_i(Fe_,VII,l2s_)
      allphot_vec(Fe_,VII,l2p_)  = Gamma_i(Fe_,VII,l2p_)
      allphot_vec(Fe_,VII,l3s_)  = Gamma_i(Fe_,VII,l3s_)
      allphot_vec(Fe_,VII,l3p_)  = Gamma_i(Fe_,VII,l3p_)
      allphot_vec(Fe_,VII,l3d_)  = Gamma_i(Fe_,VII,l3d_)
      allphot_vec(Fe_,VII,l4s_)  = Gamma_i(Fe_,VII,l4s_)
      allphot_vec(Fe_,VII,lall_) = allphot_vec(Fe_,VII,l1s_) +
     +           allphot_vec(Fe_,VII,l2s_) + allphot_vec(Fe_,VII,l2p_) +
     +           allphot_vec(Fe_,VII,l3s_) + allphot_vec(Fe_,VII,l3p_) +
     +           allphot_vec(Fe_,VII,l3d_) + allphot_vec(Fe_,VII,l4s_) 

      allphot_vec(Fe_,VIII,l1s_)  = Gamma_i(Fe_,VIII,l1s_)
      allphot_vec(Fe_,VIII,l2s_)  = Gamma_i(Fe_,VIII,l2s_)
      allphot_vec(Fe_,VIII,l2p_)  = Gamma_i(Fe_,VIII,l2p_)
      allphot_vec(Fe_,VIII,l3s_)  = Gamma_i(Fe_,VIII,l3s_)
      allphot_vec(Fe_,VIII,l3p_)  = Gamma_i(Fe_,VIII,l3p_)
      allphot_vec(Fe_,VIII,l3d_)  = Gamma_i(Fe_,VIII,l3d_)
      allphot_vec(Fe_,VIII,l4s_)  = Gamma_i(Fe_,VIII,l4s_)
      allphot_vec(Fe_,VIII,lall_) = allphot_vec(Fe_,VIII,l1s_) +
     +         allphot_vec(Fe_,VIII,l2s_) + allphot_vec(Fe_,VIII,l2p_) +
     +         allphot_vec(Fe_,VIII,l3s_) + allphot_vec(Fe_,VIII,l3p_) +
     +         allphot_vec(Fe_,VIII,l3d_) + allphot_vec(Fe_,VIII,l4s_) 

      allphot_vec(Fe_,IX,l1s_)  = Gamma_i(Fe_,IX,l1s_)
      allphot_vec(Fe_,IX,l2s_)  = Gamma_i(Fe_,IX,l2s_)
      allphot_vec(Fe_,IX,l2p_)  = Gamma_i(Fe_,IX,l2p_)
      allphot_vec(Fe_,IX,l3s_)  = Gamma_i(Fe_,IX,l3s_)
      allphot_vec(Fe_,IX,l3p_)  = Gamma_i(Fe_,IX,l3p_)
      allphot_vec(Fe_,IX,l3d_)  = Gamma_i(Fe_,IX,l3d_)
      allphot_vec(Fe_,IX,l4s_)  = Gamma_i(Fe_,IX,l4s_)
      allphot_vec(Fe_,IX,lall_) = allphot_vec(Fe_,IX,l1s_) +
     +             allphot_vec(Fe_,IX,l2s_) + allphot_vec(Fe_,IX,l2p_) +
     +             allphot_vec(Fe_,IX,l3s_) + allphot_vec(Fe_,IX,l3p_) +
     +             allphot_vec(Fe_,IX,l3d_) + allphot_vec(Fe_,IX,l4s_) 

      allphot_vec(Fe_,X,l1s_)  = Gamma_i(Fe_,X,l1s_)
      allphot_vec(Fe_,X,l2s_)  = Gamma_i(Fe_,X,l2s_)
      allphot_vec(Fe_,X,l2p_)  = Gamma_i(Fe_,X,l2p_)
      allphot_vec(Fe_,X,l3s_)  = Gamma_i(Fe_,X,l3s_)
      allphot_vec(Fe_,X,l3p_)  = Gamma_i(Fe_,X,l3p_)
      allphot_vec(Fe_,X,l3d_)  = Gamma_i(Fe_,X,l3d_)
      allphot_vec(Fe_,X,l4s_)  = Gamma_i(Fe_,X,l4s_)
      allphot_vec(Fe_,X,lall_) = allphot_vec(Fe_,X,l1s_) +
     +              allphot_vec(Fe_,X,l2s_) + allphot_vec(Fe_,X,l2p_) +
     +              allphot_vec(Fe_,X,l3s_) + allphot_vec(Fe_,X,l3p_) +
     +              allphot_vec(Fe_,X,l3d_) + allphot_vec(Fe_,X,l4s_) 

      allphot_vec(Fe_,XI,l1s_)  = Gamma_i(Fe_,XI,l1s_)
      allphot_vec(Fe_,XI,l2s_)  = Gamma_i(Fe_,XI,l2s_)
      allphot_vec(Fe_,XI,l2p_)  = Gamma_i(Fe_,XI,l2p_)
      allphot_vec(Fe_,XI,l3s_)  = Gamma_i(Fe_,XI,l3s_)
      allphot_vec(Fe_,XI,l3p_)  = Gamma_i(Fe_,XI,l3p_)
      allphot_vec(Fe_,XI,l3d_)  = Gamma_i(Fe_,XI,l3d_)
      allphot_vec(Fe_,XI,l4s_)  = Gamma_i(Fe_,XI,l4s_)
      allphot_vec(Fe_,XI,lall_) = allphot_vec(Fe_,XI,l1s_) +
     +             allphot_vec(Fe_,XI,l2s_) + allphot_vec(Fe_,XI,l2p_) +
     +             allphot_vec(Fe_,XI,l3s_) + allphot_vec(Fe_,XI,l3p_) +
     +             allphot_vec(Fe_,XI,l3d_) + allphot_vec(Fe_,XI,l4s_) 

      allphot_vec(Fe_,XII,l1s_)  = Gamma_i(Fe_,XII,l1s_)
      allphot_vec(Fe_,XII,l2s_)  = Gamma_i(Fe_,XII,l2s_)
      allphot_vec(Fe_,XII,l2p_)  = Gamma_i(Fe_,XII,l2p_)
      allphot_vec(Fe_,XII,l3s_)  = Gamma_i(Fe_,XII,l3s_)
      allphot_vec(Fe_,XII,l3p_)  = Gamma_i(Fe_,XII,l3p_)
      allphot_vec(Fe_,XII,l3d_)  = Gamma_i(Fe_,XII,l3d_)
      allphot_vec(Fe_,XII,l4s_)  = Gamma_i(Fe_,XII,l4s_)
      allphot_vec(Fe_,XII,lall_) = allphot_vec(Fe_,XII,l1s_) +
     +           allphot_vec(Fe_,XII,l2s_) + allphot_vec(Fe_,XII,l2p_) +
     +           allphot_vec(Fe_,XII,l3s_) + allphot_vec(Fe_,XII,l3p_) +
     +           allphot_vec(Fe_,XII,l3d_) + allphot_vec(Fe_,XII,l4s_) 

      allphot_vec(Fe_,XIII,l1s_)  = Gamma_i(Fe_,XIII,l1s_)
      allphot_vec(Fe_,XIII,l2s_)  = Gamma_i(Fe_,XIII,l2s_)
      allphot_vec(Fe_,XIII,l2p_)  = Gamma_i(Fe_,XIII,l2p_)
      allphot_vec(Fe_,XIII,l3s_)  = Gamma_i(Fe_,XIII,l3s_)
      allphot_vec(Fe_,XIII,l3p_)  = Gamma_i(Fe_,XIII,l3p_)
      allphot_vec(Fe_,XIII,l3d_)  = Gamma_i(Fe_,XIII,l3d_)
      allphot_vec(Fe_,XIII,l4s_)  = Gamma_i(Fe_,XIII,l4s_)
      allphot_vec(Fe_,XIII,lall_) = allphot_vec(Fe_,XIII,l1s_) +
     +         allphot_vec(Fe_,XIII,l2s_) + allphot_vec(Fe_,XIII,l2p_) +
     +         allphot_vec(Fe_,XIII,l3s_) + allphot_vec(Fe_,XIII,l3p_) +
     +         allphot_vec(Fe_,XIII,l3d_) + allphot_vec(Fe_,XIII,l4s_) 

      allphot_vec(Fe_,XIV,l1s_)  = Gamma_i(Fe_,XIV,l1s_)
      allphot_vec(Fe_,XIV,l2s_)  = Gamma_i(Fe_,XIV,l2s_)
      allphot_vec(Fe_,XIV,l2p_)  = Gamma_i(Fe_,XIV,l2p_)
      allphot_vec(Fe_,XIV,l3s_)  = Gamma_i(Fe_,XIV,l3s_)
      allphot_vec(Fe_,XIV,l3p_)  = Gamma_i(Fe_,XIV,l3p_)
      allphot_vec(Fe_,XIV,l3d_)  = Gamma_i(Fe_,XIV,l3d_)
      allphot_vec(Fe_,XIV,l4s_)  = Gamma_i(Fe_,XIV,l4s_)
      allphot_vec(Fe_,XIV,lall_) = allphot_vec(Fe_,XIV,l1s_) +
     +           allphot_vec(Fe_,XIV,l2s_) + allphot_vec(Fe_,XIV,l2p_) +
     +           allphot_vec(Fe_,XIV,l3s_) + allphot_vec(Fe_,XIV,l3p_) +
     +           allphot_vec(Fe_,XIV,l3d_) + allphot_vec(Fe_,XIV,l4s_) 

      allphot_vec(Fe_,XV,l1s_)  = Gamma_i(Fe_,XV,l1s_)
      allphot_vec(Fe_,XV,l2s_)  = Gamma_i(Fe_,XV,l2s_)
      allphot_vec(Fe_,XV,l2p_)  = Gamma_i(Fe_,XV,l2p_)
      allphot_vec(Fe_,XV,l3s_)  = Gamma_i(Fe_,XV,l3s_)
      allphot_vec(Fe_,XV,l3p_)  = Gamma_i(Fe_,XV,l3p_)
      allphot_vec(Fe_,XV,l3d_)  = Gamma_i(Fe_,XV,l3d_)
      allphot_vec(Fe_,XV,l4s_)  = Gamma_i(Fe_,XV,l4s_)
      allphot_vec(Fe_,XV,lall_) = allphot_vec(Fe_,XV,l1s_) +
     +             allphot_vec(Fe_,XV,l2s_) + allphot_vec(Fe_,XV,l2p_) +
     +             allphot_vec(Fe_,XV,l3s_) + allphot_vec(Fe_,XV,l3p_) +
     +             allphot_vec(Fe_,XV,l3d_) + allphot_vec(Fe_,XV,l4s_) 

      allphot_vec(Fe_,XVI,l1s_)  = Gamma_i(Fe_,XVI,l1s_)
      allphot_vec(Fe_,XVI,l2s_)  = Gamma_i(Fe_,XVI,l2s_)
      allphot_vec(Fe_,XVI,l2p_)  = Gamma_i(Fe_,XVI,l2p_)
      allphot_vec(Fe_,XVI,l3s_)  = Gamma_i(Fe_,XVI,l3s_)
      allphot_vec(Fe_,XVI,l3p_)  = Gamma_i(Fe_,XVI,l3p_)
      allphot_vec(Fe_,XVI,l3d_)  = Gamma_i(Fe_,XVI,l3d_)
      allphot_vec(Fe_,XVI,l4s_)  = Gamma_i(Fe_,XVI,l4s_)
      allphot_vec(Fe_,XVI,lall_) = allphot_vec(Fe_,XVI,l1s_) +
     +           allphot_vec(Fe_,XVI,l2s_) + allphot_vec(Fe_,XVI,l2p_) +
     +           allphot_vec(Fe_,XVI,l3s_) + allphot_vec(Fe_,XVI,l3p_) +
     +           allphot_vec(Fe_,XVI,l3d_) + allphot_vec(Fe_,XVI,l4s_) 

      allphot_vec(Fe_,XVII,l1s_)  = Gamma_i(Fe_,XVII,l1s_)
      allphot_vec(Fe_,XVII,l2s_)  = Gamma_i(Fe_,XVII,l2s_)
      allphot_vec(Fe_,XVII,l2p_)  = Gamma_i(Fe_,XVII,l2p_)
      allphot_vec(Fe_,XVII,l3s_)  = Gamma_i(Fe_,XVII,l3s_)
      allphot_vec(Fe_,XVII,l3p_)  = Gamma_i(Fe_,XVII,l3p_)
      allphot_vec(Fe_,XVII,l3d_)  = Gamma_i(Fe_,XVII,l3d_)
      allphot_vec(Fe_,XVII,l4s_)  = Gamma_i(Fe_,XVII,l4s_)
      allphot_vec(Fe_,XVII,lall_) = allphot_vec(Fe_,XVII,l1s_) +
     +         allphot_vec(Fe_,XVII,l2s_) + allphot_vec(Fe_,XVII,l2p_) +
     +         allphot_vec(Fe_,XVII,l3s_) + allphot_vec(Fe_,XVII,l3p_) +
     +         allphot_vec(Fe_,XVII,l3d_) + allphot_vec(Fe_,XVII,l4s_) 

      allphot_vec(Fe_,XVIII,l1s_)  = Gamma_i(Fe_,XVIII,l1s_)
      allphot_vec(Fe_,XVIII,l2s_)  = Gamma_i(Fe_,XVIII,l2s_)
      allphot_vec(Fe_,XVIII,l2p_)  = Gamma_i(Fe_,XVIII,l2p_)
      allphot_vec(Fe_,XVIII,l3s_)  = Gamma_i(Fe_,XVIII,l3s_)
      allphot_vec(Fe_,XVIII,l3p_)  = Gamma_i(Fe_,XVIII,l3p_)
      allphot_vec(Fe_,XVIII,l3d_)  = Gamma_i(Fe_,XVIII,l3d_)
      allphot_vec(Fe_,XVIII,l4s_)  = Gamma_i(Fe_,XVIII,l4s_)
      allphot_vec(Fe_,XVIII,lall_) = allphot_vec(Fe_,XVIII,l1s_) +
     +       allphot_vec(Fe_,XVIII,l2s_) + allphot_vec(Fe_,XVIII,l2p_) +
     +       allphot_vec(Fe_,XVIII,l3s_) + allphot_vec(Fe_,XVIII,l3p_) +
     +       allphot_vec(Fe_,XVIII,l3d_) + allphot_vec(Fe_,XVIII,l4s_) 

      allphot_vec(Fe_,XIX,l1s_)  = Gamma_i(Fe_,XIX,l1s_)
      allphot_vec(Fe_,XIX,l2s_)  = Gamma_i(Fe_,XIX,l2s_)
      allphot_vec(Fe_,XIX,l2p_)  = Gamma_i(Fe_,XIX,l2p_)
      allphot_vec(Fe_,XIX,l3s_)  = Gamma_i(Fe_,XIX,l3s_)
      allphot_vec(Fe_,XIX,l3p_)  = Gamma_i(Fe_,XIX,l3p_)
      allphot_vec(Fe_,XIX,l3d_)  = Gamma_i(Fe_,XIX,l3d_)
      allphot_vec(Fe_,XIX,l4s_)  = Gamma_i(Fe_,XIX,l4s_)
      allphot_vec(Fe_,XIX,lall_) = allphot_vec(Fe_,XIX,l1s_) +
     +           allphot_vec(Fe_,XIX,l2s_) + allphot_vec(Fe_,XIX,l2p_) +
     +           allphot_vec(Fe_,XIX,l3s_) + allphot_vec(Fe_,XIX,l3p_) +
     +           allphot_vec(Fe_,XIX,l3d_) + allphot_vec(Fe_,XIX,l4s_) 

      allphot_vec(Fe_,XX,l1s_)  = Gamma_i(Fe_,XX,l1s_)
      allphot_vec(Fe_,XX,l2s_)  = Gamma_i(Fe_,XX,l2s_)
      allphot_vec(Fe_,XX,l2p_)  = Gamma_i(Fe_,XX,l2p_)
      allphot_vec(Fe_,XX,l3s_)  = Gamma_i(Fe_,XX,l3s_)
      allphot_vec(Fe_,XX,l3p_)  = Gamma_i(Fe_,XX,l3p_)
      allphot_vec(Fe_,XX,l3d_)  = Gamma_i(Fe_,XX,l3d_)
      allphot_vec(Fe_,XX,l4s_)  = Gamma_i(Fe_,XX,l4s_)
      allphot_vec(Fe_,XX,lall_) = allphot_vec(Fe_,XX,l1s_) +
     +             allphot_vec(Fe_,XX,l2s_) + allphot_vec(Fe_,XX,l2p_) +
     +             allphot_vec(Fe_,XX,l3s_) + allphot_vec(Fe_,XX,l3p_) +
     +             allphot_vec(Fe_,XX,l3d_) + allphot_vec(Fe_,XX,l4s_) 

      allphot_vec(Fe_,XXI,l1s_)  = Gamma_i(Fe_,XXI,l1s_)
      allphot_vec(Fe_,XXI,l2s_)  = Gamma_i(Fe_,XXI,l2s_)
      allphot_vec(Fe_,XXI,l2p_)  = Gamma_i(Fe_,XXI,l2p_)
      allphot_vec(Fe_,XXI,l3s_)  = Gamma_i(Fe_,XXI,l3s_)
      allphot_vec(Fe_,XXI,l3p_)  = Gamma_i(Fe_,XXI,l3p_)
      allphot_vec(Fe_,XXI,l3d_)  = Gamma_i(Fe_,XXI,l3d_)
      allphot_vec(Fe_,XXI,l4s_)  = Gamma_i(Fe_,XXI,l4s_)
      allphot_vec(Fe_,XXI,lall_) = allphot_vec(Fe_,XXI,l1s_) +
     +           allphot_vec(Fe_,XXI,l2s_) + allphot_vec(Fe_,XXI,l2p_) +
     +           allphot_vec(Fe_,XXI,l3s_) + allphot_vec(Fe_,XXI,l3p_) +
     +           allphot_vec(Fe_,XXI,l3d_) + allphot_vec(Fe_,XXI,l4s_) 

      allphot_vec(Fe_,XXII,l1s_)  = Gamma_i(Fe_,XXII,l1s_)
      allphot_vec(Fe_,XXII,l2s_)  = Gamma_i(Fe_,XXII,l2s_)
      allphot_vec(Fe_,XXII,l2p_)  = Gamma_i(Fe_,XXII,l2p_)
      allphot_vec(Fe_,XXII,l3s_)  = Gamma_i(Fe_,XXII,l3s_)
      allphot_vec(Fe_,XXII,l3p_)  = Gamma_i(Fe_,XXII,l3p_)
      allphot_vec(Fe_,XXII,l3d_)  = Gamma_i(Fe_,XXII,l3d_)
      allphot_vec(Fe_,XXII,l4s_)  = Gamma_i(Fe_,XXII,l4s_)
      allphot_vec(Fe_,XXII,lall_) = allphot_vec(Fe_,XXII,l1s_) +
     +         allphot_vec(Fe_,XXII,l2s_) + allphot_vec(Fe_,XXII,l2p_) +
     +         allphot_vec(Fe_,XXII,l3s_) + allphot_vec(Fe_,XXII,l3p_) +
     +         allphot_vec(Fe_,XXII,l3d_) + allphot_vec(Fe_,XXII,l4s_) 

      allphot_vec(Fe_,XXIII,l1s_)  = Gamma_i(Fe_,XXIII,l1s_)
      allphot_vec(Fe_,XXIII,l2s_)  = Gamma_i(Fe_,XXIII,l2s_)
      allphot_vec(Fe_,XXIII,l2p_)  = Gamma_i(Fe_,XXIII,l2p_)
      allphot_vec(Fe_,XXIII,l3s_)  = Gamma_i(Fe_,XXIII,l3s_)
      allphot_vec(Fe_,XXIII,l3p_)  = Gamma_i(Fe_,XXIII,l3p_)
      allphot_vec(Fe_,XXIII,l3d_)  = Gamma_i(Fe_,XXIII,l3d_)
      allphot_vec(Fe_,XXIII,l4s_)  = Gamma_i(Fe_,XXIII,l4s_)
      allphot_vec(Fe_,XXIII,lall_) = allphot_vec(Fe_,XXIII,l1s_) +
     +       allphot_vec(Fe_,XXIII,l2s_) + allphot_vec(Fe_,XXIII,l2p_) +
     +       allphot_vec(Fe_,XXIII,l3s_) + allphot_vec(Fe_,XXIII,l3p_) +
     +       allphot_vec(Fe_,XXIII,l3d_) + allphot_vec(Fe_,XXIII,l4s_) 

      allphot_vec(Fe_,XXIV,l1s_)  = Gamma_i(Fe_,XXIV,l1s_)
      allphot_vec(Fe_,XXIV,l2s_)  = Gamma_i(Fe_,XXIV,l2s_)
      allphot_vec(Fe_,XXIV,l2p_)  = Gamma_i(Fe_,XXIV,l2p_)
      allphot_vec(Fe_,XXIV,l3s_)  = Gamma_i(Fe_,XXIV,l3s_)
      allphot_vec(Fe_,XXIV,l3p_)  = Gamma_i(Fe_,XXIV,l3p_)
      allphot_vec(Fe_,XXIV,l3d_)  = Gamma_i(Fe_,XXIV,l3d_)
      allphot_vec(Fe_,XXIV,l4s_)  = Gamma_i(Fe_,XXIV,l4s_)
      allphot_vec(Fe_,XXIV,lall_) = allphot_vec(Fe_,XXIV,l1s_) +
     +         allphot_vec(Fe_,XXIV,l2s_) + allphot_vec(Fe_,XXIV,l2p_) +
     +         allphot_vec(Fe_,XXIV,l3s_) + allphot_vec(Fe_,XXIV,l3p_) +
     +         allphot_vec(Fe_,XXIV,l3d_) + allphot_vec(Fe_,XXIV,l4s_) 

      allphot_vec(Fe_,XXV,l1s_)  = Gamma_i(Fe_,XXV,l1s_)
      allphot_vec(Fe_,XXV,l2s_)  = Gamma_i(Fe_,XXV,l2s_)
      allphot_vec(Fe_,XXV,l2p_)  = Gamma_i(Fe_,XXV,l2p_)
      allphot_vec(Fe_,XXV,l3s_)  = Gamma_i(Fe_,XXV,l3s_)
      allphot_vec(Fe_,XXV,l3p_)  = Gamma_i(Fe_,XXV,l3p_)
      allphot_vec(Fe_,XXV,l3d_)  = Gamma_i(Fe_,XXV,l3d_)
      allphot_vec(Fe_,XXV,l4s_)  = Gamma_i(Fe_,XXV,l4s_)
      allphot_vec(Fe_,XXV,lall_) = allphot_vec(Fe_,XXV,l1s_) +
     +           allphot_vec(Fe_,XXV,l2s_) + allphot_vec(Fe_,XXV,l2p_) +
     +           allphot_vec(Fe_,XXV,l3s_) + allphot_vec(Fe_,XXV,l3p_) +
     +           allphot_vec(Fe_,XXV,l3d_) + allphot_vec(Fe_,XXV,l4s_) 

      allphot_vec(Fe_,XXVI,l1s_)  = Gamma_i(Fe_,XXVI,l1s_)
      allphot_vec(Fe_,XXVI,l2s_)  = Gamma_i(Fe_,XXVI,l2s_)
      allphot_vec(Fe_,XXVI,l2p_)  = Gamma_i(Fe_,XXVI,l2p_)
      allphot_vec(Fe_,XXVI,l3s_)  = Gamma_i(Fe_,XXVI,l3s_)
      allphot_vec(Fe_,XXVI,l3p_)  = Gamma_i(Fe_,XXVI,l3p_)
      allphot_vec(Fe_,XXVI,l3d_)  = Gamma_i(Fe_,XXVI,l3d_)
      allphot_vec(Fe_,XXVI,l4s_)  = Gamma_i(Fe_,XXVI,l4s_)
      allphot_vec(Fe_,XXVI,lall_) = allphot_vec(Fe_,XXVI,l1s_) +
     +         allphot_vec(Fe_,XXVI,l2s_) + allphot_vec(Fe_,XXVI,l2p_) +
     +         allphot_vec(Fe_,XXVI,l3s_) + allphot_vec(Fe_,XXVI,l3p_) +
     +         allphot_vec(Fe_,XXVI,l3d_) + allphot_vec(Fe_,XXVI,l4s_) 


      return
      end
