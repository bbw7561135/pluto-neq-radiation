c234567
c  =========================================================================
c      program Main - Isochoric Cooling 
c  =========================================================================
      implicit none 
c     calculates the abundances and cooling efficiencies
c     as a function of time in an isochorically cooling gas parcel
c
c     Version of May 3, 2006
c
c     === NOTE !! edit variable "run_cloudy" to point to right Cloudy path ===
c     
c     To compile on a pc:
c /opt/intel_fc_80/bin/ifort -r8 -axN -O3 Gamma.f ct_rec.f ct_ion.f he_ct_rec.f he_ct_ion.f colion.f allrec.f opkda1.f opkda2.f dlsoda.f JFCN.f FCN.f cases.f  Main.f net_cooling.f interp_spec.f cloudy_input.f time_step.f -o isochoric.exe
c


c  =========================================================================
c     variable declerations:
c  =========================================================================

      double precision   k_B        ! [erg K^-1] k_Boltzman.

      character*80 spec_filename    !file containing spectrum data
      character*80 initial_filename !file containing initial values for dens.
      character*80 results_filename !file to put results in
      character*80 cmd_rm           !str with system command rm cloudy's input
      character*80 run_cloudy       !str with system command run cloudy

      integer  i       !loops index
      integer  j

c --- The DLSODA  variables:
      External         DLSODA     ! integrator
      integer          Num        ! number of differential equations
      parameter (Num=102) ! HI, HeI-II, CI-VI.......FeI-FeXXVI
      external         FCN        ! function that calculates dx/dy
      external         JFCN       ! function that calculates the Jacobian
      double precision time       ! independent variable (= r/ Ro)
      double precision rtol       ! relative  tolerances
      double precision atol(Num)  ! absolute tolerances
      integer          itol,itask,istate,iopt
      double precision rwork(15000)
      integer          lrw
      integer          iwork(300)
      integer          liw
      integer          jt
      double precision x(Num)  ! relative abundances: 
                          ! on input x is the initial values (equilibrium)
                          ! on output, its the solution.

c      double precision dxdt(Num)! derivative value   dx/dy = ()[+ - + - + -]
      double precision xp(Num)    ! x value to print, 0 if less than tolerance

      integer          H_I
      integer          He_I, He_II
      parameter (H_I= 1)
      parameter (He_I= 2, He_II= 3)

      double precision Tlow        ! stop intergartion when reaching this value
      double precision cooling     ! net cooling rate (erg s^-1 cm^-3)   
      double precision net_cooling ! function
      external         net_cooling
      double precision ntot        ! total particle density (H+He+e)
      double precision D_time      ! time step
      double precision time_step   ! function
      external         time_step
      external         Dcloudy_input

      external         dtime,etime   !functions: how long does run take?
      double precision dtime,etime
      double precision TIMEARRAY(2) ! user and cpu time for function etime,dtime

     
      double precision H_tol          ! Hydrogen accuracy
      double precision He_tol         ! Helium accuracy
      double precision met_tol        ! metals accuracy

c------------------------------------------------
c------------------------------------------------
c   **  B E W A R E   ! ! !  ** - common stuff:
c------------------------------------------------
c------------------------------------------------

c     COMMON
c --- Recombination coefficients:
      COMMON/RECB/re(300),unused(300),unused2(300)
      double precision re, unused, unused2
      !"re" is vector of recombination coefficients: rad + dr + 3bod
      !it is "common" with Hagai's code.


c     COMMON
c --- Radiation Field:
      common/spectrum/nu(10000),Jnu(10000),VecSize  
      double precision nu  ! frquency vector (Hz)
      double precision Jnu ! Rad. field vector 
                                  ! (erg s^-1 Hz^-1 sr^-1 cm^-2)
      integer VecSize    ! actual size of vectors (max size is 10,000).
      !this is only here because in is passed away to the function "Gamma"
      !(in FCN.f) which calculates the photoionization cross section.
      
c     COMMON
c --- Model (WIM/HIM) Parameters:
      common/model/PoverK,T_HIM,nH,currentT,old_time,new_time,epsl,D,
     + H_HE_case,HeIIcase
      double precision PoverK   ! pressure in units of P/k_B (cm^-3 K).
      double precision T_HIM    ! initial temperature.
      double precision nH       ! (cm^-3) H den 
      double precision currentT ! present temperature
      double precision epsl
      double precision old_time
      double precision new_time
      double precision D        ! cloud size
      integer H_HE_case,HeIIcase

c     COMMON
c --- Gas properties:
      common/Gas/X_He, X_C, X_N, X_O, X_Si, X_S, X_Ne, X_Fe, X_Mg
      double precision X_He, X_C, X_N, X_O, X_Si, X_S !abundances relative to H
c --- added Feb 14, 2005
      double precision X_Ne, X_Fe
c --- added Apr 14, 2005
      double precision X_Mg

      double precision Z  !gas metallicity

c------------------------------------------------
c------------------------------------------------
c   **  THE END (of the commons) **
c------------------------------------------------
c------------------------------------------------

      double precision tau13_6, tau24_6,tau54_4
      integer HcaseA_HecaseA, HcaseB_HecaseA
      integer HcaseA_HecaseB, HcaseB_HecaseB
      integer HeIIcaseA, HeIIcaseB

c --- Meaningful names for cases:
      parameter (HcaseA_HecaseA=1, HcaseB_HecaseA=2)
      parameter (HcaseA_HecaseB=3, HcaseB_HecaseB=4)
      parameter (HeIIcaseA=5, HeIIcaseB=6)

c  =========================================================================
c     Define parameters and initialize variables
c  =========================================================================

      parameter(k_B=1.38e-16)  ![erg K^-1] k_Boltzman.

c      itol = 1       ! 1 means that rtol and atol are scalars (see dlsoda) 
c      rtol = 1.e-2   ! relative tolerance
c      atol = 0.0     ! absolute tolerance
      itol = 2        ! 2 means that rtol is a scalar and atol an array
      rtol = 0.0      ! relative tolerance

      H_tol   = 1.0e-6
      He_tol  = 1.0e-5
      met_tol = 1.0e-4

      lrw = 15000     ! size of dlsoda's double precision working array
      liw = 300       ! size of dlsoda's integer working array

      itask  = 1      ! give output at y=yEnd
      istate = 1      ! flag. 1 means initial entry, 2- success, <0 - error. 
      iopt   = 0      ! no optional parameters 
      jt     = 2      ! dlsoda estimates the Jacobian alone 

      time    = 0.0   ! initial value of y = r / Ro (y=1 => r=Ro)


      print *, 'Enter initial temperature [K]:'
      read (5,*) T_HIM
      print *, 'Enter Hydrogen density:'
      read (5,*) nH
      print *, 'Gas Metallicity (enter -1 to specify abundances):'
      read (5,*) Z
      if (Z.lt.0) then
         print *,'Helium abundance (He / H):'
         read(5,*) X_He
         print *,'Carbon abundance (C / H):'
         read(5,*) X_C
         print *,'Nitrogen abundance (N / H):'
         read(5,*) X_N
         print *,'Oxygen abundance (O / H):'
         read(5,*) X_O
         print *,'Neon abundance (Ne / H):'
         read(5,*) X_Ne
         print *,'Magnesium abundance (Mg / H):'
         read(5,*) X_Mg
         print *,'Silicon abundance (Si / H):'
         read(5,*) X_Si
         print *,'Sulfur abundance (S / H):'
         read(5,*) X_S
         print *,'Iron abundance (Fe / H):'
         read(5,*) X_Fe

      else
c --- Oct 06, 2005: 
c     Asplund et al. 2005 2005ASPC..336...25A
c     Except for Ne: Bahcall 2005, ApJ, 631, 1281
         X_He = 1./12.
         X_C  = Z * 2.45e-4
         X_N  = Z * 6.03e-5
         X_O  = Z * 4.57e-4
         X_Ne = Z * 1.95e-4
         X_Mg = Z * 3.39e-5
         X_Si = Z * 3.24e-5
         X_S  = Z * 1.38e-5
         X_Fe = Z * 2.82e-5
      endif

      print *, 'C/H=',X_C,' N/H=',X_N,' O/H=',X_O,' Ne/H=',X_Ne,
     +         ' Mg/H=',X_Mg,'Si/H=',X_Si,' S/H=',X_S,' Fe/H=',X_Fe 
 

c --- read name of radiation field file, and initialize common block
c     spectrum: [nu] , [4piJnu] , VecSize
      print *, ' '
      print *, ' '
      print *, 'RADIATION FIELD:'
      print *, ' '
      print *, 'File-name for ionizing spectrum [nu, 4piJnu]:'
      print *, '  note: # of lines in file must not exceed 9,500'
      read (5,*) spec_filename
 
c --- read data from spectrum file:
      open (11, file=spec_filename, status='old', ERR=17)
      do vecsize=1,10000
         read (11,*,END=11,ERR=91) nu(VecSize), Jnu(VecSize)
      enddo
 11   close(11)
      VecSize = VecSize - 1     !number of lines in file
      print *,'Read ',VecSize,' lines from spectrum file.'
      call interp_spec          ! add ionization edges to list 


      print *, ' ' 
      print *, 'Read pressure resolution epsilon:'
      print *, '  ( P_i+1 - P_i = epsilon * P_i )'
      read (5,*) epsl
      print *, ' ' 
      print *, 'T_low (temperature to stop integration):'
      read (5,*) Tlow

c --- read name of initial valeus file,
      print *, ' '
      print *, ' '
      print *, 'INITIAL VALUES:'
      print *, ' '
      print *, 'File-name for initial values of densities:'
      read (5,*) initial_filename
      
      open (13, file=initial_filename,status='old')

c - read initial conditions: in input x = ion/element
c   then convert to ion/H
      read (13,*,END=93,ERR=94) x(1)       !read H (I)
      atol(1) = H_tol
      do i=2,3                             !read He (I-II)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_He
         atol(i) = X_He * He_tol
      enddo
      do i=4,9                             !read C (I-VI)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_C
         atol(i) = X_C * met_tol
      enddo
      do i=10,16                           !read N (I-VII)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_N
         atol(i) = X_N * met_tol
      enddo
      do i=17,24                           !read O (I-VIII)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_O
         atol(i) = X_O * met_tol
      enddo
      do i=55,64                           !read Ne (I-X)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_Ne
         atol(i) = X_Ne * met_tol
      enddo
      do i=91,102                          !read Mg (I-XII)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_Mg
         atol(i) = X_Mg * met_tol
      enddo
      do i=25,38                           !read Si (I-XIV)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_Si
         atol(i) = X_Si * met_tol
      enddo
      do i=39,54                           !read S (I-XVI)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_S
         atol(i) = X_S * met_tol
      enddo
      do i=65,90                           !read Fe (I-XXVI)
         read (13,*,END=93,ERR=94) x(i)
         x(i) = x(i) * X_Fe
         atol(i) = X_Fe * met_tol
      enddo

      close(13)

      print *,'Enter Cloud size [kpc]:'
      read (5,*) D          ! read D in kpc
      D = D * 3.08568025e21 ! now D in cm
      print *,'Size is ',D,' cm.'

c --- read name of results file:
      print *, ' '
      print *, ' '
      print *, '-------------------------------------------------------'
      print *, ' '
      print *, 'File-name for results:'
      read (5,*) results_filename
      open (14, FILE=results_filename, status='new')

c --- title for results file
      write (14 ,35) T_HIM,nH
 35   format('% T_HIM=',1E16.10,'[K], nH=',E10.4,'[cm^-3].')
      write (14,36) X_He,X_C,X_N
 36   format('% He/H=',E8.2,', C/H=',E8.2,',  N/H=',E8.2)
      write (14,37) X_O,X_Ne,X_Mg
 37   format('% O/H=',E8.2,',  Ne/H=',E8.2,', Mg/H=',E8.2)
      write (14,38) X_Si,X_S,X_Fe
 38   format('% Si/H=',E8.2,', S/H=',E8.2,',  Fe/H=',E8.2)
      write (14,39) H_tol, He_tol, met_tol
 39   format('% Accuracies: H: ',E9.3,', He: ',E9.3,', metals: 'E9.3)
      write (14,40) D
 40   format('% D=',E9.3,' [kpc].')
      write (14,32) spec_filename
      write (14,34) initial_filename
 32   format ('% name of radiation field file: ',a20)
 33   format ('% name of temperature profile file: ',a20)
 34   format ('% name of initial conditions file: ',a20)
      write (14, *) '% '
      write (14, *) '% t, T, x(HI), x(HeI), x(HeII), x(CI), x(CII),',
     +      '% x(CIII), x(CIV), x(CV), X(CVI), x(NI), x(NII), x(NIII),',
     +      '% x(NIV), x(NV), X(NVI), x(NVII), x(OI), x(OII), x(OIII),',
     +      '% x(OIV), x(OV), x(OVI), x(OVII), x(OVIII), x(NeI),',
     +      '% x(NeII), x(NeIII), x(NeIV), xNe(V), xNe(VI), x(NeVII),',
     +      '% x(NeVII), x(NeIX), x(NeX), x(MgI), x(MgII), x(MgIII),',
     +      '% x(MgIV), x(MgV), x(MgVI), x(MgVII), x(MgVIII), x(MgIX),',
     +      '% x(MgX), x(MgXI), x(MgXII), x(SiI), x(SiII)',
     +      '% x(SiIII), x(SiIV), x(SiV), x(SiVI), x(SiVII),',
     +      '% x(SiVIII), x(SiIX), x(SiX), x(SiXI), x(SiXII),',
     +      '% x(SiXIII), x(SiXIV), x(SI), x(SII), x(SIII), x(SIV),',
     +      '% x(SV), x(SVI), x(SVII), x(SVIII), x(SIX), x(SX),',
     +      '% x(SXI), x(SXII), x(SXIII), x(SXIV), x(SXV), x(SXVI),',
     +      '% x(FeI), x(FeII), x(FeIII), x(FeIV), x(FeV), x(FeVI),',
     +      '% x(FeVII), x(FeVIII), x(FeIX), x(FeX), x(FeXI),',
     +      '% x(FeXII), x(FeXIII), x(FeXIV), x(FeXV), x(FeXVI),',
     +      '% x(FeXVII), x(FeXVIII), x(FeXIX), x(FeXX), x(FeXXI),',
     +      '% x(FeXXII), x(FeXXIII), x(FeXXIV), x(FeXXV), x(FeXXVI)'

      do j=1,Num
         if (10*x(j).lt.atol(j)) then
             xp(j) = 0
         else
             xp(j) = x(j)
         endif
      enddo

c - print initial conditions
         write(14,'(2(E16.10,2X),102(1PE13.4,2X))') 
     +            0, T_HIM,  xp(1),  xp(2)/X_He,  xp(3)/X_He,
     +            xp(4)/X_C ,  xp(5)/X_C,   xp(6)/X_C,   xp(7)/X_C,
     +            xp(8)/X_C,   xp(9)/X_C, 
     +            xp(10)/X_N,  xp(11)/X_N,  xp(12)/X_N,  xp(13)/X_N, 
     +            xp(14)/X_N,  xp(15)/X_N,  xp(16)/X_N, 
     +            xp(17)/X_O,  xp(18)/X_O,  xp(19)/X_O,  xp(20)/X_O, 
     +            xp(21)/X_O,  xp(22)/X_O,  xp(23)/X_O,  xp(24)/X_O, 
     +            xp(55)/X_Ne, xp(56)/X_Ne, xp(57)/X_Ne, xp(58)/X_Ne,
     +            xp(59)/X_Ne, xp(60)/X_Ne, xp(61)/X_Ne, xp(62)/X_Ne,
     +            xp(63)/X_Ne, xp(64)/X_Ne,
     +            xp(91)/X_Mg, xp(92)/X_Mg, xp(93)/X_Mg, xp(94)/X_Mg, 
     +            xp(95)/X_Mg, xp(96)/X_Mg, xp(97)/X_Mg, xp(98)/X_Mg,
     +            xp(99)/X_Mg, xp(100)/X_Mg, xp(101)/X_Mg, xp(102)/X_Mg,
     +            xp(25)/X_Si, xp(26)/X_Si, xp(27)/X_Si, xp(28)/X_Si, 
     +            xp(29)/X_Si, xp(30)/X_Si, xp(31)/X_Si, xp(32)/X_Si,
     +            xp(33)/X_Si, xp(34)/X_Si, xp(35)/X_Si, xp(36)/X_Si,
     +            xp(37)/X_Si, xp(38)/X_Si,
     +            xp(39)/X_S , xp(40)/X_S , xp(41)/X_S,  xp(42)/X_S, 
     +            xp(43)/X_S,  xp(44)/X_S,  xp(45)/X_S,  xp(46)/X_S,
     +            xp(47)/X_S,  xp(48)/X_S,  xp(49)/X_S,  xp(50)/X_S,
     +            xp(51)/X_S,  xp(52)/X_S,  xp(53)/X_S,  xp(54)/X_S,
     +            xp(65)/X_Fe, xp(66)/X_Fe, xp(67)/X_Fe, xp(68)/X_Fe,
     +            xp(69)/X_Fe, xp(70)/X_Fe, xp(71)/X_Fe, xp(72)/X_Fe,
     +            xp(73)/X_Fe, xp(74)/X_Fe, xp(75)/X_Fe, xp(76)/X_Fe,
     +            xp(77)/X_Fe, xp(78)/X_Fe, xp(79)/X_Fe, xp(80)/X_Fe,
     +            xp(81)/X_Fe, xp(82)/X_Fe, xp(83)/X_Fe, xp(84)/X_Fe,
     +            xp(85)/X_Fe, xp(86)/X_Fe, xp(87)/X_Fe, xp(88)/X_Fe,
     +            xp(89)/X_Fe, xp(90)/X_Fe

c  =========================================================================
c  =========================================================================


      currentT = dtime(TIMEARRAY) !currentT is initialized below, here just 
                                  !need to put dtime in something...
      print *, ' '
      print *, ' '
      print *, ' '
      print *, 'Starting to integrate... (this may take a while)'

      cmd_rm = 'rm cloudy943infile'
      run_cloudy=
     +'/home/orlyg/research/c07_02_00/osource/o.exe<cloudy943infile>res'
c      run_cloudy ='~/hvc/c0602/source/cloudy.exe <cloudy943infile> res'

c --- prepare Cloudy input file:
      call Dcloudy_input(nH,T_HIM,x,Num,H_tol,He_tol,met_tol)
c --- run cloudy:
      call system(run_cloudy)

c --- read cooling rate from cloudy output:
      cooling = net_cooling()
      call system(cmd_rm)

      currentT = T_HIM   ! --- now initialize currentT ---

      PoverK = nH * (x(H_I) + 2*(1-x(H_I)) + x(He_I) + 2*x(He_II) + 
     +                  3*(X_He-x(He_I)-x(He_II))) * T_HIM

c=========================================================================
c --- start loop:
      do while ((currentT > Tlow).and.(cooling.gt.0))
      !  stop loop if got to T_low, or is stopped cooling

         ntot =  nH * (x(H_I) + 2*(1-x(H_I)) + x(He_I) + 2*x(He_II) + 
     +                  3*(X_He-x(He_I)-x(He_II)))


         D_time = time_step(PoverK,epsl,cooling)


c ---    print run time data to screen:
         print 41, dtime(TIMEARRAY)/60.0 , etime(TIMEARRAY)/60.0
 41      format('This loop was ',E9.3,' min. Total: ',E9.3,' min.')

 
         old_time = time
         new_time = time+D_time


c ---   Which case ??
c        choose: H caseA/B (at 13.6eV), He caseA/B (actualy H+He at 24.6eV)
   
         tau13_6 = 6.34e-18 * x(H_I) * nH * D
         tau24_6 = (1.24e-18 * x(H_I) + 7.43e-18 * x(He_I))* nH * D
   
         if ((tau13_6.lt.1.).and.(tau24_6.lt.1.)) then
          H_HE_case = HcaseA_HecaseA
c                     meaning: thin  to ~13.6 eV photons,
c                              thin  to ~24.6 eV photons.
          print *,'   H case A,    He case A'
         elseif ((tau13_6.ge.1.).and.(tau24_6.lt.1.)) then
          H_HE_case = HcaseB_HecaseA 
c                     meaning: thick to ~13.6 eV photons,
c                              thin  to ~24.6 eV photons.
          print *,'   H case B,    He case A'
         elseif ((tau13_6.lt.1.).and.(tau24_6.ge.1.)) then
          H_HE_case = HcaseA_HecaseB
c                     meaning: thin  to ~13.6 eV photons,
c                              thick to ~24.6 eV photons.
          print *,'   H case A,    He case B'
         else
          H_HE_case = HcaseB_HecaseB
c                     meaning: thick to ~13.6 eV photons,
c                              thick to ~24.6 eV photons.
          print *,'   H case B,    He case B'
         endif

c        choose: HeIII caseA/B (at 54.4V)
         tau54_4 =  1.59E-018 * x(He_II) * nH * D
c        sigma(HeII,54.4eV) = 1.59e-18
         if (tau54_4.lt.1) then
            HeIIcase = HeIIcaseA
            print *,'   HeII case A'
         else
            HeIIcase = HeIIcaseB
            print *,'   HeII case B'
         endif


c ---    call integrator - to work out current time step:
         call DLSODA(FCN, Num, x, old_time, new_time, itol, rtol, atol,
     +               itask, istate, iopt, rwork, lrw, iwork, liw, JFCN,
     +               jt)

c ---    advance pressure by step = epsl*P
         PoverK = (1.0 - epsl) * PoverK
         time = new_time !new_time is output from integrator
c ---    calculate T at new pressure:
         currentT = PoverK / (nH * (x(H_I) + 2*(1-x(H_I)) + x(He_I) + 
     +              2*x(He_II) + 3*(X_He-x(He_I)-x(He_II))))

         call system(cmd_rm)
c ---    make cloudy input file, run cloudy, and get current cooling:
         call Dcloudy_input(nH,currentT,x,Num,H_tol,He_tol,met_tol)
         call system(run_cloudy)
         cooling = net_cooling()

c ---    xp(j) are values to print to file. only print significant numbers
c        (that are > 0.1 x Accuracy)
         do j=1,Num
            if (10*x(j).lt.atol(j)) then
                xp(j) = 0
            else
                xp(j) = x(j)
            endif
         enddo

c ---    print new abundances into file:
         write(14,'(2(E16.10,2X),102(1PE13.4,2X))') 
     +            time, currentT,  xp(1),  xp(2)/X_He,  xp(3)/X_He,
     +            xp(4)/X_C ,  xp(5)/X_C,   xp(6)/X_C,   xp(7)/X_C,
     +            xp(8)/X_C,   xp(9)/X_C, 
     +            xp(10)/X_N,  xp(11)/X_N,  xp(12)/X_N,  xp(13)/X_N, 
     +            xp(14)/X_N,  xp(15)/X_N,  xp(16)/X_N, 
     +            xp(17)/X_O,  xp(18)/X_O,  xp(19)/X_O,  xp(20)/X_O, 
     +            xp(21)/X_O,  xp(22)/X_O,  xp(23)/X_O,  xp(24)/X_O, 
     +            xp(55)/X_Ne, xp(56)/X_Ne, xp(57)/X_Ne, xp(58)/X_Ne,
     +            xp(59)/X_Ne, xp(60)/X_Ne, xp(61)/X_Ne, xp(62)/X_Ne,
     +            xp(63)/X_Ne, xp(64)/X_Ne,
     +            xp(91)/X_Mg, xp(92)/X_Mg, xp(93)/X_Mg, xp(94)/X_Mg, 
     +            xp(95)/X_Mg, xp(96)/X_Mg, xp(97)/X_Mg, xp(98)/X_Mg,
     +            xp(99)/X_Mg, xp(100)/X_Mg, xp(101)/X_Mg, xp(102)/X_Mg,
     +            xp(25)/X_Si, xp(26)/X_Si, xp(27)/X_Si, xp(28)/X_Si, 
     +            xp(29)/X_Si, xp(30)/X_Si, xp(31)/X_Si, xp(32)/X_Si,
     +            xp(33)/X_Si, xp(34)/X_Si, xp(35)/X_Si, xp(36)/X_Si,
     +            xp(37)/X_Si, xp(38)/X_Si,
     +            xp(39)/X_S , xp(40)/X_S , xp(41)/X_S,  xp(42)/X_S, 
     +            xp(43)/X_S,  xp(44)/X_S,  xp(45)/X_S,  xp(46)/X_S,
     +            xp(47)/X_S,  xp(48)/X_S,  xp(49)/X_S,  xp(50)/X_S,
     +            xp(51)/X_S,  xp(52)/X_S,  xp(53)/X_S,  xp(54)/X_S,
     +            xp(65)/X_Fe, xp(66)/X_Fe, xp(67)/X_Fe, xp(68)/X_Fe,
     +            xp(69)/X_Fe, xp(70)/X_Fe, xp(71)/X_Fe, xp(72)/X_Fe,
     +            xp(73)/X_Fe, xp(74)/X_Fe, xp(75)/X_Fe, xp(76)/X_Fe,
     +            xp(77)/X_Fe, xp(78)/X_Fe, xp(79)/X_Fe, xp(80)/X_Fe,
     +            xp(81)/X_Fe, xp(82)/X_Fe, xp(83)/X_Fe, xp(84)/X_Fe,
     +            xp(85)/X_Fe, xp(86)/X_Fe, xp(87)/X_Fe, xp(88)/X_Fe,
     +            xp(89)/X_Fe, xp(90)/X_Fe


      enddo   
c --- end loop
c=========================================================================

c --- if didnt get to T_low, say so:
      if (cooling.le.0) then
         print *,'Stopped because heating > cooling @ T=',currentT,'K.' 
      endif

 998  print *, ' '
      print *, ' '
      print *, 'Done. Results are in: ',results_filename
      print *, '      Cooling in: major_coolants.'

      print *, '======================================================='
      print *, ' '
      print *, ' '
      close(14)

      goto 999  !skip error messeges.



c  =======================================================================
c  =======================================================================

c --- Errors:
c     =======
 17   print *,'Unable to open file.'
      stop
 91   print *,'Error occured while reading spectrum file...', 
     +        '(too many lines?)',
     +        ' Terminating.'
      stop
 93   print *,'Not enough initial values in initial values file...',
     +        ' Terminating.'
      stop
 94   print *,'Error occured while reading initial values file...',
     +        ' Terminating.'

c  =======================================================================
c  =======================================================================

 999  end
