c234567
      function he_ct_r(nz,state,T)
      implicit none
c state: as defined in FNC.f: electron deficiency relative to neutral state.
c for example, for CIV: ct_rec(6,3,1e4)

c Based on:
c Fits to the rates of charge transfer reactions.
c M. Arnaud & R. Rothenflug (1985, A&AS, 60, 425).
c http://www.pa.uky.edu/~verner/chtr.html
 
      double precision CTr, T
      integer nz, state, state1
      double precision he_ct_r

      he_ct_r = 0
      state1 = state + 1 
c     if not neutral and with possible num of electrons:
      if ((state1.gt.1).and.(state1.le.nz+1)) then
c        call the Kingdon and Ferland routine:
!        if (state1.le.4) then   !kartick **
            call He_CTR(state1,nz,T,CTr)
            he_ct_r = CTr
!        endif
      endif

      return
      end

*     ** In the old code sent by orly, this condition was included. This means that
*      the CT with HI for CV and onwwards is neglected. The same modification is
*      also done in ct_rec.f. But in the new version of the code sent by Orly, this
*      condition is not present.
* 

*==================================================================
      subroutine He_CTR(ion,nelem,te,CTr)
*     ion is stage of ionization, 2 for the ion going to the atom
*     nelem is atomic number of element, 2 up to 30
*     Example:  O+ + He => O + He+ is He_CTR(2,8)
      integer ion , nelem
      double precision te, CTr
      common/HeCTR/ HECTR(6,4,30)
*
*     local variables
      double precision tused 
      integer ipIon
*
      ipIon = ion - 1
*
c     ipIon = ion
      if( ipIon.gt.4 ) then 
*       use statistical charge transfer for ion > 4
        CTr = 1.92e-9 * ipIon                ! kartick
        return
      endif
*
*     Make sure te is between temp. boundaries; set constant outside of range
      tused = max( te,HeCTR(5,ipIon,nelem) )
      tused = min( tused , HeCTR(6,ipIon,nelem) )
      tused = tused * 1.0e-4
*
*     the interpolation equation
      CTr = HeCTR(1,ipIon,nelem)* 1.0e-9 * 
     1 (tused**HeCTR(2,ipIon,nelem)) *
     2 (1. + 
     3 HeCTR(3,ipIon,nelem) * exp(HeCTR(4,ipIon,nelem)*tused) )
*
      end
********************************************************************************

      block data ctdata4
      double precision HeCTR
      common/HeCTR/ HECTR(6,4,30)
      integer i
      data (HeCTR(i,1,2),i=1,6)/6*0./
      data (HeCTR(i,2,2),i=1,6)/6*0./
      data (HeCTR(i,1,3),i=1,6)/6*0./
      data (HeCTR(i,2,3),i=1,6)/6*0./
      data (HeCTR(i,3,3),i=1,6)/6*0./
      data (HeCTR(i,1,4),i=1,6)/6*0./
      data (HeCTR(i,2,4),i=1,6)/6*0./
      data (HeCTR(i,3,4),i=1,6)/6*0./
      data (HeCTR(i,4,4),i=1,6)/6*0./
      data (HeCTR(i,1,5),i=1,6)/6*0./
      data (HeCTR(i,2,5),i=1,6)/6*0./
      data (HeCTR(i,3,5),i=1,6)/6*0./
      data (HeCTR(i,4,5),i=1,6)/6*0./
      data (HeCTR(i,1,6),i=1,6)/6*0./
      data (HeCTR(i,2,6),i=1,6)/1.00E-01,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+04,1.00E+04/
      data (HeCTR(i,3,6),i=1,6)/4.60E-02,2.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,6),i=1,6)/1.00E-05,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,7),i=1,6)/6*0./
      data (HeCTR(i,2,7),i=1,6)/3.30E-01,2.90E-01,1.30E+00,-4.50E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,3,7),i=1,6)/1.50E-01,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,7),i=1,6)/1.70E+00,0.00E+00,2.50E+00,-3.70E+01,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,8),i=1,6)/6*0./
      data (HeCTR(i,2,8),i=1,6)/2.00E-01,9.50E-01,0.00E+00,0.00E+00,
     + 5.00E+03,5.00E+04/
      data (HeCTR(i,3,8),i=1,6)/1.00E+00,0.00E+00,1.25E+00,-5.80E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,8),i=1,6)/6.40E-01,0.00E+00,2.00E+00,-5.50E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,9),i=1,6)/6*0./
      data (HeCTR(i,2,9),i=1,6)/6*0./
      data (HeCTR(i,3,9),i=1,6)/6*0./
      data (HeCTR(i,4,9),i=1,6)/6*0./
      data (HeCTR(i,1,10),i=1,6)/6*0./
      data (HeCTR(i,2,10),i=1,6)/1.00E-05,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,3,10),i=1,6)/1.00E-05,5.10E-01,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,10),i=1,6)/1.70E+00,5.20E-01,3.30E+00,-5.30E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,11),i=1,6)/6*0./
      data (HeCTR(i,2,11),i=1,6)/6*0./
      data (HeCTR(i,3,11),i=1,6)/6*0./
      data (HeCTR(i,4,11),i=1,6)/6*0./
      data (HeCTR(i,1,12),i=1,6)/6*0./
      data (HeCTR(i,2,12),i=1,6)/6*0./
      data (HeCTR(i,3,12),i=1,6)/7.50E-01,0.00E+00,1.25E+00,-5.80E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,12),i=1,6)/2.20E+00,3.30E-01,8.80E-01,-1.85E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,13),i=1,6)/6*0./
      data (HeCTR(i,2,13),i=1,6)/6*0./
      data (HeCTR(i,3,13),i=1,6)/6*0./
      data (HeCTR(i,4,13),i=1,6)/6*0./
      data (HeCTR(i,1,14),i=1,6)/6*0./
      data (HeCTR(i,2,14),i=1,6)/6*0./
      data (HeCTR(i,3,14),i=1,6)/9.50E-01,7.50E-01,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,14),i=1,6)/1.20E+00,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,15),i=1,6)/6*0./
      data (HeCTR(i,2,15),i=1,6)/6*0./
      data (HeCTR(i,3,15),i=1,6)/6*0./
      data (HeCTR(i,4,15),i=1,6)/6*0./
      data (HeCTR(i,1,16),i=1,6)/6*0./
      data (HeCTR(i,2,16),i=1,6)/6*0./
      data (HeCTR(i,3,16),i=1,6)/1.10E+00,5.60E-01,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,16),i=1,6)/7.60E-04,3.20E-01,3.40E+00,-5.25E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,17),i=1,6)/6*0./
      data (HeCTR(i,2,17),i=1,6)/6*0./
      data (HeCTR(i,3,17),i=1,6)/6*0./
      data (HeCTR(i,4,17),i=1,6)/6*0./
      data (HeCTR(i,1,18),i=1,6)/6*0./
      data (HeCTR(i,2,18),i=1,6)/1.30E-01,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,3,18),i=1,6)/1.00E-05,0.00E+00,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,4,18),i=1,6)/1.00E+00,3.00E-01,0.00E+00,0.00E+00,
     + 1.00E+03,3.00E+04/
      data (HeCTR(i,1,19),i=1,6)/6*0./
      data (HeCTR(i,2,19),i=1,6)/6*0./
      data (HeCTR(i,3,19),i=1,6)/6*0./
      data (HeCTR(i,4,19),i=1,6)/6*0./
      data (HeCTR(i,1,20),i=1,6)/6*0./
      data (HeCTR(i,2,20),i=1,6)/6*0./
      data (HeCTR(i,3,20),i=1,6)/6*0./
      data (HeCTR(i,4,20),i=1,6)/6*0./
      data (HeCTR(i,1,21),i=1,6)/6*0./
      data (HeCTR(i,2,21),i=1,6)/6*0./
      data (HeCTR(i,3,21),i=1,6)/6*0./
      data (HeCTR(i,4,21),i=1,6)/6*0./
      data (HeCTR(i,1,22),i=1,6)/6*0./
      data (HeCTR(i,2,22),i=1,6)/6*0./
      data (HeCTR(i,3,22),i=1,6)/6*0./
      data (HeCTR(i,4,22),i=1,6)/6*0./
      data (HeCTR(i,1,23),i=1,6)/6*0./
      data (HeCTR(i,2,23),i=1,6)/6*0./
      data (HeCTR(i,3,23),i=1,6)/6*0./
      data (HeCTR(i,4,23),i=1,6)/6*0./
      data (HeCTR(i,1,24),i=1,6)/6*0./
      data (HeCTR(i,2,24),i=1,6)/6*0./
      data (HeCTR(i,3,24),i=1,6)/6*0./
      data (HeCTR(i,4,24),i=1,6)/6*0./
      data (HeCTR(i,1,25),i=1,6)/6*0./
      data (HeCTR(i,2,25),i=1,6)/6*0./
      data (HeCTR(i,3,25),i=1,6)/6*0./
      data (HeCTR(i,4,25),i=1,6)/6*0./
      data (HeCTR(i,1,26),i=1,6)/6*0./
      data (HeCTR(i,2,26),i=1,6)/6*0./
      data (HeCTR(i,3,26),i=1,6)/6*0./
      data (HeCTR(i,4,26),i=1,6)/6*0./
      data (HeCTR(i,1,27),i=1,6)/6*0./
      data (HeCTR(i,2,27),i=1,6)/6*0./
      data (HeCTR(i,3,27),i=1,6)/6*0./
      data (HeCTR(i,4,27),i=1,6)/6*0./
      data (HeCTR(i,1,28),i=1,6)/6*0./
      data (HeCTR(i,2,28),i=1,6)/6*0./
      data (HeCTR(i,3,28),i=1,6)/6*0./
      data (HeCTR(i,4,28),i=1,6)/6*0./
      data (HeCTR(i,1,29),i=1,6)/6*0./
      data (HeCTR(i,2,29),i=1,6)/6*0./
      data (HeCTR(i,3,29),i=1,6)/6*0./
      data (HeCTR(i,4,29),i=1,6)/6*0./
      data (HeCTR(i,1,30),i=1,6)/6*0./
      data (HeCTR(i,2,30),i=1,6)/6*0./
      data (HeCTR(i,3,30),i=1,6)/6*0./
      data (HeCTR(i,4,30),i=1,6)/6*0./
      end
