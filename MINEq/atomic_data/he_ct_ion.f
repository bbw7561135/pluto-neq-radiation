      function he_ct_i(nz,state,T)
      implicit none

c state: as defined in FNC.f: electron deficiency relative to neutral state.
c for example, for CIV: ctrec(6,3,1e4)
 
c Based on:
c Fits to the rates of charge transfer reactions.
c M. Arnaud & R. Rothenflug (1985, A&AS, 60, 425).
c http://www.pa.uky.edu/~verner/chtr.html
 
      double precision CTi, T
      integer nz,state,state1
      double precision he_ct_i

      state1 = state+1
      he_ct_i = 0
c     if not fully ionized and with possible num of electrons:
      if ((state1.gt.0).and.(state1.le.nz)) then
c        call the Kingdon and Ferland routine:
         if (state1.lt.4) then   !in the CTIon database... 
            call  He_CTI(state1,nz,T,CTi)
            he_ct_i = CTi
         endif
      endif
 
      return
      end

******************************************************************************
      subroutine He_CTI(ion,nelem,te,CTi)
*     ion is stage of ionization, 1 for atom
*     nelem is atomic number of element, 2 up to 30
*     Example:  O + He+ => O+ + He is He_CTI(1,8)
      double precision te, CTi
      integer ion , nelem
      common/HeCTI/ HECTI(6,4,30)
*
*     local variables
      double precision tused 
      integer ipIon
*
      ipIon = ion
*
*     Make sure te is between temp. boundaries; set constant outside of range
      tused = max( te,HeCTI(5,ipIon,nelem) )
      tused = min( tused , HeCTI(6,ipIon,nelem) )
      tused = tused * 1e-4
*
*     the interpolation equation
      CTi = HeCTI(1,ipIon,nelem)* 1e-9 * 
     1 (tused**HeCTI(2,ipIon,nelem)) *
     2 (exp(-HeCTI(3,ipIon,nelem)*tused)) *
     3 exp(-HeCTI(4,ipIon,nelem)/(8.61328e-1*tused))
* 8.61328e-1 = k_B * 1e4 * 1erg->eV
*              (1e4 to get T_4 back to real T)
*              HeCTI(4,...) is in eV
      if (tused.eq.0) then
         CTi = 0
      endif
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      block data ctdata3
      double precision HeCTI
      common/HeCTI/ HeCTI(6,4,30)
      integer i
      data (HeCTI(i,1,3),i=1,6)/6*0./
      data (HeCTI(i,2,3),i=1,6)/6*0./
      data (HeCTI(i,3,3),i=1,6)/6*0./
      data (HeCTI(i,1,4),i=1,6)/6*0./
      data (HeCTI(i,2,4),i=1,6)/6*0./
      data (HeCTI(i,3,4),i=1,6)/6*0./
      data (HeCTI(i,1,5),i=1,6)/6*0./
      data (HeCTI(i,2,5),i=1,6)/6*0./
      data (HeCTI(i,3,5),i=1,6)/6*0./
      data (HeCTI(i,1,6),i=1,6)/6*0./
      data (HeCTI(i,2,6),i=1,6)/5.00E-03,2.00E+00,7.00E-02,6.29E+00,
     + 5.00E+03,5.00E+04/ 
      data (HeCTI(i,3,6),i=1,6)/6*0./
      data (HeCTI(i,1,7),i=1,6)/6*0./
      data (HeCTI(i,2,7),i=1,6)/3.70E-03,2.10E+00,6.30E-02,1.44E+00,
     + 1.00E+04,3.00E+05/
      data (HeCTI(i,3,7),i=1,6)/6*0./
      data (HeCTI(i,1,8),i=1,6)/6*0./
      data (HeCTI(i,2,8),i=1,6)/6*0./
      data (HeCTI(i,3,8),i=1,6)/6*0./
      data (HeCTI(i,1,9),i=1,6)/6*0./
      data (HeCTI(i,2,9),i=1,6)/6*0./
      data (HeCTI(i,3,9),i=1,6)/6*0./
      data (HeCTI(i,1,10),i=1,6)/6*0./
      data (HeCTI(i,2,10),i=1,6)/6*0./
      data (HeCTI(i,3,10),i=1,6)/6*0./
      data (HeCTI(i,1,11),i=1,6)/6*0./
      data (HeCTI(i,2,11),i=1,6)/6*0./
      data (HeCTI(i,3,11),i=1,6)/6*0./
      data (HeCTI(i,1,12),i=1,6)/6*0./
      data (HeCTI(i,2,12),i=1,6)/6*0./
      data (HeCTI(i,3,12),i=1,6)/6*0./
      data (HeCTI(i,1,13),i=1,6)/6*0./
      data (HeCTI(i,2,13),i=1,6)/6*0./
      data (HeCTI(i,3,13),i=1,6)/6*0./
      data (HeCTI(i,1,14),i=1,6)/6*0./
      data (HeCTI(i,2,14),i=1,6)/1.50E-01,2.40E-01,0.00E+00,6.91E+00,
     + 1.00E+04,3.00E+05/
      data (HeCTI(i,3,14),i=1,6)/1.15E+00,4.40E-01,0.00E+00,8.88E+00,
     + 1.00E+04,3.00E+05/
      data (HeCTI(i,1,15),i=1,6)/6*0./
      data (HeCTI(i,2,15),i=1,6)/6*0./
      data (HeCTI(i,3,15),i=1,6)/6*0./
      data (HeCTI(i,1,16),i=1,6)/6*0./
      data (HeCTI(i,2,16),i=1,6)/2.80E-02,1.20E+00,3.60E-02,9.20E+00,
     + 1.00E+04,3.00E+05/
      data (HeCTI(i,3,16),i=1,6)/1.40E-02,1.60E+00,4.60E-02,1.05E+01,
     + 1.00E+04,3.00E+05/
      data (HeCTI(i,1,17),i=1,6)/6*0./
      data (HeCTI(i,2,17),i=1,6)/6*0./
      data (HeCTI(i,3,17),i=1,6)/6*0./
      data (HeCTI(i,1,18),i=1,6)/6*0./
      data (HeCTI(i,2,18),i=1,6)/1.10E-01,0.00E+00,0.00E+00,3.04E+00,
     + 1.00E+04,3.00E+05/
      data (HeCTI(i,3,18),i=1,6)/6*0./
      data (HeCTI(i,1,19),i=1,6)/6*0./
      data (HeCTI(i,2,19),i=1,6)/6*0./
      data (HeCTI(i,3,19),i=1,6)/6*0./
      data (HeCTI(i,1,20),i=1,6)/6*0./
      data (HeCTI(i,2,20),i=1,6)/6*0./
      data (HeCTI(i,3,20),i=1,6)/6*0./
      data (HeCTI(i,1,21),i=1,6)/6*0./
      data (HeCTI(i,2,21),i=1,6)/6*0./
      data (HeCTI(i,3,21),i=1,6)/6*0./
      data (HeCTI(i,1,22),i=1,6)/6*0./
      data (HeCTI(i,2,22),i=1,6)/6*0./
      data (HeCTI(i,3,22),i=1,6)/6*0./
      data (HeCTI(i,1,23),i=1,6)/6*0./
      data (HeCTI(i,2,23),i=1,6)/6*0./
      data (HeCTI(i,3,23),i=1,6)/6*0./
      data (HeCTI(i,1,24),i=1,6)/6*0./
      data (HeCTI(i,2,24),i=1,6)/6*0./
      data (HeCTI(i,3,24),i=1,6)/6*0./
      data (HeCTI(i,1,25),i=1,6)/6*0./
      data (HeCTI(i,2,25),i=1,6)/6*0./
      data (HeCTI(i,3,25),i=1,6)/6*0./
      data (HeCTI(i,1,26),i=1,6)/6*0./
      data (HeCTI(i,2,26),i=1,6)/6*0./
      data (HeCTI(i,3,26),i=1,6)/6*0./
      data (HeCTI(i,1,27),i=1,6)/6*0./
      data (HeCTI(i,2,27),i=1,6)/6*0./
      data (HeCTI(i,3,27),i=1,6)/6*0./
      data (HeCTI(i,1,28),i=1,6)/6*0./
      data (HeCTI(i,2,28),i=1,6)/6*0./
      data (HeCTI(i,3,28),i=1,6)/6*0./
      data (HeCTI(i,1,29),i=1,6)/6*0./
      data (HeCTI(i,2,29),i=1,6)/6*0./
      data (HeCTI(i,3,29),i=1,6)/6*0./
      data (HeCTI(i,1,30),i=1,6)/6*0./
      data (HeCTI(i,2,30),i=1,6)/6*0./
      data (HeCTI(i,3,30),i=1,6)/6*0./
      end
