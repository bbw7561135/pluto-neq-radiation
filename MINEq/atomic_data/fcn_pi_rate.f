c =============== new subroutine to deliver the pi-rate =======

	subroutine get_pi_rate(el_id, ion_id, prate, sdata)
	implicit none
c	define universal constants
	double precision k_B, kpc2cm
	double precision   m_H        ! proton mass (gr)

c	declare functions/subroutines to be used
	external colion, ct_rec, ct_ion, allrec
	double precision colion, ct_rec, ct_ion
	external he_ct_r, he_ct_i
	double precision he_ct_r, he_ct_i
	external He_alpha1, He_alphaB, H_alphaB,HeII_alphaB
	double precision He_alpha1, He_alphaB, H_alphaB,HeII_alphaB
	external recfer1
	double precision recfer1
	external Auger_pop_rate, Gamma
	double precision Auger_pop_rate, Gamma
	double precision allrec_mat(1:30, 0:30)

c	declare common variables/arrays
	COMMON/RECB/re(300),unused(300),unused2(300)
	double precision re, unused, unused2
	common/spectrum/nu(10000),Jnu(10000),VecSize
	double precision nu, Jnu
	integer Vecsize
        common/allphot_mat/allphot_vec(1:30,0:30,0:7)      
	double precision allphot_vec

	common/model/PoverK,T_HIM,nH,currentT,old_time,new_time,epsl,D,
     + H_HE_case,HeIIcase
	double precision PoverK   ! pressure in units of P/k_B (cm^-3 K).
	double precision T_HIM    ! initial temperature.
	double precision nH       ! (cm^-3) H den 
	double precision currentT ! present temperature
	double precision epsl
	double precision old_time
	double precision new_time
	double precision D        ! cloud size
	integer H_HE_case,HeIIcase

	common/Gas/X_He, X_C, X_N, X_O, X_Si, X_S, X_Ne, X_Fe, X_Mg
	double precision X_He, X_C, X_N, X_O, X_Si, X_S !abundances relative to H
	double precision X_Ne, X_Fe, X_Mg

c	variables 
	double precision xHII, xHeIII, xCVII, xNVIII, xOIX, xSiXV, xSXVII
	double precision xNeXI, xFeXXVII, xMgXIII
	integer H_ , He_, C_, N_, O_, Ne_, Mg_, Si_, S_, Fe_
	integer I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII
	integer XIV,XV,XVI,XVII
	integer XVIII,XIX,XX,XXI,XXII,XXIII,XXIV,XXV,XXVI,XXVII

	integer H_I
	integer He_I, He_II
	integer C_I, C_II, C_III, C_IV, C_V, C_VI
	integer N_I, N_II, N_III, N_IV, N_V, N_VI, N_VII
	integer O_I, O_II, O_III, O_IV, O_V, O_VI, O_VII, O_VIII
	integer Ne_I, Ne_II, Ne_III, Ne_IV, Ne_V, Ne_VI, Ne_VII, Ne_VIII
	integer Ne_IX, Ne_X
	integer Mg_I, Mg_II, Mg_III, Mg_IV, Mg_V, Mg_VI, Mg_VII, Mg_VIII
	integer Mg_IX, Mg_X, Mg_XI, Mg_XII
	integer Si_I,Si_II,Si_III,Si_IV,Si_V,Si_VI,Si_VII,Si_VIII,Si_IX
	integer Si_X,Si_XI,Si_XII,Si_XIII,Si_XIV
	integer S_I, S_II, S_III, S_IV, S_V, S_VI, S_VII, S_VIII, S_IX
	integer S_X, S_XI, S_XII, S_XIII, S_XIV, S_XV, S_XVI
	integer Fe_I, Fe_II, Fe_III, Fe_IV, Fe_V, Fe_VI, Fe_VII, Fe_VIII
	integer Fe_IX, Fe_X, Fe_XI, Fe_XII, Fe_XIII, Fe_XIV, Fe_XV, Fe_XVI
	integer Fe_XVII, Fe_XVIII, Fe_XIX, Fe_XX, Fe_XXI, Fe_XXII
	integer Fe_XXIII, Fe_XXIV, Fe_XXV, Fe_XXVI

	double precision temp, e_den, prate, dnuP
        integer el_id, ion_id, Nnu
c	integer arr
c	parameter(Nnu=1000)
	double precision sdata(20001)

c	initialise variables
	parameter (H_=1,He_=2,C_=6,N_=7,O_=8,Ne_=10,Mg_=12,Si_=14)
	parameter (S_=16,Fe_=26)
	parameter (I=0,II=1,III=2,IV=3,V=4,VI=5,VII=6,VIII=7)
	parameter (IX=8,X=9,XI=10,XII=11,XIII=12,XIV=13,XV=14)
	parameter (XVI=15,XVII=16)
	parameter (XVIII=17,XIX=18,XX=19,XXI=20,XXII=21,XXIII=22)
	parameter (XXIV=23,XXV=24,XXVI=25,XXVII=26)

c --- define meaningfull names for indices in vector n:
	parameter (H_I= 1)
	parameter (He_I= 2, He_II= 3)
	parameter (C_I=4 , C_II= 5, C_III= 6, C_IV= 7, C_V= 8, C_VI= 9)
	parameter (N_I= 10, N_II= 11, N_III= 12, N_IV= 13, N_V= 14)
	parameter (N_VI= 15, N_VII= 16)
	parameter (O_I= 17, O_II= 18, O_III= 19, O_IV= 20, O_V= 21)
	parameter (O_VI= 22, O_VII= 23, O_VIII= 24)
	parameter (Si_I= 25, Si_II= 26, Si_III= 27, Si_IV= 28, Si_V= 29)
	parameter (Si_VI= 30, Si_VII= 31, Si_VIII= 32, Si_IX= 33)
	parameter (Si_X= 34, Si_XI= 35, Si_XII= 36, Si_XIII= 37)
	parameter (Si_XIV= 38)
	parameter (S_I= 39, S_II= 40, S_III= 41, S_IV= 42, S_V= 43)
	parameter (S_VI= 44)
	parameter (S_VII= 45, S_VIII= 46, S_IX= 47, S_X= 48, S_XI= 49)
	parameter (S_XII= 50, S_XIII= 51, S_XIV= 52, S_XV= 53, S_XVI= 54)
	parameter (Ne_I=55,Ne_II=56,Ne_III=57,Ne_IV=58,Ne_V=59,Ne_VI=60)
	parameter (Ne_VII=61,Ne_VIII=62,Ne_IX=63,Ne_X=64)
	parameter (Fe_I=65,Fe_II=66,Fe_III=67,Fe_IV=68,Fe_V=69,Fe_VI=70)
	parameter (Fe_VII=71,Fe_VIII=72,Fe_IX=73,Fe_X=74,Fe_XI=75)
	parameter (Fe_XII=76,Fe_XIII=77,Fe_XIV=78,Fe_XV=79,Fe_XVI=80)
	parameter (Fe_XVII=81,Fe_XVIII=82,Fe_XIX=83,Fe_XX=84,Fe_XXI=85)
	parameter (Fe_XXII=86,Fe_XXIII=87,Fe_XXIV=88,Fe_XXV=89,Fe_XXVI=90)
	parameter (Mg_I=91,Mg_II=92,Mg_III=93,Mg_IV=94,Mg_V=95,Mg_VI=96)
        parameter (Mg_VII=97,Mg_VIII=98,Mg_IX=99,Mg_X=100,Mg_XI=101)
        parameter (Mg_XII=102)

        parameter (kpc2cm=3.08e21,k_B=1.38e-16,m_H=1.6726e-24)
        parameter (e_den=1.0)

c	define UVB spectrum   
	Nnu = int(sdata(1))
	nu  = sdata(2:Nnu+1)
	Jnu = sdata(Nnu+2:2*Nnu+1)
	VecSize = Nnu

c	================== Photo-ionisation rates =======================
        call allphot_mat_set(allphot_vec)
c       now allphot_vec(C,IV,3) is photoionization from 2p shell in CIV
c       shell indices: 0: all, 1: 1s, 2: 2s, 3: 2p, 4: 3s, 5: 3p, 6: 3d, 7: 4s
	    
	prate = Gamma(el_id, ion_id)

	end

