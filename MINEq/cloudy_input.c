
#include "pluto.h"

void Write_cloudy_input(double nH, double *v, char *infile, double T)
{

 int i;
 FILE *fcl = fopen(infile, "w");

 fprintf(fcl, "hden    %2.6f \n", log10(nH));
 fprintf(fcl, "table HM12, z=0\n");
// fprintf(fcl, "metals 1.0\n");
 fprintf(fcl, "abundances all -10\n");
 fprintf(fcl, "element helium abundance   -1.0791812\n");
 fprintf(fcl, "element Carbon abundance   -3.6108339\n");
 fprintf(fcl, "element Nitrogen abundance   -4.2196827\n");
 fprintf(fcl, "element Oxygen abundance   -3.3400838\n");
 fprintf(fcl, "element Neon abundance   -3.7099654\n");
 fprintf(fcl, "element Magnesium abundance   -4.4698003\n");
 fprintf(fcl, "element Silicon abundance   -4.4894550\n");
 fprintf(fcl, "element Sulphur abundance   -4.8601209\n");
 fprintf(fcl, "element Iron abundance   -4.5497509\n");
 fprintf(fcl, "element Hydrogen ionization %2.4f %2.4f\n", MAX(log10(v[HI]), -9), MAX(log10(1.0-v[HI]), -9));
 fprintf(fcl, "element Helium ionization %2.4f %2.4f %2.4f\n", 
                                MAX(log10(v[HeI]), -9), MAX(log10(v[HeII]), -9), MAX(log10(v[HeIII]), -9));
 fprintf(fcl, "element Carbon ionization "); 
        for (i=CI; i<=CI+C_IONS; i++)   fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Nitrogen ionization "); 
        for (i=NI; i<NI+N_IONS; i++)    fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Oxygen ionization "); 
        for (i=OI; i<OI+O_IONS; i++)    fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Neon ionization ");
        for (i=NeI; i<NeI+Ne_IONS; i++) fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Magnesium ionization "); 
        for (i=MgI; i<MgI+Mg_IONS; i++) fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Silicon ionization "); 
        for (i=SiI; i<SiI+Si_IONS; i++) fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Sulphur ionization "); 
        for (i=SI; i<SI+S_IONS; i++)    fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 fprintf(fcl, "element Iron ionization "); 
        for (i=FeI; i<FeI+Fe_IONS; i++) fprintf(fcl," %2.4f  ", MAX(log10(v[i]), -9)); fprintf(fcl, " \n"); 
 
 fprintf(fcl, "constant temperature t= %2.4e k\n", T);
 fprintf(fcl, "set trim -15\n");
 fprintf(fcl, "set dr 0\n");
 fprintf(fcl, "stop zone 1\n");
 fprintf(fcl, "iterate to converge\n");
 fprintf(fcl, "save cooling \"cool.cloudy\" last\n");
 fprintf(fcl, "save heating \"heat.cloudy\" last\n");
 fprintf(fcl, "print heating\n");
 fprintf(fcl, "no molecules\n");

 fclose(fcl);
}
